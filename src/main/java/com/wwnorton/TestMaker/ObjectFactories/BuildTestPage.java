package com.wwnorton.TestMaker.ObjectFactories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonObject;
import com.wwnorton.TestMaker.utilities.BaseDriver;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.ReadUIJsonFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.getPOS;

import ru.yandex.qatools.allure.annotations.Step;

public class BuildTestPage {
	WebDriver driver;
	Actions actions;
	JavascriptExecutor js;
	static int chapValue;

	@FindBy(how = How.ID, using = "__input__text__search__1")
	public WebElement SearchQuestionTextbox;
	@FindBy(how = How.ID, using = "__input__button__searchLink__0")
	public WebElement SearchLink;
	@FindBy(how = How.XPATH, using = "//main[@id='regionMain']/aside[@id='regionAside']//div[starts-with(@class,'clearfix')]/div[contains(.,'Question')]")
	public WebElement QuestionCount;
	@FindBy(how = How.ID, using = "__input__button__Add__Filters__0")
	public WebElement AddFiltersButton;

	@FindBy(how = How.ID, using = "__input__button__Clear__Filters__0")
	public WebElement ClearFiltersButton;

	@FindBy(how = How.ID, using = "__input__button__Cancel__0")
	public WebElement CancelButton;

	@FindBy(how = How.ID, using = "__input__button__Apply__0")
	public WebElement ApplyButton;
	@FindBy(how = How.ID, using = "__input__button__Build__0")
	public WebElement BuildViewLink;

	@FindBy(how = How.XPATH, using = "//div/button[@type='button'][contains(text(),'Chapters')]")
	public WebElement ChaptersButton;

	@FindBy(how = How.XPATH, using = "//div[@class='center large-body-text mt2-5']/span")
	public WebElement ReachedEndOfResultsText;

	@FindBy(how = How.XPATH, using = "//div/button[@type='button'][contains(text(),'Question Types')]")
	public WebElement QuestionTypesButton;

	@FindBy(how = How.XPATH, using = "//label[@for='__input__checkbox__Question__Types__0']/input[@name='MULTIPLE CHOICE']/following-sibling::span")
	public WebElement MultipleChoiceCheckbox;

	@FindBy(how = How.XPATH, using = "//label[@for='__input__checkbox__Question__Types__1']/input[@name='SHORT ANSWER']/following-sibling::span")
	public WebElement ShortAnswereCheckbox;

	@FindBy(how = How.XPATH, using = "//div/button[contains(text(),'Bloom')]")
	public WebElement BloomsTaxonomy;
	@FindBy(how = How.XPATH, using = "//div/button[contains(text(),'Difficulty')]")
	public WebElement difficulty;

	@FindBy(how = How.XPATH, using = "//div[@class='bg-white ml1-5 mr1-5 mb1-5  lg-col-12']/div[@class='pt2-5']/div[contains(text(),'Answer Key')]")
	public WebElement AnswerKeyText;

	@FindBy(how = How.XPATH, using = "//div[@class='bg-white ml1-5 mr1-5 mb1-5  lg-col-12']/div[@class='pt2-5']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']/div[1]")
	public WebElement TestNameTextonPreviewMode;
	@FindBy(how = How.XPATH, using = "//div[@class='bg-white ml1-5 mr1-5 mb1-5  lg-col-12']/div[@class='pt2-5']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']/div[@class='h4']")
	public WebElement CourseNameonPreviewMode;

	@FindBy(how = How.XPATH, using = "//aside[@id='regionAside']/*[name()='svg']")
	public WebElement CollapseIcon;
	@FindBy(how = How.XPATH, using = "//div[@id='addQuestionsHeaderDiv']/div/p[@class='h2']")
	public WebElement AddQuestionsText;

	@FindBy(how = How.XPATH, using = "//div[@id='addQuestionsHeaderDiv']/div/p[@class='darkGray h5 mt2-5 mb1']")
	public WebElement TestBankText;

	@FindBy(how = How.XPATH, using = "//div[@id='addQuestionsHeaderDiv']/div[@class='px3-0 mt3-5 pb1-5']/div[@class='clearfix mb1-5 mt1 flex']/span[@class='col sm-col-3 col-8']")
	public WebElement BookDetails;

	@FindBy(how = How.XPATH, using = "//*[name()='svg' and @id='__icon__gear__________0']")
	public WebElement gearbutton;

	@FindBy(how = How.XPATH, using = "//button[@type='button'][contains(text(),'AP Learning Objectives')]/*[name()='svg']")
	public WebElement APLearningObjective;

	@FindBy(how = How.XPATH, using = "//button[@type='button'][contains(text(),'Learning Objectives')]/*[name()='svg']")
	public WebElement LearningObjective;

	@FindBy(how = How.XPATH, using = "//*[name()='svg' and @id='__icon__accordioncollapsgrey__________0']")
	public WebElement collapsgrey;

	@FindBy(how = How.XPATH, using = "//*[name()='svg' and @id='__icon__accordionexpandgrey__________0']")
	public WebElement expandgrey;

	@FindBy(how = How.ID, using = "__input__button__Remove__0")
	public WebElement removebutton;
	@FindBy(how = How.XPATH, using = "//*[name()='svg' and @alt='Remove filter']")
	public WebElement removeFiltericon;

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJsonObject.readUIJason();

	// Initializing Web Driver and PageFactory.
	public BuildTestPage() throws Exception {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@Step("Click Search Questions Text Box and Enter Search Questions in  Text Box,  Method: {method} ")
	public void clickSearchQuestionsTextbox(String questionText)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("__input__text__search__1")));
		SearchQuestionTextbox.click();
		Thread.sleep(2000);
		// questionText =
		// jsonObj.getAsJsonObject("TMSearchQuestions").get("SearchQuestionText1").getAsString();
		SearchQuestionTextbox.sendKeys(questionText);
	}

	@Step("Get the Search text key from Search text box,  Method: {method} ")
	public String getSearchKey() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("__input__text__search__1")));
		String searchKey = SearchQuestionTextbox.getAttribute("value");
		Thread.sleep(2000);
		return searchKey;
	}

	@Step(" click SearchLink button,  Method: {method} ")
	public void clickSearchLinkbutton() {
		SearchLink.click();

	}

	@Step(" Get Question Count after Question Search,  Method: {method} ")
	public String getQuestionCountAfterQuestionSearch() {
		driver.findElement(By.id("regionAside")).click();
		ReusableMethods.scrollIntoView(driver, AddFiltersButton);
		boolean isdisplayed =driver.findElements(By
				.xpath("//main[@id='regionMain']/aside[@id='regionAside']//div[starts-with(@class,'clearfix')]/div[contains(.,'Question')]")).size() >0;
		if(isdisplayed==true){
		/*WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[starts-with(@class,'clearfix')]/div[contains(.,'Question')]")));*/
		ReusableMethods.scrollIntoView(driver, QuestionCount);
		return QuestionCount.getText();
		}
		
		return null;

	}

	@Step(" Verify Questions Count,  Method: {method} ")
	public String getQuestionsCount() {
		String questionsCount = null;
		List<WebElement> questionHeader = driver
				.findElements(By
						.xpath("//div[@id='addQuestionsHeaderDiv']/div[@class='px3-0 mt3-5 pb1-5']/div[@class='clearfix mb1-5 mt1 flex']/span"));
		for (int i = 0; i < questionHeader.size(); i++) {
			if (i == 1) {
				questionsCount = questionHeader.get(i).getText();
				break;
			}
		}
		return questionsCount;
	}

	@Step(" Verify Questions Header informations,  Method: {method} ")
	public String getQuestionsHeaderDetails() {
		String questionHeaderText = null;
		List<WebElement> questionHeader = driver
				.findElements(By
						.xpath("//div[@id='addQuestionsHeaderDiv']/div[@class='px3-0 mt3-5 pb1-5']/div[@class='clearfix mb1-5 mt1 flex']/span"));
		for (int i = 0; i < questionHeader.size(); i++) {
			if (i == 0) {
				questionHeaderText = questionHeader.get(i).getText();
				break;
			}
		}
		return questionHeaderText;

	}

	@Step(" Verify All Questions are displayed in Search Questions page,  Method: {method} ")
	public void getQuestionsCountDetailsAfterSearch() {
		// Find element by link text and store in variable "Element"
		// WebElement Element =
		// driver.findElement(By.id("__input__button__backToTop__0"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// This will scroll the page till the element is found
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");

	}

	@Step("Click the Add Filters Button,  Method: {method} ")
	public void clickAddFiltersButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//button[starts-with(@id,'__input__button__Add__Filters__')]")));
		boolean addFilterEnabled = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Add__Filters__')]"))
				.size() > 0;

		/*
		 * WebElement addFiltersbuttonEnabled = driver.findElement(By
		 * .id("__input__button__Add__Filters__0"));
		 */
		if (addFilterEnabled == true) {
			// AddFiltersButton.click();
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", AddFiltersButton);
			LogUtil.log("Add Filter button is clicked");
			System.out.println("Add Filter button is clicked");
		} else {
			LogUtil.log("Add Filters button is disabled");
		}

	}

	@Step("Click the Clear Filters Button,  Method: {method} ")
	public void clickClearFiltersButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(ClearFiltersButton));
		driver.findElement(By.id("regionAside")).click();
		ReusableMethods.scrollIntoView(driver, ClearFiltersButton);
		ClearFiltersButton.click();
	}

	@Step("Click the Apply Button,  Method: {method} ")
	public void clickApplyButton() {
		ReusableMethods.scrollIntoView(driver, ApplyButton);
		boolean isApplyButton = driver.findElements(By.id("__input__button__Apply__0")).size() >0;
		if(isApplyButton==true && driver.findElement(By.id("__input__button__Apply__0")).isEnabled()){	
		ReusableMethods.scrollIntoViewClick(driver, ApplyButton);
	//	ApplyButton.click();
		}

	}

	@Step("Click the Cancel Button,  Method: {method} ")
	public void clickCancelButton() {
		CancelButton.click();

	}

	@Step("Click the Chapter Button,  Method: {method} ")
	public void clickChaptersButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ChaptersButton));
		ChaptersButton.click();

	}

	@Step("Click the Bloom's Taxonomy Button,  Method: {method} ")
	public void clickBloomsTaxonomyButton() {
		BloomsTaxonomy.click();

	}

	@Step("Click the Difficulty Button,  Method: {method} ")
	public void clickDifficultyButton() {
		difficulty.click();

	}
	// Test Bank label displays left aligned in left panel under “Add Questions”
	// header and above search bar
	@Step("Verify the Add Questions Text,  Method: {method} ")
	public String getAddQuestionsText() {
		return AddQuestionsText.getText();

	}

	@Step("Verify the Test Bank Text,  Method: {method} ")
	public String getTestBankText() {
		return TestBankText.getText();

	}

	@Step("Application displays Test Bank details as a single line item below Test Bank header - <Title> <Subtitle> <Edition> <Volume>,  Method: {method} ")
	public String getAddQuestionsBookDetails() {
		String getbookdetails = BookDetails.getText().replaceAll("[-+.^:]","");
		return getbookdetails;

	}

	@Step("Verify the Filters Names {0} Button,  Method: {method} ")
	public boolean getFilterName(String filterName) {
		boolean elementdisplayed = driver.findElements(
				By.xpath("//div/button[@type='button'][contains(text(),'"
						+ filterName + "')]")).size() > 0;
		if (elementdisplayed == true) {
			WebElement filterNameelement = driver.findElement(By
					.xpath("//div/button[@type='button'][contains(text(),'"
							+ filterName + "')]"));
			filterName = filterNameelement.getText();
			return filterName != null;
		}
		return elementdisplayed;
	}

	@Step("Click the Filters {0} Button,  Method: {method} ")
	public String clickFiltersButton(String filterName) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//div/button[@type='button'][contains(text(),'"
						+ filterName + "')]")));
		WebElement filterNameelement = driver.findElement(By
				.xpath("//div/button[@type='button'][contains(text(),'"
						+ filterName + "')]"));
		ReusableMethods.scrollIntoViewClick(driver, filterNameelement);
		//filterNameelement.click();
		return filterName;
	}

	@Step("Get the Filters Count {0},  Method: {method} ")
	public String getCountSelectedFilter(String buttonName) {
		WebElement element = driver.findElement(By
				.xpath("//div/button[@type='button'][contains(text(),'"
						+ buttonName + "')]"));

		String filterText = element.getText();
		String getDigits = filterText.replaceAll("\\D+", "");
		return getDigits;

	}

	@Step("Select the Chapter Checkboxes and Get the Name of selected Chapters,  Method: {method} ")
	public String clickChaptersCheckboxesandChaptersName(int count) {
		String chapterName = null;
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*")));
		List<WebElement> selectChaptercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*"));
		List<WebElement> selectChapter = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input"));
		if (selectChapter.size() == 0) {
			Assert.assertNull(selectChapter, "Chapter is not displayed");

		} else {
			for (int i = 0; i < selectChaptercheckbox.size(); i++) {
				for (int j = 0; j < selectChapter.size(); j++) {
					if (i == count && j == count) {
						chapterName = selectChapter.get(j).getAttribute("name");
						selectChaptercheckbox.get(i).click();
						break;
					}
				}
			}
		}
		return chapterName;
	}

	@Step("get the chapters filter count,  Method: {method} ")
	public int getchaptersFilterscount() {
		// String chapterName = null;
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*")));
		List<WebElement> selectChaptercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*"));
		int chapterCount = selectChaptercheckbox.size();
		return chapterCount;

	}

	@Step("verify Filters name after applying the filters,  Method: {method} ")
	public String verifyFilterName(int count) {
		String filterName = null;

		List<WebElement> checkfilterName = driver.findElements(By
				.xpath("//div[@class='left pl3 mt3 mr3-0']/div/span"));
		for (int i = 0; i < checkfilterName.size(); i++) {
			if (i == count) {
				filterName = checkfilterName.get(i).getText();
				break;
			}
		}
		return filterName;

	}

	@Step("verify Chapters details on the Search Questions section,  Method: {method} ")
	public String verifyChaptersDetails() {
		String chapterDetails = null;
		// boolean endOfResult = ReachedEndOfResultsText.isDisplayed();
		WebElement element = driver.findElement(By.id("regionAside"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", element);

		List<WebElement> checkChapterDeatils = driver
				.findElements(By
						.xpath("//div[@class='flex flex-wrap sm-col-4']/div[@class='xs-col-4 sm-col-8 black pt1']/div[@class='clearfix col sm-col-9 pb1 small-body-text']"));
		for (int i = 0; i < checkChapterDeatils.size(); i++) {
			chapterDetails = checkChapterDeatils.get(i).getText();

		}

		return chapterDetails;

	}

	@Step("Click the Question Types Button,  Method: {method} ")
	public void clickQuestionTypesButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div/button[@type='button'][contains(text(),'Question Types')]")));
		boolean isQtypeDisplayed = driver
				.findElements(
						By.xpath("//div/button[@type='button'][contains(text(),'Question Types')]"))
				.size() > 0;
		if (isQtypeDisplayed == true) {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", QuestionTypesButton);
			// QuestionTypesButton.click();
		}
	}

	@Step("Select the Check box for MultipleChoice,  Method: {method} ")
	public void selectMultipleChoiceCheckbox() {
		MultipleChoiceCheckbox.click();

	}

	@Step("Select the Check box for in Question Type,  Method: {method} ")
	public void selectQuestionTypeCheckbox(String checkBoxName)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//button[@type='button'][contains(.,'Question Types')]/*[name()='svg'][starts-with(@id,'__icon__accordioncollapsgrey__________')]")));

		WebElement questionTypeCheckBox = driver.findElement(By
				.xpath("//input[@name='" + checkBoxName
						+ "']/following-sibling::span"));
		driver.findElement(By.id("regionAside")).click();
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();", questionTypeCheckBox);
		try {
			if (questionTypeCheckBox.isDisplayed()) {

				questionTypeCheckBox.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Step("Click the button on Build Test Page,  Method: {method} ")
	public void clickBuildTestTabs(String tabs) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		WebElement buildTestTabs = driver
				.findElement(By
						.xpath("//div[@class='bg-white full-width flex flex-row justify-center']/div/div[@class='p1-5']/button[contains(text(),'"
								+ tabs + "')]"));
		// ReusableMethods.scrollIntoView(driver, gearbutton);

		Thread.sleep(1000);
		buildTestTabs.click();
	}

	@Step("get the tabs name on Test Page,  Method: {method} ")
	public String getTabsName(String tabs) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		WebElement buildTestTabs = driver
				.findElement(By
						.xpath("//div[@class='bg-white full-width flex flex-row justify-center']/div/div[@class='p1-5']/button[contains(text(),'"
								+ tabs + "')]/div"));
		// ReusableMethods.scrollIntoView(driver, gearbutton);

		Thread.sleep(1000);
		return buildTestTabs.getAttribute("class");

	}

	@Step("Select the Check box for Short Answer,  Method: {method} ")
	public void selectShortAnswerCheckbox() {
		ShortAnswereCheckbox.click();

	}

	@Step("Add Questions from Questions section,  Method: {method} ")
	public void clickAddQuestionsIcon(int count) throws InterruptedException {
		Thread.sleep(2000);
		boolean isShowButton = driver.findElements(By.xpath("//button[starts-with(@id,'__input__button__Show__')]")).size()>0;
		if(isShowButton==true){
		List<WebElement> getShowList =driver.findElements(By.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
		for(int s =0; s<getShowList.size(); s++){
			getShowList.get(s).click();
		}
		}
		List<WebElement> addQuestionslist = driver
				.findElements(By
						.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__')]"));

		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__')]"));
		questionskeleton();
		for (int i = 0; i < addQuestionslist.size(); i++) {
			if (count == 1) {
				addQuestionslist.get(i).click();
				break;
			} else if (i < count) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();",
						addQuestionslist.get(i));
				// addQuestionslist.get(i).click();
				Thread.sleep(2000);
				ReusableMethods.scrollQuestionSection(driver);
				if (i > count) {
					break;
				}
			}
		}

	}

	@Step("Add Matching Questions from Questions section,  Method: {method} ")
	public void clickAddMatchingQuestionsIcon(int qCount)
			throws InterruptedException {
		String questionIDs = null;
		List<WebElement> matchingQaddQuestionslist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]"));
		List<String> matchingQID = new ArrayList<String>();
		for (WebElement qID : matchingQaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			matchingQID.add(questionIDs);
		}
		for (int i = 0; i < matchingQID.size(); i++) {
			String qid = matchingQID.get(i);
			WebElement questionID = driver.findElement(By.id(qid));
			boolean isimageQDisplayed = driver.findElements(
					By.xpath("//div[@id='" + qid
							+ "']//canvas[contains(@id,'.jpg')]")).size() > 0;
			if (isimageQDisplayed == true) {
				continue;
			}
			ReusableMethods.scrollIntoView(driver, questionID);
			if (i == qCount) {
				WebElement addbutton = driver.findElement(By
						.xpath("//div[@id='" + qid
								+ "']//button[@name='addbutton']"));
				addbutton.click();
				break;
			}
		}
	}

	@Step("Click Add Questions from Questions section for DBQ,  Method: {method} ")
	public void clickDBQAddQuestionsIcon(int count) {

		List<WebElement> dbqaddQuestionslist = driver.findElements(By
				.xpath("//div[starts-with(@class,'px1-5 ')]/div/button"));
		questionskeleton();
		for (int i = 0; i < count; i++) {
			dbqaddQuestionslist.get(i).click();
			LogUtil.log(getQuestionAddedText());

		}

	}

	@Step("Verify the Question Added to Test build in Add Question Section,  Method: {method} ")
	public String verifyQuestioninTestBuild() throws InterruptedException {
		String questiontext = null;
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 50L);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='flex-0-0-auto flex flex-row justify-between items-center my1-5']/span")));
		List<WebElement> getQuestionText = driver
				.findElements(By
						.xpath("//div[@class='flex-0-0-auto flex flex-row justify-between items-center my1-5']/span"));
		int getCount = getQuestionText.size();
		LogUtil.log("The Questions Count", getCount);
		for (int i = 0; i < getQuestionText.size(); i++) {
			questiontext = getQuestionText.get(i).getText();
		}
		return questiontext;
	}

	@Step("Verify the Question Added to Test build in Search Question Section,  Method: {method} ")
	public String verifyQuestioninTestBuildSearchResult() {
		WebElement getQuestionText = null;
		boolean isDisplayed = driver.findElements(
				By.xpath("//div[starts-with(@class,'black px2-5 mb2-5')]"))
				.size() > 0;
		if (isDisplayed == true) {
			getQuestionText = driver.findElement(By
					.xpath("//div[@class='black px2-5 mb2-5']"));
			ReusableMethods.scrollIntoView(driver, getQuestionText);
			LogUtil.log("The Questions Count", getQuestionText);
		}
		return getQuestionText.getText();
	}

	@Step("Verify the Question Added Text in Search Question Section,  Method: {method} ")
	public List<String> getQuestionAddedText() {
		String questionaddedText = null;
		List<String> questionaddedTextList = new ArrayList<String>();
		List<WebElement> getQuestionAddedText = driver.findElements(By
				.xpath("//div[starts-with(@class,'left-align mt1 ml3-0')]"));
		for (int i = 0; i < getQuestionAddedText.size(); i++) {
			questionaddedText = getQuestionAddedText.get(i).getText();
			/*
			 * JavascriptExecutor executor = (JavascriptExecutor) driver; Object
			 * elementAttributes = executor.executeScript(
			 * "var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;"
			 * ,getQuestionAddedText);
			 * System.out.println(elementAttributes.toString());
			 */
			questionaddedTextList.add(questionaddedText);
		}

		return questionaddedTextList;
	}

	@Step("Verify Undo Feature text in Search Question Section ,  Method: {method} ")
	public List<String> getUndoText() {
		List<String> undoList = new ArrayList<String>();
		List<WebElement> getUndoButtonList = driver.findElements(By
				.xpath("//div/button[contains(text(),'Undo')]"));
		for (int i = 0; i < getUndoButtonList.size(); i++) {
			String undoText = getUndoButtonList.get(i).getText();
			undoList.add(undoText);
		}

		return undoList;
	}

	@Step("click Undo Button in Search Question Section  ,  Method: {method} ")
	public void clickUndoButton(int count) {
		List<WebElement> getUndoButtonList = driver.findElements(By
				.xpath("//div/button[contains(text(),'Undo')]"));
		for (int i = 0; i < getUndoButtonList.size(); i++) {
			if (i == count) {
				getUndoButtonList.get(i).click();
			}
		}

	}

	@Step("Read the Question text in Search Question Section,  Method: {method} ")
	public String readQuestionText(int count) {
		String questionText = null;
		List<WebElement> getQuestionText = driver
				.findElements(By
						.xpath("//div[@class='pb2 mt3 ml1 pl2']/div[@class='ml3 sub-sup-i']"));
		for (int i = 0; i < getQuestionText.size(); i++) {
			if (i <= count) {
				questionText = getQuestionText.get(i).getText();
				LogUtil.log(questionText);

			}
		}
		return questionText;

	}

	@Step("After adding get the Question text in Build Section ,  Method: {method} ")
	public String getQuestionTextinBuildView(int count) {
		String questionTextBuildView = null;
		List<WebElement> getQuestionTextBuildView = driver
				.findElements(By
						.xpath("//div[@class='pb2 mt2']/div[starts-with(@class,'ml3 sub-sup-i')]/p"));
		for (int i = 0; i < getQuestionTextBuildView.size(); i++) {
			if (i <= count) {
				ReusableMethods.scrollIntoView(driver,
						getQuestionTextBuildView.get(i));
				questionTextBuildView = getQuestionTextBuildView.get(i)
						.getText();
				LogUtil.log(questionTextBuildView);
			}
		}

		return questionTextBuildView;
	}

	@Step("Remove the Question text in Build Section ,  Method: {method} ")
	public void removeQuestionBuildView(int count) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//button[contains(text(),'Remove')]")));
		List<WebElement> removeQuestionBuildView = driver.findElements(By
				.xpath("//button[contains(text(),'Remove')]"));
		for (int i = 0; i < removeQuestionBuildView.size(); i++) {
			if (i == count) {
				scrolltoQuestiontext();
				Thread.sleep(2000);
				driver.findElement(By.id("__input__button__Remove__0")).click();
				Thread.sleep(2000);
				break;
			} /*
			 * else if (i < count) { scrolltoQuestiontext(); Thread.sleep(2000);
			 * driver.findElement(By.id("__input__button__Remove__0")).click();
			 * Thread.sleep(2000); scrolltoQuestiontext(); }
			 */
			Thread.sleep(3000);

		}

	}

	@Step("Select the Bloom's Taxonomy Checkboxes and Get the Name of selected Bloom's Taxonomy,  Method: {method} ")
	public String clickBloomsTaxonomy(int count) {
		String bloomsTaxonomytext = null;
		List<WebElement> selectBloomsTaxonomycheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint=\"Bloom's Taxonomy\"]/following-sibling::*"));
		List<WebElement> getBloomsTaxonomytext = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label[@class='container inline relative align-middle labelCheckbox']/input[@texthint=\"Bloom's Taxonomy\"]/parent::*"));
		for (int i = 0; i < selectBloomsTaxonomycheckbox.size(); i++) {
			if (i <= count) {
				selectBloomsTaxonomycheckbox.get(i).click();
			}
		}
		for (int j = 0; j < getBloomsTaxonomytext.size(); j++) {
			bloomsTaxonomytext = getBloomsTaxonomytext.get(j).getText();

		}
		return bloomsTaxonomytext;
	}

	@Step("Select the Bloom's Taxonomy Checkbox based on provided Name,  Method: {method} ")
	public void selectBloomsTaxonomy(String bloomName) {
		String bloomsTaxonomytext = null;
		List<WebElement> selectBloomsTaxonomycheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint=\"Bloom's Taxonomy\"]/following-sibling::*"));
		List<WebElement> getBloomsTaxonomytext = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label[@class='container inline relative align-middle labelCheckbox']/input[@texthint=\"Bloom's Taxonomy\"]/parent::*"));
		for (int i = 0; i < selectBloomsTaxonomycheckbox.size(); i++) {
			bloomsTaxonomytext = getBloomsTaxonomytext.get(i).getText();
			if (bloomsTaxonomytext.equalsIgnoreCase(bloomName)) {
				selectBloomsTaxonomycheckbox.get(i).click();
			}
		}

	}

	
	@Step("Select the Difficulty Checkbox based on provided Name,  Method: {method} ")
	public void selectDifficultyCheckbox(String diffName) {
		String difficultyName = null;
		List<WebElement> selectdifficultycheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Difficulty']/following-sibling::*"));
		List<WebElement> getdifficultytext = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label[@class='container inline relative align-middle labelCheckbox']/input[@texthint='Difficulty']/parent::*"));
		for (int i = 0; i < selectdifficultycheckbox.size(); i++) {
			difficultyName = getdifficultytext.get(i).getText();
			if (difficultyName.equalsIgnoreCase(diffName)) {
				selectdifficultycheckbox.get(i).click();
				break;
			}
		}

	}
	

	// Verify the Answer Key Text
	@Step("Verify Answer Key Text,  Method: {method} ")
	public String getAnswerKeyText() {
		Assert.assertTrue(AnswerKeyText.isEnabled());
		LogUtil.log(AnswerKeyText.getText());
		return AnswerKeyText.getText();

	}

	// Verify the Test Name and Course Name
	@Step("Verify Test Name in Preview Mode,  Method: {method} ")
	public String getTestNamePreviewMode() {
		Assert.assertTrue(TestNameTextonPreviewMode.isEnabled());
		LogUtil.log(TestNameTextonPreviewMode.getText());
		return TestNameTextonPreviewMode.getText();

	}

	@Step("Verify Course Name in Preview Mode,  Method: {method} ")
	public String getCourseNamePreviewMode() {
		Assert.assertTrue(CourseNameonPreviewMode.isEnabled());
		LogUtil.log(CourseNameonPreviewMode.getText());
		return CourseNameonPreviewMode.getText();

	}

	// Verify the Answer list for question added in Test
	@Step("Verify the Answer list for question added for Test in Preview Mode,  Method: {method} ")
	public String getAnswerListPreviewMode() {
		String answerListText = null;
		List<WebElement> answerList = driver
				.findElements(By
						.xpath("//div[@class='px2-5 pt1-5']/div/ul[@class='list-reset body-text']/li[@class='table sub-sup-i']"));
		for (int i = 0; i < answerList.size(); i++) {
			answerListText = answerList.get(i).getAttribute("innerText");
			LogUtil.log(answerListText);
		}
		return answerListText;

	}

	// verify Questions are in Collapsed mode in List View
	@Step("Verify the Added Questions are in collapsed mode in List View,  Method: {method} ")
	public void validateQuestionsCollapsedListwMode() {

		List<WebElement> questionsCollapsedList = driver
				.findElements(By
						.xpath("//*[name()='svg'][starts-with(@id,'__icon__reorderIconGrey')]/following-sibling::button"));
		for (int i = 0; i < questionsCollapsedList.size(); i++) {

			String isCollapsed = questionsCollapsedList.get(i).getAttribute(
					"aria-expanded");
			if (isCollapsed.equalsIgnoreCase("false")) {
				Assert.assertTrue(true, "Added Questions are in collapsed mode");
			} else if (isCollapsed.equalsIgnoreCase("true")) {
				Assert.assertTrue(true, "Added Questions are in expanded mode");
			} else {
				Assert.assertNull(isCollapsed);
			}
		}

	}

	// verify Questions are in Collapsed mode in List View
	@Step("Verify the Added Questions are in collapsed mode in Test View,  Method: {method} ")
	public boolean validateQuestionsCollapsedwMode() {

		List<WebElement> questionsCollapsedList = driver
				.findElements(By
						.xpath("//*[name()='svg'][starts-with(@id,'__icon__reorderIconGrey')]/following-sibling::button"));
		for (int i = 0; i < questionsCollapsedList.size();) {
			String isCollapsed = questionsCollapsedList.get(i).getAttribute(
					"aria-expanded");
			if (isCollapsed.equalsIgnoreCase("false")) {
				Assert.assertTrue(true, "Added Questions are in collapsed mode");
				LogUtil.log("Added Questions are in collapsed mode");
				return true;
			} else if (isCollapsed.equalsIgnoreCase("true")) {
				Assert.assertTrue(true,
						"Added Questions are in expanded mode hence unable to perform Darg and Drop");
				LogUtil.log("Added Questions are in expanded mode hence unable to perform Darg and Drop");
			}
			break;
		}
		return false;

	}

	@Step("Verify the More info section is  collapsed mode in Build View,  Method: {method} ")
	public void validateMoreInfoCollapsedBuildView() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		List<WebElement> moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		List<WebElement> moreInfoCollapsedList = driver
				.findElements(By
						.xpath("//*[name()='svg' and @id='__icon__accordionexpandgrey__________0']/following::div[@class='display-none']"));
		for (int i = 0; i < moreInfoCollapsedList.size(); i++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(i));
			String isCollapsed = moreInfoCollapsedList.get(i).getCssValue(
					"display");
			String expectedstatus = "none";
			Assert.assertEquals(isCollapsed, expectedstatus);

		}

	}

	// Click Question Title and Validated the Expanded Mode
	@Step("Click Question Title in List View,  Method: {method} ")
	public void clickQuestionTitleListView(int count) {
		List<WebElement> questionsCollapsedList = driver
				.findElements(By
						.xpath("//div[@class='flex flex-column justify-start full-width m0 p0 col xs-col-4 sm-col-4 md-col-8 lg-col-12 bg-lightestGray cursorGrabbing']/div[@class='m0 z2 p0 border border-lightGray flex-1-1-auto bg-white flex flex-column justify-start css-161xm4f']/div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
		for (int i = 0; i < questionsCollapsedList.size(); i++) {
			if (i == count) {
				questionsCollapsedList.get(i).click();
			}
		}
	}

	@Step("Drag and Drop Questions in List View,  Method: {method} ")
	public void dragDropQuestionsTitleListView(int count)
			throws InterruptedException {
		List<WebElement> dragdropElements = driver
				.findElements(By
						.xpath("//div[@class='black px2-5 mb2-5']/following::*[name()='svg']//*[name()='defs']//*[name()='clipPath']//*[name()='path']"));

		WebElement target = driver
				.findElement(By
						.xpath("//div[@class='flex flex-column justify-start full-width m0 p0 col xs-col-4 sm-col-4 md-col-8 lg-col-12 bg-lightestGray cursorGrabbing']"));
		for (int i = 0; i < dragdropElements.size(); i++) {
			if (i < count) {
				dragdropElements.get(i);
				Actions act = new Actions(driver);
				WebElement draggedFromEle = driver
						.findElement(By
								.xpath("//*[name()='svg' and @id='__icon__reorderIconGrey__________"
										+ i + "']"));
				act.dragAndDrop(draggedFromEle, target).build().perform();
				Thread.sleep(3000);
			}
		}
	}
	
	@Step("Drag and Drop Child Questions in List View,  Method: {method} ")
	public void dragDropQuestionsChildQG(int count)
			throws InterruptedException {
		List<WebElement> dragdropElements = driver
				.findElements(By
						.xpath("//div[@class='black px2-5 mb2-5']/following::*[name()='svg']//*[name()='defs']//*[name()='clipPath']//*[name()='path']"));
       
		WebElement target = driver
				.findElement(By
						.xpath("//div[@class='flex flex-column justify-start full-width m0 p0 col xs-col-4 sm-col-4 md-col-8 lg-col-12 bg-lightestGray cursorGrabbing'][1]"));
		for (int i = 1; i < dragdropElements.size(); i++) {
			if (i < count) {
				dragdropElements.get(i);
				Actions act = new Actions(driver);
				WebElement draggedFromEle = driver
						.findElement(By
								.xpath("//*[name()='svg' and @id='__icon__reorderIconGrey__________"
										+ i + "']"));
				act.dragAndDrop(draggedFromEle, target).build().perform();
				Thread.sleep(3000);
			}
		}
	}

	// Verify the question number order
	@Step("Question number order is arranged correctly after dropping the dragged question,  Method: {method} ")
	public void questionSequenceafterDragDrop() {
		List<WebElement> questionSequenceList = driver
				.findElements(By
						.xpath("//div[@class='m0 p0 border border-lightGray flex-1-1-auto bg-white flex flex-column justify-start css-161xm4f']/div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button/span[@class='h5 pr1 right-align']"));
		List<String> questionseq = new ArrayList<String>();
		for (int i = 0; i < questionSequenceList.size(); i++) {
			String questionSequence = questionSequenceList.get(i).getText();
			questionseq.add(questionSequence);
		}

		// make a copy of the list
		List<String> sortedQuestionNumbers = new ArrayList<String>(questionseq);
		Collections.sort(sortedQuestionNumbers);
		LogUtil.log(sortedQuestionNumbers);
		Assert.assertTrue(sortedQuestionNumbers.equals(questionseq));
	}

	// Verify the sequence of the Questions and Answers in Preview Mode
	@Step("Question number order is arranged correctly after dropping the dragged question on Preview Mode,  Method: {method} ")
	public void questionSequenceafterDragDropOnPreviewMode() {
		List<WebElement> questionSequenceList = driver
				.findElements(By
						.xpath("//div[@class='bg-white m1-5 xs-col-4 sm-col-4 md-col-8 lg-col-10 lg-col-12']/div[@class='px2-5']/div[@class='pl2-125 pt1-5']/div/div[@class='px1-5 ']/div/div[@class='left ']"));
		List<String> questionseq = new ArrayList<String>();
		for (int i = 0; i < questionSequenceList.size(); i++) {
			String questionSequence = questionSequenceList.get(i).getText();
			questionseq.add(questionSequence);
		}

		// make a copy of the list
		List<String> sortedQuestionNumbers = new ArrayList<String>(questionseq);
		Collections.sort(sortedQuestionNumbers);
		LogUtil.log(sortedQuestionNumbers);
		Assert.assertTrue(sortedQuestionNumbers.equals(questionseq));
	}

	// Verify the sequence of the Questions and Answers in Preview Mode
	@Step("Answer is arranged correctly after dropping the dragged question on Preview Mode,  Method: {method} ")
	public void answerSequenceafterDragDropOnPreviewMode() {
		List<WebElement> answerSequenceList = driver
				.findElements(By
						.xpath("//div[@class='px2-5 pt1-5']/div[@class='pb1 sm-col-4']/ul/li/span[@class='table-cell width2-5 right-align']"));
		List<String> answerseq = new ArrayList<String>();
		for (int i = 0; i < answerSequenceList.size(); i++) {
			String answerSequence = answerSequenceList.get(i).getText();
			answerseq.add(answerSequence.replace(".", ""));
		}

		// make a copy of the list
		ArrayList<String> sortedAnswerNumbers = new ArrayList<String>(answerseq);
		List<Integer> newList = new ArrayList<Integer>(
				sortedAnswerNumbers.size());
		for (String intvalue : sortedAnswerNumbers) {
			newList.add(Integer.valueOf(intvalue.replace(".", "")));
		}

		Collections.sort(newList);
		LogUtil.log(newList);
		Assert.assertEquals(newList.toString(), answerseq.toString());
	}

	// Application displays each chapter name is followed by associated chapter
	// number
	@Step("Application displays each chapter name is followed by associated chapter number,  Method: {method} ")
	public String[] checkChapterNameandChapterNumber() {
		String chapterNameNum;
		ArrayList<String> chapNum = new ArrayList<String>();
		ArrayList<String> chapNames = new ArrayList<String>();
		List<WebElement> chapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']"));
		for (int i = 0; i < chapterNumberNameList.size(); i++) {
			chapterNameNum = chapterNumberNameList.get(i).getAttribute("name");
			System.out.println(chapterNameNum);
			String str[] = chapterNameNum.split("(:)||(—)") ;
			/*
			 * System.out.println(str[0]); System.out.println(str[1]);
			 */
			chapNum.add(str[0]);
			chapNames.add(str[1]);
			Assert.assertNotEquals(str[0], str[1]);

		}
		return new String[] { chapNum.toString(), chapNames.toString() };

	}

	@Step("Select the Multiple Chapter Checkboxes and Get the Name of selected Chapters,  Method: {method} ")
	public List<String> clickMultipleChaptersCheckboxesandChaptersName(int count) {
		String chapterName = null;
		List<WebElement> selectChaptercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*"));
		List<WebElement> selectChapter = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input"));
		List<String> chapNames = new ArrayList<String>();
		for (int i = 0; i < count; i++) {

			chapterName = selectChapter.get(i).getAttribute("name");
			chapNames.add(i, chapterName);
			boolean checkboxSel = selectChapter.get(i)
					.getAttribute("aria-checked").equalsIgnoreCase("false");
			if (checkboxSel == false) {
				selectChaptercheckbox.get(i).click();
				continue;
			} else {
				selectChaptercheckbox.get(i).click();
				continue;
			}

		}

		return chapNames;
	}

	@Step("verify Multiple Filters name after applying the filters,  Method: {method} ")
	public List<String> verifyMultipleFilterName() {
		String filterName = null;
		List<String> filterNames = new ArrayList<String>();
		List<WebElement> checkfilterName = driver.findElements(By
				.xpath("//div[@class='left pl3 mt3 mr3-0']/div/span"));
		for (int i = 0; i < checkfilterName.size(); i++) {
			filterName = checkfilterName.get(i).getText();
			filterNames.add(filterName);
			continue;

		}

		return filterNames;

	}

	// Click More info Scetion

	@Step("Click the More Info button from Build Test View ,  Method: {method} ")
	public void clickMoreInfobutton(int count) {
		List<WebElement> clickMoreInfoButton = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int i = 0; i < clickMoreInfoButton.size(); i++) {
			if (i == count) {
				clickMoreInfoButton.get(i).click();
				break;
			}
		}
	}

	// Click Collapse or Expand Icon
	@Step("Click the collapse or expand button,  Method: {method} ")
	public void clickcollapseExpandbutton() {
		CollapseIcon.click();
	}

	// Verify the Chapter filter available in metadata section in Build test
	// section
	@Step("Application displays chapter name is followed by associated chapter number in Build view Metadata section,  Method: {method} ")
	public String[] checkChapterNameandChapterNumberMetaDatasection() {
		String chapterNameNum;
		ArrayList<String> chapNum = new ArrayList<String>();
		ArrayList<String> chapNames = new ArrayList<String>();
		List<WebElement> chapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div[@class='xs-col-4 sm-col-8 black pt1']/div[contains(text(),'Chapter')]/following-sibling::div[@class='clearfix col sm-col-9 pb1 small-body-text']/p"));
		for (int i = 0; i < chapterNumberNameList.size(); i++) {
			chapterNameNum = chapterNumberNameList.get(i).getAttribute(
					"innerHTML");
			String str[] = chapterNameNum.split(":");
			/*
			 * System.out.println(str[0]); System.out.println(str[1]);
			 */
			chapNum.add(str[0]);
			chapNames.add(str[1]);
			Assert.assertNotEquals(str[0], str[1]);

		}
		return new String[] { chapNum.toString(), chapNames.toString() };

	}

	// Verify the Chapter details information on Add Questions Section
	@Step("Application displays chapter name is followed by associated chapter number in Add Questions section,  Method: {method} ")
	public String getChapterTextAddQuestionsection() {
		String chaptertext = null;
		List<WebElement> chapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div[@class='xs-col-4 sm-col-8 black pt1']/div[contains(text(),'Chapter')]/following-sibling::div[@class='clearfix col sm-col-9 pb1 small-body-text']/p"));
		for (int i = 0; i < chapterNumberNameList.size(); i++) {
			chaptertext = chapterNumberNameList.get(i)
					.getAttribute("innerHTML");

		}
		return chaptertext;

	}

	// Application allow to click on learning objective filter and displays List
	// of learning objectives associated to selected chapters
	@SuppressWarnings("unused")
	@Step("Application allow to click on learning objective filter and displays List of learning objectives associated to selected chapters,  Method: {method} ")
	public List<String> getLearningObjectiveFilterforChapter()
			throws InterruptedException {

		String learningObjName;
		List<String> loFilterNames = new ArrayList<String>();
		driver.findElement(By.id("regionAside")).click();
		ReusableMethods.scrollIntoView(driver, ApplyButton);
		// List<String> leaningObjList = new ArrayList<String>();
		clickChaptersButton();
		List<WebElement> selectChaptercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*"));

		for (chapValue = 0; chapValue < selectChaptercheckbox.size(); chapValue++) {
			selectChaptercheckbox.get(chapValue).click();
			collapsgrey.click();
			Thread.sleep(2000);
			if (expandgrey.isDisplayed()) {
				clickLearningObjectiveFilter();
			}

			ReusableMethods.scrollIntoView(driver, LearningObjective);
			boolean isLOlistDisplayed = driver
					.findElements(
							By.xpath("//button[contains(text(),'Learning Objectives')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/p"))
					.size() > 0;
			if (isLOlistDisplayed == true) {
				clickLearningObjectiveFilter();
				expandgrey.click();
				selectChaptercheckbox.get(chapValue).click();

			} else {
				List<WebElement> leaningObjListEle = driver
						.findElements(By
								.xpath("//button[contains(text(),'Learning Objectives')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input"));
				int count = leaningObjListEle.size();
				LogUtil.log(count);
				for (int lo = 0; lo < leaningObjListEle.size(); lo++) {
					learningObjName = leaningObjListEle.get(lo).getAttribute(
							"name");
					loFilterNames.add(learningObjName);
					LogUtil.log(learningObjName);
					// leaningObjListEle.get(lo).click();
					// leaningObjListEle.get(lo).getText();
					clickLearningObjectivecheckboxes(lo);
					break;
				}

			}
			break;

		}

		return loFilterNames;

	}

	@Step("Get the LO CheckboxesCount,  Method: {method} ")
	public int getLearningObjectiveCheckBoxesCount() {
		List<WebElement> leaningObjList = driver
				.findElements(By
						.xpath("//button[contains(text(),'Learning Objectives')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input"));
		int count = leaningObjList.size();
		LogUtil.log(count);
		return count;
	}

	@Step("Click the Learning Objective Check boxes,  Method: {method} ")
	public void clickLearningObjectivecheckboxes(int count)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//button[contains(text(),'Learning Objectives')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/span")));
		driver.findElements(By
				.xpath("//button[contains(text(),'Learning Objectives')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/span"));
		WebElement leaningObjListcheckbox = driver
				.findElement(By
						.xpath("//button[contains(text(),'Learning Objectives')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/span"));

		/* for (int i = 0; i < count; i++) { */
		leaningObjListcheckbox.click();
		clickApplyButton();
		boolean isquestionsDisplayed = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'center large-body-text mt2-5')][contains(text(),'No results that match these filters.')]"))
				.size() > 0;
		if (isquestionsDisplayed == true) {
			List<WebElement> removeFiltericon = driver.findElements(By
					.xpath("//*[name()='svg' and @alt='Remove filter']"));
			for (int r = 0; r < removeFiltericon.size(); r++) {
				removeFiltericon.get(1).click();
				clickAddFiltersButton();
				clickLearningObjectiveFilter();
			}

		}

	}

	// select Chapter Checkboxes based on Name
	@Step("Select the Chapter Checkbox based on Provided Name,  Method: {method} ")
	public String checkChapterCheckbox(String chapterName) {
		WebElement selectChaptercheckbox = driver
				.findElement(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label[starts-with(.,'"
								+ chapterName + "')]/span"));
		selectChaptercheckbox.click();
		return chapterName;
	}

	// Get the Chapter details in Add Question section metadata
	@Step("get the Chapter details on Add Questions Page, Method :{method} ")
	public List<String> getChapterNamesList() throws InterruptedException {
		Thread.sleep(4000);
		String chapterName;
		List<String> chapterNames = new ArrayList<String>();
		List<WebElement> getChapterlist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'m0 p0 border border-lightGray')]//div[contains(@class,'col xs-col-4 sm-col-4')][contains(text(),'Chapter')]/following-sibling::div/p[starts-with(.,'Chapter')]"));
		for (int i = 0; i < getChapterlist.size(); i++) {
			chapterName = getChapterlist.get(i).getText();
			chapterNames.add(i, chapterName);
		}
		return chapterNames;
	}

	// Get the Learning Objective in Add Question section metadata
	@Step("get the Learning Objective details on Add Questions Page, Method :{method} ")
	public List<String> getLearningObjectiveList() throws InterruptedException {
		Thread.sleep(4000);
		String learningObjective;
		List<String> learningObjectiveNames = new ArrayList<String>();
		List<WebElement> getLearningObjectivelist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'m0 p0 border border-lightGray')]//div[contains(@class,'col xs-col-4 sm-col-4')][contains(text(),'Learning Objective')]/following-sibling::div"));
		for (int i = 0; i < getLearningObjectivelist.size(); i++) {
			learningObjective = getLearningObjectivelist.get(i).getText();
			learningObjectiveNames.add(i, learningObjective);
			Assert.assertNotNull(learningObjective);
		}
		return learningObjectiveNames;
	}

	// unCheck All Filter Checkboxes
	@Step("unCheck Checkboxes for selected Filters, Method :{method} ")
	public void unCheckChecboxes(String filterName) {
		List<WebElement> selectFiltercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='"
								+ filterName + "']/following-sibling::*"));
		List<WebElement> selectFilter = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input"));

		for (int i = 0; i < selectFiltercheckbox.size(); i++) {
			boolean checkboxSel = selectFilter.get(i)
					.getAttribute("aria-checked").equalsIgnoreCase("true");
			if (checkboxSel == true) {
				selectFiltercheckbox.get(i).click();
				continue;
			}
		}
	}

	// get the child FilterNames from the Parent Name
	@Step("get the Filter Options from the selected filter, Method :{method} ")
	public List<String> getFilterOptions() {
		String childFiltersName = null;
		List<String> childFiltersNameList = new ArrayList<String>();
		List<WebElement> getChildFiltersNameList = driver.findElements(By
				.xpath("//div[@class='full-height block']/div/div/label"));

		for (int i = 0; i < getChildFiltersNameList.size(); i++) {
			childFiltersName = getChildFiltersNameList.get(i).getText();
			childFiltersNameList.add(childFiltersName);
		}
		return childFiltersNameList;

	}

	// unCheck Checkboxes based on User Provided Name
	@Step("unCheck Checkboxes for selected Filters, Method :{method} ")
	public void unCheckChecboxesName(String filterName) {
		List<WebElement> selectFilter = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input"));

		for (int i = 0; i < selectFilter.size(); i++) {
			boolean checkboxSel = selectFilter.get(i)
					.getAttribute("aria-checked").equalsIgnoreCase("true");

			if (checkboxSel == true) {
				List<WebElement> childFilterText = driver
						.findElements(By
								.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label"));
				for (int j = 0; j < childFilterText.size(); j++) {
					String questionTypesName = childFilterText.get(j).getText();

					if (questionTypesName.equalsIgnoreCase(filterName)) {
						Actions action = new Actions(driver);
						WebElement checkbox = driver
								.findElement(By
										.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/span[@class='checkmark absolute center border border-darkBlue top-0 left-0 inline-flex items-center justify-center bg-darkBlue undefined']"));
						action.moveToElement(checkbox).click().build()
								.perform();

					}
				}
				break;
			}
		}
	}

	// Verify “Blooms” metadata title displayed as “Bloom’s Taxonomy” in
	// question metadata block
	@Step("verify Bloom's Taxonomy Text on the Search Questions section,  Method: {method} ")
	public String getBloomsTaxonomyText() {
		String bloomsTaxonomytext = null;
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//div[@class='full-height block']//div[@class='xs-col-4 sm-col-2  flex-column pt1']/div[starts-with(text(),'Bloom')]")));
		List<WebElement> bloomsTaxonomyDetailsList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']//div[@class='xs-col-4 sm-col-2  flex-column pt1']/div[starts-with(text(),'Bloom')]"));
		for (int i = 0; i < bloomsTaxonomyDetailsList.size(); i++) {
			bloomsTaxonomytext = bloomsTaxonomyDetailsList.get(i).getText();

		}

		return bloomsTaxonomytext;
	}

	// Verify “Blooms” metadata title displayed as “Bloom’s Taxonomy” in Added
	// question metadata block
	@Step("verify Bloom's Taxonomy Text on the Added Questions section,  Method: {method} ")
	public String getBloomsTaxonomyTextAddedSection() {
		String bloomsTaxonomytext = null;
		List<WebElement> bloomsTaxonomyDetailsList = driver
				.findElements(By
						.xpath("//button[@type='button'][contains(text(),'More Info')]/following::div[@class='full-height block']/div/div/div[starts-with(@class,'flex flex')]/div/div[starts-with(text(),'Bloom')]"));
		for (int i = 0; i < bloomsTaxonomyDetailsList.size(); i++) {
			bloomsTaxonomytext = bloomsTaxonomyDetailsList.get(i).getText();

		}

		return bloomsTaxonomytext;
	}

	// Verify “Blooms” metadata title displayed as “Bloom’s Taxonomy” in Added
	// question metadata block in List view
	@Step("verify Bloom's Taxonomy Text on the Added Questions section in List view,  Method: {method} ")
	public String getBloomsTaxonomyTextAddedSectionListView() {
		String bloomsTaxonomytext = null;
		boolean isbloomtaxonomyDisplayed = driver
				.findElements(
						By.xpath("//button[@aria-controls='__input__button__Accordion__0'][@aria-expanded!='false']/following::div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div[@class='xs-col-4 sm-col-2  flex-column pt1']/div[@class='sm-col-2 left-align darkerGray small-body-text font-weight-4'][starts-with(text(),'Bloom')]"))
				.size() > 0;
		if (isbloomtaxonomyDisplayed == true) {
			List<WebElement> bloomsTaxonomyDetailsList = driver
					.findElements(By
							.xpath("//button[@aria-controls='__input__button__Accordion__0'][@aria-expanded!='false']/following::div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div[@class='xs-col-4 sm-col-2  flex-column pt1']/div[@class='sm-col-2 left-align darkerGray small-body-text font-weight-4'][starts-with(text(),'Bloom')]"));
			for (int i = 0; i < bloomsTaxonomyDetailsList.size(); i++) {
				bloomsTaxonomytext = bloomsTaxonomyDetailsList.get(i).getText();
				if (bloomsTaxonomytext.equalsIgnoreCase("Bloom's Taxonomy")) {
					return bloomsTaxonomytext.trim().toString();
					
				}

			}
		}
		return null;

		
	}

	// Select the Multiple Check Boxes for Question Type filter
	@Step("Select All question type checkboxes,  Method: {method} ")
	public void selectAllCheckboxesQuestionType() throws InterruptedException {
		List<WebElement> selectAllCheckboxesList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@type='checkbox']/following-sibling::*"));
		for (WebElement checkboxel : selectAllCheckboxesList) {
			checkboxel.click();
			Thread.sleep(2000);

		}
	}

	// Select the Multiple Check Boxes for Question Type filter except DBQ and
	// Matching
	@Step("Select All question type checkboxes,  Method: {method} ")
	public void selectmultipleCheckboxesQuestionType()
			throws InterruptedException {
		List<WebElement> getQuestionTypeName = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input"));
		for (WebElement getQtypeName : getQuestionTypeName) {
			String questionTypeName = getQtypeName.getAttribute("name");
			if (questionTypeName.equalsIgnoreCase("ESSAY")
					|| questionTypeName.equalsIgnoreCase("MULTIPLE CHOICE")
					|| questionTypeName.equalsIgnoreCase("SHORT ANSWER")
					|| questionTypeName.equalsIgnoreCase("TRUE/FALSE")) {
				WebElement selectCheckboxesList = driver
						.findElement(By
								.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@name='"
										+ questionTypeName
										+ "']/following-sibling::*"));
				selectCheckboxesList.click();
			}
		}
		/*
		 * List<WebElement> selectAllCheckboxesList = driver .findElements(By
		 * .xpath(
		 * "//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@type='checkbox']/following-sibling::*"
		 * )); for (WebElement checkboxel : selectAllCheckboxesList) { String
		 * textName =checkboxel.getText();
		 * if(textName.equalsIgnoreCase("MULTIPLE CHOICE")){ checkboxel.click();
		 * } Thread.sleep(2000); }
		 */
	}

	@Step("Select All chapters checkboxes,  Method: {method} ")
	public void selectAllCheckboxesChapters() throws InterruptedException {
		List<WebElement> selectAllCheckboxesList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@type='checkbox']/following-sibling::*"));
		for (WebElement checkboxel : selectAllCheckboxesList) {
			checkboxel.click();
			Thread.sleep(2000);

		}
	}
	
	@Step("Select chapters checkboxes,  Method: {method} ")
	public void selectCheckboxesChapters(int count) throws InterruptedException {
		List<WebElement> selectAllCheckboxesList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@type='checkbox']/following-sibling::*"));
		for(int i=0; i<selectAllCheckboxesList.size(); i++){
			if(i<count){
				selectAllCheckboxesList.get(i).click();
			}
			Thread.sleep(2000);

		}
	}

	// Get the Questions Metadata
	@Step(" get the Questions metadata and verify no Blank metadata header in metadata section,  Method: {method} ")
	public String getQuestionMetadataText() throws InterruptedException {
		String metaDataQuestionsText = null;
		List<WebElement> getQuestionsandOptionsTextlist = driver
				.findElements(By.xpath("//div[@class='pb2 mt3 ml1 pl2']"));
		for (WebElement questionsText : getQuestionsandOptionsTextlist) {
			metaDataQuestionsText = questionsText.getText();
			Assert.assertNotNull(metaDataQuestionsText);
			// System.out.println(metaDataQuestionsText);
			Thread.sleep(2000);
		}
		return metaDataQuestionsText;

	}

	@Step(" Get the Bloom taxonomy  from Question Meta data section,  Method: {method} ")
	public String getbloomtaxonomyMetadata() throws Exception {
		String qlistID = null;
		driver.findElement(By.id("regionAside")).click();
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		String getQuestionsCount = getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int totalQuests = Integer.parseInt(str[0]);
		List<String> qlist = new ArrayList<String>();
		qlist = getQuestionidAddQuestionSection();
		int counter = 10;
		for (int ele = 0; ele < totalQuests; ele++) {
			if (ele >= counter) {
				// actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				driver.findElement(By.id("regionAside")).click();
				actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				Thread.sleep(2000);
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
				actions.keyDown(Keys.CONTROL).release().perform();
				Thread.sleep(5000);
				qlist = getQuestionidAddQuestionSection();
				counter = qlist.size();
				Thread.sleep(2000);
			}
			qlistID = qlist.get(ele).toString();
			//
			Thread.sleep(2000);
			WebElement qID = driver.findElement(By.xpath("//div[@id='"
					+ qlistID + "']"));
			ReusableMethods.scrollIntoView(driver, qID);
			List<WebElement> bloomText = driver
					.findElements(By
							.xpath("//div[@id='"
									+ qlistID
									+ "']//div[contains(text(),\"Bloom's Taxonomy\")]/following-sibling::div/p"));
			Thread.sleep(2000);
			// btp.getQuestionMetadataText();
			int bloomtextCount = bloomText.size();
			if (bloomtextCount > 1) {
				clickAddQuestionbyQID(qlistID);
				Thread.sleep(5000);
				break;
			} else {
				getBacktotoplink();
			}

		}
		return qlistID;

	}

	@Step(" Get the Multiple Difficulty from Question Meta data section,  Method: {method} ")
	public String getmultipleDifficultyMetadata() throws Exception {
		String qlistID = null;
		driver.findElement(By.id("regionAside")).click();
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		String getQuestionsCount = getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int totalQuests = Integer.parseInt(str[0]);
		List<String> qlist = new ArrayList<String>();
		qlist = getQuestionidAddQuestionSection();
		int counter = 10;
		for (int ele = 0; ele < totalQuests; ele++) {
			if (ele >= counter) {
				// actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				driver.findElement(By.id("regionAside")).click();
				actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				Thread.sleep(2000);
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
				actions.keyDown(Keys.CONTROL).release().perform();
				Thread.sleep(5000);
				qlist = getQuestionidAddQuestionSection();
				counter = qlist.size();
				Thread.sleep(2000);
			}
			qlistID = qlist.get(ele).toString();
			//
			Thread.sleep(2000);
			WebElement qID = driver.findElement(By.xpath("//div[@id='"
					+ qlistID + "']"));
			ReusableMethods.scrollIntoView(driver, qID);
			List<WebElement> difficultyText = driver
					.findElements(By
							.xpath("//div[@id='"
									+ qlistID
									+ "']//div[contains(text(),'Difficulty')]/following-sibling::div/p"));
			Thread.sleep(2000);
			// btp.getQuestionMetadataText();
			int difficultytextCount = difficultyText.size();
			if (difficultytextCount > 1) {
				clickAddQuestionbyQID(qlistID);
				Thread.sleep(5000);
				break;
			} else {
				getBacktotoplink();
			}

		}
		return qlistID;

	}

	// Question have metadata with associated values
	@Step(" Verify Question have metadata with associated values and verify no Blank metadata metadata section,  Method: {method} ")
	public String getMetadataAssociatedValues(String metaDataName)
			throws InterruptedException {
		String questionsTypeMetaData = null;
		boolean isdisplayed = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'"
								+ metaDataName + "')]/following-sibling::*")).size() >0;
		if(isdisplayed==true){
		List<WebElement> getQuestionTypeList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'"
								+ metaDataName + "')]/following-sibling::*"));
		
		for (WebElement questionsType : getQuestionTypeList) {
			questionsTypeMetaData = questionsType.getText();
			Assert.assertNotNull(questionsTypeMetaData);
			LogUtil.log(questionsTypeMetaData);
			Thread.sleep(1000);
		}
		
		}
		return questionsTypeMetaData;
	}

	// Question have metadata with associated values
	@Step(" Verify Question Group have metadata with associated values and verify no Blank metadata metadata section,  Method: {method} ")
	public List<String> getMetadataAssociatedValuesQuestionGroup(
			String metaDataName) throws InterruptedException {
		ArrayList<String> questionMetaDataText = new ArrayList<String>();
		String questionsTypeMetaData = null;
		List<WebElement> getQuestionTypeList = driver
				.findElements(By
						.xpath("//button[@class='outline-none circle width2-5 height2-5 mr2 bg-yellow false undefined outline-none left mt3 cursorPointer webkit-appearance-caret']/following-sibling::div/div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'"
								+ metaDataName + "')]/following-sibling::*"));
		for (WebElement questionsType : getQuestionTypeList) {
			questionsTypeMetaData = questionsType.getText();
			Assert.assertNotNull(questionsTypeMetaData);
			LogUtil.log(questionsTypeMetaData);
			Thread.sleep(1000);
			questionMetaDataText.add(questionsTypeMetaData);
		}
		return questionMetaDataText;
	}

	@Step("Click the Single Question ffrom the Question group,  Method: {method} ")
	public void clickSingleQuestionQG(int count,String qTypeOption) throws InterruptedException {
		AddFiltersButton.click();
		clickChaptersButton();
		// read Question Group Chapter Name from JSON File
		String getQGChapterName = ReusableMethods.readQGChapter(qTypeOption);
		List<WebElement> chapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']"));
		for (int c = 0; c < chapterNumberNameList.size(); c++) {
			String chapterName = chapterNumberNameList.get(c).getAttribute(
					"name");
			if (chapterName.contains(getQGChapterName.toString())) {
				WebElement selectCheckbox = driver
						.findElement(By
								.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[contains(@name,'"
										+ chapterName
										+ "')]/following-sibling::span"));
				selectCheckbox.click();
			}
		}
		clickApplyButton();
		// Total Questions returned data
		boolean isQuestion = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]|//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
				.size() > 0;
		if (isQuestion == true) {
			List<String> qlist = new ArrayList<String>();
			String getQuestionsCount = getQuestionCountAfterQuestionSearch();
			String regex = "[^\\d]+";
			String[] str = getQuestionsCount.split(regex);
			int totalQuests = Integer.parseInt(str[0]);
			qlist = getQuestionidRegionMainSection();
			// int listQuestions = qlist.size();
			int counter = 10;
			for (int j = 0; j < totalQuests; j++) {
				if (j >= counter) {
					actions = new Actions(driver);
					actions.sendKeys(Keys.PAGE_DOWN).build().perform();
					Thread.sleep(1000);
					actions.sendKeys(Keys.PAGE_UP).build().perform();
					Thread.sleep(1000);
					actions.sendKeys(Keys.PAGE_DOWN).build().perform();
					Thread.sleep(5000);
					qlist = getQuestionidRegionMainSection();
					counter = qlist.size();
					Thread.sleep(3000);
					// end of if loop
				}
				boolean isQGGroup = driver
						.findElements(
								By.xpath("//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
						.size() > 0;
				if (isQGGroup == true) {
					questiongroupShowlink(count);
					break;
				} else {
					String qlistID = qlist.get(j).toString();
					WebElement qID = driver.findElement(By
							.xpath("//main[@id='regionMain']//div[@id='"
									+ qlistID + "']"));
					ReusableMethods.scrollIntoView(driver, qID);

					Thread.sleep(2000);
					boolean isNormalQuestion = driver
							.findElements(
									By.xpath("//main[@id='regionMain']//div[@id='"
											+ qlistID
											+ "'][starts-with(@class,'px1-5 fr-element fr-view ')]"))
							.size() > 0;
					if (isNormalQuestion == true) {
						Thread.sleep(3000);
						boolean isQGGroupa = driver
								.findElements(
										By.xpath("//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
								.size() > 0;
						if (isQGGroupa == true) {
							boolean isShowQuestionslink = driver
									.findElements(
											By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"))
									.size() > 0;
							if (isShowQuestionslink == true) {
								WebElement clickShowQuestionslink = driver
										.findElement(By
												.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"));
								clickShowQuestionslink.click();
								List<WebElement> getQuestionCount = driver
										.findElements(By
												.xpath("//button[starts-with(@class,'outline-none circle width2-5 height2-5 mr2 bg-yellow false undefined outline-none left mt3 cursorPointer webkit-appearance-caret')]"));
								for (int qGQuestion = 0; qGQuestion < getQuestionCount
										.size(); qGQuestion++) {
									if (qGQuestion == count) {
										getQuestionCount.get(qGQuestion)
												.click();
										break;
									}

								}
							}
						}
					}
					continue;
				}
			}
		}

	}
	
	@Step("Select the Question Group by Selecting the Chapter,  Method: {method} ")
	public void selectQuestionGroupQuestions(String qTypeOption) {
		AddFiltersButton.click();
		clickChaptersButton();
		// read Question Group Chapter Name from JSON File
		String getQGChapterName = ReusableMethods.readQGChapter(qTypeOption);
		List<WebElement> chapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']"));
		for (int c = 0; c < chapterNumberNameList.size(); c++) {
			String chapterName = chapterNumberNameList.get(c).getAttribute(
					"name");
			if (chapterName.contains(getQGChapterName.toString())) {
				WebElement selectCheckbox = driver
						.findElement(By
								.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[contains(@name,'"
										+ chapterName
										+ "')]/following-sibling::span"));
				selectCheckbox.click();
			}
		}
		clickApplyButton();
	}
	

	@Step("Click the Single Question from the Question group based on Question Type,  Method: {method} ")
	public void clickSingleQuestionQGQuestionType() throws InterruptedException {
		List<String> qlist = new ArrayList<String>();
		ReusableMethods.isQuestionGroupDisplayed(driver);
		boolean isShowQuestionslink = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"))
				.size() > 0;
		if (isShowQuestionslink == true) {
			WebElement clickShowQuestionslink = driver
					.findElement(By
							.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"));
			clickShowQuestionslink.click();
		}
		qlist = getQuestionGroupidAddQuestionSection();
		// get Question count

		for (int qGQuestion = 0; qGQuestion < qlist.size(); qGQuestion++) {
			String qlistID = qlist.get(qGQuestion).toString();
			boolean isshortAnswerQType = driver.findElements(
					By.xpath("//div[@id='" + qlistID
							+ "']//div/p[text()='Short Answer']")).size() > 0;
			if (isshortAnswerQType == true) {
				clickAddQuestionbyQID(qlistID);
				break;

			}

		}

	}

	// Verify the Chapter filter available in metadata section in Build test
	// section
	@Step("Application displays chapter details Metadata section and Metadata is not displayed blank,  Method: {method} ")
	public void getChapterdetailsMetaDatasection() throws InterruptedException {
		String chapterDetails;
		List<WebElement> getchapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div[@class='xs-col-4 sm-col-8 black pt1']/div[contains(text(),'Chapter')]/following-sibling::div[@class='clearfix col sm-col-9 pb1 small-body-text']/p"));
		for (WebElement chaptermetaData : getchapterNumberNameList) {
			chapterDetails = chaptermetaData.getText();
			Assert.assertNotNull(chapterDetails);
			Thread.sleep(1000);
			LogUtil.log(chapterDetails);
		}

	}

	// Get the Questions Metadata in Added Question section
	@Step(" get the Questions metadata and verify no Blank metadata header in metadata section in BuildTest View,  Method: {method} ")
	public void getQuestionMetadataTextBuildTestView()
			throws InterruptedException {
		String metaDataQuestionsText = null;
		List<WebElement> getQuestionsandOptionsTextlist = driver
				.findElements(By.xpath("//div[@class='pb2 mt2']"));
		for (WebElement questionsText : getQuestionsandOptionsTextlist) {
			metaDataQuestionsText = questionsText.getText();
			Assert.assertNotNull(metaDataQuestionsText);
			// System.out.println(metaDataQuestionsText);
			Thread.sleep(2000);
		}

	}

	// Question have metadata with associated values in Add Questions section
	@Step(" Verify Question have metadata with associated values and verify no Blank metadata in Added Question section Build Test View,  Method: {method} ")
	public String getMetadataAssociatedValuesBuildTestView(String metaDataName)
			throws InterruptedException {
		List<WebElement> getmetadataList;
		String metaData = null;

		if (metaDataName.startsWith("Bloom")) {
			getmetadataList = driver
					.findElements(By
							.xpath("//button[@type='button'][contains(text(),'More Info')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div/div[starts-with(text(),'Bloom')]/following-sibling::*"));
		} else {
			getmetadataList = driver
					.findElements(By
							.xpath("//button[@type='button'][contains(text(),'More Info')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'"
									+ metaDataName + "')]/following-sibling::*"));
		}
		for (WebElement questionsType : getmetadataList) {
			metaData = questionsType.getText();
			Assert.assertNotNull(metaData);
			LogUtil.log(metaData);
			Thread.sleep(2000);
		}
		return metaData;
	}

	@Step("Application displays chapter details Metadata section and Metadata is not displayed blank in Build View,  Method: {method} ")
	public String getChapterdetailsMetaDatainBuildView()
			throws InterruptedException {
		String chapterDetails = null;
		List<WebElement> getchapterDetailseList = driver
				.findElements(By
						.xpath("//button[@type='button'][contains(text(),'More Info')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'Chapter')]/following-sibling::div[@class='clearfix col sm-col-9 pb1 small-body-text']"));
		for (WebElement chaptermetaData : getchapterDetailseList) {
			chapterDetails = chaptermetaData.getText();
			Assert.assertNotNull(chapterDetails);
			LogUtil.log(chapterDetails);
			Thread.sleep(2000);

		}
		return chapterDetails;

	}

	@Step("Application displays Learning Objective details Metadata section and Metadata is not displayed blank in Build View,  Method: {method} ")
	public void getLearningObjdetailsMetaDatainBuildView()
			throws InterruptedException {
		String learningobjDetails;
		List<WebElement> getLOList = driver
				.findElements(By
						.xpath("//button[@type='button'][contains(text(),'More Info')]/following::div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'Learning Objective')]/following-sibling::*"));
		for (WebElement learningObjmetaData : getLOList) {
			learningobjDetails = learningObjmetaData.getText();
			Assert.assertNotNull(learningobjDetails);
			LogUtil.log(learningobjDetails);
			Thread.sleep(2000);

		}

	}

	// Question have metadata with associated values in Add Questions section
	// List view
	@Step(" Verify Question have metadata with associated values and verify no Blank metadata in Added Question section List View,  Method: {method} ")
	public String getMetadataAssociatedValuesListView(String metaDataName,
			String qid) throws InterruptedException {
		String metaData = null;
		String metaName = null;
		boolean isWebElementPresent1 = driver.findElements(
				By.xpath("//div[@id='regionYourBuildTest']//div[@id='" + qid
						+ "']//div[contains(text(),'" + metaDataName + "')]"))
				.size() > 0;
		/*
		 * boolean isWebElementPresent2 = driver .findElements(
		 * By.xpath("//div[@id='regionYourBuildTest']//div[contains(text(),'" +
		 * metaDataName + "')]/following-sibling::div")) .size() > 0;
		 */
		if (isWebElementPresent1 == true) {
			WebElement metaNameEle = driver.findElement(By
					.xpath("//div[@id='regionYourBuildTest']//div[@id='" + qid
							+ "']//div[contains(text(),'" + metaDataName
							+ "')]"));
			WebElement getmetadataList = driver.findElement(By
					.xpath("//div[@id='regionYourBuildTest']//div[@id='" + qid
							+ "']//div[contains(text(),'" + metaDataName
							+ "')]/following-sibling::div|div/p"));
			metaData = getmetadataList.getText();
			Assert.assertNotNull(metaData);
			metaName = metaNameEle.getText() + "=" + metaData;
			LogUtil.log(metaData);
			Thread.sleep(2000);
		} else {
			return null;
		}
		return metaName;

	}

	@Step("Application displays chapter details Metadata section and Metadata is not displayed blank in List View,  Method: {method} ")
	public String getChapterdetailsMetaDatainListView()
			throws InterruptedException {
		String chapterDetails = null;
		List<WebElement> getchapterDetailseList = driver
				.findElements(By
						.xpath("//button[@aria-expanded='true']/following::div[@class='mx1-5 border-none bg-white block']/span/div/div[2]/div[@class='m0 p0 border border-lightGray flex-1-1-auto flex flex-column justify-start bg-lightestGray']/div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'Chapter')]/following-sibling::div[@class='clearfix col sm-col-9 pb1 small-body-text']/p"));
		for (WebElement chaptermetaData : getchapterDetailseList) {
			chapterDetails = chaptermetaData.getText();
			Assert.assertNotNull(chapterDetails);
			LogUtil.log(chapterDetails);
			Thread.sleep(2000);

		}
		return chapterDetails;

	}

	@Step("Application displays Learning Objective details Metadata section and Metadata is not displayed blank in List View,  Method: {method} ")
	public String getLearningObjdetailsMetaDatainListView()
			throws InterruptedException {
		String learningobjDetails = null;
		List<WebElement> getLOList = driver
				.findElements(By
						.xpath("//button[@aria-expanded='true']/following::div[@class='mx1-5 border-none bg-white block']/span/div/div[2]/div[@class='m0 p0 border border-lightGray flex-1-1-auto flex flex-column justify-start bg-lightestGray']/div[@class='full-height block']/div[@class='mx1-5 py1-5 pb1 border-none']/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'Learning Objective')]/following-sibling::div[@class='clearfix col pb1 small-body-text']/p"));
		for (WebElement learningObjmetaData : getLOList) {
			learningobjDetails = learningObjmetaData.getText();
			Assert.assertNotNull(learningobjDetails);
			LogUtil.log(learningobjDetails);
			Thread.sleep(2000);

		}
		return learningobjDetails;

	}

	// Get the Question Text

	@Step("get the Question Text, Method :{method} ")
	public void getQuestionsText() {

		String questionsText;
		// List<String> questionsList = new ArrayList<String>();
		List<WebElement> questionTextList = driver.findElements(By
				.xpath("//div[@class='pb2 mt3 ml1 pl2']/div/p"));
		Iterator<WebElement> i = questionTextList.iterator();
		while (i.hasNext()) {
			WebElement questionText = i.next();
			questionsText = questionText.getText();
			LogUtil.log(questionsText);
		}
		// return questionsText;

	}

	// find the Duplicates of Questions after the Search

	@Step("verify duplicate questions text and ID Number ', Method :{method} ")
	public void fetchDuplicateQuestionText() {
		List<WebElement> questionsTextElements = driver
				.findElements(By
						.xpath("//div[@class='mx3']/div[@class='px1-5 ']/div/div[@class='pb2 mt3 ml1 pl2']/div[@class='ml3 sub-sup-i']/p"));
		ArrayList<String> questionsList = new ArrayList<String>();
		Set<String> questionsSet = new HashSet<>();
		// for(int i=0; i<questionsTextElements.size(); i++){
		for (WebElement questionsText : questionsTextElements) {
			// String questionText = questionsText.getText();
			questionsList.add(questionsText.getText());
			questionsSet.add(questionsText.getText());
		}

		Assert.assertEquals(questionsList.size(), questionsSet.size());

	}

	// find the Duplicates of Questions ID Number after the Search

	@Step("verify duplicate questions  ID Number ', Method :{method} ")
	public void fetchDuplicateQuestionIDNumber(String metaDataName) {
		List<WebElement> getQuestionTypeList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'"
								+ metaDataName + "')]/following-sibling::*"));
		ArrayList<String> questionsIDList = new ArrayList<String>();
		Set<String> questionsIDSet = new HashSet<>();
		// for(int i=0; i<questionsTextElements.size(); i++){
		for (WebElement questionsText : getQuestionTypeList) {
			// String questionText = questionsText.getText();
			questionsIDList.add(questionsText.getText());
			questionsIDSet.add(questionsText.getText());
		}

		// Assert.assertEquals(questionsIDList.size(), questionsIDSet.size());

	}

	// get the questions Counts
	@Step("get the Questions count, Method :{method} ")
	public int getQuestionsList() {
		int questionsCount;
		List<WebElement> questionsElements = driver.findElements(By
				.xpath("//div[@class='mx3']/div[@class='px1-5 ']"));

		questionsCount = questionsElements.size();

		return questionsCount;
	}

	// Verify Skeleton
	@Step("verify questions skeleton is  displayed, Method : {method}")
	public void questionskeleton() {

		WebDriverWait wait = new WebDriverWait(driver, 50L);
		if (driver.findElements(By.xpath("//div[@class='mx1-5 ']")).size() != 0) {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By
					.xpath("//div[@class='mx1-5 ']"))); // wait for loader to
														// appear
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By
					.xpath("//div[@class='mx1-5 ']"))); // wait for loader to
														// disappear

		}

	}

	// Verify Back TO Top Link
	@Step("verify Back To top Link is displayed and End of Search results  is  displayed, Method : {method}")
	public boolean getBacktotoplink() throws InterruptedException {

		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 50);
		 * wait.until(ExpectedConditions.elementToBeClickable(By
		 * .id("__input__button__backToTop__0"))); WebElement backToTop =
		 * driver.findElement(By .id("__input__button__backToTop__0")); if
		 * (backToTop.isDisplayed()) { backToTop.click(); }
		 */
		boolean isPresent = driver.findElements(
				By.id("__input__button__backToTop__0")).size() > 0;
		if (isPresent == true) {
			Thread.sleep(1000);
			String notificationText = driver
					.findElement(
							By.xpath("//div[@class='center large-body-text mt2-5']/span"))
					.getText();

			Assert.assertEquals("You have reached the end of your results.",
					notificationText);
			driver.findElement(By.id("__input__button__backToTop__0")).click();
		}

		return true;
	}

	// Verify Matching Question First Section Application displays a list of
	// Question Answer Option listed one another below and serialized with
	// a.b.c.. Alphabets
	@Step("Application displays a list of Question Answer Option listed one another below and serialized with a.b.c.. Alphabets, Method : {method}")
	public boolean getMatchingQuestionserialisedwithalphabets() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> alphabetsQuestions = driver
				.findElements(By
						.xpath("//div[@class='right-align mr2']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : alphabetsQuestions) {
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Matching Question Second Question
	@Step("Application displays list of Question listed one another below and serialised with number, Method : {method}")
	public boolean getMatchingQuestionserialisedwithnumberOption() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> flexItemsQuestions = driver
				.findElements(By
						.xpath("//div[@class='flex items-top']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : flexItemsQuestions) {
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify displayed Correct Mark and Answer option for each question
	@Step("Correct Mark and Answer option for each question, Method : {method}")
	public boolean isCorrectMarkDisplayed() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> correctMarks = driver.findElements(By
				.xpath("//*[name()='svg' and @alt='Correct Answer']"));
		for (WebElement correctMark : correctMarks) {
			if (correctMark.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Matching Question First Section Application displays a list of
	// Question Answer Option listed one another below and serialized with
	// a.b.c.. Alphabets in Build View Mode
	@Step("Application displays a list of Question Answer Option listed one another below and serialized with a.b.c.. Alphabets, Method : {method}")
	public boolean getMatchingQuestionserialisedwithalphabetsBuildViewMode() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> alphabetsQuestions = driver
				.findElements(By
						.xpath("//div/button[@id='__input__button__Remove__0']/following::div[@class='right-align mr2']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : alphabetsQuestions) {
			ReusableMethods.scrollIntoView(driver, questionText);
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Matching Question Second Question Build View
	@Step("Application displays list of Question listed one another below and serialised with number, Method : {method}")
	public boolean getMatchingQuestionserialisedwithnumberOptionBuildViewMode() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> flexItemsQuestions = driver
				.findElements(By
						.xpath("//div/button[@id='__input__button__Remove__0']/following::div[@class='flex items-top']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : flexItemsQuestions) {
			ReusableMethods.scrollIntoView(driver, questionText);
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify displayed Correct Mark and Answer option for each question in
	// Build View
	@Step("Correct Mark and Answer option for each question, Method : {method}")
	public boolean isCorrectMarkDisplayedBuildView() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> correctMarks = driver
				.findElements(By
						.xpath("//div/button[@id='__input__button__Remove__0']/following::*[name()='svg' and @alt='Correct Answer']"));
		for (WebElement correctMark : correctMarks) {
			if (correctMark.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Matching Question First Section Application displays a list of
	// Question Answer Option listed one another below and serialized with
	// a.b.c.. Alphabets in List Mode
	@Step("Application displays a list of Question Answer Option listed one another below and serialized with a.b.c.. Alphabets, Method : {method}")
	public boolean getMatchingQuestionserialisedwithalphabetsListViewMode() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> alphabetsQuestions = driver
				.findElements(By
						.xpath("//div/button[@aria-controls='__input__button__Accordion__0']/following::div[@class='right-align mr2']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : alphabetsQuestions) {
			ReusableMethods.scrollIntoView(driver, questionText);
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Matching Question Second Question List View
	@Step("Application displays list of Question listed one another below and serialised with number, Method : {method}")
	public boolean getMatchingQuestionserialisedwithnumberOptionListViewMode() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> flexItemsQuestions = driver
				.findElements(By
						.xpath("//div/button[@aria-controls='__input__button__Accordion__0']/following::div[@class='flex items-top']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : flexItemsQuestions) {
			ReusableMethods.scrollIntoView(driver, questionText);
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify displayed Correct Mark and Answer option for each question in List
	// View
	@Step("Correct Mark and Answer option for each question, Method : {method}")
	public boolean isCorrectMarkDisplayedListView() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> correctMarks = driver
				.findElements(By
						.xpath("//div/button[@aria-controls='__input__button__Accordion__0']/following::*[name()='svg' and @alt='Correct Answer']"));
		for (WebElement correctMark : correctMarks) {
			if (correctMark.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Matching Question First Section Application displays a list of
	// Question Answer Option listed one another below and serialized with
	// a.b.c.. Alphabets in Preview Mode
	@Step("Application displays a list of Question Answer Option listed one another below and serialized with a.b.c.. Alphabets, Method : {method}")
	public boolean getMatchingQuestionserialisedwithalphabetsPreviewMode() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> alphabetsQuestions = driver
				.findElements(By
						.xpath("//div[@class='h1 pl2-5 pb1-5 pt1 py3']/following::div[@class='right-align mr2']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : alphabetsQuestions) {
			// ReusableMethods.scrollIntoView(driver, questionText);
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Matching Question Second Question
	@Step("Application displays list of Question listed one another below and serialised with number, Method : {method}")
	public boolean getMatchingQuestionserialisedwithnumberOptionPreviewMode() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='pt1-5']/li")));
		List<WebElement> flexItemsQuestions = driver
				.findElements(By
						.xpath("//div[@class='h1 pl2-5 pb1-5 pt1 py3']/following::div[@class='flex items-top']/ancestor::div[@class='pt1-5']"));
		for (WebElement questionText : flexItemsQuestions) {
			// ReusableMethods.scrollIntoView(driver, questionText);
			if (questionText.isDisplayed()) {
				return true;

			}
		}
		return false;

	}

	// Verify Question group Question
	@Step("Application displays Questions group Questions, Method : {method}")
	public boolean getquestiongroupQuestions() throws InterruptedException {
		// ReusableMethods.scrollIntoView(driver, AddFiltersButton);
		driver.findElement(By.id("regionAside")).click();
		// actions = new Actions(driver);

		clickAddFiltersButton();
		clickChaptersButton();
		List<WebElement> selectChaptercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*"));

		for (int i = 0; i < selectChaptercheckbox.size(); i++) {
			selectChaptercheckbox.get(i).click();
			clickApplyButton();
			boolean resultsdisplayed = driver
					.findElements(
							By.xpath("//div[text()='No results that match these filters.']"))
					.size() > 0;
			if (resultsdisplayed == true) {
				List<WebElement> removeFiltericon = driver.findElements(By
						.xpath("//*[name()='svg' and @alt='Remove filter']"));
				removeFiltericon.get(0).click();
				clickAddFiltersButton();
				clickChaptersButton();
				continue;
			}
			// ReusableMethods.loadingWaitDisapper(driver);
			String getQuestionsCount = getQuestionCountAfterQuestionSearch();
			String regex = "[^\\d]+";
			String[] str = getQuestionsCount.split(regex);
			int totalQuests = Integer.parseInt(str[0]);
			List<String> qlist = new ArrayList<String>();
			//qlist = getQuestionidAddQuestionSection();
			qlist=getQuestionGroupidAddQuestionSection();
			int counter = 10;
			for (int ele = 0; ele < totalQuests; ele++) {
				if (ele >= counter) {
					ReusableMethods.scrollQuestionSection(driver);
					qlist = getQuestionGroupIidAddQuestionSection();
					counter = qlist.size();
					Thread.sleep(2000);
				}
				String qlistID = null;
				try {
					qlistID = qlist.get(0).toString();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				WebElement qID = driver.findElement(By.xpath("//div[@id='"+qlistID+"']"));
				ReusableMethods.scrollIntoView(driver, qID);
				boolean questionGroupele = driver
						.findElements(
								By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]"))
						.size() > 0;
				//
				if (ele == totalQuests - 1) {
					driver.findElements(By.id("__input__button__backToTop__0"))
							.size();
					WebElement backtoTop = driver.findElement(By
							.id("__input__button__backToTop__0"));
					backtoTop.click();
					clickClearFiltersButton();
				}
				if (questionGroupele == true) {
					ReusableMethods.scrollQuestionSection(driver);
					WebDriverWait wait = new WebDriverWait(driver, 40);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By
							.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]")));
					List<WebElement> questionGroupEle = driver
							.findElements(By
									.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]"));
					for (int qsize = 0; i < questionGroupEle.size(); qsize++) {
						String questiongroupText = questionGroupEle.get(qsize)
								.getText();
						if (questiongroupText
								.equalsIgnoreCase("Question Group")) {
							break;
						}
					}

				}
			}

		}
		return true;

	}

	// Get the Question group Question and Click Add
	@Step("Application displays Questions group Questions, Method : {method}")
	public void questiongroupQuestions(String qTypeOption) throws InterruptedException {
		// ReusableMethods.scrollIntoView(driver, AddFiltersButton);
		driver.findElement(By.id("regionAside")).click();
		// actions = new Actions(driver);

		clickChaptersButton();
		List<WebElement> selectChaptercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*"));

		for (int i = 0; i < selectChaptercheckbox.size(); i++) {
			selectChaptercheckbox.get(i).click();
			clickApplyButton();
			ReusableMethods.loadingWaitDisapper(driver);
			String getQuestionsCount = getQuestionCountAfterQuestionSearch();
			String regex = "[^\\d]+";
			String[] str = getQuestionsCount.split(regex);
			int totalQuests = Integer.parseInt(str[0]);
			List<String> qlist = new ArrayList<String>();
			qlist = getQuestionidAddQuestionSection();
			int counter = 10;
			for (int ele = 0; ele < totalQuests; ele++) {
				if (ele >= counter) {
					ReusableMethods.scrollQuestionSection(driver);
					qlist = getQuestionGroupIidAddQuestionSection();
					counter = qlist.size();
					Thread.sleep(2000);
				}
				String qlistID = qlist.get(ele).toString();
				WebElement qID = driver.findElement(By.xpath("//div[@id='"
						+ qlistID + "']"));
				ReusableMethods.scrollIntoView(driver, qID);
				boolean questionGroupele = driver
						.findElements(
								By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]"))
						.size() > 0;
				//
				if (ele == totalQuests - 1) {
					driver.findElements(By.id("__input__button__backToTop__0"))
							.size();
					WebElement backtoTop = driver.findElement(By
							.id("__input__button__backToTop__0"));
					backtoTop.click();
					clickClearFiltersButton();
				}
				if (questionGroupele == true) {
					ReusableMethods.scrollQuestionSection(driver);
					int questionCount = 0;
					WebDriverWait wait = new WebDriverWait(driver, 40);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By
							.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]")));
					List<WebElement> questionGroupEle = driver
							.findElements(By
									.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]"));
					for (int qsize = 0; i < questionGroupEle.size(); qsize++) {
						String questiongroupText = questionGroupEle.get(qsize)
								.getText();
						if (questiongroupText
								.equalsIgnoreCase("Question Group")) {
							clickAddquestiongroupQuestions(questionCount,qTypeOption);
							break;
						}
					}

				}

			}

		}

	}

	@Step("Click Add Questions  for Questions group Questions, Method : {method}")
	public void clickAddquestiongroupQuestions(int questionCount,String qTypeOption)
			throws InterruptedException {
		AddFiltersButton.click();
		Thread.sleep(2000);
		clickChaptersButton();
		// read Question Group Chapter Name from JSON File
		String getQGChapterName = ReusableMethods.readQGChapter(qTypeOption);
		List<WebElement> chapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']"));
		for (int c = 0; c < chapterNumberNameList.size(); c++) {
			String chapterName = chapterNumberNameList.get(c).getAttribute(
					"name");
			if(Pattern.compile(Pattern.quote(getQGChapterName), Pattern.CASE_INSENSITIVE).matcher(chapterName).find()){
			//if (chapterName.contains(getQGChapterName.toString())) {
				WebElement selectCheckbox = driver
						.findElement(By
								.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[contains(@name,'"
										+ chapterName
										+ "')]/following-sibling::span"));
				
				selectCheckbox.click();
			}
		}
		// end if catch loop
		clickApplyButton();
		// Total Questions returned data
		boolean isQuestion = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]|//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
				.size() > 0;
		if (isQuestion == true) {
			List<String> qlist = new ArrayList<String>();
			String getQuestionsCount = getQuestionCountAfterQuestionSearch();
			String regex = "[^\\d]+";
			String[] str = getQuestionsCount.split(regex);
			int totalQuests = Integer.parseInt(str[0]);
			qlist = getQuestionidRegionMainSection();
			// int listQuestions = qlist.size();
			int counter = 10;
			for (int j = 0; j < totalQuests; j++) {
				if (j >= counter) {
					actions = new Actions(driver);
					actions.sendKeys(Keys.PAGE_DOWN).build().perform();
					Thread.sleep(1000);
					actions.sendKeys(Keys.PAGE_UP).build().perform();
					Thread.sleep(1000);
					actions.sendKeys(Keys.PAGE_DOWN).build().perform();
					Thread.sleep(5000);
					qlist = getQuestionidRegionMainSection();
					counter = qlist.size();
					Thread.sleep(3000);
					// end of if loop
				}
				boolean isQGGroup = driver
						.findElements(
								By.xpath("//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
						.size() > 0;
				if (isQGGroup == true) {
					WebElement clickQG = driver
							.findElement(By
									.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(.,'Question Group')]/preceding::*[name()='svg'][@id='__icon__plus__________0'][position()=1]"));
					clickQG.click();
					break;
				} else {
					String qlistID = qlist.get(j).toString();
					WebElement qID = driver.findElement(By
							.xpath("//main[@id='regionMain']//div[@id='"
									+ qlistID + "']"));
					ReusableMethods.scrollIntoView(driver, qID);

					Thread.sleep(2000);
					boolean isNormalQuestion = driver
							.findElements(
									By.xpath("//main[@id='regionMain']//div[@id='"
											+ qlistID
											+ "'][starts-with(@class,'px1-5 fr-element fr-view ')]"))
							.size() > 0;
					if (isNormalQuestion == true) {
						boolean isQGGroup1 = driver
								.findElements(
										By.xpath("//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
								.size() > 0;
						if (isQGGroup1 == true) {
							WebElement clickQG = driver
									.findElement(By
											.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(.,'Question Group')]/preceding::*[name()='svg'][@id='__icon__plus__________0'][position()=1]"));
							clickQG.click();
							break;
						}
						continue;
					}
					if (j == (totalQuests - 1)) {
						driver.findElement(
								By.xpath("//*[name()='svg'][@id='__icon__backToTop__________0']"))
								.click();
						Thread.sleep(3000);
					}
				}
				continue;
			}
			
		}

	}

	@Step("Click Add Questions link from Questions group Questions, Method : {method}")
	public String clickAddquestiongrouplink() throws InterruptedException {
		getquestiongroupQuestions();
		String questionsText = null;
		List<WebElement> questionsAddlinkButton = driver
				.findElements(By
						.xpath("//div[@class='ml1-5 mr3']/button[starts-with(@id,'__input__button__Show__')]"));
		for (int i = 0; i < questionsAddlinkButton.size();) {

			ReusableMethods.scrollIntoView(driver,
					questionsAddlinkButton.get(i));
			questionsText = questionsAddlinkButton.get(i).getText();
			questionsAddlinkButton.get(i).click();
			break;
		}
		return questionsText;
	}

	@Step("Question group Label on test view , Method : {method}")
	public void getQuestionGroupLabel() {
		boolean questionsGroupLabel = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'px1-5')]/div/span[contains(./text(),'Question Group')]"))
				.isEmpty();
		if (questionsGroupLabel == true) {
			Assert.assertTrue(questionsGroupLabel);
		} else {
			WebElement questionsGroupLabelEle = driver
					.findElement(By
							.xpath("//div[starts-with(@class,'px1-5')]/div/span[contains(./text(),'Question Group')]"));
			String qText = questionsGroupLabelEle.getText();
			Assert.assertNotNull(qText);
		}

	}

	@Step("get Question group Question and then add questions, Method : {method}")
	public void getQuestionGroupLabelQuestions() {
		boolean questionsGroupLabel = driver
				.findElements(
						By.xpath("//button[@name='addbutton']/following::div[contains(text(),'Question Group')]"))
				.size() > 0;

		if (questionsGroupLabel == true) {
			List<WebElement> qgroupQuestions = driver
					.findElements(By
							.xpath("//div[contains(text(),'Question Group')]/preceding-sibling::button/*[name()='svg'][@id='__icon__plus__________0']"));
			for (int i = 0; i < qgroupQuestions.size();) {
				WebElement addQuestion = qgroupQuestions
						.get(i)
						.findElement(
								By.xpath("//div[contains(text(),'Question Group')]/preceding-sibling::button/*[name()='svg'][@id='__icon__plus__________0']"));
				addQuestion.click();
				break;
			}
		}

	}
	
	
	@Step("Select the Chapters for Questions group Questions, Method : {method}")
	public void selectQGQuestionChapters(int questionCount,String qTypeOption)
			throws InterruptedException {
		AddFiltersButton.click();
		clickChaptersButton();
		// read Question Group Chapter Name from JSON File
		String getQGChapterName = ReusableMethods.readQGChapter(qTypeOption);
		List<WebElement> chapterNumberNameList = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']"));
		for (int c = 0; c < chapterNumberNameList.size(); c++) {
			String chapterName = chapterNumberNameList.get(c).getAttribute(
					"name");
			if (chapterName.contains(getQGChapterName.toString())) {
				WebElement selectCheckbox = driver
						.findElement(By
								.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[contains(@name,'"
										+ chapterName
										+ "')]/following-sibling::span"));
				
				selectCheckbox.click();
			}
		}
		// end if catch loop
		clickApplyButton();
	}

	@Step("Remove All  Label on test view , Method : {method}")
	public void getRemoveAllLabel() {
		boolean questionsGroupLabel = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'px1-5')]/div/button[contains(./text(),'Remove All')]"))
				.isEmpty();
		if (questionsGroupLabel == true) {
			Assert.assertTrue(questionsGroupLabel);
		} else {
			WebElement questionsGroupLabelEle = driver
					.findElement(By
							.xpath("//div[starts-with(@class,'px1-5')]/div/button[contains(./text(),'Remove All')]"));
			String qText = questionsGroupLabelEle.getText();
			Assert.assertNotNull(qText);
		}

	}

	@Step("Click Remove All  link on test view , Method : {method}")
	public void clickRemoveAlllink() {
		WebElement questionsGroupLabelEle = driver.findElement(By
				.xpath("//button[contains(./text(),'Remove All')]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);",
				questionsGroupLabelEle);
		js.executeScript("window.scrollBy(0,-250)", "");
		questionsGroupLabelEle.click();

	}

	@Step("Question group prompt displays before the individual question(s) in test and Question group prompt displayed without sequential number order, Method : {method}")
	public String getQuestionGroupPromptText() {
		List<WebElement> questionsGroupPromptText = driver.findElements(By
				.xpath("//div[@class='px2 mb1']"));
		questionsGroupPromptText.size();
		String questiongrouptext = questionsGroupPromptText.get(0).getText();
		String str = questiongrouptext;
		char ch = str.charAt(0);
		if (Character.isDigit(ch)) {
			Assert.assertTrue(Character.isDigit(ch));
		}
		return questiongrouptext;
	}

	@Step("Question group questions ordered sequentially in the test, Method : {method}")
	public void getquestionsSeqOrdered() {

		ArrayList<String> seqNumbers = new ArrayList<String>();
		List<WebElement> checkSeq = driver.findElements(By
				.xpath("//div[@class='px2-5 ']/div/div[@class='left ']"));
		for (WebElement seqEle : checkSeq) {
			seqNumbers.add(seqEle.getText());

		}
		Collections.sort(seqNumbers);
		LogUtil.log(" The Sequeence Number is " + seqNumbers);

	}

	@Step("Click individual Questions from Question group, Method : {method}")
	public void clickIndividualAddQuestions(int count) {
		List<WebElement> individualQbutton = driver
				.findElements(By
						.xpath("//div[@class='col xs-col-4 sm-col-4 md-col-8 lg-col-12']/button[@name='addbutton']/*[name()='svg']"));
		for (int i = 0; i < count; i++) {
			individualQbutton.get(i).click();

		}

	}

	@Step("Click individual Questions from Question group, Method : {method}")
	public void clickQuestionGroupUndoButton() throws InterruptedException {
		WebElement questionsGroupUndoButton = driver
				.findElement(By
						.xpath("//div[@class='mx3']//div[contains(text(),'Question Group Added')]/following-sibling::div/button[@id='__input__button__Undo__0']"));
		questionsGroupUndoButton.click();
		Thread.sleep(3000);
	}

	@Step("Click expand link in List view against Question Group, method :[method]")
	public void clickexpandiconQuestiongroup() {
		WebElement clickexpandicon = driver
				.findElement(By
						.xpath("//span[@class='body-text pr1-5 right-align'][contains(text(),'Question Group')]/following-sibling::*[name()='svg']"));
		clickexpandicon.click();
	}

	@Step("Document Header on preview each questions documents sequence will start from 1 , Method : {method}")
	public String getDocumentHeader() {
		String documentHeaderSeq = null;
		List<WebElement> documentHeader = driver.findElements(By
				.xpath("//div[@name='divHeading']"));
		for (int i = 0; i < documentHeader.size(); i++) {
			documentHeaderSeq = documentHeader.get(i).getText();
		}
		return documentHeaderSeq;

	}

	@Step(" Verify Question metadata is not displayed on Preview mode for DBQ ,  Method: {method} ")
	public void getMetaDataBlankOnPreview() {

		boolean isMeatadatasection = driver
				.findElements(
						By.xpath("//button[@type='button'][contains(text(),'More Info')]"))
				.size() != 0;
		if (isMeatadatasection == true) {
			Assert.assertNotNull("Meta data is not displayed");
		} else {
			Assert.assertNull(null);
		}

	}

	@Step(" Next to the Add Question Icon, application displays the DBQ question stem including description ,  Method: {method} ")
	public void getDBQQuestionDescription() {
		List<WebElement> dBQQuestionDescription = driver
				.findElements(By
						.xpath("//div[@class='col xs-col-4 sm-col-4 md-col-8 lg-col-12']/button/following::div[@class='pb2 mt3 ml1 pl2']/div/p[@class='m0']"));
		for (int i = 0; i < dBQQuestionDescription.size(); i++) {
			ReusableMethods.scrollIntoView(driver,
					dBQQuestionDescription.get(i));
			String dbqDesc = dBQQuestionDescription.get(i).getText();
			LogUtil.log(dbqDesc);
		}

	}

	@Step(" get the Document Sequence number details for each Question ,  Method: {method} ")
	public void getDBQDocument() {
		String questionIDs = null;
		boolean isdisplayed = false;
		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);
		}

		for (int i = 0; i < documentQID.size(); i++) {
			String qid = documentQID.get(i);
			WebElement questionID = driver.findElement(By.id(qid));
			ReusableMethods.scrollIntoView(driver, questionID);
			isdisplayed = driver
					.findElements(
							By.xpath("//div[@id='"
									+ qid
									+ "']/div/div[@class='pb2 mt3 ml1 pl2']//div/div[@class='flex mt1-5']/div[@class='ml3 border-top border-bottom border-lightGray full-width pt2 pb2']/button"))
					.size() > 0;

			if (isdisplayed == true) {
				driver.findElement(
						By.xpath("//*[name()='svg' and @id='__icon__chevronSmallDownBlue__________0']"))
						.click();
				List<WebElement> dBQdocumentlist = driver
						.findElements(By
								.xpath("//div[@id='"
										+ qid
										+ "']//div[@name='divHeadingWrapper']/div[@name='divHeading']"));
				List<String> documentDBQseq = new ArrayList<String>();
				for (int j = 0; j < dBQdocumentlist.size(); j++) {
					ReusableMethods.scrollIntoView(driver,
							dBQdocumentlist.get(j));
					/*
					 * boolean isReloadDocumentlink =
					 * driver.findElements(By.xpath("//div[@id='"+ qid +
					 * "']//div[@class='mr1-5 border-left-4 border-lightBlue pb3']/div[@class='mx1-5 center mt3']/div/span[contains(text(),'Document Question could not be loaded')]"
					 * )).size() > 0; if(isReloadDocumentlink==true){
					 * Assert.assertTrue(isReloadDocumentlink, "+ qid +" +
					 * "Document Question could not be loaded"); continue; }
					 */
					String dbqDescShowDocuments = dBQdocumentlist.get(j)
							.getText();
					if (dbqDescShowDocuments == null) {
						Assert.assertNull(dbqDescShowDocuments,
								"Document Question could not be loaded");
						continue;
					}
					LogUtil.log(qid + dbqDescShowDocuments);
					documentDBQseq.add(dbqDescShowDocuments);
					List<String> sortedDBQDocumentNumbers = new ArrayList<String>(
							documentDBQseq);
					Collections.sort(sortedDBQDocumentNumbers);
					LogUtil.log(sortedDBQDocumentNumbers);
				}
			} else {
				boolean isReloadDocumentlink = driver
						.findElements(
								By.xpath("//div[@id='"
										+ qid
										+ "']//div[@class='mr1-5 border-left-4 border-lightBlue pb3']/div[@class='mx1-5 center mt3']/div/span[contains(text(),'Document Question could not be loaded')]"))
						.size() > 0;
				if (isReloadDocumentlink == true) {
					Assert.assertTrue(isReloadDocumentlink, "+ qid +"
							+ "Document Question could not be loaded");
					continue;
				}
				WebElement dBQdocument = driver
						.findElement(By
								.xpath("//div[@name='divHeadingWrapper']/div[@name='divHeading']"));
				ReusableMethods.scrollIntoView(driver, dBQdocument);
				String dbqDesc = dBQdocument.getText();
				LogUtil.log(qid + dbqDesc);
			}

		}

	}

	@Step(" get the Document Source and Description  details for each Question ,  Method: {method} ")
	public List<String> getDocumentSourceDescFilterview() {
		String questionIDs = null;
		boolean isdisplayed = false;
		List<String> documentDBQseq = new ArrayList<String>();
		List<WebElement> dbqaddQuestionslist = driver.findElements(By
				.xpath("//div[starts-with(@class,'px1-5 ')][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);
		}

		for (int i = 0; i < documentQID.size(); i++) {
			String qid = documentQID.get(i);
			WebElement questionID = driver.findElement(By.id(qid));
			ReusableMethods.scrollIntoView(driver, questionID);
			isdisplayed = driver
					.findElements(
							By.xpath("//div[@id='"
									+ qid
									+ "']/div/div[@class='pb2 mt3 ml1 pl2']//div/div[@class='flex mt1-5']/div[@class='ml3 border-top border-bottom border-lightGray full-width pt2 pb2']/button"))
					.size() > 0;

			if (isdisplayed == true) {
				boolean isExpanded = driver
						.findElements(
								By.xpath("//*[name()='svg' and @id='__icon__chevronSmallUpBlue__________0']"))
						.size() > 0;
				if (isExpanded == false) {
					driver.findElement(
							By.xpath("//*[name()='svg' and @id='__icon__chevronSmallDownBlue__________0']"))
							.click();
				}

				List<WebElement> dBQdocumentlist = driver.findElements(By
						.xpath("//div[starts-with(@class,'pl2-5')]"));

				for (int j = 0; j < dBQdocumentlist.size(); j++) {
					ReusableMethods.scrollIntoView(driver,
							dBQdocumentlist.get(j));
					boolean isReloadDocumentlink = driver
							.findElements(
									By.xpath("//div[@id='"
											+ qid
											+ "']//div[@class='mr1-5 border-left-4 border-lightBlue pb3']/div[@class='mx1-5 center mt3']/div/span[contains(text(),'Document Question could not be loaded')]"))
							.size() > 0;
					if (isReloadDocumentlink == true) {
						Assert.assertTrue(isReloadDocumentlink, "+ qid +"
								+ "Document Question could not be loaded");
						continue;
					}
					String dbqDescandSourceContent = dBQdocumentlist.get(j)
							.getText();
					LogUtil.log(dbqDescandSourceContent);
					documentDBQseq.add(dbqDescandSourceContent);

				}
			} else {
				WebElement sourcestext = driver.findElement(By
						.xpath("//div[starts-with(@class,'pl2-5')]"));
				ReusableMethods.scrollIntoView(driver, sourcestext);
				String dbqDesc = sourcestext.getText();
				LogUtil.log(qid + dbqDesc);
			}
		}
		return documentDBQseq;
	}

	@Step(" Add the DBQ Questions ,  Method: {method} ")
	public void addDBQUestions(int qCount) {
		String questionIDs = null;
		List<WebElement> dbqaddQuestionslist = driver.findElements(By
				.xpath("//div[starts-with(@class,'px1-5 ')][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);
		}
		for (int i = 0; i < documentQID.size(); i++) {
			String qid = documentQID.get(i);
			WebElement questionID = driver.findElement(By.id(qid));
			ReusableMethods.scrollIntoView(driver, questionID);
			WebElement addButton = driver.findElement(By
					.xpath("//button[@name='addbutton']"));
			if (i < qCount) {
				addButton.click();
			}
		}
	}

	@Step(" get the Document details and  Sequence number details for each Question on build View ,  Method: {method} ")
	public void getDBQDocumentBuildView() {
		String questionIDs = null;
		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[@class='px1-5 '][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);
		}
		for (int i = 0; i < documentQID.size(); i++) {
			String qid = documentQID.get(i);
			WebElement questionID = driver.findElement(By.id(qid));
			ReusableMethods.scrollIntoView(driver, questionID);
			boolean isdisplayed = driver.findElements(
					By.xpath("//div[@id='regionYourBuildTest']//div[@id='"
							+ qid + "']//div[@class='flex mt1-5']/div/button"))
					.size() > 0;
			if (isdisplayed == true) {
				driver.findElement(
						By.xpath("//div[@id='regionYourBuildTest']//div[@id='"
								+ qid
								+ "']//div[@class='flex mt1-5']/div/button//*[name()='svg' and @id='__icon__chevronSmallDownBlue__________0']"))
						.click();
				List<WebElement> dBQdocumentlist = driver
						.findElements(By
								.xpath("//div[@id='regionYourBuildTest']//div[@id='"
										+ qid
										+ "']//div[@name='divHeadingWrapper']/div[@name='divHeading']"));
				List<String> documentDBQseq = new ArrayList<String>();
				for (int j = 0; j < dBQdocumentlist.size(); j++) {
					ReusableMethods.scrollIntoView(driver,
							dBQdocumentlist.get(j));
					boolean isReloadDocumentlink = driver
							.findElements(
									By.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div[@class='mr1-5 border-left-4 border-lightBlue pb3']/div[@class='mx1-5 center mt3']/div/span[contains(text(),'Document Question could not be loaded')]"))
							.size() > 0;
					if (isReloadDocumentlink == true) {
						Assert.assertTrue(isReloadDocumentlink, "+ qid +"
								+ "Document Question could not be loaded");
					}
					String dbqDescShowDocuments = dBQdocumentlist.get(j)
							.getText();
					LogUtil.log(dbqDescShowDocuments);
					documentDBQseq.add(dbqDescShowDocuments);
					List<String> sortedDBQDocumentNumbers = new ArrayList<String>(
							documentDBQseq);
					Collections.sort(sortedDBQDocumentNumbers);
					LogUtil.log(sortedDBQDocumentNumbers);
				}
			} else {
				boolean isReloadDocumentlink = driver
						.findElements(
								By.xpath("//div[@id='regionYourBuildTest']//div[@id='"
										+ qid
										+ "']//div[@class='mr1-5 border-left-4 border-lightBlue pb3']/div[@class='mx1-5 center mt3']/div/span[contains(text(),'Document Question could not be loaded')]"))
						.size() > 0;
				if (isReloadDocumentlink == true) {
					Assert.assertTrue(isReloadDocumentlink, "+ qid +"
							+ "Document Question could not be loaded");
				}
				WebElement dBQdocument = driver
						.findElement(By
								.xpath("//div[@id='regionYourBuildTest']//div[@id='"
										+ qid
										+ "']//div[@name='divHeadingWrapper']/div[@name='divHeading']"));

				ReusableMethods.scrollIntoView(driver, dBQdocument);
				String dbqDesc = dBQdocument.getText();
				LogUtil.log(dbqDesc);
				// get Document Source and Description

			}

		}

	}

	@Step(" get the Document Source and Description  details for each Question ,  Method: {method} ")
	public List<String> getDocumentSourceDescBuildView() {
		String questionIDs = null;
		String qid = null;
		boolean isdisplayed = false;
		List<String> documentDBQseq = new ArrayList<String>();
		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[starts-with(@class,'px1-5 ')]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);
		}

		for (int i = 0; i < documentQID.size(); i++) {
			qid = documentQID.get(i);
			WebElement questionID = driver.findElement(By.id(qid));
			ReusableMethods.scrollIntoView(driver, questionID);
			isdisplayed = driver
					.findElements(
							By.xpath("//div[@id='regionYourBuildTest']//div[@id='"
									+ qid
									+ "']/div/div[@class='pb2 mt3 ml1 pl2']//div/div[@class='flex mt1-5']/div[@class='ml3 border-top border-bottom border-lightGray full-width pt2 pb2']/button"))
					.size() > 0;
		}
		if (isdisplayed == true) {
			boolean isExpanded = driver
					.findElements(
							By.xpath("//*[name()='svg' and @id='__icon__chevronSmallUpBlue__________0']"))
					.size() > 0;
			if (isExpanded == false) {
				driver.findElement(
						By.xpath("//*[name()='svg' and @id='__icon__chevronSmallDownBlue__________0']"))
						.click();
			}

			List<WebElement> dBQdocumentlist = driver
					.findElements(By
							.xpath("//div[@id='regionYourBuildTest']//div[@class='pl2-5']"));

			for (int j = 0; j < dBQdocumentlist.size(); j++) {
				ReusableMethods.scrollIntoView(driver, dBQdocumentlist.get(j));
				boolean isReloadDocumentlink = driver
						.findElements(
								By.xpath("//div[@id='regionYourBuildTest']//div[@id='"
										+ qid
										+ "']//div[@class='mr1-5 border-left-4 border-lightBlue pb3']/div[@class='mx1-5 center mt3']/div/span[contains(text(),'Document Question could not be loaded')]"))
						.size() > 0;
				if (isReloadDocumentlink == true) {
					Assert.assertTrue(isReloadDocumentlink, "+ qid +"
							+ "Document Question could not be loaded");
				}
				String dbqDescandSourceContent = dBQdocumentlist.get(j)
						.getText();
				if (dbqDescandSourceContent
						.contains("Reload Document Question")) {
					Assert.assertNull(dBQdocumentlist.get(j).getText());
				}
				LogUtil.log(dbqDescandSourceContent);
				documentDBQseq.add(dbqDescandSourceContent);

			}

		}
		return documentDBQseq;
	}

	public boolean areElementsOverlapping(WebElement element1,
			WebElement element2) {
		Point location = element1.getLocation();
		Dimension size = element1.getSize();

		Point location2 = element2.getLocation();
		Dimension size2 = element2.getSize();

		if (location.getY() > location2.getY()
				|| size.getHeight() > size2.getHeight()) {
			return false;
		}
		if (location.getY() < location2.getY()
				|| size.getHeight() < size2.getHeight()) {
			return false;
		}
		Assert.assertTrue(true,
				"The Book Specification is not overlapped with Questions Text");
		return true;

	}

	// Click Meta data More Info button for each Question Types
	@Step(" click More Info section for each DBQ Question and Verify Meta data ,  Method: {method} ")
	public void clickMoreinfoBuildView() {
		String questionIDs = null;

		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[@class='px1-5 ']"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);
		}

		for (int i = 0; i < documentQID.size(); i++) {
			String qid = documentQID.get(i);
			WebElement questionID = driver.findElement(By.id(qid));
			ReusableMethods.scrollIntoView(driver, questionID);
			List<WebElement> clickMoreInfoButton = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			ReusableMethods.scrollIntoView(driver, clickMoreInfoButton.get(i));
			clickMoreInfoButton.get(i).click();
		}
	}

	// Verify DBQ text on List View for Added Questions
	public void getDBQtextinListview() {
		List<WebElement> getDBQtext = driver
				.findElements(By
						.xpath("//*[name()='svg']/preceding-sibling::span[@class='body-text pr1-5 right-align']"));
		for (int i = 0; i < getDBQtext.size(); i++) {
			String dbqtext = getDBQtext.get(i).getText();
			Assert.assertNotNull(dbqtext);
		}
	}

	// /AP LO

	@Step(" Verify AP Filter,  Method: {method} ")
	public boolean apFilter() {
		driver.findElement(By.id("regionAside")).click();
		ReusableMethods.scrollIntoView(driver, APLearningObjective);
		APLearningObjective.isDisplayed();
		return true;

	}

	@Step(" Click AP Filter and AP Learning Objective,  Method: {method} ")
	public void clickapLearningObjectiveFilter() {
		APLearningObjective.click();
	}

	@Step(" Click LearningObjective Filter,  Method: {method} ")
	public void clickLearningObjectiveFilter() {
		LearningObjective.click();
	}

	@Step(" Verify  AP Filter Panel,  Method: {method} ")
	public List<String> getapLearningObjectiveFilterPanel() {
		List<WebElement> apPanelList = driver.findElements(By
				.xpath("//button[@type='button'][contains(text(),'AP ')]"));
		List<String> apList = new ArrayList<String>();
		for (int i = 0; i < apPanelList.size(); i++) {
			String apFilterNames = apPanelList.get(i).getText();
			apList.add(apFilterNames);
		}
		return apList;
	}

	@Step(" Verify  AP Learning Objective Filter Panel display based on chapter selection,  Method: {method} ")
	public List<String> apLearningObjectiveFilter() throws InterruptedException {
		driver.findElement(By.id("regionAside")).click();
		ReusableMethods.scrollIntoView(driver, ApplyButton);
		List<String> getCheckboxesNameList = new ArrayList<String>();
		String getCheckboxesName = null;
		clickChaptersButton();
		List<WebElement> selectChaptercheckbox = driver
				.findElements(By
						.xpath("//div[@class='mx1-5 py1-5 pb1 border-none']/div/label/input[@texthint='Chapters']/following-sibling::*"));

		for (int i = 0; i < selectChaptercheckbox.size(); i++) {
			selectChaptercheckbox.get(i).click();
			Thread.sleep(2000);
			ReusableMethods.scrollIntoView(driver, APLearningObjective);
			boolean iscollapsgrey = driver
					.findElements(
							By.xpath("//*[name()='svg' and @id='__icon__accordioncollapsgrey__________0']"))
					.size() > 0;
			if (iscollapsgrey == true) {
				clickapLearningObjectiveFilter();
			}
			// ReusableMethods.scrollIntoView(driver, APLearningObjective);
			// clickapLearningObjectiveFilter();
			boolean APWebElement = driver
					.findElements(
							By.xpath("//label[starts-with(@for,'__input__checkbox__AP__Learning__Objectives__')]"))
					.size() > 0;
			if (APWebElement == true) {
				clickapLearningObjectiveFilter();
				List<WebElement> apWebElementcheckboxes = driver
						.findElements(By
								.xpath("//label[starts-with(@for,'__input__checkbox__AP__Learning__Objectives__')]/span"));
				List<WebElement> apWebElementlabel = driver
						.findElements(By
								.xpath("//label[starts-with(@for,'__input__checkbox__AP__Learning__Objectives__')]"));
				for (int apc = 0; apc < apWebElementcheckboxes.size(); apc++) {
					apWebElementcheckboxes.get(apc).click();
					getCheckboxesName = apWebElementlabel.get(apc).getText();
					getCheckboxesNameList.add(getCheckboxesName);
				}
				break;
			} else {
				clickapLearningObjectiveFilter();
				Thread.sleep(2000);
				ReusableMethods.scrollIntoView(driver,
						selectChaptercheckbox.get(i));
				selectChaptercheckbox.get(i).click();
				continue;

			}

		}
		return getCheckboxesNameList;
	}

	@Step("verify AP Learning Objective Details in Meta data ', Method :{method} ")
	public List<String> getApLearningObjective(String metaDataName) {
		List<String> qlist = new ArrayList<String>();
		List<String> metaDataValue = new ArrayList<String>();
		String apLOFilterData = null;
		List<WebElement> getQuestionTypeList = driver
				.findElements(By
						.xpath("//div[@class='full-height block']/div/div/div[@class='flex flex-wrap sm-col-4']/div/div[contains(text(),'"
								+ metaDataName + "')]/following-sibling::*"));
		// for(int i=0; i<questionsTextElements.size(); i++){
		qlist = getQuestionidAddQuestionSection();
		for (int qGQuestion = 0; qGQuestion < qlist.size(); qGQuestion++) {
			@SuppressWarnings("unused")
			String qlistID = qlist.get(qGQuestion).toString();
			apLOFilterData = getQuestionTypeList.get(qGQuestion).getText();
			metaDataValue.add(apLOFilterData);
		}

		/*
		 * for (WebElement questionsText : getQuestionTypeList) { // String
		 * questionText = questionsText.getText();
		 * questionsIDList.add(questionsText.getText());
		 * 
		 * }
		 */
		return metaDataValue;
	}

	// Question Source
	@Step("verify Question Source Option Names when User Click Question Source Filter  ', Method :{method} ")
	public List<String> getQuestionSourceOptionNames() {
		List<String> getQuestionSourceOptionList = new ArrayList<String>();
		String getQuestionSourceCheckboxName = null;
		driver.findElement(By.id("regionAside")).click();
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//label[starts-with(@for,'__input__checkbox__Question__Source__0')]"));
		boolean QSWebElement = driver
				.findElements(
						By.xpath("//label[starts-with(@for,'__input__checkbox__Question__Source__')]"))
				.size() > 0;
		if (QSWebElement == true) {
			List<WebElement> qsWebElementcheckboxes = driver
					.findElements(By
							.xpath("//label[starts-with(@for,'__input__checkbox__Question__Source__')]/span"));
			List<WebElement> qsWebElementlabel = driver
					.findElements(By
							.xpath("//label[starts-with(@for,'__input__checkbox__Question__Source__')]"));
			for (int qs = 0; qs < qsWebElementcheckboxes.size(); qs++) {
				qsWebElementcheckboxes.get(qs).click();
				getQuestionSourceCheckboxName = qsWebElementlabel.get(qs)
						.getText();
				getQuestionSourceOptionList.add(getQuestionSourceCheckboxName);
			}
		}
		return getQuestionSourceOptionList;
	}

	@Step("Select Question Source Option Names when User Click Question Source Filter  ', Method :{method} ")
	public void selectQuestionSourceOptionNames(int options) {
		driver.findElement(By.id("regionAside")).click();
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//label[starts-with(@for,'__input__checkbox__Question__Source__0')]"));
		boolean QSWebElement = driver
				.findElements(
						By.xpath("//label[starts-with(@for,'__input__checkbox__Question__Source__')]"))
				.size() > 0;
		if (QSWebElement == true) {
			List<WebElement> qsWebElementcheckboxes = driver
					.findElements(By
							.xpath("//label[starts-with(@for,'__input__checkbox__Question__Source__')]/span"));

			for (int qs = 0; qs < qsWebElementcheckboxes.size(); qs++) {
				if (qs == options) {
					qsWebElementcheckboxes.get(qs).click();
					break;
				}

			}
		}

	}

	public void scrolltoQuestiontext() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement questiontext = driver.findElement(By
				.xpath("//div[@class='black px2-5 mb2-5']"));
		js.executeScript("arguments[0].scrollIntoView(true);", questiontext);
	}

	public void scrollMetadata(WebDriver driver) throws InterruptedException {
		driver.findElement(By.id("regionAside")).click();
		Actions actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 240; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}
	}

	public void scrollMetadata20Question(WebDriver driver)
			throws InterruptedException {
		driver.findElement(By.id("regionAside")).click();
		Actions actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}
	}

	public boolean reloadQuestionError(String qid) {
		boolean isReloadDocumentlink = driver
				.findElements(
						By.xpath("//div[@id='"
								+ qid
								+ "']//div[@class='mr1-5 border-left-4 border-lightBlue pb3']/div[@class='mx1-5 center mt3']/div/span[contains(text(),'Document Question could not be loaded')]"))
				.size() > 0;
		if (isReloadDocumentlink == true) {
			Assert.assertTrue(isReloadDocumentlink, "+ qid +"
					+ "Document Question could not be loaded");
		}
		return true;
	}

	public List<String> getQuestionid() {
		String questionIDs = null;

		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[starts-with(@class,'px1-5 ')]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);

		}
		return documentQID;

	}

	public List<String> getQuestionidRegionMainSection() {
		String questionIDs = null;

		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//main[@id='regionMain']//div[starts-with(@class,'px1-5 ')][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);

		}
		return documentQID;

	}

	public List<String> getQuestionidAddQuestionSection() {
		String questionIDs = null;

		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);

		}
		return documentQID;

	}

	public List<String> getQuestionGroupidAddQuestionSection() throws InterruptedException {
		String questionIDs = null;		
		List<String> questionGroupQID = new ArrayList<String>();
		boolean isShowLink = driver.findElements(By.xpath("//button[starts-with(@id,'__input__button__Show')]")).size() >0;
		if(isShowLink==true){
			driver.findElement(By.xpath("//button[starts-with(@id,'__input__button__Show')]")).click();
		}
		Thread.sleep(1000);
		List<WebElement> qGaddQuestionslist = driver
				.findElements(By
						.xpath("//button[starts-with(@id,'__input__button__Hide')]/following::div[starts-with(@class,'pr1-5 fr-element fr-view')][@id]"));
		for (WebElement qID : qGaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			questionGroupQID.add(questionIDs);

		}
		return questionGroupQID;

	}

	public List<String> getQuestionGroupIidAddQuestionSection() {
		String questionIDs = null;

		List<WebElement> questiongrouplist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'pr1-5 fr-element fr-view ')][@id]"));
		List<String> qgroupQID = new ArrayList<String>();
		for (WebElement qID : questiongrouplist) {
			questionIDs = qID.getAttribute("id");
			qgroupQID.add(questionIDs);

		}
		return qgroupQID;

	}

	@Step("Click the Add Question button based on Question ID,  Method: {method} ")
	public void clickAddQuestionbyQID(String qID) {
		WebElement qIDButton = driver
				.findElement(By
						.xpath("//div[@id='"
								+ qID
								+ "']/div/button[starts-with(@id,'__input__button__addbutton__')]"));
		qIDButton.click();

	}

	@Step("click the Add Question button which is having image,  Method: {method} ")
	public void clickimageQuestionMetaData() throws InterruptedException {
		driver.findElement(By.id("regionAside")).click();
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		String getQuestionsCount = getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int totalQuests = Integer.parseInt(str[0]);
		List<String> qlist = new ArrayList<String>();
		qlist = getQuestionidAddQuestionSection();
		int counter = 10;
		for (int ele = 0; ele < totalQuests; ele++) {
			if (ele >= counter) {

				// actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				actions.sendKeys(Keys.PAGE_UP).build().perform();
				Thread.sleep(5000);
				qlist = getQuestionidAddQuestionSection();
				counter = qlist.size();
				Thread.sleep(2000);
			}

			String qlistID = qlist.get(ele).toString();
			WebElement qID = driver.findElement(By.xpath("//div[@id='"
					+ qlistID + "']"));
			ReusableMethods.scrollIntoView(driver, qID);
			boolean imageqID = driver.findElements(
					By.xpath("//div[@id='" + qlistID + "']//div//p//canvas"))
					.size() > 0;

			if (imageqID == true) {
				Thread.sleep(2000);
				WebElement jpegImage = driver.findElement(By
						.xpath("//div[@id='" + qlistID + "']//div//p//canvas"));
				String jpgimage = jpegImage.getAttribute("id");
				if (jpgimage.contains(".jpg")) {
					Assert.assertTrue(true, "Question Contains jpeg Image");
					clickAddQuestionbyQID(qlistID);
					Thread.sleep(2000);
					break;
				}
			}
		}
	}

	@Step("Build View Question Contains JPG Image,  Method: {method} ")
	public void getimageQuestionBuildView() throws InterruptedException {
		/*
		 * String getQuestionsCount = verifyQuestioninTestBuildSearchResult();
		 * String regex = "[^\\d]+"; String[] str =
		 * getQuestionsCount.split(regex); int totalQuests =
		 * Integer.parseInt(str[0]);
		 */
		List<String> qlist = new ArrayList<String>();
		qlist = getQuestionid();
		for (int ele = 0; ele < qlist.size(); ele++) {
			String qlistID = qlist.get(ele).toString();
			WebElement qID = driver.findElement(By
					.xpath("//div[@id='regionYourBuildTest']//div[@id='"
							+ qlistID + "']"));
			ReusableMethods.scrollIntoView(driver, qID);
			boolean imageqID = driver.findElements(
					By.xpath("//div[@id='regionYourBuildTest']//div[@id='"
							+ qlistID + "']//div//p//canvas")).size() > 0;

			if (imageqID == true) {
				Thread.sleep(2000);
				WebElement jpegImage = driver.findElement(By
						.xpath("//div[@id='regionYourBuildTest']//div[@id='"
								+ qlistID + "']//div//p//canvas"));
				String jpgimage = jpegImage.getAttribute("id");
				if (jpgimage.contains(".jpg")) {
					Assert.assertTrue(true, "Question Contains jpeg Image");
					Thread.sleep(2000);
					break;
				}
			}
		}
	}

	@Step("Build View Question metadata is displayed in a New line,  Method: {method} ")
	public int getmetaDataCount(String metaDataName) {
		List<WebElement> getmetaData = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[@class='sm-col-2 left-align darkerGray small-body-text font-weight-4'][contains(text(),'"
								+ metaDataName + "')]/following-sibling::*"));
		return getmetaData.size();
	}

	@Step("Select the each AP Filter Options ,  Method: {method} ")
	public List<String> selectAPFilter(String metaDataName) {
		List<String> getCheckboxesNameList = new ArrayList<String>();
		String getCheckboxesName = null;
		boolean APWebElement = driver.findElements(
				By.xpath("//button[@type='button'][contains(text(),'"
						+ metaDataName + "')]")).size() > 0;

		if (APWebElement == true) {
			WebElement APFilterNameEle = driver.findElement(By
					.xpath("//button[@type='button'][contains(text(),'"
							+ metaDataName + "')]"));
			APFilterNameEle.click();
			// Split String
			String str = metaDataName;
			String[] arrStr = str.split(" ");

			List<WebElement> apWebElementcheckboxes = driver.findElements(By
					.xpath("//label[starts-with(@for,'__input__checkbox__AP__"
							+ arrStr[1] + "')]/span"));
			List<WebElement> apWebElementlabels = driver.findElements(By
					.xpath("//label[starts-with(@for,'__input__checkbox__AP__"
							+ arrStr[1] + "')]"));
			for (int apc = 0; apc < apWebElementcheckboxes.size(); apc++) {
				apWebElementcheckboxes.get(apc).click();
				getCheckboxesName = apWebElementlabels.get(apc).getText();
				getCheckboxesNameList.add(getCheckboxesName);
			}

		}
		return getCheckboxesNameList;
	}

	@Step("Select the List View and Click on More info section,  Method: {method} ")
	public void getListViewQuestionMetaData() throws InterruptedException {
		List<String> qids = new ArrayList<String>();
		qids = getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			js = (JavascriptExecutor) driver;
			List<WebElement> listviewButton = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
			for (int j = 0; j < listviewButton.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@class='border-none bg-white none']"))
							.size() > 0;
					if (isexpanded == true) {
						js.executeScript("arguments[0].click();",
								listviewButton.get(j));
					}
					Thread.sleep(1000);

				}
			}
		}
	}

	public void questiongroupShowlink(int count) {
		boolean isQGGroup = driver
				.findElements(
						By.xpath("//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
				.size() > 0;
		if (isQGGroup == true) {
			boolean isShowQuestionslink = driver
					.findElements(
							By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"))
					.size() > 0;
			if (isShowQuestionslink == true) {
				WebElement clickShowQuestionslink = driver
						.findElement(By
								.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"));
				clickShowQuestionslink.click();
				List<WebElement> getQuestionCount = driver
						.findElements(By
								.xpath("//button[starts-with(@class,'outline-none circle width2-5 height2-5 mr2 bg-yellow false undefined outline-none left mt3 cursorPointer webkit-appearance-caret')]"));
				for (int qGQuestion = 0; qGQuestion < getQuestionCount.size(); qGQuestion++) {
					if (qGQuestion == count) {
						getQuestionCount.get(qGQuestion).click();
						break;
					}

				}
			}
		}
	}
	
	public String getDBQSearchText(){
		String questionIDs = null;
		String SearchText = null;
		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);
			ReusableMethods.scrollIntoView(driver, qID);
			WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='"
					+ questionIDs + "']/div/div[1]/div"));

			String getText = getIDvalue.getText();
			ArrayList<String> searchKeys = new ArrayList<String>();
			getPOS POS = new getPOS();
			
			try {
				SearchText = POS
							.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
									searchKeys);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return SearchText;
	}
	
	@Step("Click the Single Question ffrom the Question group,  Method: {method} ")
	public void SelectandAddSingleQuestionQG(int count,String qTypeOption) throws InterruptedException {
		// Total Questions returned data
		boolean isQuestion = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]|//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
				.size() > 0;
		if (isQuestion == true) {
			List<String> qlist = new ArrayList<String>();
			String getQuestionsCount = getQuestionCountAfterQuestionSearch();
			String regex = "[^\\d]+";
			String[] str = getQuestionsCount.split(regex);
			int totalQuests = Integer.parseInt(str[0]);
			qlist = getQuestionidRegionMainSection();
			// int listQuestions = qlist.size();
			int counter = 10;
			for (int j = 0; j < totalQuests; j++) {
				if (j >= counter) {
					actions = new Actions(driver);
					actions.sendKeys(Keys.PAGE_DOWN).build().perform();
					Thread.sleep(1000);
					actions.sendKeys(Keys.PAGE_UP).build().perform();
					Thread.sleep(1000);
					actions.sendKeys(Keys.PAGE_DOWN).build().perform();
					Thread.sleep(5000);
					qlist = getQuestionidRegionMainSection();
					counter = qlist.size();
					Thread.sleep(3000);
					// end of if loop
				}
				boolean isQGGroup = driver
						.findElements(
								By.xpath("//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
						.size() > 0;
				if (isQGGroup == true) {
					questiongroupShowlink(count);
					break;
				} else {
					String qlistID = qlist.get(j).toString();
					WebElement qID = driver.findElement(By
							.xpath("//main[@id='regionMain']//div[@id='"
									+ qlistID + "']"));
					ReusableMethods.scrollIntoView(driver, qID);

					Thread.sleep(2000);
					boolean isNormalQuestion = driver
							.findElements(
									By.xpath("//main[@id='regionMain']//div[@id='"
											+ qlistID
											+ "'][starts-with(@class,'px1-5 fr-element fr-view ')]"))
							.size() > 0;
					if (isNormalQuestion == true) {
						Thread.sleep(3000);
						boolean isQGGroupa = driver
								.findElements(
										By.xpath("//*[name()='svg'][@id='__icon__plus__________0']/following::div[contains(.,'Question Group')]"))
								.size() > 0;
						if (isQGGroupa == true) {
							boolean isShowQuestionslink = driver
									.findElements(
											By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"))
									.size() > 0;
							if (isShowQuestionslink == true) {
								WebElement clickShowQuestionslink = driver
										.findElement(By
												.xpath("//div[starts-with(@class,'ml1-5 mr3')]/button[starts-with(@id,'__input__button__Show__')]"));
								clickShowQuestionslink.click();
								List<WebElement> getQuestionCount = driver
										.findElements(By
												.xpath("//button[starts-with(@class,'outline-none circle width2-5 height2-5 mr2 bg-yellow false undefined outline-none left mt3 cursorPointer webkit-appearance-caret')]"));
								for (int qGQuestion = 0; qGQuestion < getQuestionCount
										.size(); qGQuestion++) {
									if (qGQuestion == count) {
										getQuestionCount.get(qGQuestion)
												.click();
										break;
									}

								}
							}
						}
					}
					continue;
				}
			}
		}

	}
	
	public void noDataAfterSearch(){
		boolean isquestionsDisplayed = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'center large-body-text mt2-5')][contains(text(),'No results that match these filters.')]"))
				.size() > 0;
		if (isquestionsDisplayed == true) {
			List<WebElement> removeFiltericon = driver.findElements(By
					.xpath("//*[name()='svg' and @alt='Remove filter']"));
			for (int r = 0; r < removeFiltericon.size(); r++) {
				removeFiltericon.get(1).click();
			}
		}
	}
	
}
