package com.wwnorton.TestMaker.ObjectFactories;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wwnorton.TestMaker.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class ExportWindow {

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//div[@class='ReactModal__Content ReactModal__Content--after-open']")
	public WebElement ExportTestDialogOpenWindow;
	
	@FindBy(how = How.ID, using = "__input__radio__DOCX__0")
	public WebElement MicrosoftWordRadioButton;
	
	@FindBy(how = How.ID, using = "__input__radio__CC__1")
	public WebElement IMSCCRadioButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='pl1-5']/button[@id='__input__button__Export__0']")
	public WebElement ExportButton;
		
	@FindBy(how = How.XPATH, using = "//div[@class='white h4 break-word css-0']")
	public WebElement Exporttext;
	
	@FindBy(how = How.ID, using = "__link__W____W____Norton__Support____0")
	public WebElement wwNortonlink;
	
	@FindBy(how = How.ID, using = "__input__button__closeModal__1")
	public WebElement closeexportwindow;
	
	@FindBy(how = How.XPATH, using = "//label/input[@name='createVersions']/following::span")
	public WebElement checkCreateVersionCheckbox;
	
	// Initializing Web Driver and PageFactory.
		public ExportWindow() throws Exception {
			this.driver = BaseDriver.getDriver();
			PageFactory.initElements(driver, this);
		}
	
	@Step("Select the MicroSoft Word Option and Click Export Button from export test window,  Method: {method} ")
	public void clickMSwordExportButton() throws InterruptedException	 {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(ExportTestDialogOpenWindow));
		MicrosoftWordRadioButton.click();
		Thread.sleep(2000);
		ExportButton.click();
	
	}
	
	@Step("Select the IMSCC Option and Click Export Button from export test window,  Method: {method} ")
	public void clickIMSCCExportButton() throws InterruptedException	 {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(ExportTestDialogOpenWindow));
		IMSCCRadioButton.click();
		Thread.sleep(2000);
		ExportButton.click();
	
	}
	@Step("Select the IMSCC Option from export test window,  Method: {method} ")
	public void selectIMSCCExportButton() throws InterruptedException	 {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(ExportTestDialogOpenWindow));
		if(IMSCCRadioButton.isEnabled()){
			IMSCCRadioButton.click();
		} else{
			Assert.assertTrue(!IMSCCRadioButton.isEnabled());
		}
	}
	
	@Step(" IMSCC Option is disabled for Matching Question Type from export test window,  Method: {method} ")
	public boolean iMSCCExportButtonDisabled() throws InterruptedException	 {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(ExportTestDialogOpenWindow));
		if(IMSCCRadioButton.isEnabled()){
			Assert.assertFalse(IMSCCRadioButton.isEnabled());
		} else{
			Assert.assertTrue(!IMSCCRadioButton.isEnabled());
		}
		return true;
	}
	
	
	@Step("verify Export Text when user click the export button,  Method: {method} ")
	public String  getExportText() throws InterruptedException	 {
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(Exporttext)); 
		return Exporttext.getText();
	}
	@Step("Verify messaging text displayed below selected export option, Method:(method)")
	public String getimsccmessageText() throws InterruptedException	 {
		Thread.sleep(2000);
		WebElement  labelTextEle = driver.findElement(By.xpath("//label[@for='__input__radio__CC__1']"));
		String labelText =labelTextEle.getText();
		WebElement  labellargebodyTextEle = driver.findElement(By.xpath("//div[@class='pr3-0']/div"));
		String labellargeText =labellargebodyTextEle.getText();
		return labelText+labellargeText;
		
	}
	
	@Step("Click on W.W.Norton Support link., Method:(method)")
	public void clickWWnortonlink()  {
		wwNortonlink.click();
	}
	
	@Step("close export modal, Method:(method)")
	public void clickExportModal()  {
		closeexportwindow.click();
	}
	/*//Download the Word File 
	String zipFilePath = System.getProperty("user.dir") + "\\Downloads";
	// PDF File Download

	String destFilePath = System.getProperty("user.dir") + "\\Downloads";
	ReusableMethods.processZipFile(zipFilePath, destFilePath);*/
	@Step("Application open Norton Support page in new tab, Method:(method)")
	public String verifyNortonSupportPage(WebDriver driver, String parentWindow)  {
		String childWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		Windowhandles.remove(parentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != parentWindow) {
			childWindow = winHandle;
		}
		driver.switchTo().window(childWindow);
		return driver.findElement(By.xpath("//div/h1[contains(.,'Import tests to Learning Management Systems')]")).getText();
	}
	
	@Step("Check the Create Version Checkbox ,  Method: {method} ")
	public void clickCreateVersionCheckbox() throws InterruptedException	 {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(ExportTestDialogOpenWindow));
		checkCreateVersionCheckbox.click();
	
	}
	
	@Step("Create Version display the Error message withoutselecting the version ,  Method: {method} ")
	public String getVersionErrorMessage() throws InterruptedException	 {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(ExportTestDialogOpenWindow));
		WebElement getErrorText = driver.findElement(By.xpath("//div[@class='pt1']/span[text()='This is a required field.']"));
		String errorMessage = getErrorText.getText();
		return errorMessage;	
	}
	
	@Step("Create Version will not display Error After selecting the version,  Method: {method} ")
	public boolean isErrorMessage() throws InterruptedException	 {
		boolean isElementPresent;
		isElementPresent = driver.findElements(By.xpath("//div[@class='pt1']/span[text()='This is a required field.']")).size() != 0;
		if (isElementPresent == false) {
			return true;
		}
		return isElementPresent;
	}
	
	@Step("Select the value from Create Version Select list box ,  Method: {method} ")
	public void selectCreateVersion() throws InterruptedException	 {
		WebElement selectValuelist = driver.findElement(By.xpath("//div[@class='pt2']//following-sibling::div//div[starts-with(@class,' css-')]/div[@class=' css-2w99ta']/div"));
		selectValuelist.click();
		List<WebElement> listitems = driver.findElements(By
				.xpath("//div[@class='pt1']/div//following-sibling::div[@class=' css-crwyl1-menu']/div/div[starts-with(@id,'react-select-')]"));
		for (int i = 0; i < listitems.size();) {
			WebElement getlistValue = listitems.get(0);
			getlistValue.click();
			Thread.sleep(2000);
			break;
		}
	}
}
