package com.wwnorton.TestMaker.ObjectFactories;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;











import org.testng.Assert;

import com.wwnorton.TestMaker.utilities.BaseDriver;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class TestMakerLoginPage 
{
	WebDriver driver;

	// Finding Web Elements on New Question page using PageFactory.

	@FindBy(how = How.XPATH, using = "//input[@id='loginEmail']|//input[@id='txtEmail-input']")
	public WebElement TestMakerLoginEmail;
	
	@FindBy(how = How.XPATH, using = "//input[@id='loginPassword']|//input[@id='txtpassword-input']")
	public WebElement TestMakerLoginPassword;
	
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement TestMakerLogInButton;
	
	@FindBy(how = How.XPATH, using = "//button[@name='profile'][@id='__input__button__profile__0']")
	public WebElement ProfileName;
	
	@FindBy(how = How.XPATH, using = "//div[@id='regionYourTestBank']/div/div[@class='left']")
	public WebElement bookdetails;
	
	@FindBy(how = How.XPATH, using ="//main[starts-with(@class,'full-height full-width clearfix flex flex-column items-center pt4')]//div/p[starts-with(@class,'h2')]")
	public WebElement YouDontacessHeaderText;
	
	@FindBy(how = How.XPATH, using ="//main[starts-with(@class,'full-height full-width clearfix flex flex-column items-center pt4')]//div/p[starts-with(@class,'large-text center')]")
	public WebElement LargeText;
	
	@FindBy(how = How.XPATH, using ="//main[starts-with(@class,'full-height full-width clearfix flex flex-column items-center pt4')]//div/p[starts-with(@class,'large-text center')]/a[@id='__link__contact__us__0']")
	public WebElement ContactUsbutton;
	
	@FindBy(how = How.XPATH, using ="//a[@id='ContactUs']")
	public WebElement WWNortonTechSupport;
	
	@FindBy(how = How.XPATH, using ="//a[@id='ResourceUrl']")
	public WebElement InstructorResource;
	
	@FindBy(how = How.ID, using ="__link__Click__here__to__Request__Access__0")
	public WebElement RequestAccessbutton;
	
	@FindBy(how = How.ID, using ="__input__button__Forgot__your__password____0")
	public WebElement forgotPasswordLink;
	
	@FindBy(how = How.XPATH, using ="//div[@class='nds-field__feedback']/ul[@id='txtEmail-err']/li")
	public WebElement getErrorText;

		
	com.wwnorton.TestMaker.utilities.ReadUIJsonFile readJsonObject = new com.wwnorton.TestMaker.utilities.ReadUIJsonFile();
	com.google.gson.JsonObject jsonObj = readJsonObject.readUIJason();	
	
	// Initializing Web Driver and PageFactory.
		public TestMakerLoginPage() throws Exception {

			this.driver = BaseDriver.getDriver();
			PageFactory.initElements(driver, this);
			
		}
		
		@Step("Instructor Login to Test Maker Application,  Method: {method} ")
		public void loginTestMakerApp() throws InterruptedException {
			ReusableMethods.checkPageIsReady(driver);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='loginEmail']|//input[@id='txtEmail-input']")));
			TestMakerLoginEmail.click();
			String userName = jsonObj.getAsJsonObject("TMInstructorLoginCredentials").get("userName").getAsString();
			TestMakerLoginEmail.sendKeys(userName);
			String passWord = jsonObj.getAsJsonObject("TMInstructorLoginCredentials").get("password").getAsString();
			TestMakerLoginPassword.sendKeys(passWord);
			TestMakerLogInButton.click();
			ReusableMethods.loadingWaitDisapper(driver);
		}
		
		@Step("Instructor Login to Test Maker Application to verify default ISBN,  Method: {method} ")
		public void instloginTestMakerApp(String userName, String Password) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='loginEmail']|//input[@id='txtEmail-input']")));
			TestMakerLoginEmail.click();
			TestMakerLoginEmail.sendKeys(userName);
			TestMakerLoginPassword.sendKeys(Password);
			TestMakerLogInButton.click();
			ReusableMethods.loadingWaitDisapper(driver);
		}
		
		@Step("UnAuthorized Instructor Login to Test Maker Application,  Method: {method} ")
		public void unAuthloginTestMakerApp() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='loginEmail']|//input[@id='txtEmail-input']")));
			TestMakerLoginEmail.click();
			String userName = jsonObj.getAsJsonObject("UnAuthInstructorLoginCredentials").get("userName").getAsString();
			TestMakerLoginEmail.sendKeys(userName);
			String passWord = jsonObj.getAsJsonObject("UnAuthInstructorLoginCredentials").get("password").getAsString();
			TestMakerLoginPassword.sendKeys(passWord);
			TestMakerLogInButton.click();
			ReusableMethods.loadingWaitDisapper(driver);
		}
		
		@Step("Instructor enters inValid Credenatisl,  Method: {method} ")
		public void loginTestMakerAppInvalidCred() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='loginEmail']|//input[@id='txtEmail-input']")));
			TestMakerLoginEmail.click();
			String userName = jsonObj.getAsJsonObject("TMInstructorLoginCredentials").get("userName").getAsString();
			TestMakerLoginEmail.sendKeys(userName);
			String passWord = "invalid@123";
			TestMakerLoginPassword.sendKeys(passWord);
			TestMakerLogInButton.click();
			
		}
		
		@Step("Instructor Login to Test Maker Application to Verify Question Source,  Method: {method} ")
		public void loginTestMakerAppQUestionSource() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='loginEmail']|//input[@id='txtEmail-input']")));
			String userName = jsonObj.getAsJsonObject("TMQuestionSourceInstCredentails").get("userName").getAsString();
			TestMakerLoginEmail.sendKeys(userName);
			String passWord = jsonObj.getAsJsonObject("TMQuestionSourceInstCredentails").get("password").getAsString();
			TestMakerLoginPassword.sendKeys(passWord);
			TestMakerLogInButton.click();
			ReusableMethods.loadingWaitDisapper(driver);
		}
		
		@Step("Unauthorised User logs to Test Maker Application,  Method: {method} ")
		public void unAuthLoginTestMakerApp() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='loginEmail']|//input[@id='txtEmail-input']")));
			String userName = jsonObj.getAsJsonObject("TMNONInstructorLoginCredentials").get("userName").getAsString();
			TestMakerLoginEmail.sendKeys(userName);
			String passWord = jsonObj.getAsJsonObject("TMNONInstructorLoginCredentials").get("password").getAsString();
			TestMakerLoginPassword.sendKeys(passWord);
			TestMakerLogInButton.click();
			Thread.sleep(3000);
		}
		
		@Step("Verify Profile Information,  Method: {method} ")
		public String profileTestMakerApp() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOf(ProfileName));
			String userName = ProfileName.getText();
			LogUtil.log(userName);
			return userName;
		}
		
		@Step("get the book details,  Method: {method} ")
		public String getbookinformation() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			String bookInfo = bookdetails.getText();
			System.out.println(bookInfo);
			LogUtil.log(bookInfo);
			return bookInfo;
			
		}
		
		@Step("Verify Unauthorized Message,  Method: {method} ")
		public String getUnauthorizedMessage() throws InterruptedException {
			Thread.sleep(5000);
			String getUnauthorizedMessage;
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOf(ProfileName));
			String youdonthaveaccess =YouDontacessHeaderText.getAttribute("innerText");
		//	Assert.assertEquals(youdonthaveaccess, expected);
			String largeText =LargeText.getText();
			//String contactus =ContactUsbutton.getText();
			String requestAccess =RequestAccessbutton.getText();
			getUnauthorizedMessage =youdonthaveaccess+"\n"+largeText+" "+"\n"+"or"+"\n"+requestAccess+"";
			return getUnauthorizedMessage;
		}
		
		@Step("Click Contact Us button and Application open Smartsheet application in another tab,  Method: {method} ")
		public void clickContactUsButton() throws InterruptedException {
			ContactUsbutton.click();
			String winHandleBefore = driver.getWindowHandle();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			Thread.sleep(5000);
			WebDriverWait  wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='css-h3zj1a']/section[@class='css-474oue']")));
			WebElement testmakerAlphaTestReportsText =driver.findElement(By.xpath("//div[@class='css-h3zj1a']/section[@class='css-474oue']"));
			Assert.assertEquals("Testmaker Alpha Test Reports", testmakerAlphaTestReportsText.getText());
			driver.close();
			driver.switchTo().window(winHandleBefore);
		}
		
		@Step("Click Request Access button and Application open Website application in another tab and respective ISBN Page and displays Request Access button,  Method: {method} ")
		public void clickRequestAccessButton() throws InterruptedException {
			RequestAccessbutton.click();
			String winHandleBefore = driver.getWindowHandle();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			Thread.sleep(5000);
			WebDriverWait  wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/button[@class='center'][contains(text(),'Request Access')]")));
			WebElement requestAccessbutton =driver.findElement(By.xpath("//div/button[@class='center'][contains(text(),'Request Access')]"));
			Assert.assertEquals("Request Access", requestAccessbutton.getText());
			driver.close();
			driver.switchTo().window(winHandleBefore);
		}
		
		@Step("get the book title,  Method: {method} ")
		public String getbooktitle() throws InterruptedException {
			String bookTitle = null;
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			boolean booktitle = driver.findElements(By.xpath("//div[@class='left']/div/div[@class='bold'][1]")).size()>0;
			if(booktitle==true){
				WebElement bookTitleInfo = driver.findElement(By.xpath("//div[@class='left']/div/div[@class='bold'][1]"));
				bookTitle = bookTitleInfo.getText();
			}else {
				return "Test Course Name";
			}
			LogUtil.log(bookTitle);
			return bookTitle;
			
		}
		
		@Step("get the book title in Build/List/Preview mode,  Method: {method} ")
		public String getbooktitleBuildTest() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourBuildTest")));
			WebElement booktitle = driver.findElement(By.xpath("//div[@class='h4'][1]"));
			String bookTitle = booktitle.getText();
			LogUtil.log(bookTitle);
			return bookTitle;
			
		}
		
		@Step("get the book sub title,  Method: {method} ")
		public String getbooksubtitle() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			WebElement booksubtitle = driver.findElement(By.xpath("//div[@class='left']/div/div[@class='bold'][2]"));
			String bookSubTitle = booksubtitle.getText();
			LogUtil.log(bookSubTitle);
			return bookSubTitle;
			
		}
		
		@Step("get the book Edition,  Method: {method} ")
		public String getbookEdition() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			WebElement bookedition = driver.findElement(By.xpath("//div[@class='left']/div/div[3]"));
			if(bookedition.isDisplayed()){
			String bookEdition = bookedition.getText().trim();
			LogUtil.log(bookEdition);
			return bookEdition;
			} else {
				return null;
			}
			
		}
		
		@Step("get the book Authors,  Method: {method} ")
		public String getbookAuthors() throws InterruptedException {
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("regionYourTestBank")));
			WebElement bookauthor = driver.findElement(By.xpath("//div[@class='left']/div/div[@class='italic']"));
			String bookAuthors = bookauthor.getText();
			LogUtil.log(bookAuthors);
			return bookAuthors;
		}
		
		public void appendISBN(String ISBN) throws InterruptedException{
		String newURL = null;
		String index = null;
		String url=	driver.getCurrentUrl();
		ReusableMethods.checkPageIsReady(driver);
		if(url.contains("titleshome")){
		index  = (url.substring(url.lastIndexOf("titleshome")));
		Thread.sleep(2000);		
		newURL = url.replace(index, "tests/"+ISBN);
		}else if(url.contains("errors")){
			index  = (url.substring(url.lastIndexOf("errors")));
			Thread.sleep(2000);		
			newURL = url.replace(index, "tests/"+ISBN);	
			
		} else if(driver.getCurrentUrl() != null && !url.contains("tests")){
			 String updateISBNURL = url.toString();
				newURL = updateISBNURL.concat("tests/"+ISBN);
		} 
		else {
	    String updateISBNURL = url.substring(url.lastIndexOf("tests"));
		newURL = url.replace(updateISBNURL, "tests/"+ISBN);
		}
		Thread.sleep(2000);
		driver.get(newURL);
		Thread.sleep(1000);
		}
		
		@Step("Click Contact Us button and Application open WebSite Application -Help Page is displayed,  Method: {method} ")
		public void clickContactUslink() {
			ContactUsbutton.click();
			
		}
		@Step("Click If you have a verified instructor account and are unable to log in, please visit W. W. Norton Tech Support open WebSite Application -Help Page is displayed,  Method: {method} ")
		public void clickwwNortonTechSupportlink() {
			WWNortonTechSupport.click();
			
		}
		@Step("Click Instructor Resources link and navigate to Instructor resources page,  Method: {method} ")
		public void clickInstresourceslink() throws InterruptedException {
			InstructorResource.click();			
			
		}
		
		@Step("Click on Forgot Password link,  Method: {method} ")
		public void clickForgotPasswordlink() throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.elementToBeClickable(forgotPasswordLink));
			forgotPasswordLink.click();
			
		}
		@Step("Error Messages are displayed for Non Default ISBN for User, Method: {method} ")
		public String ErrorMessage(int count) throws InterruptedException {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='large-body-text']//div/ol/li")));
			String errorMsg = null;
			List<WebElement> errormsg1 = driver.findElements(By.xpath("//div[@class='large-body-text']//div/ol/li"));
			List<WebElement> list = driver.findElements(By.xpath("//div[@class='large-body-text']//div/ol/li/span/a")); 
			
			for(int i=0; i<errormsg1.size(); i++){
				if(i==count){
				errorMsg = errormsg1.get(count).getAttribute("innerText");
					list.get(count).getAttribute("href");							
					if(list.get(count).getTagName() != null){
						list.get(count).click();
					}
				
				}
			}
			return errorMsg;
			
		}
}
