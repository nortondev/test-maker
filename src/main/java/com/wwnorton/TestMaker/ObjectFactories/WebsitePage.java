package com.wwnorton.TestMaker.ObjectFactories;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonObject;
import com.wwnorton.TestMaker.utilities.BaseDriver;
import com.wwnorton.TestMaker.utilities.FakerNames;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReadUIJsonFile;

import ru.yandex.qatools.allure.annotations.Step;

public class WebsitePage {
	WebDriver driver;
	JavascriptExecutor js;
	String UserName;
	String Password;
	WebDriverWait wait;
	
	@FindBy(how = How.ID, using = "globalInput")
	public WebElement WebsiteSearchTextbox;

	@FindBy(how = How.XPATH, using = "//main[@class='bookDetails']/div[@id='bookDetailsText']/header/div/h1")
	public WebElement WebsiteBookTitle;

	@FindBy(how = How.XPATH, using = "//main[@class='bookDetails']/div[@id='bookDetailsText']/header/div/h2")
	public WebElement WebsiteBookSubTitle;

	@FindBy(how = How.XPATH, using = "//main[@class='bookDetails']/div[@id='bookDetailsText']/header/div/div[@data-module='badge']/div[@class='badgeInfo']")
	public WebElement WebsiteBookEdition;

	@FindBy(how = How.XPATH, using = "//main[@class='bookDetails']/div[@id='bookDetailsText']/header/div/div[@data-module='badge']/div[@class='badgeVolume']")
	public WebElement WebsiteVolume;

	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'pageNotFound')]/div[starts-with(text(),'Our')]")
	public WebElement Ourapologiestext;

	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'headerDefault')]/h1")
	public WebElement getHelptext;
	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'headerDefault')]/h1[starts-with(text(),'Inst')]")
	public WebElement getInstructorResourcesstext;

	@FindBy(how = How.XPATH, using = "//span[@id='profileLogin']")
	public WebElement Norton_Log_in;

	@FindBy(how = How.ID, using = "loginEmail")
	public WebElement loginEmail;
	@FindBy(how = How.ID, using = "loginPassword")
	public WebElement loginPassword;
	@FindBy(how = How.ID, using = "LoginSubmitButton")
	public WebElement LoginSubmitButton;
	@FindBy(how = How.XPATH, using = "//div[@class='panel-body']/ul/li/div/a[contains(text(),'Profile')]")
	public WebElement ProfileLink;

	@FindBy(how = How.XPATH, using = "//div[@class='panelsection']/div[contains(text(),'Name')]/following-sibling::div/div[@class='account-left-section']")
	public WebElement FirstName_LastName;

	@FindBy(how = How.XPATH, using = "//div[@class='panelsection']/div[contains(text(),'Email')]/following-sibling::div/div[@class='account-left-section']")
	public WebElement Email;

	@FindBy(how = How.ID, using = "creatAccountEmail")
	public WebElement CreateaccountEmail;
	@FindBy(how = How.ID, using = "CreateAccountSubmitButton")
	public WebElement CreateAccountSubmitButton;

	@FindBy(how = How.ID, using = "accountFirstName")
	public WebElement accountFirstName;

	@FindBy(how = How.ID, using = "accountLastName")
	public WebElement accountLastName;
	@FindBy(how = How.ID, using = "accountReTypeEmail")
	public WebElement accountReTypeEmail;

	@FindBy(how = How.ID, using = "accountPassword")
	public WebElement accountPassword;

	@FindBy(how = How.XPATH, using = "//span[@class='headerText emailHeaderText']")
	public WebElement EmailText;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement ContinueButton;

	@FindBy(how = How.ID, using = "globalInput")
	public WebElement SearchText;
	@FindBy(how = How.XPATH, using = "//button[@class='searchButton']")
	public WebElement searchButton;

	@FindBy(how = How.XPATH, using = "//div[@class='ResultTitle']/a")
	public WebElement BookTitle;

	@FindBy(how = How.XPATH, using = "//*[@id='avaliableFormatPanelGrp-body-1']/div/div[2]/div/button")
	public WebElement ViewAllOptionsbutton;

	@FindBy(how = How.XPATH, using = "//div/a[contains(text(),'Log Out')]")
	public WebElement LogOut;
	@FindBy(how = How.LINK_TEXT, using = "Forgot your password?")
	public WebElement Forgotyourpasswordlink;

	@FindBy(how = How.LINK_TEXT, using = "Change Password")
	public WebElement ChangePasswordlink;

	@FindBy(how = How.XPATH, using = "//div[@id='currentPasswordContainer']/div/input[@id='currentPassword']")
	public WebElement currentPasswordTextbox;

	@FindBy(how = How.ID, using = "accountPassword")
	public WebElement accountPasswordTextbox;

	@FindBy(how = How.ID, using = "accountRetypePassword")
	public WebElement accountRetypePasswordTextbox;

	@FindBy(how = How.ID, using = "LoginSubmitButton")
	public WebElement loginSubmitButton;

	@FindBy(how = How.ID, using = "accountEmail")
	public WebElement accountEmail;

	@FindBy(how = How.XPATH, using = "//div[@id='btnConfirmationClose']/div[@id='buttonContainer']/div/button[contains(text(),'Close')]")
	public WebElement Closebutton;

	@FindBy(how = How.XPATH, using = "//div[@class='forgotPasswordPage']/div/span[contains(text(),'Password Reset Email Has Been Sent')]")
	public WebElement PasswordResetEmailtext;

	@FindBy(how = How.XPATH, using = "//div/button[contains(text(),'Open Instructor Resources')]")
	public WebElement OpenInstructorResourcesbutton;

	@FindBy(how = How.XPATH, using = "//button[@class='Samplebutton'][contains(.,'Demo')]")
	public WebElement DemoButton;

	@FindBy(how = How.XPATH, using = "//button[@class='ViewButton'][contains(.,'View Full Site')]")
	public WebElement ViewFullSitebutton;

	// Initializing Web Driver and PageFactory.
	public WebsitePage() throws Exception {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJason();

	@Step("Navigate to Website URL,  Method: {method} ")
	public void navigateWebsiteURL() {

		js = (JavascriptExecutor) driver;
		js.executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(
				driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		PropertiesFile.webSiteURL();

	}

	@Step("Click on Website Search Text box,  Method: {method} ")
	public void clickSearch(String ISBN) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("globalInput")));
		WebsiteSearchTextbox.click();
		WebsiteSearchTextbox.sendKeys(ISBN);
	}

	@Step("Click on Search Icon,  Method: {method} ")
	public void clickSearchIcon() {
		driver.findElement(
				By.xpath("//button[@class='searchButton']//*[name()='svg']"))
				.click();
	}

	@Step("get the Title of the Book,  Method: {method} ")
	public String getBookTitle() {
		return WebsiteBookTitle.getText();
	}

	@Step("get the Sub Title of the Book,  Method: {method} ")
	public String getBookSubTitle() {
		return WebsiteBookSubTitle.getText();
	}

	@Step("get the Edition of the Book,  Method: {method} ")
	public String getBookEdition() {

		boolean volumeexist = driver
				.findElements(
						By.xpath("//main[@class='bookDetails']/div[@id='bookDetailsText']/header/div/div[@data-module='badge']/div[@class='badgeVolume']"))
				.size() > 0;
		boolean WebBookEdition = driver
				.findElements(
						By.xpath("//main[@class='bookDetails']/div[@id='bookDetailsText']/header/div/div[@data-module='badge']/div[@class='badgeInfo']"))
				.size() > 0;
		if (volumeexist == true) {
			return WebsiteBookEdition.getText().trim() + ", "
					+ WebsiteVolume.getText().trim();

		} else if (WebBookEdition == true) {
			return WebsiteBookEdition.getText().trim();

		} else {
			return null;
		}

	}

	@Step("get the Authors of the Book,  Method: {method} ")
	public List<String> getBookAuthors() {
		String authorsName = null;
		List<String> authorsNameList = new ArrayList<String>();
		boolean seeMoreLink = driver.findElements(
				By.xpath("//button[contains(text(),'See more')]")).size() != 0;
		if (seeMoreLink == true) {
			driver.findElement(
					By.xpath("//button[contains(text(),'See more')]")).click();
			List<WebElement> authorsList = driver
					.findElements(By
							.xpath("//div[@class='contributor']/div[@id='ContributorContent']/div/span/span/a"));
			for (int i = 0; i < authorsList.size(); i++) {
				authorsName = authorsList.get(i).getText();
				authorsNameList.add(authorsName);
			}
		} else {
			List<WebElement> authorsList = driver
					.findElements(By
							.xpath("//div[@class='contributor']/div[@id='ContributorContent']/div//a"));
			for (int i = 0; i < authorsList.size(); i++) {
				authorsName = authorsList.get(i).getText();
				authorsNameList.add(authorsName);
			}
		}

		return authorsNameList;

	}

	@Step("Navigate to Website Application,  Method: {method} ")
	public void getWebsiteWindow(WebDriver driver, String parentWindow) {
		String childWindow = null;
		Set<String> Windowhandles = driver.getWindowHandles();
		Windowhandles.remove(parentWindow);
		String winHandle = Windowhandles.iterator().next();
		if (winHandle != parentWindow) {
			childWindow = winHandle;
		}
		driver.switchTo().window(childWindow);

	}

	@Step("Validatee Ww Norton Help Text is displayed on Website Application,  Method: {method} ")
	public String getWWNortonHelpText() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(getHelptext));
		String helpText = getHelptext.getText();
		return helpText;

	}

	@Step("Validatee Instructor Resourcesstext Text is displayed on Website Application,  Method: {method} ")
	public String getInstructorResourcesstextText() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(getInstructorResourcesstext));
		String helpText = getInstructorResourcesstext.getText();
		Assert.assertEquals(helpText, "Instructor Resources");
		return helpText;

	}

	@Step("Validatee Ww Norton Help Text is displayed on Website Application,  Method: {method} ")
	public void getpageNotfoundText() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(Ourapologiestext));
		String Ourapologies = Ourapologiestext.getText();
		LogUtil.log(Ourapologies);

	}
	@Step("Click on Login on WWNorton Website application,  Method: {method} ")
	public void Login_NortonApplication(String userName, String Password)
			throws InterruptedException {
		UserName = userName;
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.elementToBeClickable(Norton_Log_in));
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Norton_Log_in);
		SignIn.click();
		SignIn.perform();
		wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		loginEmail.sendKeys(UserName);
		loginPassword.sendKeys(Password);
		LoginSubmitButton.click();
		wait.until(ExpectedConditions.visibilityOf(ProfileLink));
		ProfileLink.click();

	}

	// Allure Steps Annotations and Methods
	@Step("Create a New Account on WWNorton Website application,  Method: {method} ")
	public String NewAccount_NortonApplication() throws InterruptedException {
		Thread.sleep(5000);
		String EmailID = null;
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(CreateaccountEmail));
		CreateaccountEmail.click();
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();

		EmailID = "tests" +FirstName + "_" + LastName +"@mailinator.com";
		// String AccountEmailID =
		// jsonobject.getAsJsonObject("NortonWebsiteLogin").get("NewAccountEmailID").getAsString();
		CreateaccountEmail.sendKeys(EmailID);
		CreateAccountSubmitButton.click();
		Thread.sleep(5000);
		wait = new WebDriverWait(driver, 20);
		TimeUnit.SECONDS.sleep(20);
		wait.until(ExpectedConditions.visibilityOf(accountFirstName));

		accountFirstName.sendKeys(FirstName);

		accountLastName.sendKeys(LastName);
		accountReTypeEmail.sendKeys(EmailID);
		accountPassword.sendKeys(jsonobject
				.getAsJsonObject("TMInstructorLoginCredentials").get("password")
				.getAsString());
		Thread.sleep(5000);
		driver.findElement(By.xpath("//div[@class='banner-close']/span")).click();
		Thread.sleep(2000);
		LoginSubmitButton.click();
		return EmailID;

	}

	@Step("LogOut WWNorton Website application,  Method: {method} ")
	public void Logout_NortonApplication() throws InterruptedException {
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(Norton_Log_in);
		SignIn.click();
		SignIn.perform();
		wait = new WebDriverWait(driver, 20);
		TimeUnit.SECONDS.sleep(20);
		wait.until(ExpectedConditions.elementToBeClickable(LogOut));
		Actions logout = new Actions(driver);
		logout.moveToElement(LogOut);
		logout.click();
		logout.perform();

	}

}
