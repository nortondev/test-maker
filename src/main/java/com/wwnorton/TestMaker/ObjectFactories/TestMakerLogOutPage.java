package com.wwnorton.TestMaker.ObjectFactories;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

import com.wwnorton.TestMaker.utilities.BaseDriver;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

public class TestMakerLogOutPage {
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using ="//button[starts-with(@id,'__input__button____empty____')]")
	public WebElement LogOutTestMaker;
	
	@FindBy(how = How.XPATH, using ="//*[name()='svg'][@id='__icon__gear__________0']")
	public WebElement clickIconGearButton;
	
	
	
	// Initializing Web Driver and PageFactory.
			public TestMakerLogOutPage() throws Exception {

				this.driver = BaseDriver.getDriver();
				PageFactory.initElements(driver, this);
				
			}
			@Step("LogOut TestMaker application,  Method: {method} ")
			public void logOutTestMakerApp() throws InterruptedException {
				ReusableMethods.checkPageIsReady(BaseDriver.getDriver());
				WebDriverWait wait = new WebDriverWait(driver, 20);
				TimeUnit.SECONDS.sleep(5);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@name='profile']")));
				WebElement clickProfile= driver.findElement(By.xpath("//button[@name='profile']"));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", clickProfile);
			    //clickIconGearButton.click();				
				Thread.sleep(2000);				
				//LogOutTestMaker.click();
				WebElement logOut = driver.findElement(By.xpath("//button[starts-with(@id,'__input__button____empty____')][contains(.,'Log Out')]"));
				if(logOut.isDisplayed()){
					logOut.click();
				}
				
							
			}
}
