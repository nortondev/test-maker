package com.wwnorton.TestMaker.ObjectFactories;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.yandex.qatools.allure.annotations.Step;

import com.google.gson.JsonObject;
import com.wwnorton.TestMaker.utilities.BaseDriver;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.ReadUIJsonFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

public class EditQuestions {
	WebDriver driver;
	Actions actions;
	JavascriptExecutor js;
	BuildTestPage btp;
	@FindBy(how = How.XPATH, using = "//button[starts-with(@id,'__input__button__Edit__Question__')][contains(text(),'Edit Question')]")
	public WebElement EditQuestionbutton;
	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]/p")
	public WebElement EditQuestionText;
	@FindBy(how = How.XPATH, using = "//button[starts-with(@id,'__input__button____empty____')]//div[contains(text(),'Add an Answer Choice')]")
	public WebElement AddQuestionChoice;
	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'h5 ml1-5 ')][contains(text(),'Your Question')]")
	public WebElement YourQuestiontag;
	@FindBy(how = How.XPATH, using = "//div[@id='regionYourBuildTest']//ul[@class='list-reset body-text mt1-5']/div[@class='flex items-top']/span/*[name()='svg'][@alt='Correct Answer']/following::li/div/p")
	public WebElement getCorrectAnswerText;

	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'flex bg-lightestGray')]/div[contains(text(),'Question Not Saved')]")
	public WebElement QuestionNotSavedpopup;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@id,'__link__contact__us__')][contains(text(),'contact us')]")
	public WebElement Contactuslink;
	@FindBy(how = How.XPATH, using = "//*[name()='svg'][@id='__icon__closeGrey____close__1']")
	public WebElement CloseLink;

	@FindBy(how = How.ID, using = "__input__button__Keep__Editing__0")
	public WebElement keepEditingButton;

	@FindBy(how = How.ID, using = "__input__button__Confirm__Cancel__0")
	public WebElement ConfirmCancelButton;

	@FindBy(how = How.XPATH, using = "//div[@class=' css-2w99ta'][contains(.,'Select...')]")
	public WebElement selectTextonListbox;

	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'flex bg-lightestGray p1-5')][contains(.,'Cancel Confirmation')]")
	public WebElement CancelConfirmationPopUp;

	@FindBy(how = How.ID, using = "__input__button__Edit__Prompt__0")
	public WebElement editPromptbutton;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[text()='Question Stem*']")
	public WebElement questionStemLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div/span[text()='Edit Question']")
	public WebElement editQuestionLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Question Type')]")
	public WebElement questionTypeLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Question Type')]/following-sibling::div")
	public WebElement questionType;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Chapter')]")
	public WebElement ChapterLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Chapter')]/following-sibling::div")
	public WebElement ChapterName;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Reference')]")
	public WebElement ReferenceLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Learning Objectives')]")
	public WebElement LearningObjectivesLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[text()='Question Prompt']")
	public WebElement questionPromptLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div/span[text()='Question Information']")
	public WebElement QuestionInformationLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[text()='Question Answer']")
	public WebElement questionAnswerLabel;
	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[text()='Question Documents*']")
	public WebElement questiondocumentLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[text()='Answer Choices*']")
	public WebElement answerChoicesLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[text()='Lettered Options*']")
	public WebElement letteredOptionsLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[text()='Numbered Questions*']")
	public WebElement NumberedOptionsLabel;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div/p[starts-with(@class,'bg-lightestGray mt2 p1')]")
	public WebElement editQuestionDisclaimer;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div/p[starts-with(@class,'bg-lightestGray mt2 p1')]/span")
	public WebElement editQuestiontext;

	@FindBy(how = How.XPATH, using = "//div[@id='errorSection']/div/span")
	public WebElement errorSection;

	@FindBy(how = How.XPATH, using = "//button[@id='__input__button____empty____0']/div/*[name()='svg' and @id='__icon__addChoice__________0']")
	public WebElement addDocumentButton;

	@FindBy(how = How.XPATH, using = "//button[@data-cmd='insertImage']")
	public WebElement clickImageIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='fr-form']/input[@type='file']")
	public WebElement uploadImage;

	@FindBy(how = How.XPATH, using = "//button[@data-cmd='imageAlign']")
	public WebElement imageAlign;

	@FindBy(how = How.XPATH, using = "//div[starts-with(@id,'dropdown-menu-imageAlign')]//ul[@class='fr-dropdown-list']/li/a[@data-param1='left']")
	public WebElement alignLeft;

	@FindBy(how = How.XPATH, using = "//button[starts-with(@id,'__input__button____empty____')]//span[contains(.,'Add a Numbered Question')]")
	public WebElement addNumberedOptionButton;

	@FindBy(how = How.XPATH, using = "//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]")
	public WebElement editQuestionbox;
	
	@FindBy(how = How.XPATH, using ="//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')][not(text())]/following::div[contains(@class,'indicatorContainer')]/*[name()='svg'][@aria-hidden='true']")
	public WebElement numberedListSingleValue;
	
	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJsonObject.readUIJason();

	// Initializing Web Driver and PageFactory.
	public EditQuestions() throws Exception {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	@Step("Verify  Edit Question Button,  Method: {method} ")
	public boolean verifyEditQuestion() {
		boolean isEditQDisplayed = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Edit__Question__')][contains(text(),'Edit Question')]"))
				.size() > 0;
		if (isEditQDisplayed == true) {
			Assert.assertTrue(isEditQDisplayed);
		}
		return false;

	}

	@Step("Click Edit Question Button,  Method: {method} ")
	public void clickEditQuestion() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//button[starts-with(@id,'__input__button__Edit__Question__')][contains(text(),'Edit Question')]")));
       		EditQuestionbutton.click();
      
	}

	@Step("Get the Question text from Edit Questions region, Method: (method) ")
	public String getQuestionText() throws InterruptedException {
		String questionText = null;
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]/p[@class='m0']")));
		List<WebElement> editQuestionText =driver.findElements(By.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')][@contenteditable]/p"));
		for(WebElement editQtext : editQuestionText ){
		questionText =editQtext.getText();
		if(!questionText.isEmpty()){
		break;
		}	
		}
		return questionText;
		
	}

	@Step("Update the Question Stem text Edit Questions region, Method: (method) ")
	public String updateQuestionText() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]/p[@class='m0']")));
		String questionText = EditQuestionText.getText();
		EditQuestionText.clear();
		WebElement editQuestionbox = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]"));
		editQuestionbox.click();
		editQuestionbox.sendKeys(questionText);
		return questionText;
	}

	@Step("Modify the Question Stem text Edit Questions region, Method: (method) ")
	public void modifyQuestionText(String questionText)
			throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')][@contenteditable]")));
		WebElement editQuestionbox = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')][@contenteditable]"));

		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id='regionEditQuestion']//div[text()='Question Stem*']"));
		Thread.sleep(1000);
		editQuestionbox.click();
		/*
		 * JavascriptExecutor executor = (JavascriptExecutor)driver;
		 * executor.executeScript("arguments[0].click();", editQuestionbox);
		 */
		editQuestionbox.clear();
		Thread.sleep(2000);
		// actions.sendKeys(questionText).build().perform();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("document.getElementsByClassName('fr-element fr-view')[0].innerText='"
				+ questionText + "'");
		executor.executeScript("arguments[0].click();", editQuestionbox);
		editQuestionbox.sendKeys(Keys.UP);
	}

	@Step("Insert image in Question Stem text Edit Questions region, Method: (method) ")
	public void insertImage(String path) throws InterruptedException {
		Actions builder = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]")));
		WebElement editQuestionbox = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]"));

		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id='regionEditQuestion']//div[text()='Question Stem*']"));
		Thread.sleep(1000);
		editQuestionbox.click();
		ReusableMethods.scrollIntoView(driver, questionStemLabel);
		boolean istoolbardisplayed = driver
				.findElements(
						By.xpath("//div[@class='fr-toolbar fr-desktop fr-top fr-basic fr-sticky-off fade-in']"))
				.size() > 0;
		if (istoolbardisplayed == true) {
			Thread.sleep(2000);
			builder.moveToElement(clickImageIcon).click().perform();
			// clickImageIcon.click();
			uploadImage.sendKeys(path);
		}
		boolean isalignbardisplayed = driver
				.findElements(
						By.xpath("//div[@class='fr-popup fr-desktop fr-above fr-active']"))
				.size() > 0;
		if (isalignbardisplayed == true) {
			Thread.sleep(2000);
			WebElement alignBar = driver
					.findElement(By
							.xpath("//div[@class='fr-popup fr-desktop fr-above fr-active']"));
			ReusableMethods.scrollIntoView(driver, alignBar);
			builder.moveToElement(imageAlign).click().perform();
			builder.moveToElement(alignLeft).click().perform();

		}

	}

	@Step("Remove the Question text from Edit Questions region, Method: (method) ")
	public void removeQuestionText() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]/p[@class='m0']")));
		List<WebElement> editQuestionbox = driver
				.findElements(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')][@contenteditable]/p"));
		
		//EditQuestionText.click();
		for (int i = 0; i < editQuestionbox.size(); i++) {
			editQuestionbox.get(i).clear();
		}
		boolean isdisplayed = driver.findElements(By.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]/dl/dd/table")).size() >0;
		if(isdisplayed==true){
			driver.findElement(By.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]/dl/dd/table")).clear();
		}
		ReusableMethods.scrollIntoView(driver, questionStemLabel);
		js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-50)", "");
		WebElement editQuestionboxclick = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')][@contenteditable]"));
		editQuestionboxclick.click();

	}

	@Step("Click on the Edit Question Stem box, Method: (method) ")
	public void clickeditQuestionbox() {
		WebElement editQuestionboxclick = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]"));
		ReusableMethods.scrollIntoViewClick(driver, editQuestionboxclick);
	}

	@Step("Remove the Question Prompt from Edit Questions region, Method: (method) ")
	public void removeQuestionPrompt() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@class='fr-element fr-view']/p")));
		clickTabKey();
		List<WebElement> editQuestionPrompt = driver.findElements(By
				.xpath("//div[@class='fr-element fr-view']/p"));
		for (WebElement editPrompt : editQuestionPrompt) {
			// editQuestionPrompt.click();
			editPrompt.clear();
		}
		Thread.sleep(2000);
		WebElement editQuestionboxclick = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]"));
		editQuestionboxclick.click();

	}

	@Step("Update the Question Prompt from Edit Questions region, Method: (method) ")
	public void updateQuestionPrompt(String questionpromptText)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]")));
		WebElement editQuestionbox = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pt1']//div[starts-with(@class,'fr-element fr-view')]"));

		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id='regionEditQuestion']//div[text()='Question Prompt*']"));
		Thread.sleep(1000);
		editQuestionbox.click();
		/*
		 * JavascriptExecutor executor = (JavascriptExecutor)driver;
		 * executor.executeScript("arguments[0].click();", editQuestionbox);
		 */
		editQuestionbox.clear();
		Thread.sleep(2000);
		// actions.sendKeys(questionText).build().perform();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("document.getElementsByClassName('fr-element fr-view')[0].innerText='"
				+ questionpromptText + "'");
		editQuestionbox.click();
	}

	@Step("Click Add an Answer Choice button from Edit Questions region, Method: (method) ")
	public void clickAddAnswerChoice() {
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@class='pt3 pb3']/div/span[contains(text(),'Question Information')]|//div[@id='regionEditQuestion']//div[text()='Lettered Options*']"));
		boolean isAnsChoiceButtonDisplayed = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button____empty____')]//div[contains(text(),'Add an Answer Choice')]"))
				.size() > 0;
		if (isAnsChoiceButtonDisplayed == true) {
			ReusableMethods.scrollIntoViewClick(driver, AddQuestionChoice);
		} else {
			ReusableMethods.scrollIntoViewClick(driver, AddQuestionChoice);
		}

	}

	@Step("Click Last Remove button from Edit Questions region, Method: (method) ")
	public void clickLastremovebutton() {
		// ReusableMethods.scrollIntoView(driver, AddQuestionChoice);
		List<WebElement> removebuttonList = driver
				.findElements(By
						.xpath("//*[name()='svg'][starts-with(@id,'__icon__removeChoice__________0')]"));
		int count = removebuttonList.size();
		WebElement removeElement = removebuttonList.get(count - 1);
		/*WebElement lastElement = driver.findElement(By.xpath("//button[@id='__input__button____empty____0']//div[contains(.,'Add an Answer Choice')]"));
		Actions movetoremoveQ  = new Actions(driver);
		movetoremoveQ.moveToElement(lastElement).build().perform();
		*/

		boolean iselementDisplayed = removeElement.isDisplayed();
		if(iselementDisplayed==true){
			removeElement.click();
		}

	}

	@Step("Verify your Question tag in BuildTest View for Edited Questions , Method: (method) ")
	public boolean getYourQuestiontag() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//button[starts-with(@id,'__input__button__Edit__Question__')][contains(text(),'Edit Question')]")));
		boolean isyourQuestiontag = YourQuestiontag.isDisplayed();

		Assert.assertTrue(isyourQuestiontag);
		return isyourQuestiontag;

	}

	@Step("Verify your Question tag in Search Questiona section , Method: (method) ")
	public String getYourQuestionlabel() {
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_UP).build().perform();
		String isyourQuestiontaglogo = YourQuestiontag.getText();
		return isyourQuestiontaglogo;

	}

	@Step(" Get the ID Number from Question Meta data section,  Method: {method} ")
	public String getIDNumberMetadata(String IDNumber) throws Exception {
		String idNumbermeatadata = null;
		String qlistID = null;
		driver.findElement(By.id("regionAside")).click();
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		btp = new BuildTestPage();

		String getQuestionsCount = btp.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int totalQuests = Integer.parseInt(str[0]);
		List<String> qlist = new ArrayList<String>();
		qlist = btp.getQuestionidAddQuestionSection();
		int counter = 10;
		for (int ele = 0; ele < totalQuests; ele++) {
			if (ele >= counter) {

				actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				actions.sendKeys(Keys.PAGE_UP).build().perform();
				Thread.sleep(5000);
				qlist = btp.getQuestionidAddQuestionSection();
				counter = qlist.size();
				Thread.sleep(2000);
			}

			
			try {
				qlistID = qlist.get(ele);
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//
			boolean isShowLinkDisplayed = driver
					.findElements(
							By.xpath("//button[starts-with(@id,'__input__button__Show__')]"))
					.size() > 0;
			if (isShowLinkDisplayed == true) {
				WebElement clickShowlink = driver
						.findElement(By
								.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
				clickShowlink.click();
			}
			Thread.sleep(2000);
			WebElement idNumber = driver
					.findElement(By
							.xpath("//div[@id='"
									+ qlistID
									+ "']//div[contains(text(),'ID Number')]/following-sibling::div"));
			WebElement qID = driver.findElement(By.xpath("//div[@id='"
					+ qlistID + "']"));
			ReusableMethods.scrollIntoView(driver, qID);
			Thread.sleep(2000);
			// btp.getQuestionMetadataText();
			idNumbermeatadata = idNumber.getText();
			Assert.assertNotNull(idNumbermeatadata);
			LogUtil.log(idNumbermeatadata);
			if (idNumbermeatadata.equalsIgnoreCase(IDNumber)) {
				clickAddQuestionbyQID(qlistID);
				Thread.sleep(5000);
				break;
			}

		}
		return idNumbermeatadata;
	}

	@Step(" Get the ID Number from Question Group Meta data section,  Method: {method} ")
	public String getIDNumberMetadataQuestionGroup(String IDNumber)
			throws Exception {
		String idNumbermeatadata = null;
		driver.findElement(By.id("regionAside")).click();
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		btp = new BuildTestPage();

		String getQuestionsCount = btp.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int totalQuests = Integer.parseInt(str[0]);
		List<String> qlist = new ArrayList<String>();
		boolean isShowLinkDisplayed = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Show__')]"))
				.size() > 0;
		if (isShowLinkDisplayed == true) {
			WebElement clickShowlink = driver
					.findElement(By
							.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
			clickShowlink.click();
		}
		qlist = btp.getQuestionGroupIidAddQuestionSection();
		//System.out.println(qlist);
		int counter = 10;
		for (int ele = 0; ele < totalQuests; ele++) {
			if (ele >= counter) {

				// actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				actions.sendKeys(Keys.PAGE_UP).build().perform();
				Thread.sleep(5000);
				qlist = btp.getQuestionGroupidAddQuestionSection();
				counter = qlist.size();
				Thread.sleep(2000);
			}

			String qlistID = qlist.get(ele);
			//

			Thread.sleep(2000);
			WebElement idNumber = driver
					.findElement(By
							.xpath("//div[@id='"
									+ qlistID
									+ "']//div[contains(text(),'ID Number')]/following-sibling::div"));
			WebElement qID = driver.findElement(By.xpath("//div[@id='"
					+ qlistID + "']"));
			ReusableMethods.scrollIntoView(driver, qID);
			Thread.sleep(2000);
			// btp.getQuestionMetadataText();
			idNumbermeatadata = idNumber.getText();
			Assert.assertNotNull(idNumbermeatadata);
			LogUtil.log(idNumbermeatadata);
			System.out.println(idNumbermeatadata);
			if (idNumbermeatadata.contains(IDNumber)) {
				clickAddQuestionbyQID(qlistID);
				Thread.sleep(5000);
				break;
			}
			

		}
		return idNumbermeatadata;
	}

	@Step(" Click the Add button for Norton Questions,  Method: {method} ")
	public void clickAddNortonQuestion() throws Exception {
		driver.findElement(By.id("regionAside")).click();
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		btp = new BuildTestPage();

		String getQuestionsCount = btp.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int totalQuests = Integer.parseInt(str[0]);
		List<String> qlist = new ArrayList<String>();
		// get list of question for 10 Question ID's
		qlist = btp.getQuestionidAddQuestionSection();
		int counter = 10;

		for (int ele = 0; ele < totalQuests; ele++) {
			boolean isQgroup = driver
					.findElements(
							By.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(text(),'Question Group')]"))
					.size() > 0;
			// boolean isIDNumber =
			// driver.findElements(By.xpath("//div[starts-with(@class,'sm-col-2 left-align darkerGray small-body-text font-weight-')][contains(text(),'Question Type')]")).size()
			// > 0;
			while (isQgroup == true) {
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
				actions.keyDown(Keys.CONTROL).release().perform();
				isQgroup = !isQgroup;
				if (!btp.getQuestionidAddQuestionSection().isEmpty()) {
					actions.sendKeys(Keys.PAGE_UP).build().perform();
					actions.sendKeys(Keys.PAGE_DOWN).build().perform();
					actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
					actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
					actions.keyDown(Keys.CONTROL).release().perform();
				}

			}

			if (ele >= counter) {
				driver.findElement(By.id("regionAside")).click();
				actions.sendKeys(Keys.PAGE_UP).build().perform();
				actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				Thread.sleep(2000);
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
				actions.keyDown(Keys.CONTROL).release().perform();
				Thread.sleep(1000);
				qlist = btp.getQuestionidAddQuestionSection();
				counter = qlist.size();
				Thread.sleep(2000);
				actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				actions.sendKeys(Keys.PAGE_UP).build().perform();
				Thread.sleep(2000);
			}
			/*
			 * boolean isQgroup = driver.findElements(By.xpath(
			 * "//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(text(),'Question Group')]"
			 * )).size() > 0; if(isQgroup==true){ continue; }
			 */
			String qlistID = qlist.get(ele);
			WebElement qID = driver.findElement(By.xpath("//div[@id='"
					+ qlistID + "']"));

			ReusableMethods.scrollIntoView(driver, qID);

			Thread.sleep(2000);
			WebElement idNumber = driver
					.findElement(By
							.xpath("//div[@id='"
									+ qlistID
									+ "']//div[contains(text(),'ID Number')]/following-sibling::div"));
			// ReusableMethods.scrollIntoView(driver, idNumber);
			// btp.getQuestionMetadataText();
			String idNumbermeatadata = idNumber.getText();
			System.out.println("Counter " + ele + " Id Number"
					+ idNumbermeatadata);
			if (!idNumbermeatadata.contains("E")) {
				clickAddQuestionbyQID(qlistID);
				break;
			}

		}

	}

	@Step("Verify Save Button is disabled Mode on Edit Questions page,  Method: {method} ")
	public boolean saveButtonState() throws InterruptedException {
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("__input__button__Save__0")));
		WebElement SavebuttonState = driver
				.findElement(By
						.xpath("//footer[@id='regionFooter']/button[@id='__input__button__Save__0']"));
		String disabledState = SavebuttonState.getAttribute("class");
		if (disabledState
				.equalsIgnoreCase("btn black rounded bg-lighterGray outline-none undefined mt2 mb2 mr1-5 ml1")) {
			LogUtil.log("Save button is disabled");
		}
		return true;

	}

	@Step("Toggle the radio button for True False Question on  Edit Questions page,  Method: {method} ")
	public void toggleRadioButton() {
		List<WebElement> radioButtonList = driver.findElements(By
				.xpath("//input[@class='radio'][@name='truefalsegroup']"));
		for (WebElement radioButton : radioButtonList) {
			if (radioButton.isSelected() == false) {
				radioButton.click();
				break;
			}
		}

	}

	@Step("Get the Answer Choice for True/False Question,  Method: {method} ")
	public String getCorrectAnswerText() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(getCorrectAnswerText));
		String getCorrectAnswer = getCorrectAnswerText.getText();
		return getCorrectAnswer;

	}

	@Step("Click the Add Question button based on Question ID,  Method: {method} ")
	public void clickAddQuestionbyQID(String qID) {
		WebElement qIDButton = driver
				.findElement(By
						.xpath("//div[@id='"
								+ qID
								+ "']/div/button[starts-with(@id,'__input__button__addbutton__')]"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", qIDButton);
		//qIDButton.click();

	}

	@Step("Verify Question Not Saved Pop up is displayed after removing the Question Step and Clicking Save button,  Method: {method} ")
	public boolean questionNotSavedpopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(QuestionNotSavedpopup));
		QuestionNotSavedpopup.isDisplayed();
		return true;

	}

	@Step("Verify Error Message on Pop Up,  Method: {method} ")
	public String questionNotSavedpopUpErrorMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(QuestionNotSavedpopup));
		WebElement errorTextElement = driver
				.findElement(By
						.xpath("//div[starts-with(@class,'center px3-0 pb3-5 line-height-4 large-body-text')]"));
		String errorText = errorTextElement.getText();
		return errorText;

	}

	@Step("Click Close link from Question not saved pop up window,  Method: {method} ")
	public void clickCloselink() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(CloseLink));
		CloseLink.click();

	}

	@Step("Verify \"This Field Required\" text is displayed after removing required field,  Method: {method} ")
	public String getErrormsgQuestionStembox(String EditFieldLabel) {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement getError = null;
		String getErrorText;
		if (EditFieldLabel.contains("Question Stem")
				|| EditFieldLabel.contains("Question Prompt*")) {
			getError = driver
					.findElement(By
							.xpath("//div[starts-with(text(),'"
									+ EditFieldLabel
									+ "')]/following-sibling::div[contains(text(),'This is a required field.')]"));
		} else {
			getError = driver
					.findElement(By
							.xpath("//div[starts-with(.,'"
									+ EditFieldLabel
									+ "')]/following::div[contains(text(),'This is a required field.')]"));
		}
		ReusableMethods.scrollIntoView(driver, getError);
		getErrorText = getError.getText();
		return getErrorText;

	}

	@Step("Verify \"This Field Required\" text is Not displayed when value is entered,  Method: {method} ")
	public boolean isErrorDisplayed(String EditFieldLabel) {
		boolean isElementPresent;

		if (EditFieldLabel.contains("Question Stem")) {
			isElementPresent = driver
					.findElements(
							By.xpath("//div[starts-with(text(),'"
									+ EditFieldLabel
									+ "')]/following-sibling::div[contains(text(),'This is a required field.')]"))
					.size() != 0;

		} else {
			isElementPresent = driver
					.findElements(
							By.xpath("//div[starts-with(@class,'bold darkGray pt1')][starts-with(text(),'"
									+ EditFieldLabel
									+ "')]/following-sibling::div[@class='pt1']/div[contains(text(),'This is a required field.')]"))
					.size() != 0;
		}
		if (isElementPresent == false) {
			return true;
		}

		return true;

	}

	@Step("Click the contactUs link from Question Not Saved Pop up,  Method: {method} ")
	public void clickContactUslink() {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", Contactuslink);

	}

	@Step("Click on Add Question button next to Instructor recently edited question,  Method: {method} ")
	public void clickAddButtonEditedQuestion(String idNumber) throws Exception {
		WebElement editQuestionID = driver
				.findElement(By
						.xpath("//div[starts-with(@class,'xs-col-4 sm-col-2')]//div[starts-with(@class,'sm-col-4 pb1 small-body-text')]/p[contains(text(),'"
								+ idNumber
								+ "')]/ancestor-or-self::div/button[starts-with(@id,'__input__button__addbutton__')]"));
		editQuestionID.click();
	}

	@Step("Navigate to List box,  Method: {method} ")
	public void clickEditListbox(String metaDataName) {
		WebElement editListbox = driver
				.findElement(By
						.xpath("//div[@class='pt2']/div[contains(text(),'"
								+ metaDataName
								+ "')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')] | //div[@class='pt2']/div[contains(text(),'"
								+ metaDataName
								+ "')]/following-sibling::div//div[contains(@class,'css')]//*[name()='svg'][starts-with(@class,'css-')]"));
		ReusableMethods.scrollIntoView(driver, editListbox);
		editListbox.click();

	}
	@Step("Navigate to List box and clear the value,  Method: {method} ")
	public void deleteListValue(String metaDataName) throws InterruptedException {
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@class='pt2']/div[contains(text(),'Bloom')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]"));
		boolean isListitemsDisplayed = driver
				.findElements(
						By.xpath("//div[@class='pt2']/div[contains(text(),'"
								+ metaDataName
								+ "')]/following-sibling::div//div[contains(@class,'multi')]//*[name()='svg'][starts-with(@class,'css-')]"))
				.size() > 0;
		if (isListitemsDisplayed == true) {
			List<WebElement> listSelectexist = driver
					.findElements(By
							.xpath("//div[@class='pt2']/div[contains(text(),'"
									+ metaDataName
									+ "')]/following-sibling::div//div[contains(@class,'multiValue')]//*[name()='svg'][starts-with(@class,'css-')]//*[name()='path']"));
			int count = listSelectexist.size();
			for (int i = 0; i < count; i++) {
				if (listSelectexist.size() >0) {
					try {
						listSelectexist.get(i).click();
						Thread.sleep(2000);
						} catch (StaleElementReferenceException e) {
						System.out.println(i);
					}
				} else {
					listSelectexist.get(0).click();
					break;
				}
			}
			/*boolean isindicatorContainer = driver.findElements(By.xpath("//div[@class='pt2']/div[contains(text(),'"
									+ metaDataName
									+ "')]/following::div[contains(@class,'indicatorContainer')][@aria-hidden='true']//*[name()='svg']")).size()>0;
			if(isindicatorContainer==true){
				driver.findElement(By.xpath("//div[@class='pt2']/div[contains(text(),'"
						+ metaDataName
						+ "')]/following::div[contains(@class,'indicatorContainer')][@aria-hidden='true']//*[name()='svg']")).click();	
			}*/
		} else {
			boolean isdisplayed = driver
					.findElements(
							By.xpath("//div[@class='pt2']/div[contains(text(),'"
									+ metaDataName
									+ "')]/following-sibling::div//div[contains(@class,'placeholder')]"))
					.size() > 0;
			if (isdisplayed == true) {
				LogUtil.log("The metadata  : " + metaDataName
						+ " does not contain any value");
			}

		}

	}

	@Step("Validate the Values are displayed in {0} list box, Method:{method}")
	public boolean listValueDisplayed(String metaDataName) {
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@class='pt2']/div[contains(text(),'Bloom')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]"));
		boolean isListitemsDisplayed = driver
				.findElements(
						By.xpath("//div[@class='pt2']/div[contains(text(),'"
								+ metaDataName
								+ "')]/following-sibling::div//div[contains(@class,'multi')]//*[name()='svg'][starts-with(@class,'css-')]"))
				.size() > 0;
		if (isListitemsDisplayed == true) {
			LogUtil.log("Values are displayed in the " + metaDataName
					+ " List box");
		}
		return isListitemsDisplayed;
	}

	@Step("get the values displayed in {0} list box,  Method: {method} ")
	public List<String> getListValue(String metaDataName) {

		WebElement editListbox = driver
				.findElement(By
						.xpath("//div[@class='pt2']/div[contains(.,'"
								+ metaDataName
								+ "')]/following-sibling::div[starts-with(@class,'pt2')]"));
		ReusableMethods.scrollIntoView(driver, editListbox);
		List<WebElement> getvalues = driver
				.findElements(By
						.xpath("//div[@class='pt2']/div[contains(.,'"
								+ metaDataName
								+ "')]/following-sibling::div[starts-with(@class,'pt2')]//div[contains(@class,'multiValue')]/div[@class='css-1hm8c1m']"));
		List<String> textValues = new ArrayList<String>();
		Iterator<WebElement> itr = getvalues.iterator();
		while (itr.hasNext()) {
			WebElement textEle = itr.next();
			textValues.add(textEle.getText());
		}
		return textValues;

	}

	@Step("Select the value from the list box,  Method: {method} ")
	public void changeListValue(String metaDataName)
			throws InterruptedException {
		List<WebElement> listclickEle = driver
				.findElements(By
						.xpath("//div[@class='pt2']/div[contains(.,'"
								+ metaDataName
								+ "')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]"));
		ReusableMethods.scrollToElement(driver, By
						.xpath("//div[@class='pt2']/div[contains(.,'"
								+ metaDataName
								+ "')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]"));
		Thread.sleep(2000);
		if(listclickEle.size()>0){
		listclickEle.get(0).click();
		} else {
			driver
			.findElement(By
					.xpath("//div[@class='pt2']/div[contains(.,'"
							+ metaDataName
							+ "')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]")).click();
		}
		List<WebElement> listitems = driver.findElements(By
				.xpath("//div[starts-with(@id,'react-select-')]"));

		// List<WebElement> listitems =
		// driver.findElements(By.xpath("//div[starts-with(@class,' css-crwyl1-menu')]/div/div"));
		for (int i = 0; i < listitems.size();) {
			WebElement getlistValue = listitems.get(0);
			getlistValue.click();
			Thread.sleep(2000);
			break;
		}

	}

	@Step("Select the value from the list box,  Method: {method} ")
	public void selectMultipleListValue(String metaDataName)
			throws InterruptedException {
		List<WebElement> listclickEle = driver
				.findElements(By
						.xpath("//div[@class='pt2']/div[contains(text(),'"
								+ metaDataName
								+ "')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]"));
		boolean islistbox =driver
				.findElements(By
						.xpath("//div[@class='pt2']/div[contains(text(),'"
								+ metaDataName
								+ "')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]")).size() >0;
		if(islistbox==false){
			driver
			.findElement(By
					.xpath("//div[@class='pt2']/div[contains(text(),'"
							+ metaDataName
							+ "')]/following-sibling::div//*[name()='svg'][starts-with(@class,'css-')]")).click();
		}
		
		listclickEle.get(0).click();
		List<WebElement> listitems = driver.findElements(By
				.xpath("//div[starts-with(@id,'react-select-')]"));

		// List<WebElement> listitems =
		// driver.findElements(By.xpath("//div[starts-with(@class,' css-crwyl1-menu')]/div/div"));
		for (int i = 0; i < listitems.size(); i++) {
			WebElement selectOptions = driver.findElement(By
					.xpath("//div[starts-with(@id,'react-select-')]"));
			selectOptions.click();
			Thread.sleep(3000);
			//clickEditListbox(metaDataName);
			listclickEle.get(0).click();
		}

	}

	@Step("Select multiple values from the Difficulty list box,  Method: {method} ")
	public void selectMultipleListValues(String metaDataName)
			throws InterruptedException {
		Actions act = new Actions(driver);
		driver.findElement(
				By.xpath("//div[@class='pt2']/div[contains(text(),'"
						+ metaDataName + "')]/following-sibling::div")).click();
		List<WebElement> listitems = driver
				.findElements(By
						.xpath("//div[starts-with(@class,' css-crwyl1-menu')]/div/div[starts-with(@id,'react-')]"));
		for (int i = 0; i < listitems.size(); i++) {

			String value = listitems
					.get(i)
					.findElement(
							By.xpath("//div[starts-with(@class,' css-crwyl1-menu')]/div/div[starts-with(@id,'react-')]"))
					.getText();
			// /stitems.get(i).getText();
			System.out.println(value);
			WebElement getlistValue = listitems.get(i);
			act.click(getlistValue).build().perform();
			// getlistValue.click();
			Thread.sleep(3000);
			driver.findElement(
					By.xpath("//div[@class='pt2']/div[contains(text(),'"
							+ metaDataName
							+ "')]/following-sibling::div//div[@class='css-1g6gooi']/div/input[starts-with(@id,'react-select-')]"))
					.click();
		}
	}

	@Step("Click the Confirm Cancel Button,  Method: {method} ")
	public void clickConfirmCancel() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ConfirmCancelButton));
		ConfirmCancelButton.click();

	}

	@Step("verify Cancel Confirmation pop up,  Method: {method} ")
	public boolean cancelConfirmationPopUp() {
		boolean cancelConfpopup = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'flex bg-lightestGray p1-5')][contains(.,'Cancel Confirmation')]"))
				.size() > 0;
		if (cancelConfpopup == true) {
			Assert.assertTrue(cancelConfpopup);
			LogUtil.log("Cancel Confirmation pop up is displayed ");
			return true;
		} else {
			Assert.assertFalse(cancelConfpopup);
			LogUtil.log("Cancel Confirmation pop up is not displayed ");
			return false;
		}
	}

	@Step("verify Edit prompt Button and Click the Edit prompt,  Method: {method} ")
	public void clickEditPromptButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(editPromptbutton));
		editPromptbutton.click();
	}

	@Step("verify Editor Sections on Question Editor Page,  Method: {method} ")
	public boolean editorLabelSections(String sectionName) {
		WebElement labelName = null;
		String strsectionName;
		if (sectionName.contains("Bloom")) {
			ReusableMethods
					.scrollToElement(
							driver,
							By.xpath("//div[@id='regionEditQuestion']//div[@class='pt2']/div[starts-with(.,'Bloom')]"));
			labelName = driver
					.findElement(By
							.xpath("//div[@id='regionEditQuestion']//div[@class='pt2']/div[starts-with(.,'Bloom')]"));
			strsectionName = labelName.getText();
			Assert.assertEquals(sectionName, strsectionName);
		} else if (sectionName.equalsIgnoreCase("Edit Question")
				|| sectionName.equalsIgnoreCase("Question Information")) {
			ReusableMethods.scrollToElement(driver, By
					.xpath("//div[@id='regionEditQuestion']//div/span[text()='"
							+ sectionName + "']"));
			labelName = driver.findElement(By
					.xpath("//div[@id='regionEditQuestion']//div/span[text()='"
							+ sectionName + "']"));
			strsectionName = labelName.getText();
			Assert.assertEquals(sectionName, strsectionName);
		} else {
			boolean isSectionName = driver.findElements(
					By.xpath("//div[@id='regionEditQuestion']//div[text()='"
							+ sectionName + "']")).size() > 0;
			boolean islabelName = driver.findElements(By
					.xpath("//div[@id='regionEditQuestion']//div[text()='"
							+ sectionName + "']")).size() >0;
			if (isSectionName == true && islabelName ==true) {
				ReusableMethods.scrollToElement(driver, By
						.xpath("//div[@id='regionEditQuestion']//div[text()='"
								+ sectionName + "']"));
				labelName = driver.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[text()='"
								+ sectionName + "']"));
				strsectionName = labelName.getText();
				Assert.assertEquals(sectionName, strsectionName);
			}
		}
		if(labelName==null){
			Assert.assertNull(labelName);
		}else {
		boolean sectiondisplayed = labelName.isDisplayed();
		if (sectiondisplayed == true) {
			return true;
		}
		
		}

		return false ;

	}

	@Step("verify Edit question disclaimer on Question Editor Page,  Method: {method} ")
	public String editorQuestionDisclaimer() {
		String editqDisclaimer = editQuestionDisclaimer
				.getAttribute("innerText");
		return editqDisclaimer;

	}

	@Step("Focus should be on Question Prompt by pressing Tab key, Method: {method}")
	public void clickTabKey() {
		driver.findElement(
				By.xpath("//p[starts-with(@class,'bg-lightestGray mt2 p1')]"))
				.click();
		Actions actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).perform();

	}

	@Step("System displays editor menu options, Method: {method}")
	public boolean editorMenuOptions() {
		boolean editPromptTabKey = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'fr-toolbar fr-desktop fr-top fr-basic fr-sticky-on fade-out')]"))
				.size() > 0;
		if (editPromptTabKey == true) {
			Assert.assertTrue(editPromptTabKey, "Editor Menu is displayed");
		}
		return editPromptTabKey;
	}

	@Step("Remove Answer Choices Text and Click the Save Button, Method: {method}")
	public void deleteAnswerchoices() throws InterruptedException {
		ReusableMethods.scrollIntoView(driver, answerChoicesLabel);
		List<WebElement> getAnsChoiceText = driver
				.findElements(By
						.xpath("//div[text()='Answer Choices*']/following::div[@class='fr-element fr-view']/p"));
		for (int i = 0; i < getAnsChoiceText.size(); i++) {
			//
			getAnsChoiceText.get(i).click();
			Thread.sleep(1000);
			getAnsChoiceText.get(i).clear();
			Thread.sleep(1000);

		}
		for (int c = 0; c < getAnsChoiceText.size(); c++) {
			driver.findElements(
					By.xpath("//div[text()='Answer Choices*']/following::div[@class='fr-element fr-view']"))
					.get(c).click();
		}

	}

	@Step("Update Answer Choices Text and Click the Save Button, Method: {method}")
	public void updateAnswerchoices(int i, String answerchoiceText)
			throws InterruptedException {
		ReusableMethods.scrollIntoView(driver, answerChoicesLabel);
		List<WebElement> getAnsChoiceText = driver
				.findElements(By
						.xpath("//div[text()='Answer Choices*']/following::div[@class='fr-element fr-view']"));
		getAnsChoiceText.get(i).click();
		getAnsChoiceText.get(i).sendKeys(answerchoiceText);

		/*
		 * for (int c = 0; c < getAnsChoiceText.size(); c++) {
		 * driver.findElements( By.xpath(
		 * "//div[text()='Answer Choices*']/following::div[@class='fr-element fr-view']"
		 * )) .get(c).click(); }
		 */

	}

	@Step("Remove Answer Choices Section except the selected answer choice and Click the Save Button, Method: {method}")
	public void deleteAnswerchoicesSection(boolean value)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		ReusableMethods.scrollIntoView(driver, answerChoicesLabel);
		List<WebElement> radioButtons = driver.findElements(By
				.xpath("//input[starts-with(@id,'__input__radio____')]"));
		int sizeofRbuttons = radioButtons.size();

		for (int i = sizeofRbuttons - 1; i >= 0; i--) {
			wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//input[starts-with(@id,'__input__radio____')]")));
			boolean isCorrectAnsCheck = false;
			try {
				isCorrectAnsCheck = radioButtons.get(i).isSelected();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}

			if (isCorrectAnsCheck == value) {
				Thread.sleep(5000);
				driver.findElements(
						By.xpath("//*[name()='svg'][@id='__icon__removeChoice__________0']"))
						.get(i).click();
				Thread.sleep(2000);
			}
		}
	}

	@Step("Remove all Answer Choices Section and Click the Save Button, Method: {method}")
	public void deleteAllAnswerchoicesSection() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		ReusableMethods.scrollIntoView(driver, answerChoicesLabel);
		List<WebElement> radioButtons = driver.findElements(By
				.xpath("//input[starts-with(@id,'__input__radio____')]"));
		int sizeofRbuttons = radioButtons.size();

		for (int i = sizeofRbuttons - 1; i >= 0; i--) {
			wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//input[starts-with(@id,'__input__radio____')]")));
			driver.findElements(
					By.xpath("//*[name()='svg'][@id='__icon__removeChoice__________0']"))
					.get(i).click();
			Thread.sleep(2000);
		}

	}

	@Step("Select the Aswer Choice radio Option, Method: {method}")
	public void selectAnswerChoiceRadioButton() {
		ReusableMethods.scrollIntoView(driver, answerChoicesLabel);
		List<WebElement> radioButtons = driver.findElements(By
				.xpath("//input[starts-with(@id,'__input__radio____')]"));
		int sizeofRbuttons = radioButtons.size();
		for (int i = 0; i < sizeofRbuttons;) {
			@SuppressWarnings("unused")
			boolean bval = radioButtons.get(i).isSelected();
			if (bval = true) {
				radioButtons.get(1).click();
				break;
			} else {
				radioButtons.get(i).click();
				break;
			}
		}

	}

	@Step("Get the Error Text after removing the  Answer Choice text, Method: {method}")
	public String getErrorText() {
		boolean isdisplayed = driver.findElements(
				By.xpath("//div[@id='errorSection']/div/span")).size() > 0;
		if (isdisplayed == true) {
			String errorText = errorSection.getText();
			return errorText;
		} else {
			return null;
		}

	}

	@Step("Get the EditQuestion Text, Method: {method}")
	public String getEditQuestionText() {
		boolean isEditQdisplayed = driver.findElements(
				By.xpath("//p[@class='bg-lightestGray mt2 p1']")).size() > 0;
		if (isEditQdisplayed == true) {
			WebElement editQTextList = driver
					.findElement(By
							.xpath("//div[@id='regionEditQuestion']//div/p[starts-with(@class,'bg-lightestGray mt2 p1')]"));
			String getText = editQTextList.getAttribute("innerText");
			return getText.toString();
		} else {
			return null;
		}

	}

	@Step("Record the existing Answer Choices Text, Method: {method}")
	public List<String> getAnswerchoicesText() throws InterruptedException {
		ReusableMethods.scrollIntoView(driver, answerChoicesLabel);
		ArrayList<String> getTextlist = new ArrayList<String>();
		List<WebElement> getAnsChoiceText = driver
				.findElements(By
						.xpath("//div[text()='Answer Choices*']/following::div[@class='fr-element fr-view']/p"));
		for (int i = 0; i < getAnsChoiceText.size(); i++) {
			String getText = getAnsChoiceText.get(i).getText();
			getTextlist.add(getText);
		}
		return getTextlist;

	}

	// remove all dcoument
	@Step("Remove all DBQ documents from edit Question page, Method: {method}")
	public void removeDBQDocuments(int removeNo) throws InterruptedException {
		ReusableMethods.scrollIntoView(driver, questiondocumentLabel);
		List<WebElement> getdocuments = driver
				.findElements(By
						.xpath("//div[text()='Question Documents*']/following::div//*[name()='svg'][@id='__icon__removeChoice__________0']"));
		if (removeNo == 0) {
			for (int i = 0; i < getdocuments.size(); i++) {
				List<WebElement> removedocument = driver
						.findElements(By
								.xpath("//div[text()='Question Documents*']/following::button/*[name()='svg'][starts-with(@id,'__icon__removeChoice__________')]"));
				removedocument.get(0).click();
				Thread.sleep(2000);
				ReusableMethods.scrollIntoView(driver, questiondocumentLabel);
			}
		} else {
			for (int j = 0; j < getdocuments.size(); j++) {
				List<WebElement> removedocument = driver
						.findElements(By
								.xpath("//div[text()='Question Documents*']/following::button/*[name()='svg'][starts-with(@id,'__icon__removeChoice__________')]"));
				removedocument.get(j).click();
				Thread.sleep(2000);
				ReusableMethods.scrollIntoView(driver, questiondocumentLabel);
				if (j == 1) {
					break;
				} else {
					continue;
				}
			}
		}
	}

	@Step("Verify the Error Message after removing all the DBQ Document, Method: {method}")
	public String getErrorremoveDBQDocuments(String EditFieldLabel) {
		boolean dbqErrorEle = driver
				.findElements(
						By.xpath("//div[starts-with(text(),'"
								+ EditFieldLabel
								+ "')]/following::div/span[contains(text(),'This question requires a document to be saved.')]"))
				.size() > 0;
		if (dbqErrorEle == true) {
			WebElement dbEle = driver
					.findElement(By
							.xpath("//div[starts-with(text(),'"
									+ EditFieldLabel
									+ "')]/following::div/span[contains(text(),'This question requires a document to be saved.')]"));

			return dbEle.getText();
		}
		return null;
	}

	// get QID's in Build View
	public List<String> getQIDBuildView(WebDriver driver) {
		String questionIDs = null;

		List<WebElement> questionList = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[starts-with(@class,'px1-5 fr-element fr-view')]"));
		List<String> qQID = new ArrayList<String>();
		for (WebElement qID : questionList) {
			questionIDs = qID.getAttribute("id");
			qQID.add(questionIDs);

		}
		return qQID;

	}

	@Step("Click Add Document button,  Method: {method} ")
	public void clickaddDocumentbutton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(addDocumentButton));
		addDocumentButton.click();

	}

	@Step("Verify Add Document  button state(Enabled or Disabled) ,  Method: {method} ")
	public void isaddDocumentbutton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(addDocumentButton));
		WebElement addButton = driver
				.findElement(By
						.xpath("//div[text()='Add Document']/preceding-sibling::*[name()='svg']/parent::div/parent::*"));
		if (!addButton.isEnabled()) {
			/*
			 * String getdisabled = addButton.getAttribute("disabled");
			 * if(getdisabled != null){
			 */
			Assert.assertTrue(!addButton.isEnabled(),
					"Add Document Button is Disabled");
		} else {
			clickaddDocumentbutton();
		}
	}

	@Step("Click document Text box,  Method: {method} ")
	public void clickdocumentTextbox(String value) {
		WebElement documentele = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pb1-5 pt1']//div[starts-with(@class,'fr-element fr-view')]"));
		documentele.click();
		documentele.sendKeys(value);
	}

	@Step("Modify Question Answer,  Method: {method} ")
	public void modifyQuestionAnswer(String value) {
		WebElement questionAnswertextbox = driver
				.findElement(By
						.xpath("//div[@id='regionEditQuestion']//div[contains(text(),'Question Answer')]/following::div[starts-with(@class,'fr-element fr-view')]"));
		ReusableMethods.scrollIntoView(driver, questionAnswertextbox);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", questionAnswertextbox);
		// questionAnswertextbox.click();
		questionAnswertextbox.clear();
		questionAnswertextbox.sendKeys(value);

	}

	@Step("get the list of document,  Method: {method} ")
	public List<String> getListDocument() {
		String document = null;
		List<String> documentList = new ArrayList<String>();
		List<WebElement> getdocumentListText = driver
				.findElements(By
						.xpath("//div[@name='divHeading']/span[starts-with(text(),'DOCUMENT')] | //div[@name='divHeading']/span[starts-with(text(),'Document')] | //div[@class='bold darkGray']/span[starts-with(text(),'Document')]"));
		for (int i = 0; i < getdocumentListText.size(); i++) {
			document = getdocumentListText.get(i).getText();
			documentList.add(document);
		}
		return documentList;

	}

	@Step("get the Answer Text from Build View for added question,  Method: {method} ")
	public String getAnswerText() {
		String getAnswertext = null;
		List<WebElement> getAnswerTextList = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[@class='table items-top']/span/*[name()='svg']/following::div[starts-with(@class,'pl2 pr1 table-')]/p"));
		for (int i = 0; i < getAnswerTextList.size(); i++) {
			getAnswertext = getAnswerTextList.get(i).getAttribute("innerText");
			if (!getAnswertext.isEmpty()) {
				break;
			
			}
		}
		return getAnswertext;

	}

	// Delete the Matching Lettered Options
	@Step("Delete the Lettered options from Edit Question Page for Matching Question,  Method: {method} ")
	public void deleteLetteredOptions(String Optionvalue) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@id='regionEditQuestion']//div[text()='Lettered Options*']/following::div[@class='fr-element fr-view']")));
		ReusableMethods.scrollIntoView(driver, letteredOptionsLabel);
		WebElement deleteOptionEle = driver
				.findElement(By
						.xpath("//div[@class='flex-0-0-auto flex flex-row align-middle']/div/div[contains(text(),'"
								+ Optionvalue
								+ "')]/following::button[@id='__input__button____empty____0'][position()=1]"));
		//deleteOptionEle.click();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", deleteOptionEle);
	}

	@Step("Delete the all Lettered options from Edit Question Page for Matching Question,  Method: {method} ")
	public void deleteallLetteredOptions() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@id='regionEditQuestion']//div[text()='Lettered Options*']/following::div[@class='fr-element fr-view']")));
		ReusableMethods.scrollIntoView(driver, letteredOptionsLabel);
		List<WebElement> letteredOptele = driver
				.findElements(By
						.xpath("//div[@id='regionEditQuestion']//div[text()='Lettered Options*']/following::div[@class='align-right']"));
		for (int i = 0; i < letteredOptele.size(); i++) {
			WebElement deleteOptionEle = driver
					.findElement(By
							.xpath("//div[@class='flex-0-0-auto flex flex-row align-middle']/div/div[contains(text(),'.')]/following::button[@id='__input__button____empty____0'][position()=1]"));
			deleteOptionEle.click();
		}

	}

	@Step("Delete the all Numbered options from Edit Question Page for Matching Question,  Method: {method} ")
	public void deleteallNumberedQuestions() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@id='regionEditQuestion']//div[text()='Numbered Questions*']/following::div[@class='fr-element fr-view']")));
		ReusableMethods.scrollIntoView(driver, NumberedOptionsLabel);
		List<WebElement> NumberedOptele = driver
				.findElements(By
						.xpath("//div[@id='regionEditQuestion']//div[text()='Numbered Questions*']/following::div[@class='flex pb1-5']"));
		for (int i = 0; i < NumberedOptele.size(); i++) {
			WebElement deleteOptionEle = driver
					.findElement(By
							.xpath("//div[@id='regionEditQuestion']//div[text()='Numbered Questions*']/following::div[@class='flex-0-0-auto flex flex-row align-middle']/div/div[contains(text(),'.')]/following::button[@id='__input__button____empty____0'][position()=1]"));
			ReusableMethods.scrollIntoViewClick(driver, deleteOptionEle);
		}

	}

	// Verify lettered options are in sequence after deletion
	@Step("Verify Lettered the Options are in Sequence after deletion,  Method: {method} ")
	public List<String> getseqLetteredOptions() {
		String letteredOption = null;
		List<String> optionsList = new ArrayList<String>();
		List<WebElement> optionsListElement = driver
				.findElements(By
						.xpath("//div[contains(.,'Lettered Options*')]/following::div[@class='flex-0-0-auto flex flex-row align-middle']/div/div[@class='align-right']"));
		for (int i = 0; i < optionsListElement.size(); i++) {
			letteredOption = optionsListElement.get(i).getText();
			String letteredValue = letteredOption.replace(".", "");
			optionsList.add(letteredValue);

		}
		return optionsList;
	}

	// Verify Options value
	@Step("Verify Lettered the Options sequence from Build View section before deletion,  Method: {method} ")
	public List<String> getmoreinfoOptionsList() {
		String questionIDs = null;
		List<String> letteredOptionTextList = new ArrayList<String>();
		List<WebElement> matchingQaddbuildView = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[starts-with(@class,'px1-5 fr-element fr-view')]"));
		List<String> matchingQID = new ArrayList<String>();
		for (WebElement qID : matchingQaddbuildView) {
			questionIDs = qID.getAttribute("id");
			matchingQID.add(questionIDs);
		}

		List<WebElement> getLetteredOptionText = driver
				.findElements(By
						.xpath("//div[@id='"
								+ questionIDs
								+ "']//li/div[@class='right-align mr2']/following-sibling::div/p/font/span|//div[@id='"
								+ questionIDs
								+ "']//li/div[@class='right-align mr2']/following-sibling::div/p/font/font"));
		for (int j = 0; j < getLetteredOptionText.size(); j++) {
			letteredOptionTextList.add(getLetteredOptionText.get(j).getText());
		}
		return letteredOptionTextList;

	}

	@Step("Verify Numbered Questions sequence from Build View section before deletion,  Method: {method} ")
	public List<String> getmoreinfoNumberedQuestList() {
		String questionIDs = null;
		List<String> NumQuestList = new ArrayList<String>();
		List<WebElement> matchingQaddbuildView = driver
				.findElements(By
						.xpath("//div[@id='regionYourBuildTest']//div[starts-with(@class,'px1-5 fr-element fr-view')]"));
		List<String> matchingQID = new ArrayList<String>();
		for (WebElement qID : matchingQaddbuildView) {
			questionIDs = qID.getAttribute("id");
			matchingQID.add(questionIDs);
		}

		List<WebElement> getNumberedQuestList = driver
				.findElements(By
						.xpath("//div[@id='"
								+ questionIDs
								+ "']//div/div[@class='pb2 mt2']//div[@class='pt1-5']/div//li"));
		for (int j = 0; j < getNumberedQuestList.size(); j++) {
			NumQuestList.add(getNumberedQuestList.get(j).getText());
		}
		return NumQuestList;

	}

	@Step("get the Balnk value from the Numbered section on Edit Page,  Method: {method} ")
	public void getblankValueNumberedOptions() throws InterruptedException {
		List<WebElement> getNumberedOptionText = driver
				.findElements(By
						.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')]"));
		for (int i = 0; i < getNumberedOptionText.size(); i++) {
			String getvalue = getNumberedOptionText.get(i).getText();
			Thread.sleep(1000);
			if (getvalue.isEmpty()) {
				WebElement getEle= getNumberedOptionText
						.get(i)
						.findElement(
								By.xpath("//div[contains(.,'Numbered Questions')]//child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')][not(text())]/following::button[@id='__input__button____empty____0']"));
				ReusableMethods.scrollIntoViewClick(driver, getEle);		
				break;

			}
		}
	}

	@Step("Verify Question Type Label and associated value,  Method: {method} ")
	public String getQuestTypeandValue() {
		ReusableMethods.scrollIntoView(driver, questionTypeLabel);
		Assert.assertTrue(questionTypeLabel.isDisplayed());
		String qTypeText = questionType.getText();
		return qTypeText;
	}

	@Step("Verify Chapter Label and associated value,  Method: {method} ")
	public String getChapterandValue() {
		ReusableMethods.scrollIntoView(driver, ChapterLabel);
		Assert.assertTrue(ChapterLabel.isDisplayed());
		String qTypeText = ChapterName.getText();
		return qTypeText;
	}

	@Step("Verify Reference label on Edit page,  Method: {method} ")
	public void getReferenceLabel() {
		boolean isrefLabelDisplayed = driver
				.findElements(
						By.xpath("//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Reference')]"))
				.size() > 0;
		if (isrefLabelDisplayed == true) {
			Assert.assertTrue(isrefLabelDisplayed);
		} else {
			Assert.assertFalse(isrefLabelDisplayed,
					"Reference Label is Not displayed");
		}
	}

	@Step("Verify Learning Objectives label on Edit page,  Method: {method} ")
	public void getLearningObjectivesLabel() {
		boolean isloLabelDisplayed = driver
				.findElements(
						By.xpath("//div[@id='regionEditQuestion']//div[starts-with(@class,'bold darkGray pt1')][contains(.,'Learning Objectives')]"))
				.size() > 0;
		if (isloLabelDisplayed == true) {
			Assert.assertTrue(isloLabelDisplayed);
		} else {
			Assert.assertFalse(isloLabelDisplayed,
					"Reference Label is Not displayed");
		}
	}

	@Step("click Add Numbered Option Button,  Method: {method} ")
	public void clickAddNumberedOptionButton() {
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//button[starts-with(@id,'__input__button____empty____')]/div/span[contains(.,'Add a Numbered Question')]"));
		// boolean isEleDisplayed =
		// driver.findElements(By.xpath("//button[starts-with(@id,'__input__button____empty____')]//span[contains(.,'Add a Numbered Question')]")).size()
		// >0;
		/*
		 * while(isEleDisplayed==true){
		 * ReusableMethods.scrollSlowly(driver,addNumberedOptionButton);
		 * addNumberedOptionButton.click(); isEleDisplayed=!isEleDisplayed;
		 */
		ReusableMethods.scrollIntoViewClick(driver, addNumberedOptionButton);
	}

	@Step("Verify Add Numbered Question display Balnk value and After click display the Value ,  Method: {method} ")
	public List<String> getblankvalueinAddNumQuestion()
			throws InterruptedException {
		String NumOption = null;
		
		List<String> NumoptionsList = new ArrayList<String>();
		List<WebElement> getNumberedOptionText = driver
				.findElements(By
						.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')]"));
		for (int i = 0; i < getNumberedOptionText.size(); i++) {
			String getvalue = getNumberedOptionText.get(i).getText();
			if (getvalue.isEmpty()) {
				/*WebElement nullElement = driver.findElement(By.xpath("//div[contains(@class,'singleValue')][not(text())]"));*/
				ReusableMethods.scrollIntoView(driver, getNumberedOptionText.get(i-2));
				
				numberedListSingleValue.click();
				//ReusableMethods.scrollIntoViewClick(driver, clickNumOptionEle);
			}
		}
		Thread.sleep(3000);
		List<WebElement> getOptionsValue = driver
				.findElements(By
						.xpath("//div[contains(@class,'menu')]/div/div[starts-with(@id,'react-select-')]"));
		for (int j = 0; j < getOptionsValue.size(); j++) {
			NumOption = getOptionsValue.get(j).getText();
			NumoptionsList.add(NumOption);
		}
		return NumoptionsList;
	}
	
	public void scrolltoLastNumberedQuestion(){
		List<WebElement> getNumberedOptionText = driver
				.findElements(By
						.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')]"));
		int count = getNumberedOptionText.size();
		ReusableMethods.scrollIntoView(driver, getNumberedOptionText.get(count-1));
			
		
	}
	public void scrolltoNumberedQuestion(){
		List<WebElement> getNumberedOptionText = driver
				.findElements(By
						.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')]"));
		int count = getNumberedOptionText.size();
		ReusableMethods.scrollIntoView(driver, getNumberedOptionText.get(count));
			
		
	}

	@Step("Click on the Answer Choice Text box ,  Method: {method} ")
	public void clickansChoiceTextbox() {
		WebElement lastsectionEle = driver
				.findElement(By
						.xpath("//div[contains(.,'Numbered Questions*')]/preceding::div[@class='fr-element fr-view'][position()=1]/p"));
		// ReusableMethods.scrollToElement(driver,
		// By.xpath("//div[contains(.,'Numbered Questions*')]/preceding::div[@class='fr-element fr-view'][position()=1]/p"));
		lastsectionEle.click();
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].scrollIntoView(true);", lastsectionEle);
	}

	@Step("Click on theNumbered Question Text box ,  Method: {method} ")
	public void clickNumQuestTextbox() throws InterruptedException {
		WebElement lastsectionEle = driver
				.findElement(By
						.xpath("//span[contains(.,'Add a Numbered Question')]/preceding::div[@class='fr-element fr-view'][position()=1]"));

		lastsectionEle.click();
		Thread.sleep(2000);
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].scrollIntoView(true);", lastsectionEle);
	}

	@Step("Select the value from the Numbered List box ,  Method: {method} ")
	public void selectvalueinAddNumQuestion() throws InterruptedException {
		List<WebElement> getNumberedOptionText = driver
				.findElements(By
						.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')]"));
		for (int i = 0; i < getNumberedOptionText.size(); i++) {
			String getvalue = getNumberedOptionText.get(i).getText();
			if (getvalue.isEmpty()) {
				WebElement clickNumOptionEle = driver
						.findElement(By
								.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')][not(text())]/following::div[contains(@class,'indicatorContainer')]/*[name()='svg'][@aria-hidden='true']"));
				clickNumOptionEle.click();
			}
			List<WebElement> listitems = driver.findElements(By
					.xpath("//div[starts-with(@id,'react-select-')]"));

			for (int j = 0; j < listitems.size(); j++) {
				if (i == j) {
					WebElement getlistValue = listitems.get(i);
					getlistValue.click();
				}
			}

		}
	}

	@Step("Select the all 26 value from the Numbered List box ,  Method: {method} ")
	public void selectallvalueinAddNumQuestion() throws InterruptedException {
		List<WebElement> getNumberedOptionText = driver
				.findElements(By
						.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')]"));
		for (int i = 0; i < getNumberedOptionText.size(); i++) {
			String getvalue = getNumberedOptionText.get(i).getText();
			if (getvalue.isEmpty()) {
				WebElement clickNumOptionEle = driver
						.findElement(By
								.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')][not(text())]/following::div[contains(@class,'indicatorContainer')]/*[name()='svg'][@aria-hidden='true']"));
				clickNumOptionEle.click();
				List<WebElement> listitems = driver.findElements(By
						.xpath("//div[starts-with(@id,'react-select-')]"));
				// ReusableMethods.scrollIntoView(driver, listitems.get(i));
				for (int j = 0; j < listitems.indexOf(i); j++) {

					try {
						listitems.get(i).click();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}
	}

	public void selectListValue() {
		/*WebElement clickNumOptionEle = driver
				.findElement(By
						.xpath("//div[contains(.,'Numbered Questions')]/child::div[@class='flex pb1-5']/following::div[contains(@class,'singleValue')][not(text())]/following::div[contains(@class,'indicatorContainer')]/*[name()='svg'][@aria-hidden='true']"));
		clickNumOptionEle.click();*/
		numberedListSingleValue.click();
		List<WebElement> listOptions = driver.findElements(By
				.xpath("//div[starts-with(@id,'react-select-')]"));
		int size = listOptions.size();
		int randonNumber = ThreadLocalRandom.current().nextInt(0, size);
		listOptions.get(randonNumber).click();
	}

	public String getNumberQuestionsDigit() {
		String getValue = null;
		List<WebElement> NumberQDigitEle = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'align-left pl2 pt1')]/div[starts-with(@class,'pr2 align-right')]"));
		for (int i = 0; i < NumberQDigitEle.size(); i++) {
			getValue = NumberQDigitEle.get(i).getText();
		}
		return getValue;
	}
}
