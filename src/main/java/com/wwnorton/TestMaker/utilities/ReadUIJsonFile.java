package com.wwnorton.TestMaker.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class ReadUIJsonFile {

	public static String UserDir = System.getProperty("user.dir");
	// Read Json test data from testData.json file with the parameters and
	// values.
	Properties prop = new Properties();

	public JsonObject readUIJason() {

		JsonObject rootObject = null;
		InputStream input = null;
		try {
			input = new FileInputStream(UserDir
					+ "/src/test/resources/config.properties");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			prop.load(input);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String url = prop.getProperty("TestMakerURL");

		try {

			JsonParser parser = new JsonParser();
			// QA Environment
			if (url.equalsIgnoreCase("https://testmakerweb-qa.wwnorton.net")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "//src//test//resources//TestMakerTestData.json"));
				jReader.setLenient(true);
				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			} else if (url
					.equalsIgnoreCase("https://testmakerweb-uat.wwnorton.net")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "//src//test//resources//UATTestMakerTestData.json"));
				jReader.setLenient(true);

				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();

			} else if (url
					.equalsIgnoreCase("https://testmakerweb-qa1.wwnorton.net")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "//src//test//resources//UATTestMakerTestData.json"));
				jReader.setLenient(true);

				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			} else if (url
					.equalsIgnoreCase("https://testmaker-tm-qa.wwnorton.net")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "//src//test//resources//TMQATestMakerTestData.json"));
				jReader.setLenient(true);

				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			} else if (url
					.equalsIgnoreCase("https://testmaker-login-qa.wwnorton.net")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "//src//test//resources//TMQATestMakerTestData.json"));
				jReader.setLenient(true);

				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			} else if (url.equalsIgnoreCase("https://testmaker.wwnorton.com")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "//src//test//resources//PRODTestMakerTestData.json"));
				jReader.setLenient(true);

				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			} else if (url
					.equalsIgnoreCase("https://testmaker-tm-stg.wwnorton.net")) {
				JsonReader jReader = new JsonReader(
						new FileReader(
								UserDir
										+ "//src//test//resources//AuthStgTestMakerTestData.json"));
				jReader.setLenient(true);

				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			} else if (url
					.equalsIgnoreCase("https://testmaker-redis-qa.wwnorton.net")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "//src//test//resources//TMQATestMakerTestData.json"));
				jReader.setLenient(true);

				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			}

		}

		catch (Exception e)

		{
			e.printStackTrace();
		}

		return rootObject;

	}

	public JsonObject readSearchquestionJson() {

		JsonObject jsonobject = null;

		String userdir = System.getProperty("user.dir")
				+ "\\src\\test\\resources\\TestMakerSearchQuestion.json";
		try {
			JsonParser jsonParser = new JsonParser();
			JsonReader reader = new JsonReader(new FileReader(userdir));
			reader.setLenient(true);
			JsonElement rootElement = jsonParser.parse(reader);
			jsonobject = rootElement.getAsJsonObject();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject;

	}

}
