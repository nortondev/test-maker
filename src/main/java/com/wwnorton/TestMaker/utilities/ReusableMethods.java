package com.wwnorton.TestMaker.utilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonObject;

public class ReusableMethods {
	private static final String HTML_PATTERN = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";
	private static Pattern pattern = Pattern.compile(HTML_PATTERN);
	static ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	static JsonObject jsonObj = readJsonObject.readUIJason();

	public static void checkPageIsReady(WebDriver driver) {

		boolean isPageReady = false;
		JavascriptExecutor js = (JavascriptExecutor) driver;

		while (!isPageReady) {

			isPageReady = js.executeScript("return document.readyState")
					.toString().equals("complete");

			try {

				Thread.sleep(5000);

			} catch (InterruptedException e) {

				e.printStackTrace();

			}
		}
	}

	public static boolean elementExist(WebDriver driver, String xpath) {

		try {

			driver.findElement(By.xpath(xpath));

		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}

	public static void scrollToBottom(WebDriver driver) {
		try {
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollToBottomEle(WebDriver driver, WebElement element) {
		try {
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollToElement(WebDriver driver, By by) {
		try {
			WebElement element = driver.findElement(by);
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			Thread.sleep(1000);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static void scrollIntoView(WebDriver driver, WebElement webElement) {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollIntoViewListElements(WebDriver driver,
			List<WebElement> webElement) {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByXY(WebDriver driver, int xOffset, int yOffset) {
		try {
			String script = "scroll(" + xOffset + ", " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByX(WebDriver driver, int xOffset) {
		try {
			String script = "scroll(" + xOffset + ", 0)";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByY(WebDriver driver, int yOffset) {
		try {
			String script = "scroll(0, " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void waitForPageLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(pageLoadCondition);
	}

	public static void questionCloseLink(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.className("close")));
		driver.findElement(By.className("close")).click();
	}

	public static void WaitVisibilityOfElement(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public static void WaitElementClickable(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static void loadingWaitDisapper(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 1000L);
		boolean isLoader = driver.findElements(By
				.xpath("//div[@class='ResultLoading']/div")).size() >0;
		if(isLoader==true){
		WebElement loader = driver.findElement(By
				.xpath("//div[@class='ResultLoading']/div"));
		wait.until(ExpectedConditions.visibilityOf(loader)); // wait for loader
																// to appear
		wait.until(ExpectedConditions.invisibilityOf(loader)); // wait for
																// loader to
		}														// disappear
	}

	// using StringBuilder.append()
	public static String convertArrayToStringMethod(String[] strArray) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < strArray.length; i++) {
			stringBuilder.append(strArray[i]);
		}
		return stringBuilder.toString();
	}

	// Using String.join() method
	public static String convertArrayToStringUsingStreamAPI(String[] strArray) {
		String joinedString = String.join(" ", strArray);
		String replaced = joinedString.replace("[", "").replace("]", "");
		System.out.println(replaced);
		return replaced;
	}

	public static boolean scroll_Page(WebDriver driver, WebElement webelement,
			int scrollPoints) {
		try {
			Actions dragger = new Actions(driver);
			// drag downwards
			int numberOfPixelsToDragTheScrollbarDown = 10;
			for (int i = 10; i < scrollPoints; i = i
					+ numberOfPixelsToDragTheScrollbarDown) {
				dragger.moveToElement(webelement).clickAndHold()
						.moveByOffset(0, numberOfPixelsToDragTheScrollbarDown)
						.release(webelement).build().perform();
			}
			Thread.sleep(500);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void scrollSlowly(WebDriver driver, WebElement element) {
		for (int i = 0; i < 1000; i++) {
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1)",
					"");
			if (element.isDisplayed()) {
				ReusableMethods.scrollIntoViewClick(driver, element);
				break;
			} else {
				continue;
			}

		}
	}

	public static String getSearchText(WebDriver driver) {

		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-5')]"));
		String id = qM0.getAttribute("id");
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getSText = getIDvalue.getText();
		return getSText;
	}

	public static String waitTillTextPresent(WebDriver driver, By by,
			String text) {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(by, text));
		return text;

	}

	/*
	 * public static boolean isQuestionGroupDisplayed(WebDriver driver) throws
	 * InterruptedException { Actions actions = new Actions(driver);
	 * driver.findElement(By.id("regionAside")).click(); List<WebElement>
	 * getQuestionText = driver.findElements(By
	 * .xpath("//button[@type='button'][starts-with(text(),'Show')]")); for (int
	 * ele = 0; ele < getQuestionText.size(); ele++) {
	 * actions.sendKeys(Keys.PAGE_DOWN).build().perform(); Thread.sleep(5000);
	 * actions.sendKeys(Keys.PAGE_UP).build().perform(); } return true; }
	 */

	public static boolean isQuestionGroupDisplayed(WebDriver driver)
			throws InterruptedException {
		Actions actions = new Actions(driver);
		boolean isQGdisplayed = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(text(),'Question Group')]"))
				.size() > 0;
		while (isQGdisplayed == false) {
			driver.findElement(By.id("regionAside")).click();
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(2000);
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			actions.keyDown(Keys.CONTROL).release().perform();
			ReusableMethods
					.scrollToElement(
							driver,
							By.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(text(),'Question Group')]"));
			if (driver
					.findElements(
							By.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(text(),'Question Group')]"))
					.size() > 0) {
				isQGdisplayed = true;
			} else {
				break;
			}

		}
		if (isQGdisplayed == true) {
			ReusableMethods
					.scrollToElement(
							driver,
							By.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(text(),'Question Group')]"));
		}
		return isQGdisplayed;
	}

	// Download the File in download Folder
	public static void downLoadFile() {
		String fileDownloadPath = System.getProperty("user.dir")
				+ "\\Downloads";

		Map<String, Object> prefsMap = new HashMap<String, Object>();
		prefsMap.put("profile.default_content_settings.popups", 0);
		prefsMap.put("download.default_directory", fileDownloadPath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefsMap);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		// driver = new ChromeDriver(options);
	}

	// get Numbers from Questions
	public static int getDigits(String text) {
		String str = text.replaceAll("[^0-9]", "");
		int v = Integer.parseInt(str);
		return v;

	}

	public static String questionGroupreadJson() {
		String questionGroupISBN = jsonObj.getAsJsonObject("QuestionGroup")
				.get("QuestionGroupISBN").getAsString();
		return questionGroupISBN;
	}

	public static String matchingQuestionreadJson() {
		String matchingISBN = jsonObj.getAsJsonObject("MatchingQuestion")
				.get("MatchingISBN").getAsString();
		return matchingISBN;
	}

	public static String dbqQuestionreadJson() {
		String dbqISBN = jsonObj.getAsJsonObject("DBQQuestion").get("DBQISBN")
				.getAsString();
		return dbqISBN;
	}

	public static String uniqueQuestionreadJson() {
		String ISBN = jsonObj.getAsJsonObject("NormalISBN").get("ISBN")
				.getAsString();
		return ISBN;
	}
	public static String editQuestionISBN() {
		String ISBN = jsonObj.getAsJsonObject("EditQuestion").get("ISBN")
				.getAsString();
		return ISBN;
	}
	public static String mutipleMetaISBN() {
		String ISBN = jsonObj.getAsJsonObject("MutipleMetaDatatype").get("ISBN")
				.getAsString();
		return ISBN;
	}

	public static String apOptionreadJson() {
		String ISBN = jsonObj.getAsJsonObject("APOptions").get("ISBN")
				.getAsString();
		return ISBN;
	}

	public static String nonQuestionSourceISBNreadJson() {
		String ISBN = jsonObj.getAsJsonObject("NonQuestionSourceISBN")
				.get("ISBN").getAsString();
		return ISBN;
	}

	public static String nonQuestionGroupISBNreadJson() {
		String ISBN = jsonObj.getAsJsonObject("NonQuestionGroupISBN")
				.get("ISBN").getAsString();
		return ISBN;
	}

	public static String invalidISBN() {

		String ISBN = jsonObj.getAsJsonObject("InvalidISBN").get("ISBN")
				.getAsString();
		return ISBN;
	}

	public static String readQGChapter(String qType) {
		String QGChapter = null;
		ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
		JsonObject jsonObj = readJsonObject.readUIJason();
		if (qType.equalsIgnoreCase("MULTIPLE CHOICE")) {
			QGChapter = jsonObj.getAsJsonObject("QuestionGroupChapter")
					.get("MChapterName").getAsString();
		} else if (qType.equalsIgnoreCase("ESSAY")) {
			QGChapter = jsonObj.getAsJsonObject("QuestionGroupChapter")
					.get("EChapterName").getAsString();
		} else if (qType.equalsIgnoreCase("TRUE/FALSE")) {
			QGChapter = jsonObj.getAsJsonObject("QuestionGroupChapter")
					.get("TFChapterName").getAsString();
		} else if (qType.isEmpty()) {
			QGChapter = jsonObj.getAsJsonObject("QuestionGroupChapter")
					.get("MChapterName").getAsString();
		}
		return QGChapter;
	}

	public static String LOISBN() {

		String LOISBN = jsonObj.getAsJsonObject("LOISBN").get("LO")
				.getAsString();
		return LOISBN;
	}

	public static List<String> apFilterPanelList() {

		List<String> apPanelNames = Arrays.asList("AP Learning Objectives",
				"AP Topics", "AP Reasoning Process", "AP Skills",
				"AP Practices");

		return apPanelNames;

	}

	public static void highLighterMethod(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(
				"arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",
				element);
	}

	public static void expandQuestionsListView(WebDriver driver) {
		JavascriptExecutor js;
		js = (JavascriptExecutor) driver;
		List<WebElement> listviewButton = driver
				.findElements(By
						.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
		for (int j = 0; j < listviewButton.size(); j++) {

			js.executeScript("arguments[0].scrollIntoView(true);",
					listviewButton.get(j));
			boolean isexpanded = driver.findElements(
					By.xpath("//div[@class='border-none bg-white none']"))
					.size() > 0;
			if (isexpanded == true) {
				js.executeScript("arguments[0].click();", listviewButton.get(j));
			}

		}

	}

	public static void scrollQuestionSection(WebDriver driver)
			throws InterruptedException {
		Actions actions = new Actions(driver);
		driver.findElement(By.id("regionAside")).click();
		actions.sendKeys(Keys.PAGE_UP).build().perform();
		Thread.sleep(1000);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
	}

	public static String decodeString(String encodeString) {
		Decoder decoder = Base64.getDecoder();
		byte[] bytes = decoder.decode(encodeString);
		LogUtil.log(new String(bytes));
		return encodeString;
	}

	private static void delay(long millisecond) throws InterruptedException {
		Thread.sleep(millisecond);

	}

	public static void delay() throws InterruptedException {
		delay(5000);
	}

	public static void clickElement(WebDriver driver, WebElement Element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", Element);
	}

	public static void scrollIntoViewClick(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		js.executeScript("arguments[0].click();", element);
	}

	public static String addChar(String str, char ch, int position) {
		StringBuilder sb = new StringBuilder(str);
		sb.insert(position, ch);
		return sb.toString();
	}

	public static void sendShortCut(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.chord(Keys.CONTROL, "T")).build().perform();
	}

	public static void fileUpload(String path) throws AWTException {
		StringSelection strSelection = new StringSelection(path);

		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

		clipboard.setContents(strSelection, null);
		Robot robot = new Robot();
		robot.delay(300);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);

		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);

		robot.keyPress(KeyEvent.VK_ENTER);

		robot.delay(200);

		robot.keyRelease(KeyEvent.VK_ENTER);

	}

	// get HTML tags from a String

	public static boolean hasHTMLTags(String text) {
		Matcher matcher = pattern.matcher(text);
		return matcher.find();
	}

	public static void pressKeyTab(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB).build().perform();
		action.sendKeys(Keys.RETURN).build().perform();
	}

	// get the count of frames on the page
	public static void getFrames(WebDriver driver) {
		JavascriptExecutor exe = (JavascriptExecutor) driver;
		int f = Integer.parseInt(exe.executeScript("return window.length")
				.toString());
		System.out.println("No. of iframes on the page are " + f);
	}
	
	public static String getNNumberofWords(String str, int n){
		String firstStrs = null;
		String[] sArry = str.split("");
		for(int i=0; i<n; i++){
			firstStrs +=sArry[i] + "";
		}
		return firstStrs.trim();
	}
	
	public static String getTestCaseName(){
		String testName,guid;
		testName = jsonObj.getAsJsonObject("TMTestName").get("testName").getAsString();
		guid = GetRandomId.randomAlphaNumeric(3).toLowerCase().toString();
		guid = guid.replaceAll("[^a-zA-Z0-9]+", "a");
		return  testName + guid;
	}
}