package com.wwnorton.TestMaker.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;

public class PropertiesFile extends BaseDriver {

	public static WebDriverWait wait;
	public static String UserDir = System.getProperty("user.dir");
	// public static String browser;
	public static String url, websiteURL, essayURL, LMSURL;
	public static String DriverPath;
	static Properties prop = new Properties();

	// Read Browser name, Test url and Driver file path from config.properties.

	public static void loadProperties() throws IOException {
		InputStream input = null;
		try {
			input = new FileInputStream(UserDir
					+ "\\src\\test\\resources\\config.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prop.load(input);
	}

	public static void readPropertiesFile() throws Exception {

		loadProperties();

		// browser = prop.getProperty("browsername");
		url = prop.getProperty("TestMakerURL");
		websiteURL = prop.getProperty("WebSiteURL");
		essayURL = prop.getProperty("TestMakerEssayQTypeURL");
		LMSURL = prop.getProperty("LMSURL");
		DriverPath = UserDir + "\\Drivers\\";

	}

	// Set Browser configurations by comparing Browser name and Diver file path.
	// @Parameters({"browser"})
	public static void setBrowserConfig(String browser)
			throws IllegalAccessException, IOException {
		loadProperties();
		browser = prop.getProperty("browsername");
		BaseDriver bd = new BaseDriver();
		bd.init_driver(browser);

	}

	// Set Test URL based on config.properties file.

	public static void setURL() {

		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().get(url);
	}

	// get WebSite URL
	public static void webSiteURL() {

		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().get(websiteURL);
	}

	// get LMS URL
	public static void setlmsURL() {

		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().get(LMSURL);
	}

	// Close the driver after running test script.

	public static void tearDownTest() throws InterruptedException {

		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// getDriver().close();
		getDriver().quit();

	}

}
