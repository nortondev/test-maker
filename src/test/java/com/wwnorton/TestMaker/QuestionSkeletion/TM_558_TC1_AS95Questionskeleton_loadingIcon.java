package com.wwnorton.TestMaker.QuestionSkeletion;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_558_TC1_AS95Questionskeleton_loadingIcon extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify loading icon and questions skeleton")
	@Stories("Create a new test and search any term to verify application displays loading icon and question skeleton for next set of 20 questions")
	@Test()
	public void loadingIconQuestionSkeleton() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.clickChaptersCheckboxesandChaptersName(2);
		buildTest.clickApplyButton();
		ReusableMethods.isQuestionGroupDisplayed(driver);
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		LogUtil.log(getQuestionText);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		String getText = ReusableMethods.getSearchText(driver);
		ArrayList<String> searchKeys = new ArrayList<String>();
		String SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);
		searchKeys.add(SearchText);
		Thread.sleep(5000);
		buildTest.clickClearFiltersButton();
		Thread.sleep(5000);
		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		System.out.println(questionCountAfterFilter);
		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int v = Integer.parseInt(str[0]);
		int k = v / 10;
		for (int i = 0; i < k; i++) {
			js = (JavascriptExecutor) driver;
			actions = new Actions(driver);

			List<WebElement> addButton = driver.findElements(By
					.xpath("//button[@name='addbutton']"));
			for (int j = 0; j < addButton.size(); j++) {
				int numCount = j;
				numCount++;
				if (numCount % 10 == 0) {

					js.executeScript("arguments[0].scrollIntoView(true);",
							addButton.get(j));
					actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
					boolean loadingIcon = driver.findElements(
							By.xpath("//div[@class='ResultLoading']")).size() > 0;
					if (loadingIcon == true) {
						Assert.assertTrue(loadingIcon);
					}
				} else {
					continue;
				}

			}

		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
