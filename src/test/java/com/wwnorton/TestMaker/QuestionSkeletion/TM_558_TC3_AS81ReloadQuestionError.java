package com.wwnorton.TestMaker.QuestionSkeletion;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_558_TC3_AS81ReloadQuestionError extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	getPOS POS = new getPOS();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Reload Questions Error")
	@Stories("Create a new test and search any term to verify application displays Error message when application could not fetch a single question from the question list")
	@Test()
	public void reloadQuestionsError() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.clickChaptersCheckboxesandChaptersName(1);
		buildTest.clickApplyButton();
		ReusableMethods.isQuestionGroupDisplayed(driver);
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		LogUtil.log(getQuestionText);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		String getText = ReusableMethods.getSearchText(driver);
		ArrayList<String> searchKeys = new ArrayList<String>();
		String SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);
		searchKeys.add(SearchText);
		Thread.sleep(5000);
		buildTest.clickClearFiltersButton();
		Thread.sleep(5000);
		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		WebElement element = driver.findElement(By.id("regionAside"));
		element.click();
		Actions actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		WebElement questions = driver
				.findElement(By
						.xpath("//div[@class ='pl2 col sm-col-4 sm-col-8 h3 font-weight-2 pt1-5 pb1-5 mt1-5']"));
		String questionsCount = questions.getText();
		String regex = "[^\\d]+";
		String[] str = questionsCount.split(regex);
		System.out.println(str[0]);
		// List<WebElement> elementList =
		// driver.findElements(By.xpath("//div[@class='pb2 mt3 ml1 pl2']"));
		int v = Integer.parseInt(str[0]);
		int k = v / 10;
		for (int i = 0; i < k; i++) {
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			boolean isPresent = driver.findElements(
					By.id("__input__button__Reload__Questions__0")).size() > 0;
			if (isPresent == true) {
				String questionLoadedText = driver
						.findElement(
								By.xpath("//div[@class='mx1-5 center mt3 mb3-5']/div/span"))
						.getText();
				Assert.assertEquals("Questions could not be loaded",
						questionLoadedText);
				Thread.sleep(3000);
				driver.findElement(
						By.id("__input__button__Reload__Questions__0")).click();
				int questionsCount1 = buildTest.getQuestionsList();
				Assert.assertEquals(questionsCount1, v);
			} else {
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			}
		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
