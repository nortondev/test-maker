package com.wwnorton.TestMaker.SkipContentMenu;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_829_TC3_AS100_focusonSkipContentMenu extends PropertiesFile {

	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	BuildTestPage buildTest;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Focus on Skip to Content Menu ")
	@Stories("Log into testmaker and focus on Skip to Content menu and select each menu to verify application highlight the respective area")
	@Test()
	public void focusonSkipContentMenu() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.profileTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);
		Thread.sleep(5000);

		Actions action = new Actions(driver);
		action.keyDown(Keys.ALT).sendKeys("+").sendKeys("/").build().perform();
		WebElement skiptoContent = driver
				.findElement(By
						.xpath("//div/button/span[contains(text(),'Sections of this page')]"));
		skiptoContent.click();
		driver.findElement(By.id("landmarkNavbtn__menuOption__Your Test"))
				.click();
		Thread.sleep(3000);
		WebElement regionBuildTest = driver.findElement(By
				.id("regionYourBuildTest"));
		// WebElement activeElement = driver.switchTo().activeElement();
		regionBuildTest.getAttribute("box-shadow");
		String regionBuildTestcolor = regionBuildTest
				.getCssValue("border-color");
		String regionBuildTesthex = Color.fromString(regionBuildTestcolor)
				.asHex();
		// System.out.println(regionBuildTesthex);
		Assert.assertEquals("#49abc6", regionBuildTesthex);
		skiptoContent.click();
		skiptoContent.click();
		Thread.sleep(2000);
		driver.findElement(By.id("landmarkNavbtn__menuOption__Navigation"))
				.click();
		Thread.sleep(3000);
		WebElement regionHeaderNavigation = driver.findElement(By
				.id("regionHeaderNavigation"));
		// WebElement activeElement = driver.switchTo().activeElement();
		regionHeaderNavigation.getAttribute("box-shadow");
		String headerNavigationcolor = regionHeaderNavigation
				.getCssValue("border-color");
		String headerNavigationhex = Color.fromString(headerNavigationcolor)
				.asHex();
		// System.out.println(headerNavigationhex);
		Assert.assertEquals("#49abc6", headerNavigationhex);
		skiptoContent.click();
		skiptoContent.click();
		Thread.sleep(2000);
		driver.findElement(
				By.id("landmarkNavbtn__menuOption__Add Question Panel"))
				.click();
		Thread.sleep(3000);
		WebElement regionAside = driver.findElement(By.id("regionAside"));
		// WebElement activeElement = driver.switchTo().activeElement();
		regionAside.getAttribute("box-shadow");
		String regionAsidecolor = regionAside.getCssValue("border-color");
		String regionAsidehex = Color.fromString(regionAsidecolor).asHex();
		// System.out.println(regionAsidehex);
		Assert.assertEquals("#49abc6", regionAsidehex);
		skiptoContent.click();
		skiptoContent.click();
		Thread.sleep(2000);
		driver.findElement(By.id("landmarkNavbtn__menuOption__Action Toolbar"))
				.click();
		Thread.sleep(3000);
		WebElement regionFooter = driver.findElement(By.id("regionFooter"));
		// WebElement activeElement = driver.switchTo().activeElement();
		String regionFootercolor = regionFooter.getCssValue("border-color");
		String regionFooterhex = Color.fromString(regionFootercolor).asHex();
		// System.out.println(regionFooterhex);
		Assert.assertEquals("#49abc6", regionFooterhex);
		action.keyDown(Keys.ALT).sendKeys("+").sendKeys("/").build().perform();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
