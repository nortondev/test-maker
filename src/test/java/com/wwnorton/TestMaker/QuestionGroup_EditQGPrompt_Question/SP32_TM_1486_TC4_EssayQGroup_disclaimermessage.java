package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP32_TM_1486_TC4_EssayQGroup_disclaimermessage extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1486_TC4_User verifies that the disclaimer message is displayed correctly for ESSAY question when question and question prompt is edited from BUILD view.")
	@Stories("SP32_TM-1486_TC4_User verifies that the disclaimer message is displayed correctly for ESSAY question when question and question prompt is edited from BUILD view.")
	@Test()
	public void editEssayGroupQ_disclaimermessage() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		Thread.sleep(5000);
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		Thread.sleep(5000);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("ESSAY");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		ReusableMethods.loadingWaitDisapper(driver);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		buildTest.clickAddquestiongroupQuestions(0, "ESSAY");
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ = new EditQuestions();
		editQ.clickEditPromptButton();
		String getEditQText = editQ.getEditQuestionText();
		String actualText = "How Editing a Question Works:\nIn your exam, saving this edited question will replace the original question.This edited question and the original question will both appear in the test bank.";
		String editedPromptText = getEditQText.replace("\n", "").replace("\r",
				"");
		String expectedtextEditPrompt = actualText.replace("\n", "").replace(
				"\r", "");
		Assert.assertEquals(editedPromptText.toString().trim(),
				expectedtextEditPrompt.toString().trim());
		cnt.clickCancelButton();
		Thread.sleep(5000);
		ReusableMethods.scrollIntoViewClick(driver, editQ.EditQuestionbutton);
		//editQ.clickEditQuestion();

		String getEditQuestionText = editQ.getEditQuestionText();
		String actualTextEditQ = "How Editing a Question Works:\nIn your exam, saving this edited question will replace the original question.This edited question and the original question will both appear in the test bank.";
		String editedQuestionText = getEditQuestionText.replace("\n", "")
				.replace("\r", "");
		String expectedtext = actualTextEditQ.replace("\n", "").replace("\r",
				"");
		Assert.assertEquals(editedQuestionText.toString().trim(), expectedtext
				.toString().trim());
		cnt.clickCancelButton();
		Thread.sleep(2000);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
