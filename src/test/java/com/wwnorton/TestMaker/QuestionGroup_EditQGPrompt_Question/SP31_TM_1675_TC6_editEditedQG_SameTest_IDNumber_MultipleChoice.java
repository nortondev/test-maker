package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1675_TC6_editEditedQG_SameTest_IDNumber_MultipleChoice
		extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			getFirstIDValue, completeNewVersion, newEditedNumber;
	List<WebElement> moreInfo;
	boolean isQuestion;
	String strAfterSave;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1675_TC6_Log into Testmaker application and edit User Multi Choice group question in same test to verify Application doesn't assign new ID Number and carry the same Question number of Edited question")
	@Stories("SP31_TM-1675_TC6_Log into Testmaker application and edit User Multi Choice group question in same test to verify Application doesn't assign new ID Number and carry the same Question number of Edited question")
	@Test()
	public void editEditedMultipleChoiceGroupQIDNumber() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		ReusableMethods.checkPageIsReady(driver);
		buildTest = new BuildTestPage();
		// select filters
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		Thread.sleep(5000);
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(1);
			buildTest.clickApplyButton();
		}
		ReusableMethods.loadingWaitDisapper(driver);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		buildTest.selectQuestionGroupQuestions("MULTIPLE CHOICE");
		isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
		LogUtil.log(isQuestion);
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]"));
		ArrayList<String> idNumberQuestionsMetaData = new ArrayList<String>();
		if (isQuestion == true) {
			boolean isShowLinkDisplayed = driver
					.findElements(
							By.xpath("//button[starts-with(@id,'__input__button__Show__')]"))
					.size() > 0;
			if (isShowLinkDisplayed == true) {
				WebElement clickShowlink = driver
						.findElement(By
								.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
				clickShowlink.click();
			}
			idNumberQuestionsMetaData = (ArrayList<String>) buildTest
					.getMetadataAssociatedValuesQuestionGroup("ID Number");
			getFirstIDValue = idNumberQuestionsMetaData.get(0);
			LogUtil.log(getFirstIDValue);
			if (getFirstIDValue.contains("E")) {
				buildTest.clickSingleQuestionQG(0,"MULTIPLE CHOICE");

				String questionCountAfterSearchQuestionAfteradd = buildTest
						.verifyQuestioninTestBuildSearchResult();
				LogUtil.log(questionCountAfterSearchQuestionAfteradd);
				editQ = new EditQuestions();
				// editQ.clickAddNortonQuestion();
				ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
				// ReusableMethods.scrollIntoView(driver,
				// editQ.editPromptbutton);
				ArrayList<String> idNumberListbeforeSave = new ArrayList<String>();
				js = (JavascriptExecutor) driver;
				moreInfo = driver.findElements(By
						.xpath("//button[contains(text(),'More Info')]"));
				for (int j = 0; j < moreInfo.size(); j++) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(5000);
					IDNumber = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					LogUtil.log(IDNumber);
					idNumberListbeforeSave.add(IDNumber);
				}
				LogUtil.log(idNumberListbeforeSave);
				String strbeforesave = idNumberListbeforeSave.toString();
				strbeforesave = strbeforesave.replaceAll("\\[", "").replaceAll(
						"\\]", "");
				ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
				Thread.sleep(1000);
				editQ.clickEditQuestion();
				ReusableMethods.checkPageIsReady(driver);
				Assert.assertTrue(editQ.saveButtonState(),
						"Save Button is Disabled State");
				ReusableMethods
						.scrollToElement(
								driver,
								By.xpath("//div[@id='regionEditQuestion']//div[starts-with(text(),'Answer Choices*')]"));
				Thread.sleep(1000);
				String expectedQuestiontext = editQ.getQuestionText();
				LogUtil.log(expectedQuestiontext);
				editQ.removeQuestionText();
				editQ.modifyQuestionText(expectedQuestiontext);
				Thread.sleep(5000);
				if (editQ.listValueDisplayed("Difficulty") == false) {
					ReusableMethods.scrollToBottom(driver);
					editQ.deleteListValue("Difficulty");
					editQ.changeListValue("Difficulty");
					Thread.sleep(2000);
				}

				if (editQ.listValueDisplayed("Bloom") == false) {
					ReusableMethods.scrollToBottom(driver);
					editQ.deleteListValue("Bloom");
					editQ.changeListValue("Bloom");
				}
				Thread.sleep(2000);
				cnt.clickSave();
				Thread.sleep(3000);
				ArrayList<String> idNumberListafterSave = new ArrayList<String>();
				moreInfo = driver.findElements(By
						.xpath("//button[contains(text(),'More Info')]"));
				for (int j = 0; j < moreInfo.size(); j++) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(2000);
					IDNumberAfterSave = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					idNumberListafterSave.add(IDNumberAfterSave);
				}
				LogUtil.log(idNumberListafterSave);

				strAfterSave = idNumberListafterSave.toString();
				strAfterSave = strAfterSave.replaceAll("\\[", "").replaceAll(
						"\\]", "");
				Assert.assertNotEquals(strbeforesave, strAfterSave);
				LogUtil.log(strbeforesave + " " + strAfterSave);
				boolean containsEText = strAfterSave.contains("E");
				Assert.assertTrue(containsEText,
						" The Added Question contains letter E");
				Thread.sleep(2000);

			}
		}
		cnt.clickTestMakerBackButton();
		Thread.sleep(3000);
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		Thread.sleep(1000);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertTrue(editQ.saveButtonState(),
				"Save Button is Disabled State");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id='regionEditQuestion']//div[starts-with(text(),'Answer Choices*')]"));
		Thread.sleep(1000);
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		editQ.modifyQuestionText(expectedQuestiontext);
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
		}

		if (editQ.listValueDisplayed("Bloom") == false) {
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		Thread.sleep(2000);
		cnt.clickSave();
		Thread.sleep(3000);
		ArrayList<String> idNumberListSave = new ArrayList<String>();
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(5000);
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			LogUtil.log(IDNumber);
			idNumberListSave.add(IDNumber);
		}
		LogUtil.log(idNumberListSave);
		String strsaveve = idNumberListSave.toString();
		strsaveve = strsaveve.replaceAll("\\[", "").replaceAll("\\]", "");
		Assert.assertEquals(strsaveve, strAfterSave);
		LogUtil.log(strsaveve + " " + strAfterSave);
		boolean containsEText = strsaveve.contains("E");
		Assert.assertTrue(containsEText,
				" The Added Question contains letter E");
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
