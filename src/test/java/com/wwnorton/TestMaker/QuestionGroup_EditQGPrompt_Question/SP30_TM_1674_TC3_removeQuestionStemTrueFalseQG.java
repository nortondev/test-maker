package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP30_TM_1674_TC3_removeQuestionStemTrueFalseQG extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	WebsitePage website;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			getFirstIDValue, completeNewVersion, newEditedNumber;
	List<WebElement> moreInfo;
	boolean isQuestion;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP30_TM-1674_TC3_Log into Testmaker application and Remove question stem from True/False group question editor and try to Save the changes to verify application displays error message")
	@Stories("SP30_TM-1674_TC3_Log into Testmaker application and Remove question stem from True/False group question editor and try to Save the changes to verify application displays error message")
	@Test()
	public void editTrueFalseQuestionGroup_InlineErrorMessages()
			throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("TRUE/FALSE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		//ReusableMethods.loadingWaitDisapper(driver);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();

		buildTest.clickAddquestiongroupQuestions(0,"TRUE/FALSE");
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		editQ = new EditQuestions();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ.clickEditPromptButton();
		Thread.sleep(3000);
		editQ.removeQuestionPrompt();
		driver.findElement(By.xpath("//p[@class='bg-lightestGray mt2 p1']"))
				.click();
		cnt.clickSave();
		Thread.sleep(3000);
		editQ.questionNotSavedpopUp();
		String errorText = editQ.questionNotSavedpopUpErrorMessage();
		LogUtil.log(errorText);
		website = new WebsitePage();
		editQ.clickContactUslink();
		String parentWindow = driver.getWindowHandle();
		website.getWebsiteWindow(driver, parentWindow);
		website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow);

		// Assert.assertEquals("Your question could not be saved because of one or more errors. Please try again or \r\n contact us\r\n .",
		// errorText.toString().trim());
		editQ.clickCloselink();
		Thread.sleep(3000);
		String getQuestionPrompttext = editQ
				.getErrormsgQuestionStembox("Question Prompt*");
		Assert.assertEquals(getQuestionPrompttext.toString().trim(),
				"This is a required field.");
		cnt.cancelButton.click();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(3000);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		List<String> qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();

		LogUtil.log(qids.size());
		js = (JavascriptExecutor) driver;
		for (int i = 0; i < qids.size(); i++) {
			// String qid = qids.get(i).toString();
			List<WebElement> editQbuttons = driver
					.findElements(By
							.xpath("//div/button[starts-with(@id,'__input__button__Edit__Question__')][contains(text(),'Edit Question')]"));
			for (int j = 0; j < editQbuttons.size();) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							editQbuttons.get(0));
					js.executeScript("arguments[0].click();",
							editQbuttons.get(0));
				}
				break;
			}
		}
		Thread.sleep(1000);
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id='regionEditQuestion']//div[starts-with(text(),'Answer Choices*')]"));
		Thread.sleep(1000);
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		Thread.sleep(2000);
		editQ.toggleRadioButton();
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
		}

		if (editQ.listValueDisplayed("Bloom") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		cnt.clickSave();
		Thread.sleep(4000);
		editQ.questionNotSavedpopUp();
		website = new WebsitePage();
		String parentWindow1 = driver.getWindowHandle();
		editQ.clickContactUslink();
		website.getWebsiteWindow(driver, parentWindow1);
		website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow1);
		editQ.clickCloselink();
		String getQuestionStemtext = editQ
				.getErrormsgQuestionStembox("Question Stem");

		Assert.assertEquals(getQuestionStemtext.toString().trim(),
				"This is a required field.");
		Thread.sleep(2000);
		editQ.modifyQuestionText(expectedQuestiontext);
		Assert.assertTrue(editQ.isErrorDisplayed("Question Stem"));
		ReusableMethods.scrollToBottom(driver);
		Thread.sleep(2000);
		cnt.cancelButton.click();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
