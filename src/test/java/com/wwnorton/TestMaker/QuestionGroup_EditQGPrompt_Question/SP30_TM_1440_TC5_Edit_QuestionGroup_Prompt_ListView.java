package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP30_TM_1440_TC5_Edit_QuestionGroup_Prompt_ListView extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP30_TM-1440_TC5_User creates new test in list view, navigates to Question Group prompt by clicking on Edit Prompt button and verifies the details are displayed correctly in List View.")
	@Stories("SP30_TM-1440_TC5_User creates new test in list view, navigates to Question Group prompt by clicking on Edit Prompt button and verifies the details are displayed correctly in List View.")
	@Test()
	public void editquestionGroupEditPromptListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		/*boolean isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
		System.out.println(isQuestion);
		if (isQuestion == true) {*/
			buildTest.clickAddquestiongroupQuestions(0,"MULTIPLE CHOICE");
		
		// Click List View tab.
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		buildTest.clickBuildTestTabs("List");
		// Verify Edit Prompt and Remove All Labels
		js = (JavascriptExecutor) driver;
		WebElement listviewButton = driver
				.findElement(By
						.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
		js.executeScript("arguments[0].scrollIntoView(true);", listviewButton);
		js.executeScript("arguments[0].click();", listviewButton);
		Thread.sleep(2000);
		buildTest.getRemoveAllLabel();
		editQ = new EditQuestions();
		Assert.assertTrue(editQ.editPromptbutton.isDisplayed());

		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ.clickEditPromptButton();
		editQ.editorQuestionDisclaimer();
		editQ.editorLabelSections("Question Prompt*");
		// Click Tab Key and Validate the focus
		Thread.sleep(3000);
		editQ.clickTabKey();
		editQ.editorMenuOptions();
		editQ.saveButtonState();
		cnt.clickCancelButton();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
