package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP30_TM_1441_TC5_EditquestionUI_MultipleChoiceGroupQType extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP30_TM-1441_TC5_Log into Testmaker application and verify Edit question UI and default text for Multi Choice Group question type")
	@Stories("SP30_TM-1441_TC5_Log into Testmaker application and verify Edit question UI and default text for Multi Choice Group question type")
	@Test()
	public void editquestionUIMultipleChoiceGroupQType() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		/*boolean isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
		System.out.println(isQuestion);
		if (isQuestion == true) {*/
			buildTest.clickAddquestiongroupQuestions(0,"MULTIPLE CHOICE");
	//	}
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		editQ = new EditQuestions();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		List<String> qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();

		LogUtil.log(qids.size());
		js = (JavascriptExecutor) driver;
		for (int i = 0; i < qids.size(); i++) {
			// String qid = qids.get(i).toString();
			List<WebElement> editQbuttons = driver
					.findElements(By
							.xpath("//div/button[starts-with(@id,'__input__button__Edit__Question__')][contains(text(),'Edit Question')]"));
			for (int j = 0; j < editQbuttons.size();) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							editQbuttons.get(0));
					js.executeScript("arguments[0].click();",
							editQbuttons.get(0));
				}
				break;

			}
		}
		Thread.sleep(1000);
		// editQ.clickEditQuestion();
		String disclaimerText = editQ.editorQuestionDisclaimer();
		String actualText = "How Editing a Question Works:\nIn your exam, saving this edited question will replace the original question.\nThis edited question and the original question will both appear in the test bank.";
		Assert.assertEquals(actualText.trim(), disclaimerText.trim());
		// Verify UI part
		Assert.assertTrue(editQ.editorLabelSections("Edit Question"));
		Assert.assertTrue(editQ.editorLabelSections("Question Prompt"));
		Assert.assertTrue(editQ.editorLabelSections("Question Stem*"));
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id='regionEditQuestion']//div/span[text()='Answer Choices*']"));
		Assert.assertTrue(editQ.editorLabelSections("Answer Choices*"));
		Assert.assertTrue(editQ.editorLabelSections("Question Information"));
		// Verify Available buttons on page
		Assert.assertNotNull(cnt.cancelButton, "The Cancel Button is displayed");
		Assert.assertNotNull(cnt.SaveButton, "The Cancel Button is displayed");
		Assert.assertTrue(editQ.saveButtonState(),
				"The Save Button is in Disabled State");
		Thread.sleep(2000);
		cnt.cancelButton.click();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
