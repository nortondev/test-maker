package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1492_TC37_TC38_EditPrompt_Questiongroup_CancellationOverlay
		extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM-1492_TC37_TC38 Verify system does not display cancellation overlay and redirects user to Build and List  View when user clicks Testmaker back button without making any edit to group question.")
	@Stories("TM-1492_TC37_TC38 Verify system does not display cancellation overlay and redirects user to Build and List View when user clicks Testmaker back button without making any edit to group question.")
	@Test()
	public void editPromptQuestionGroupBuildandListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		/*
		 * buildTest.clickQuestionTypesButton();
		 * buildTest.selectAllCheckboxesQuestionType();
		 * buildTest.clickApplyButton(); questionCountAfterFilter = buildTest
		 * .getQuestionCountAfterQuestionSearch();
		 * LogUtil.log(questionCountAfterFilter);
		 * buildTest.getQuestionGroupLabelQuestions();
		 */
		//buildTest.clickAddFiltersButton();
		buildTest.clickAddquestiongroupQuestions(0,"");
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		editQ = new EditQuestions();
		// editQ.clickAddNortonQuestion();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		// ReusableMethods.scrollIntoView(driver, editQ.editPromptbutton);
		editQ.clickEditPromptButton();
		Thread.sleep(3000);
		cnt.clickTestMakerBackButton();
		editQ.cancelConfirmationPopUp();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		buildTest.clickBuildTestTabs("List");
		Thread.sleep(3000);
		ReusableMethods.expandQuestionsListView(driver);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ.clickEditPromptButton();
		cnt.clickTestMakerBackButton();
		editQ.cancelConfirmationPopUp();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}
}
