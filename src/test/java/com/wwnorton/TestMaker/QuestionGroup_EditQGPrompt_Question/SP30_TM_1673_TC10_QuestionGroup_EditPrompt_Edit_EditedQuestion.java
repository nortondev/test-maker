package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP30_TM_1673_TC10_QuestionGroup_EditPrompt_Edit_EditedQuestion
		extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber, Diffculty, Bloomstaxonomy;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			BloomstaxonomyAfterSave, IDNumberAfteraddQuestion, getFirstIDValue;
	List<WebElement> moreInfo;
	boolean isQuestion;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP30_TM-1673_TC10_User edits question prompt of a version and verifies that question prompt and number is updated correctly.")
	@Stories("SP30_TM-1673_TC10_User edits question prompt of a version and verifies that question prompt and number is updated correctly.")
	@Test()
	public void editMultipleChoiceQuestionGroupQuestionPromptSecondVersion()
			throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(1);
			buildTest.clickApplyButton();
		}
		ReusableMethods.loadingWaitDisapper(driver);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		ArrayList<String> idNumberQuestionsMetaData = new ArrayList<String>();
		buildTest.selectQuestionGroupQuestions("");
		isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
		LogUtil.log(isQuestion);
		
		if (isQuestion == true) {
			boolean isShowLinkDisplayed = driver
					.findElements(
							By.xpath("//button[starts-with(@id,'__input__button__Show__')]"))
					.size() > 0;
			if (isShowLinkDisplayed == true) {
				WebElement clickShowlink = driver
						.findElement(By
								.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
				clickShowlink.click();
			}
			idNumberQuestionsMetaData = (ArrayList<String>) buildTest
					.getMetadataAssociatedValuesQuestionGroup("ID Number");
			getFirstIDValue = idNumberQuestionsMetaData.get(0);
			LogUtil.log(getFirstIDValue);
			isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
			LogUtil.log(isQuestion);
			if (isQuestion == true) {
				List<WebElement> getQuestionCount = driver
						.findElements(By
								.xpath("//button[starts-with(@class,'outline-none circle width2-5 height2-5 mr2 bg-yellow false undefined outline-none left mt3 cursorPointer webkit-appearance-caret')]"));
				for (int qGQuestion = 0; qGQuestion < getQuestionCount.size();) {
					getQuestionCount.get(0).click();
					break;

				}
			}
		}
		// ISBN having group questions with atleast one version created.
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(questionCountAfterSearchQuestionAfteradd,
				"1 Question");
		editQ = new EditQuestions();
		// editQ.clickAddNortonQuestion();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		// ReusableMethods.scrollIntoView(driver, editQ.editPromptbutton);
		ArrayList<String> idNumberListbeforeSave = new ArrayList<String>();
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(5000);
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			LogUtil.log(IDNumber);
			idNumberListbeforeSave.add(IDNumber);
		}
		LogUtil.log(idNumberListbeforeSave);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		Thread.sleep(1000);
		editQ.clickEditPromptButton();
		Thread.sleep(3000);
		editQ.removeQuestionPrompt();
		/*
		 * actions = new Actions(driver);
		 * actions.sendKeys(Keys.TAB).build().perform();
		 */
		driver.findElement(By.xpath("//p[@class='bg-lightestGray mt2 p1']"))
				.click();
		editQ.updateQuestionPrompt("This is updated data in Question Prompt");
		cnt.clickSave();
		Thread.sleep(3000);
		ArrayList<String> idNumberListafterSave = new ArrayList<String>();
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(2000);
			IDNumberAfterSave = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			idNumberListafterSave.add(IDNumberAfterSave);
		}
		LogUtil.log(idNumberListafterSave);
		String str = idNumberListafterSave.toString();
		String str1 = getFirstIDValue.toString().replaceAll("\\[", "")
				.replaceAll("\\]", "");
		str = str.replaceAll("\\[", "").replaceAll("\\]", "");
		// Get the Second Version of the Question
		String oldVersion = str1;
		String[] splitString = oldVersion.split("E");
		int newVersion = Integer.valueOf(splitString[1]) + 1;
		String completeNewVersion = splitString[0] + newVersion;
		LogUtil.log(completeNewVersion);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {

		PropertiesFile.tearDownTest();

	}

}
