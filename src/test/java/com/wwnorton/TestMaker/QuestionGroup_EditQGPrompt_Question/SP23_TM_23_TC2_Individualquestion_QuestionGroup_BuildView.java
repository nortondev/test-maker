package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReadUIJsonFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP23_TM_23_TC2_Individualquestion_QuestionGroup_BuildView extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;
	String actualcount;

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJsonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Edge") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP23_TM-23_TC2_Log into testmaker application and Add individual question from group question to verify Question Group in Build View")
	@Stories("SP23_TM-23_TC2_Log into testmaker application and Add individual question from group question to verify Question Group in Build View")
	@Test()
	public void questionGroupBuildView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		ReusableMethods.checkPageIsReady(driver);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectmultipleCheckboxesQuestionType();
		buildTest.clickApplyButton();
		
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		int qcountApplyFilter = ReusableMethods.getDigits(getQuestionsCount);
		//Assert.assertTrue(isQuestionGroupDisplayed(),	"Question Group questions are displayed");
		
		buildTest.clickSingleQuestionQG(0,"");
		String getQuestionsCountAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		int qcountAfterAddQue = ReusableMethods
				.getDigits(getQuestionsCountAfteradd);
		int count = (qcountApplyFilter - qcountAfterAddQue);
		if(count==1){
		actualcount = count + " Question";
		}else {
			actualcount = count + " Questions";
		}
		Thread.sleep(5000);
		
		List<String> getUndoText = buildTest.getUndoText();
		for (int i = 0; i < getUndoText.size(); i++) {
			LogUtil.log(getUndoText.get(i));

		}
		buildTest.getQuestionGroupLabel();
		buildTest.getRemoveAllLabel();
		buildTest.validateMoreInfoCollapsedBuildView();
		js = (JavascriptExecutor) driver;
		List<WebElement> moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {

			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(5000);
			buildTest.getMetadataAssociatedValuesBuildTestView("Question Type");
			buildTest.getMetadataAssociatedValuesBuildTestView("ID Number");
			buildTest.getChapterdetailsMetaDatainBuildView();
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(3000);
		}

		// buildTest.removeQuestionBuildView(count);
		buildTest.clickRemoveAlllink();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

	public boolean isQuestionGroupDisplayed() throws InterruptedException {
		actions = new Actions(driver);
		driver.findElement(By.id("regionAside")).click();
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//button[@type='button'][starts-with(text(),'Show')]"));
		for (int ele = 0; ele < getQuestionText.size(); ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_UP).build().perform();

		}
		return true;
	}
}
