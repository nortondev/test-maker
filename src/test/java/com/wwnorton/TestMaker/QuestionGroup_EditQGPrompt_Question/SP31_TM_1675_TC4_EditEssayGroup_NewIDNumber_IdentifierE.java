package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1675_TC4_EditEssayGroup_NewIDNumber_IdentifierE extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber, Diffculty, Bloomstaxonomy;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			BloomstaxonomyAfterSave, IDNumberAfteraddQuestion;
	List<WebElement> moreInfo;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1675_TC4_Log into Testmaker application and edit any Norton Essay Group question to verify Application assign new ID Number with updated identifier E and number of edited question")
	@Stories("SP31_TM-1675_TC4_Log into Testmaker application and edit any Norton Essay Group question to verify Application assign new ID Number with updated identifier E and number of edited question")
	@Test()
	public void editEssayGroupQ_IdentifierE() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("ESSAY");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		ReusableMethods.loadingWaitDisapper(driver);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		buildTest.clickSingleQuestionQG(0,"ESSAY");
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(questionCountAfterSearchQuestionAfteradd,
				"1 Question");
		editQ = new EditQuestions();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		// ReusableMethods.scrollIntoView(driver, editQ.editPromptbutton);
		ArrayList<String> idNumberListbeforeSave = new ArrayList<String>();
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(5000);
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			LogUtil.log(IDNumber);
			idNumberListbeforeSave.add(IDNumber);
		}
		LogUtil.log(idNumberListbeforeSave);
		ReusableMethods.scrollToElement(driver, By.xpath("//button[starts-with(@id,'__input__button__Edit__Question__')][contains(text(),'Edit Question')]"));
		Thread.sleep(1000);
		editQ.clickEditQuestion();
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		/*actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();*/
		Thread.sleep(2000);
		editQ.modifyQuestionText(expectedQuestiontext);
		Thread.sleep(5000);
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
		}

		if (editQ.listValueDisplayed("Bloom") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		Thread.sleep(2000);
		cnt.clickSave();
		Thread.sleep(3000);
		ArrayList<String> idNumberListafterSave = new ArrayList<String>();
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(2000);
			IDNumberAfterSave = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			idNumberListafterSave.add(IDNumberAfterSave);
		}
		System.out.println(idNumberListafterSave);
		String str = idNumberListafterSave.toString();
		str = str.replaceAll("\\[", "").replaceAll("\\]", "");

		boolean containsEText = str.contains("E");
		Assert.assertTrue(containsEText,
				" The Added Question contains letter E");
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
