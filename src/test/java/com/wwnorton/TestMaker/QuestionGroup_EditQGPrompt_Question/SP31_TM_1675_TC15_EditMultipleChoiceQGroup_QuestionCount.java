package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1675_TC15_EditMultipleChoiceQGroup_QuestionCount extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	WebsitePage website;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter,
			yourquestionCountAfterFilter, filterName, buildTestName;
	String questionType, IDNumber;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			getFirstIDValue, completeNewVersion, newEditedNumber;
	List<WebElement> moreInfo;
	boolean isQuestion, isdisplayed;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1675_TC15_Log into Testmaker application and edit multi choice group question from Test and verify Application keep the Edited question to Test and Move Norton Original question back to Search result and doesn't update Question count of Test but update the Test bank total question count")
	@Stories("SP31_TM-1675_TC15_Log into Testmaker application and edit multi choice group question from Test and verify Application keep the Edited question to Test and Move Norton Original question back to Search result and doesn't update Question count of Test but update the Test bank total question count")
	@Test()
	public void editMultipleChoiceQuestionGroup_CompareQuestionCount()
			throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(1);
			buildTest.clickApplyButton();
		}
		ReusableMethods.loadingWaitDisapper(driver);
		yourquestionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		String regex1 = "[^\\d]+";
		String[] str1 = yourquestionCountAfterFilter.split(regex1);
		int yourquestionCount = Integer.parseInt(str1[0]);

		buildTest.clickClearFiltersButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(0);
		buildTest.clickApplyButton();

		ReusableMethods.loadingWaitDisapper(driver);
		/*questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();*/

		buildTest.clickAddquestiongroupQuestions(0,"MULTIPLE CHOICE");

		String questionCountAfterQGAdded = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterQGAdded);
		editQ = new EditQuestions();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		// ReusableMethods.scrollIntoView(driver, editQ.editPromptbutton);
		ArrayList<String> idNumberListbeforeSave = new ArrayList<String>();
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(5000);
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			LogUtil.log(IDNumber);
			idNumberListbeforeSave.add(IDNumber);
		}
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		Thread.sleep(1000);
		editQ.clickEditPromptButton();
		Thread.sleep(3000);
		editQ.removeQuestionPrompt();
		/*
		 * actions = new Actions(driver);
		 * actions.sendKeys(Keys.TAB).build().perform();
		 */
		driver.findElement(By.xpath("//p[@class='bg-lightestGray mt2 p1']"))
				.click();
		editQ.updateQuestionPrompt("This is updated data in Question Prompt");
		
		cnt.clickSave();
		Thread.sleep(3000);
		ArrayList<String> idNumberListafterSave = new ArrayList<String>();
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			IDNumberAfterSave = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			idNumberListafterSave.add(IDNumberAfterSave);
		}
		Assert.assertNotEquals(idNumberListafterSave, idNumberListbeforeSave,
				"The ID Number Version is updated");
		/*String questionCountAfterEdit = buildTest
				.getQuestionCountAfterQuestionSearch();*/
		/*Assert.assertEquals(questionCountAfterEdit, questionCountAfterFilter);*/
		buildTest.clickClearFiltersButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(1);
		buildTest.clickApplyButton();
		buildTest.selectQuestionGroupQuestions("MULTIPLE CHOICE");
		ReusableMethods.loadingWaitDisapper(driver);
		String questionCountAfterclearFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		String regex2 = "[^\\d]+";
		String[] str2 = questionCountAfterclearFilter.split(regex2);
		int questionCount = Integer.parseInt(str2[0]);
		questionCount = yourquestionCount + 1;
		Assert.assertEquals(questionCount, yourquestionCount + 1);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}

}
