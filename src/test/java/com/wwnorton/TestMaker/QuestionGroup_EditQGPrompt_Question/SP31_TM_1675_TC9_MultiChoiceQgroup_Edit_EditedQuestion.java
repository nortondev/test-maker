package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1675_TC9_MultiChoiceQgroup_Edit_EditedQuestion extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			getFirstIDValue, completeNewVersion, newEditedNumber;
	List<WebElement> moreInfo;
	boolean isQuestion;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1675_TC9_Log into Testmaker application and edit User Multi Choice group question in another test to verify Application assign new ID Number with updated identifier E and number of edited question")
	@Stories("SP31_TM-1675_TC9_Log into Testmaker application and edit User Multi Choice group question in another test to verify Application assign new ID Number with updated identifier E and number of edited question")
	@Test()
	public void editMultipleChoiceQuestionGroup_Identifier_E() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(1);
			buildTest.clickApplyButton();
		}
		ReusableMethods.loadingWaitDisapper(driver);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
        buildTest.selectQuestionGroupQuestions("MULTIPLE CHOICE");
		isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
		LogUtil.log(isQuestion);
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]"));
		ArrayList<String> idNumberQuestionsMetaData = new ArrayList<String>();
		if (isQuestion == true) {
			boolean isShowLinkDisplayed = driver
					.findElements(
							By.xpath("//button[starts-with(@id,'__input__button__Show__')]"))
					.size() > 0;
			if (isShowLinkDisplayed == true) {
				WebElement clickShowlink = driver
						.findElement(By
								.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
				clickShowlink.click();
			}
			idNumberQuestionsMetaData = (ArrayList<String>) buildTest
					.getMetadataAssociatedValuesQuestionGroup("ID Number");
			getFirstIDValue = idNumberQuestionsMetaData.get(0);
			LogUtil.log(getFirstIDValue);
			// ISBN having group questions with atleast one version created.
			if (getFirstIDValue.contains("E")) {
				buildTest.clickSingleQuestionQG(0,"MULTIPLE CHOICE");

				String questionCountAfterSearchQuestionAfteradd = buildTest
						.verifyQuestioninTestBuildSearchResult();
				LogUtil.log(questionCountAfterSearchQuestionAfteradd);
				editQ = new EditQuestions();
				// editQ.clickAddNortonQuestion();
				ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
				// ReusableMethods.scrollIntoView(driver,
				// editQ.editPromptbutton);
				ArrayList<String> idNumberListbeforeSave = new ArrayList<String>();
				js = (JavascriptExecutor) driver;
				moreInfo = driver.findElements(By
						.xpath("//button[contains(text(),'More Info')]"));
				for (int j = 0; j < moreInfo.size(); j++) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(5000);
					IDNumber = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					LogUtil.log(IDNumber);
					idNumberListbeforeSave.add(IDNumber);
				}
				LogUtil.log(idNumberListbeforeSave);
				ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
				Thread.sleep(1000);
				editQ.clickEditQuestion();
				ReusableMethods.checkPageIsReady(driver);
				;
				Assert.assertTrue(editQ.saveButtonState(),
						"Save Button is Disabled State");
				editQ.selectAnswerChoiceRadioButton();
				ReusableMethods.checkPageIsReady(driver);
				if (editQ.listValueDisplayed("Difficulty") == false) {
					ReusableMethods.scrollToBottom(driver);
					editQ.deleteListValue("Difficulty");
					editQ.changeListValue("Difficulty");
				}
				ReusableMethods.scrollToBottom(driver);
				if (editQ.listValueDisplayed("Bloom") == false) {
					ReusableMethods.scrollToBottom(driver);
					editQ.deleteListValue("Bloom");
					editQ.changeListValue("Bloom");
				}
				try {
					
					cnt.clickSave();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Thread.sleep(4000);
				ArrayList<String> idNumberListafterSave = new ArrayList<String>();
				moreInfo = driver.findElements(By
						.xpath("//button[contains(text(),'More Info')]"));
				for (int j = 0; j < moreInfo.size(); j++) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(2000);
					IDNumberAfterSave = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					idNumberListafterSave.add(IDNumberAfterSave);
				}
				LogUtil.log(idNumberListafterSave);
				newEditedNumber = idNumberListafterSave.toString();
				String str1 = getFirstIDValue.toString().replaceAll("\\[", "")
						.replaceAll("\\]", "");
				newEditedNumber = newEditedNumber.replaceAll("\\[", "")
						.replaceAll("\\]", "");
				// Get the Second Version of the Question
				String oldVersion = str1;
				String[] splitString = oldVersion.split("E");
				int newVersion = Integer.valueOf(splitString[1]) + 1;
				completeNewVersion = splitString[0] + newVersion;
				LogUtil.log(completeNewVersion);
			}
		}
		Thread.sleep(5000);
		isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
		LogUtil.log(isQuestion);
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[starts-with(@class,'ml1-5 mr3')]/div[contains(./text(),'Question Group')]"));
		ArrayList<String> idNumberQuestionsMetaDataAfterEdit = new ArrayList<String>();
		if (isQuestion == true) {
			ReusableMethods
					.scrollToElement(
							driver,
							By.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
			boolean isShowLinkDisplayed = driver
					.findElements(
							By.xpath("//button[starts-with(@id,'__input__button__Show__')]"))
					.size() > 0;
			if (isShowLinkDisplayed == true) {
				WebElement clickShowlink = driver
						.findElement(By
								.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
				clickShowlink.click();
			}
			idNumberQuestionsMetaDataAfterEdit = (ArrayList<String>) buildTest
					.getMetadataAssociatedValuesQuestionGroup("ID Number");
			String getFirstIDValueedit = idNumberQuestionsMetaDataAfterEdit
					.get(0);
			String[] splitStringedited = newEditedNumber.split("E");
			int editedVersion = Integer.valueOf(splitStringedited[0]) + 1;
			int editedVersiongetE = Integer.valueOf(splitStringedited[1]);
			int idNumberValue = Integer.valueOf(String.valueOf(editedVersion)
					+ String.valueOf(editedVersiongetE));
			String IdNumber = String.valueOf(idNumberValue);
			String idNumberSeries = ReusableMethods.addChar(IdNumber, 'E', 5);
			Assert.assertEquals(getFirstIDValueedit, idNumberSeries);

		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
