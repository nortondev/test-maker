package com.wwnorton.TestMaker.QuestionGroup_EditQGPrompt_Question;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReadUIJsonFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP24_TM_1184_TC3_QuestionGroupDragDropListView extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonObj = readJsonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP24_TM-1184_TC3_Instructor selects a question group questions, drags and drops in middle of list and verifies that question number and details are displayed correctly.")
	@Stories("SP24_TM-1184_TC3_Instructor selects a question group questions, drags and drops in middle of list and verifies that question number and details are displayed correctly.")
	@Test()
	public void questionGroupListViewDragDrop() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
	
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}
		
		/*buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.checkChapterCheckbox("CHAPTER 43—GROUP QUESTION FOR ALL TYPES OF QUESTIONS");
		buildTest.clickApplyButton();*/
		buildTest.selectQuestionGroupQuestions("");
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		int qcountApplyFilter = ReusableMethods.getDigits(getQuestionsCount);
		buildTest.getQuestionGroupLabel();
		// click on Show Questions Link
		String getQuestionText = buildTest.clickAddquestiongrouplink();
		int v = ReusableMethods.getDigits(getQuestionText);
		if (v == 0) {
			Assert.assertNull(null, "No record Found");

		}
		if (v == 1) {
			getAddQuestionCount = v;
		} else {
			getAddQuestionCount = GetRandomId.getRandom(v);
		}
		// int questions =GetRandomId.getRandom(v);
		buildTest.clickAddquestiongroupQuestions(v,"");
		buildTest.clickAddQuestionsIcon(getAddQuestionCount);
		String getQuestionsCountAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		int qcountAfterAddQue = ReusableMethods
				.getDigits(getQuestionsCountAfteradd);
		int count = (qcountApplyFilter - qcountAfterAddQue);
		String actualcount = count + " Questions";
		Thread.sleep(5000);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(actualcount.trim(),
				questionCountAfterSearchQuestionAfteradd.trim());
		List<String> getUndoText = buildTest.getUndoText();
		for (int i = 0; i < getUndoText.size(); i++) {
			LogUtil.log(getUndoText.get(i));

		}
		// /Drag and Drop in List view
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		buildTest.clickBuildTestTabs("List");
		// Assert.assertTrue(buildTest.validateQuestionsCollapsedListwMode());
		buildTest.clickQuestionTitleListView(0);
		// Assert.assertFalse(buildTest.validateQuestionsCollapsedListwMode());
		buildTest.clickQuestionTitleListView(0);
		buildTest.dragDropQuestionsTitleListView(1);
		buildTest.questionSequenceafterDragDrop();
		String questionCountAfterSearchQuestionAfteraddListView = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(actualcount.trim(),
				questionCountAfterSearchQuestionAfteraddListView.trim());
		// click undo Button for Question Group and then add question question
		// individually
		buildTest.clickQuestionGroupUndoButton();
		int questionscount = ReusableMethods.getDigits(getQuestionText);
		// int questionscount =GetRandomId.getRandom(getQDigits);
		if (questionscount == 0) {
			Assert.assertNull(null, "No record Found");

		}
		if (questionscount == 1) {
			getAddQuestionCount = questionscount;
		} else {
			getAddQuestionCount = GetRandomId.getRandom(questionscount);
		}
		buildTest.clickAddquestiongroupQuestions(getAddQuestionCount,"");
		String getQuestionsCountAfteradd1 = buildTest
				.getQuestionCountAfterQuestionSearch();
		int qcountAfterAddQue1 = ReusableMethods
				.getDigits(getQuestionsCountAfteradd1);
		int count1 = (qcountApplyFilter - qcountAfterAddQue1);
		String actualcount1 = count1 + " Questions";
		Thread.sleep(5000);
		String questionCountAfterSearchQuestionAfteradd1 = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(actualcount1.trim(),
				questionCountAfterSearchQuestionAfteradd1.trim());
		ReusableMethods.scrollToBottom(driver);
		buildTest.clickQuestionTitleListView(0);
		buildTest.dragDropQuestionsTitleListView(3);
		buildTest.questionSequenceafterDragDrop();
		String questionCountAfterSearchQuestionAfteraddListView1 = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(actualcount1.trim(),
				questionCountAfterSearchQuestionAfteraddListView1.trim());
		buildTest.clickexpandiconQuestiongroup();
		buildTest.clickQuestionTitleListView(0);
		buildTest.dragDropQuestionsTitleListView(4);
		// here checking the sort of Question order.
		buildTest.questionSequenceafterDragDrop();
		String questionCountAfterSearchQuestionAfteraddListView2 = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(actualcount1.trim(),
				questionCountAfterSearchQuestionAfteraddListView2.trim());
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
