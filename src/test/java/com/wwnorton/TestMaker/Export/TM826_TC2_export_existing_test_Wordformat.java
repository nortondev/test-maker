package com.wwnorton.TestMaker.Export;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.ExportWindow;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.readDocFile;

@Listeners({ TestListener.class })
public class TM826_TC2_export_existing_test_Wordformat extends PropertiesFile {

	TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage TMcreateNewTest;
	RegionYourTestsPage TMYourTests;
	BuildTestPage buildTest;
	CreateNewTestPage cntp;
	ExportWindow export;
	String buildTestName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Log into Testmaker application and export existing test in Word format from Your tests page ")
	@Stories("Log into Testmaker application and export existing test in Word format from Your tests page ")
	@Test()
	public void exportTestWordFormat() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest = new BuildTestPage();
		cntp = new CreateNewTestPage();
		cntp.clickCreateNewTest();
		cntp.createNewTestwithTestNameCourseName();
		buildTestName = cntp.getTestName();
		cntp.clickSave();

		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickChaptersButton();
		buildTest.selectCheckboxesChapters(4);
		buildTest.clickApplyButton();
		buildTest.scrollMetadata(driver);
		// LogUtil.log(questionCountAfterSearchQuestion);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int v = Integer.parseInt(str[0]);
		buildTest.clickAddQuestionsIcon(v);
		Thread.sleep(2000);
		buildTest.verifyQuestioninTestBuildSearchResult();
		cntp.clickSave();
		cntp.clickTestMakerBackButton();

		// Selecting Sort as Test Name
		cntp.selectSortByOptionsName("Last Modified");

		LogUtil.log(buildTestName);
		TMYourTests = new RegionYourTestsPage();
		// Click on action button
		TMYourTests.clickActionlink(buildTestName);
		TMYourTests.clickExportOrCopyOrDeletelink("Export");
		export = new ExportWindow();
		export.clickMSwordExportButton();
		// String exceptedText =
		// "Export ready. Download will start automatically...";
		// Assert.assertEquals(exceptedText.trim(),
		// export.getExportText().trim());

		// readDocFile.loadFile(buildTestName);
		Thread.sleep(3000);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	@AfterTest
	public void closeTest() throws Exception {
		readDocFile.deleteFile(buildTestName);
		PropertiesFile.tearDownTest();

	}

}
