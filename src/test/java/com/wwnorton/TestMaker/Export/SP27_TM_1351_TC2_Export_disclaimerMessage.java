package com.wwnorton.TestMaker.Export;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.ExportWindow;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP27_TM_1351_TC2_Export_disclaimerMessage extends PropertiesFile {
	TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;
	RegionYourTestsPage TMYourTests;
	BuildTestPage buildTest;
	CreateNewTestPage cntp;
	ExportWindow export;
	boolean isdisplayed;
	String getQuestionsCount, regex;
	String[] str;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP27_TM-1351_TC2_Log into test maker and Export test from Test Build page to verify application displays  improved disclaimer message only for IMSCC export")
	@Stories("SP27_TM-1351_TC2_Log into test maker and Export test from Test Build page to verify application displays  improved disclaimer message only for IMSCC export")
	@Test()
	public void exportTestIMSCCFormatOriginalEditedQuestions() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest = new BuildTestPage();
		cntp = new CreateNewTestPage();
		cntp.clickCreateNewTest();
		cntp.createNewTestwithTestNameCourseName();
		String buildTestName = cntp.getTestName();
		System.out.println(buildTestName);
		cntp.clickSave();
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
		}
		buildTest.clickApplyButton();
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		str = getQuestionsCount.split(regex);
		int v = Integer.parseInt(str[0]);
		buildTest.clickAddQuestionsIcon(v);
		cntp.clickSave();
		Thread.sleep(2000);
		String questioncount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		String regex1 = "[^\\d]+";
		String[] strcount = questioncount.split(regex1);
		int buildviewQcount = Integer.parseInt(strcount[0]);
		System.out.println(buildviewQcount);
		cntp.clickExportButton();
		export = new ExportWindow();
		// Verify Application does not displays any text message on Export Test
		// popup
		export.selectIMSCCExportButton();
		String imsccexportmessage = export.getimsccmessageText();
		LogUtil.log(imsccexportmessage);
		String parentWindow = driver.getWindowHandle();
		export.clickWWnortonlink();
		// Application open Norton Support page in new tab located at:
		// "https://wwnorton.knowledgeowl.com/help/import-tests-in-learning-management-systems"
		String nortonSupportpage = export.verifyNortonSupportPage(driver,
				parentWindow);
		Assert.assertEquals("Import tests to Learning Management Systems",
				nortonSupportpage.toString().trim());
		driver.close();
		driver.switchTo().window(parentWindow);
		export.clickExportModal();
		Thread.sleep(2000);
		cntp.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cntp.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
