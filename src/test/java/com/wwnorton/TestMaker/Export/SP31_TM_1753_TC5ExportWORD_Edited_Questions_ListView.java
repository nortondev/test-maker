package com.wwnorton.TestMaker.Export;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.ExportWindow;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1753_TC5ExportWORD_Edited_Questions_ListView extends
		PropertiesFile {

	TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage TMcreateNewTest;
	RegionYourTestsPage TMYourTests;
	BuildTestPage buildTest;
	CreateNewTestPage cntp;
	ExportWindow export;
	boolean isdisplayed;
	String getQuestionsCount, regex;
	String[] str;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1753_TC5_User verifies the test data is exported correctly in word document from Your Tests when test is having all edited questions")
	@Stories("SP31_TM-1753_TC5_User verifies the test data is exported correctly in word document from Your Tests when test is having all edited questions")
	@Test()
	public void exportTestWORDFormatEditedQuestionsListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest = new BuildTestPage();
		cntp = new CreateNewTestPage();
		cntp.clickCreateNewTest();
		cntp.createNewTestwithTestNameCourseName();
		String buildTestName = cntp.getTestName();
		LogUtil.log(buildTestName);
		cntp.clickSave();
		//ReusableMethods.checkPageIsReady(driver);
		buildTest.clickAddFiltersButton();
		isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(1);

		}
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.selectCheckboxesChapters(3);
		buildTest.clickApplyButton();
		
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		str = getQuestionsCount.split(regex);
		int v = Integer.parseInt(str[0]);
		buildTest.clickAddQuestionsIcon(v);
		cntp.clickSave();
		// Add Edited Question

		Thread.sleep(2000);
		String questioncount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		String regex1 = "[^\\d]+";
		String[] strcount = questioncount.split(regex1);
		int buildviewQcount = Integer.parseInt(strcount[0]);
		LogUtil.log(buildviewQcount);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		buildTest.clickBuildTestTabs("List");

		cntp.clickExportButton();
		export = new ExportWindow();
		export.clickMSwordExportButton();

		String exceptedText = "Export ready. Download will start automatically...";
		ReusableMethods.waitTillTextPresent(driver,
				By.xpath("//div[@class='white h4 break-word css-0'][contains(.,'Export ready. Download will start automatically...')]"),
				exceptedText);
		Assert.assertEquals(exceptedText.trim(), export.getExportText().trim());
		Thread.sleep(20000);
		Thread.sleep(2000);
		cntp.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cntp.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
		// readDocFile.deleteFile(buildTestName);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
