package com.wwnorton.TestMaker.Export;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.LMS.LMS_TM_Integration.LMSlogin_Instructor_Test;
import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.ExportWindow;
import com.wwnorton.TestMaker.ObjectFactories.LMSCANVAS;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.readDocFile;

@Listeners({ TestListener.class })
public class SP31_TM_1754_TC5ExportIMSCC_Edited_Questions extends
		PropertiesFile {

	TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage TMcreateNewTest;
	RegionYourTestsPage TMYourTests;
	BuildTestPage buildTest;
	CreateNewTestPage cntp;
	ExportWindow export;
	boolean isdisplayed;
	String getQuestionsCount, regex;
	String[] str;
	LMSlogin_Instructor_Test lit;
	LMSCANVAS lmscanvas;
	String buildTestName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1754_TC5_User verifies the test data is exported correctly in IMSCC document from Your Tests when test is having edited questions")
	@Stories("SP31_TM-1754_TC5_User verifies the test data is exported correctly in IMSCC document from Your Tests when test is having edited questions")
	@Test()
	public void exportTestIMSCCFormatEditedQuestionsBuildView()
			throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest = new BuildTestPage();
		cntp = new CreateNewTestPage();
		cntp.clickCreateNewTest();
		cntp.createNewTestwithTestNameCourseName();
		buildTestName = cntp.getTestName();
		System.out.println(buildTestName);
		cntp.clickSave();
		buildTest.clickAddFiltersButton();
		isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(1);

		}
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.selectCheckboxesChapters(3);
		buildTest.clickApplyButton();
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		str = getQuestionsCount.split(regex);
		int v = Integer.parseInt(str[0]);
		buildTest.clickAddQuestionsIcon(v);
		cntp.clickSave();
		// Add Edited Question

		Thread.sleep(2000);
		String questioncount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		String regex1 = "[^\\d]+";
		String[] strcount = questioncount.split(regex1);
		int buildviewQcount = Integer.parseInt(strcount[0]);
		System.out.println(buildviewQcount);
		cntp.clickExportButton();
		export = new ExportWindow();
		export.clickIMSCCExportButton();

		String exceptedText = "Export ready. Download will start automatically...";
		ReusableMethods.waitTillTextPresent(driver,
				By.xpath("//div[@class='white h4 break-word css-0']"),
				exceptedText);
		Assert.assertEquals(exceptedText.trim(), export.getExportText().trim());
		Thread.sleep(15000);
		cntp.clickTestMakerBackButton();
		String filepath = System.getProperty("user.dir") + "\\Downloads\\"
				+ buildTestName + ".IMSCC";

		System.out.println(filepath);
		// Call LMS application and upload the IMSCC File
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(
				driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		lit = new LMSlogin_Instructor_Test();
		lit.callPropertiesFile();
		lit.Login_LMS();
		Thread.sleep(2000);
		lit.uploadFile(filepath);
		// ReusableMethods.fileUpload(filepath);
		Thread.sleep(2000);
		lit.selectContentType();
		lit.clickImportbutton();
		Thread.sleep(5000);
		lit.getStatus();
		lit.clickAssignmentButton();
		Thread.sleep(2000);
		WebElement clickAssignmentLink = driver.findElement(By
				.xpath("//div[@class='ig-info']/a[normalize-space(text())='"
						+ buildTestName + "']"));
		clickAssignmentLink.click();
		lit.clickPreviewQuizButton();
		Thread.sleep(2000);
		int count = lit.getQuestionscount();
		System.out.println(count);
		Assert.assertEquals(buildviewQcount, count);
		driver.switchTo().window(tabs.get(0));
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	@AfterTest
	public void closeTest() throws Exception {
		readDocFile.deleteFile(buildTestName);
		PropertiesFile.tearDownTest();

	}
}
