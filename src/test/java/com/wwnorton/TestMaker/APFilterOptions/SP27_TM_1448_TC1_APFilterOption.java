package com.wwnorton.TestMaker.APFilterOptions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP27_TM_1448_TC1_APFilterOption extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	getPOS POS = new getPOS();
	JavascriptExecutor js;
	String apLearningObjNames, apLoNames, apReasoningProcess, apTopic, apskill,apPractice;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP27_TM-1448_TC1_Log into testmaker application and verify all AP metadata filter options in filter panel and perform action for each AP filter option")
	@Stories("SP27_TM-1448_TC1_Log into testmaker application and verify all AP metadata filter options in filter panel and perform action for each AP filter option")
	@Test()
	public void apFilterOption() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		boolean apFilter = buildTest.apFilter();
		Assert.assertTrue(apFilter);
		List<String> getAPfilterPanel = buildTest
				.getapLearningObjectiveFilterPanel();
		Assert.assertEquals(ReusableMethods.apFilterPanelList(),
				getAPfilterPanel);
		for (int ap = 0; ap < getAPfilterPanel.size(); ap++) {
			LogUtil.log(getAPfilterPanel.get(ap));

		}
		// Click the chapters checkboxes and verify AP LO
		List<String> apLONames = buildTest.apLearningObjectiveFilter();

		for (int i = 0; i < apLONames.size(); i++) {
			apLearningObjNames = apLONames.get(i).toString();
			LogUtil.log(apLearningObjNames);

		}
		// click apply button
		ReusableMethods.scrollIntoView(driver, buildTest.ApplyButton);
		buildTest.clickApplyButton();
		// get the Question Count
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
	
		List<String> apLoMetadataNames = buildTest
				.getApLearningObjective("AP Learning Objective");

		for (int i = 0; i < apLoMetadataNames.size(); i++) {
			apLoNames = apLoMetadataNames.get(i).toString();
			LogUtil.log(apLoMetadataNames);
			Assert.assertNotNull(apLoNames);
		}
		List<String> apReasoningProcesslist = buildTest
				.getApLearningObjective("AP Reasoning Process");
		for (int i = 0; i < apReasoningProcesslist.size(); i++) {
			apReasoningProcess = apReasoningProcesslist.get(i).toString();
			LogUtil.log(apReasoningProcess);
			Assert.assertNotNull(apReasoningProcess);
		}
		buildTest.clickClearFiltersButton();
		// buildTest.clickFiltersButton("AP Topics");
		buildTest.clickAddFiltersButton();
		List<String> apTopics = buildTest.selectAPFilter("AP Topics");
		for (int i = 0; i < apTopics.size(); i++) {
			apLoNames = apTopics.get(i).toString();
		}
		LogUtil.log(apTopics);
		System.out.println(apTopics);
		Assert.assertNotNull(apLoNames);
		// click apply button
		ReusableMethods.scrollIntoView(driver, buildTest.ApplyButton);
		buildTest.clickApplyButton();
		List<String> apTopicMetaDataList = buildTest
				.getApLearningObjective("AP Topic");
		for (int i = 0; i < apTopicMetaDataList.size(); i++) {
			apTopic = apTopicMetaDataList.get(i).toString();
			LogUtil.log(apTopic);
			Assert.assertNotNull(apTopic);
		}

		buildTest.clickClearFiltersButton();
		// AP Reasoning Process
		buildTest.clickAddFiltersButton();
		List<String> apReasProcess = buildTest
				.selectAPFilter("AP Reasoning Process");
		for (int i = 0; i < apReasProcess.size(); i++) {
			apLoNames = apReasProcess.get(i).toString();
		}
		LogUtil.log(apReasProcess);
		System.out.println(apReasProcess);
		Assert.assertNotNull(apLoNames);
		// click apply button
		ReusableMethods.scrollIntoView(driver, buildTest.ApplyButton);
		buildTest.clickApplyButton();
		List<String> apReasoningProcessMetaDataList = buildTest
				.getApLearningObjective("AP Reasoning Process");
		for (int i = 0; i < apReasoningProcessMetaDataList.size(); i++) {
			apReasoningProcess = apReasoningProcessMetaDataList.get(i)
					.toString();
			LogUtil.log(apReasoningProcess);
			Assert.assertNotNull(apReasoningProcess);
		}
		buildTest.clickClearFiltersButton();

		// AP Skills
		buildTest.clickAddFiltersButton();
		List<String> apskills = buildTest.selectAPFilter("AP Skills");
		for (int i = 0; i < apskills.size(); i++) {
			apLoNames = apskills.get(i).toString();
		}
		LogUtil.log(apskills);
		System.out.println(apskills);
		Assert.assertNotNull(apLoNames);
		// click apply button
		ReusableMethods.scrollIntoView(driver, buildTest.ApplyButton);
		buildTest.clickApplyButton();
		List<String> apSkillsMetaDataList = buildTest
				.getApLearningObjective("AP Skill");
		for (int i = 0; i < apSkillsMetaDataList.size(); i++) {
			apskill = apSkillsMetaDataList.get(i).toString();
			LogUtil.log(apskill);
			Assert.assertNotNull(apskill);
		}
		buildTest.clickClearFiltersButton();

		// AP Practice
		buildTest.clickAddFiltersButton();
		List<String> apPraticeList = buildTest.selectAPFilter("AP Practices");
		for (int i = 0; i < apPraticeList.size(); i++) {
			apLoNames = apPraticeList.get(i).toString();
		}
		LogUtil.log(apPraticeList);
		System.out.println(apPraticeList);
		Assert.assertNotNull(apLoNames);
		// click apply button
		ReusableMethods.scrollIntoView(driver, buildTest.ApplyButton);
		buildTest.clickApplyButton();
		List<String> apPracticesMetaDataList = buildTest
				.getApLearningObjective("AP Practice");
		for (int i = 0; i < apPracticesMetaDataList.size(); i++) {
			apPractice = apPracticesMetaDataList.get(i).toString();
			LogUtil.log(apPractice);
			Assert.assertNotNull(apPractice);
		}
		buildTest.clickClearFiltersButton();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
