package com.wwnorton.TestMaker.UndoandRemoveQuestionsfeatures;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_275_TC1_AS96_Undofeature extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	getPOS POS = new getPOS();
	String selectedChapterName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Undo functionality for added question on Build Test page")
	@Stories("Verify Undo functionality for added question on Build Test page")
	@Test()
	public void undoFeature() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		//TMlogin.appendISBN(ReusableMethods.nonQuestionGroupISBNreadJson());
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		Thread.sleep(3000);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			buildTest.clickChaptersButton();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.checkPageIsReady(driver);
		int getChapterCount = buildTest.getchaptersFilterscount();

		if (getChapterCount == 0) {
			Assert.assertNull(getChapterCount);
		} else if (getChapterCount <= 1) {
			selectedChapterName = buildTest
					.clickChaptersCheckboxesandChaptersName(getChapterCount);
			LogUtil.log(selectedChapterName);
		} else {
			int checkboxes = GetRandomId.getRandom(getChapterCount);

			List<String> selectedChaptersName = buildTest
					.clickMultipleChaptersCheckboxesandChaptersName(checkboxes);
			LogUtil.log(selectedChaptersName);

		}

		buildTest.clickApplyButton();
		buildTest.scrollMetadata20Question(driver);
		// LogUtil.log(questionCountAfterSearchQuestion);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		String regex = "[^\\d]+";
		String[] str = getQuestionsCount.split(regex);
		int v = Integer.parseInt(str[0]);
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);
		System.out.println(v);
		//isQuestionGroupDisplayed();

		Thread.sleep(3000);
		buildTest.clickAddQuestionsIcon(6);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);

		List<String> getQuestionAddedText = buildTest.getQuestionAddedText();
		for (int i = 0; i < getQuestionAddedText.size(); i++) {
			LogUtil.log(getQuestionAddedText.get(i));

		}
		List<String> getUndoText = buildTest.getUndoText();
		for (int i = 0; i < getUndoText.size(); i++) {
			LogUtil.log(getUndoText.get(i));

		}

		Thread.sleep(5000);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Thread.sleep(5000);
		Assert.assertEquals("Question 6", buildTest.verifyQuestioninTestBuild());
		Thread.sleep(5000);
		buildTest.clickUndoButton(0);
		Thread.sleep(2000);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals("Question 5", buildTest.verifyQuestioninTestBuild());
		Thread.sleep(2000);
		buildTest.clickAddQuestionsIcon(1);
		List<String> getUndoTextagain = buildTest.getUndoText();
		for (int i = 0; i < getUndoTextagain.size(); i++) {
			LogUtil.log(getUndoTextagain.get(i));

		}
		Assert.assertEquals(getUndoText, getUndoTextagain);
		Thread.sleep(2000);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals("Question 6", buildTest.verifyQuestioninTestBuild());
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);

		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();

		String selectedChapterName1 = buildTest
				.clickChaptersCheckboxesandChaptersName(2);
		LogUtil.log(selectedChapterName1);
		buildTest.clickApplyButton();
		String questionCountAfterSearch = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearch);
		buildTest.scrollMetadata20Question(driver);
		buildTest.clickAddQuestionsIcon(3);
		String questionCountAfterSearchAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchAfteradd);

		List<String> getQuestionAddedText2 = buildTest.getQuestionAddedText();
		for (int i = 0; i < getQuestionAddedText2.size(); i++) {
			LogUtil.log(getQuestionAddedText2.get(i));

		}
		List<String> getUndoText2 = buildTest.getUndoText();
		for (int i = 0; i < getUndoText2.size(); i++) {
			LogUtil.log(getUndoText2.get(i));

		}

		Thread.sleep(2000);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals("Question 9", buildTest.verifyQuestioninTestBuild());
		Thread.sleep(2000);
		buildTest.clickUndoButton(0);
		Thread.sleep(2000);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals("Question 8", buildTest.verifyQuestioninTestBuild());
		Thread.sleep(2000);
		buildTest.clickAddQuestionsIcon(1);
		List<String> getUndoTextagain2 = buildTest.getUndoText();
		for (int i = 0; i < getUndoTextagain2.size(); i++) {
			LogUtil.log(getUndoTextagain2.get(i));

		}
		Assert.assertEquals(getUndoText2, getUndoTextagain2);
		Thread.sleep(2000);
		buildTest.verifyQuestioninTestBuildSearchResult();
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		Assert.assertEquals("Question 9", buildTest.verifyQuestioninTestBuild());
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

	public boolean isQuestionGroupDisplayed() throws InterruptedException {
		driver = getDriver();
		actions = new Actions(driver);
		driver.findElement(By.id("regionAside")).click();
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//button[@type='button'][starts-with(text(),'Show')]"));
		for (int ele = 0; ele < getQuestionText.size(); ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_UP).build().perform();

		}
		return true;
	}
}
