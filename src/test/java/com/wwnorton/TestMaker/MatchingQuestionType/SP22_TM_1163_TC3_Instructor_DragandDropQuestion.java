package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP22_TM_1163_TC3_Instructor_DragandDropQuestion extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	getPOS POS = new getPOS();
	JavascriptExecutor js;
	int i;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP22_TM_1163_TC3_Instructor selects a question, drags and drops in middle of list and verifies that question number and details are displayed correctly.")
	@Stories("SP22_TM_1163_TC3_Instructor selects a question, drags and drops in middle of list and verifies that question number and details are displayed correctly.")
	@Test()
	public void matchingQuestionTypeDragDropListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Types");
		Assert.assertNotNull(buildTest.getCountSelectedFilter("Question Types"));

		List<String> childFilterNames = buildTest.getFilterOptions();
		for (int i = 0; i < childFilterNames.size(); i++) {
			
			Assert.assertNotNull(childFilterNames.get(i));
		}

		buildTest.selectQuestionTypeCheckbox("MATCHING");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);
		System.out.println(getAddQuestionCount);
		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddQuestionsIcon(3);
		}
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		String[] getQuestionCount = questionCountAfterSearchQuestionAfteradd
				.split(regex);
		int questionCountinBuildView = Integer.parseInt(getQuestionCount[0]);
		LogUtil.log(questionCountinBuildView);

		// /Drag and Drop in List view
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		buildTest.clickBuildTestTabs("List");
		// Assert.assertTrue(buildTest.validateQuestionsCollapsedListwMode());
		// buildTest.clickQuestionTitleListView(0);
		// Assert.assertFalse(buildTest.validateQuestionsCollapsedListwMode());
		// buildTest.clickQuestionTitleListView(0);
		buildTest.dragDropQuestionsTitleListView(1);
		buildTest.questionSequenceafterDragDrop();
		String questionCountinListView = buildTest
				.verifyQuestioninTestBuildSearchResult();
		String[] getQuestionCountListView = questionCountinListView
				.split(regex);
		int questionCountList = Integer.parseInt(getQuestionCount[0]);
		LogUtil.log(getQuestionCountListView);
		// Verify total number of questions is displayed correctly
		Assert.assertEquals(questionCountinBuildView, questionCountList);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
