package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

import edu.emory.mathcs.backport.java.util.Collections;

public class SP41_TM_1431_TC8_DeleteLetteredOptionMatchingQuestion extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	String buildTestName, testName;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	List<WebElement> moreInfo;
	String questionType, IDNumber, Difficulty, Bloomstaxonomy;
	String questionTypeAftercancel, IDNumberAftercancel, DiffcultyAftercancel,
	BloomstaxonomyAftercancel;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP41 TM-1431_TC8_User deletes a lettered option and verifies that the deleted option is not displayed for numbered question and remaining options are arranged correctly in alphabetical order.")
	@Stories("SP41 TM-1431_TC8_User deletes a lettered option and verifies that the deleted option is not displayed for numbered question and remaining options are arranged correctly in alphabetical order.")
	@Test()
	public void deleteLetteredOptionMatchingQuestion() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		Thread.sleep(5000);
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		try {
			buildTest.clickAddFiltersButton();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		//
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
	
		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);
		
		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddMatchingQuestionsIcon(1);
		}
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		editQ = new EditQuestions();
		List<String> buildViewletteredOptionList =editQ.getmoreinfoOptionsList();
		LogUtil.log(buildViewletteredOptionList);
		System.out.println(buildViewletteredOptionList);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		
		editQ.deleteLetteredOptions("B.");
		Thread.sleep(5000);		
		List<String> letteredOptionList = editQ.getseqLetteredOptions();
		Collections.sort(letteredOptionList);
		editQ.getblankValueNumberedOptions();
		if(editQ.listValueDisplayed("Difficulty")==false){
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		}
		ReusableMethods.scrollToBottom(driver);
		if (editQ.listValueDisplayed("Bloom") == false) {
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		
		cnt.clickSave();
		Thread.sleep(5000);
		List<String> buildViewletteredOptionListAfterSave =editQ.getmoreinfoOptionsList();
		LogUtil.log(buildViewletteredOptionListAfterSave);
		System.out.println(buildViewletteredOptionListAfterSave);
		List<String> letteredOptionListAfterSave = editQ.getseqLetteredOptions();
		Collections.sort(letteredOptionListAfterSave);
		Assert.assertNotEquals(letteredOptionList.toString().trim(), letteredOptionListAfterSave.toString().trim());
		Thread.sleep(5000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}
	

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
