package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP20_TM_1096_TC1_MatchingQuestionType extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	getPOS POS = new getPOS();
	JavascriptExecutor js;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP_20_TM_1096_TC1_Log into testmaker application to verify Mataching Question in Search result page")
	@Stories("SP_20_TM_1096_TC1_Log into testmaker application to verify Mataching Question in Search result page")
	@Test()
	public void matchingQuestionType() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.matchingQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Types");
		Assert.assertNotNull(buildTest.getCountSelectedFilter("Question Types"));

		List<String> childFilterNames = buildTest.getFilterOptions();
		for (int i = 0; i < childFilterNames.size(); i++) {
			LogUtil.log(childFilterNames.get(i));
			Assert.assertNotNull(childFilterNames.get(i));
		}
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		// LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int v = Integer.parseInt(str[0]);
	
		for (int i = 0; i < v; i++) {
			js = (JavascriptExecutor) driver;
			List<WebElement> metadatasection = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 border border-lightGray flex-1-1-auto flex flex-column justify-start bg-lightestGray']/div[@class='full-height block']"));
			for (int j = 0; j < metadatasection.size(); j++) {
				if (i == j) {
					actions = new Actions(driver);
					driver.findElement(By.id("regionAside")).click();
					js.executeScript("arguments[0].scrollIntoView(true);",
							metadatasection.get(j));
					Thread.sleep(3000);
					Assert.assertTrue(buildTest
							.getMatchingQuestionserialisedwithalphabets());
					Assert.assertTrue(buildTest
							.getMatchingQuestionserialisedwithnumberOption());
					Assert.assertTrue(buildTest.isCorrectMarkDisplayed());
					buildTest.getQuestionMetadataText();
					buildTest.getMetadataAssociatedValues("MATCHING");
					buildTest.fetchDuplicateQuestionIDNumber("ID Number");
					boolean isPresent = driver.findElements(
							By.id("__input__button__backToTop__0")).size() > 0;
					if (isPresent == true) {
						Thread.sleep(1000);
						String notificationText = driver
								.findElement(
										By.xpath("//div[@class='center large-body-text mt2-5']/span"))
								.getText();
						Assert.assertEquals(
								"You have reached the end of your results.",
								notificationText);
						driver.findElement(
								By.id("__input__button__backToTop__0")).click();
					}
				}
			}

		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
