package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

public class TM_2109_TC2_MQEditAnswerChoiceandNumQuestions extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	String buildTestName, testName;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	String editedQuestionText, questionCountAfterFilter, filterName, qid,
			questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			BloomstaxonomyAfterSave, questionTypeListView, difficultyListView,
			difficultyListView1, idNumberListView, beforebloomTaxonomy,
			difficultyListViewAfterSave, bloomvalue1;

	List<WebElement> moreInfo;
	List<String> qids;
	String expectedvalue = "Bloom's Taxonomy";

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM-2109_TC2_User edits answer choice and number question of Matching question by navigating from List View and clicks Save button. Verify that  updated data is displayed correctly and new version with identified E is created for the question.")
	@Stories("TM-2109_TC2_User edits answer choice and number question of Matching question by navigating from List View and clicks Save button. Verify that  updated data is displayed correctly and new version with identified E is created for the question.")
	@Test()
	public void editAnsChoiceandNumQuestions() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		//
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}

		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);

		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddMatchingQuestionsIcon(1);
		}
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@id='regionYourBuildTest']//*[name()='svg'][@id='__icon__accordioncollapsgrey__________0']"))
							.size() > 0;
					if (isexpanded == false) {
						js.executeScript("arguments[0].click();",
								moreInfo.get(j));
					}
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					questionTypeListView = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionTypeListView);
					System.out.println(questionTypeListView);
					difficultyListView = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					if (difficultyListView == null) {
						difficultyListView1 = "Diffculty=" + difficultyListView;
						LogUtil.log(qid + "=" + difficultyListView1);

					} else {
						LogUtil.log(qid + "=" + difficultyListView);

					}
					idNumberListView = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + idNumberListView);
					System.out.println(idNumberListView);
					beforebloomTaxonomy = buildTest
							.getMetadataAssociatedValuesListView("Bloom", qid);
					System.out.println(beforebloomTaxonomy);
					if (beforebloomTaxonomy == null) {
						bloomvalue1 = "Bloom’s Taxonomy" + beforebloomTaxonomy;
						LogUtil.log(qid + "=" + bloomvalue1);

					} else {
						LogUtil.log(qid + "=" + beforebloomTaxonomy);

					}
					buildTest.getChapterdetailsMetaDatainBuildView();
					js.executeScript("arguments[0].click();", moreInfo.get(j));
				}
			}
		}

		editQ = new EditQuestions();
		List<String> buildViewletteredOptionList = editQ
				.getmoreinfoOptionsList();
		LogUtil.log(buildViewletteredOptionList);
		ReusableMethods.scrollIntoViewClick(driver, editQ.EditQuestionbutton);
		ReusableMethods.checkPageIsReady(driver);
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[contains(.,'Numbered Questions*')]/following::div[@class='fr-element fr-view'][position()=1]"));
		editQ.AddQuestionChoice.click();
		editQ.clickansChoiceTextbox();
		WebElement lastsectionEle1 = driver
				.findElement(By
						.xpath("//div[contains(.,'Numbered Questions*')]/preceding::div[@class='fr-element fr-view'][position()=1]"));
		lastsectionEle1.sendKeys("User has added the " + 1 + " Answer Choice");

		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[contains(.,'Question Information')]/preceding::div[@class='fr-element fr-view'][position()=1]"));
		editQ.clickAddNumberedOptionButton();
		Thread.sleep(1000);
		editQ.scrolltoLastNumberedQuestion();
		editQ.selectListValue();
		Thread.sleep(1000);
		String getNumValue = editQ.getNumberQuestionsDigit();
		String replacedot = getNumValue.replace(".", "");
		int digit = Integer.parseInt(replacedot);
		WebElement NumberedQuest = driver
				.findElement(By
						.xpath("//div[contains(.,'Numbered Questions*')]/following::div[@class='fr-element fr-view'][position()="
								+ digit + "]"));
		NumberedQuest.click();
		NumberedQuest.sendKeys("User has added the " + digit
				+ " Numbered Question");

		if (editQ.listValueDisplayed("Difficulty") == false) {
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
		}
		ReusableMethods.scrollToBottom(driver);
		if (editQ.listValueDisplayed("Bloom") == false) {
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		cnt.clickSave();
		Thread.sleep(5000);
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@id='regionYourBuildTest']//*[name()='svg'][@id='__icon__accordioncollapsgrey__________0']"))
							.size() > 0;
					if (isexpanded == false) {
						js.executeScript("arguments[0].click();",
								moreInfo.get(j));
					}
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					questionTypeAfterSave = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionTypeAfterSave);
					System.out.println(questionTypeAfterSave);
					difficultyListView = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					if (difficultyListView == null) {
						difficultyListViewAfterSave = "Diffculty="
								+ difficultyListView;
						LogUtil.log(qid + "=" + difficultyListViewAfterSave);

					} else {
						LogUtil.log(qid + "=" + difficultyListView);

					}
					IDNumberAfterSave = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + IDNumberAfterSave);
					System.out.println(IDNumberAfterSave);

				}
			}
		}
		Assert.assertNotEquals(idNumberListView, IDNumberAfterSave);

		boolean containsEText = IDNumberAfterSave.contains("E");
		Assert.assertTrue(containsEText,
				" The Edited Question contains letter E");

		Thread.sleep(5000);
		cnt.clickTestMakerBackButton();
		ReusableMethods.checkPageIsReady(driver);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}

}
