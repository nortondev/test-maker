package com.wwnorton.TestMaker.MatchingQuestionType;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

public class TM_2108_TC2_ListViewDeleteQuestionStemMatchingQuestionErrorCheck extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	String buildTestName, testName;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	WebsitePage Website;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM-2108_TC2_User navigates to Edit Question mode from Build View, removes Question Stem content, clicks Save and verifies that system displays inline error message")
	@Stories("TM-2108_TC2_User navigates to Edit Question mode from Build View, removes Question Stem content, clicks Save and verifies that system displays inline error message")
	@Test()
	public void deleteQuestionStemMatchingQuestion() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		//
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		
		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);
		
		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddMatchingQuestionsIcon(1);
		}
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		buildTest.clickBuildTestTabs("List");
		buildTest.getListViewQuestionMetaData();
		editQ = new EditQuestions();
		editQ.clickEditQuestion();
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);

		editQ.removeQuestionText();
		actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		Thread.sleep(2000);
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		String parentWindow = driver.getWindowHandle();
		editQ.clickContactUslink();
		Website = new WebsitePage();
		Website.getWebsiteWindow(driver, parentWindow);
		Website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow);
		editQ.clickCloselink();
		editQ.getErrormsgQuestionStembox("Question Stem");
		editQ.modifyQuestionText("Modified Value");
		editQ.isErrorDisplayed("Question Stem");
		buildTest.clickCancelButton();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}
	

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
