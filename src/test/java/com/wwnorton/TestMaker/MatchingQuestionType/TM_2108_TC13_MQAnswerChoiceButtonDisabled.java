package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

import edu.emory.mathcs.backport.java.util.Collections;

public class TM_2108_TC13_MQAnswerChoiceButtonDisabled extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	String buildTestName, testName;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM-2108_TC13_User navigates to Edit Question page from Build Test, adds 26 Lettered Options and verifies that Add an Answer Choice button is disabled.")
	@Stories("TM-2108_TC13_User navigates to Edit Question page from Build Test, adds 26 Lettered Options and verifies that Add an Answer Choice button is disabled.")
	@Test()
	public void addAnswerChoiceButtonDisabled() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		//
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		
		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);

		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddMatchingQuestionsIcon(1);
		}
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		editQ = new EditQuestions();
		List<String> buildViewletteredOptionList = editQ
				.getmoreinfoOptionsList();
		LogUtil.log(buildViewletteredOptionList);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);

		editQ.deleteallLetteredOptions();
		cnt.clickSave();
		Thread.sleep(5000);
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();

		Assert.assertEquals(editQ.getErrorText(),
				"This question requires at least two lettered option to be saved.");
		
		ReusableMethods.scrollToElement(driver, By.xpath("//div[starts-with(@class,'bold darkGray')][contains(.,'Question Stem*')]"));
		Thread.sleep(2000);
		editQ.AddQuestionChoice.click();
		Thread.sleep(2000);
		editQ.clickansChoiceTextbox();
		WebElement lastsectionEle1 = driver
				.findElement(By
						.xpath("//div[contains(.,'Numbered Questions*')]/preceding::div[@class='fr-element fr-view'][position()=1]"));
		lastsectionEle1.sendKeys("User has added the " + 1 + " Answer Choice");
		Assert.assertEquals(editQ.getErrorText(),
				"This question requires at least two lettered option to be saved.");
		for (int i = 1; i <= 25; i++) {
			editQ.AddQuestionChoice.click();

			// List<WebElement> NumQBlock =
			// driver.findElements(By.xpath("//div[contains(@class,'placeholder')]/div[@class='fr-element fr-view']"));
			editQ.clickansChoiceTextbox();
			Thread.sleep(2000);
			WebElement lastsectionEle2 = driver
					.findElement(By
							.xpath("//div[contains(.,'Numbered Questions*')]/preceding::div[@class='fr-element fr-view'][position()=1]"));
			lastsectionEle2.sendKeys("User has added the " + (i + 1)
					+ " Answer Choice");
		}
		Assert.assertNull(editQ.getErrorText());
		boolean isAddQChoiceButtondisplayed = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button____empty____')]//div[contains(text(),'Add an Answer Choice')]"))
				.size() > 0;
		if (isAddQChoiceButtondisplayed == true) {
			Assert.assertTrue(isAddQChoiceButtondisplayed,
					"Button is displayed after 26 Attempts");
		} else {
			Assert.assertFalse(isAddQChoiceButtondisplayed,
					"Button is NOT displayed after 26 Attempts");
		}
		List<String> letteredOptionList = editQ.getseqLetteredOptions();
		Collections.sort(letteredOptionList);

		Thread.sleep(5000);
		editQ.selectvalueinAddNumQuestion();
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		cnt.clickSave();
		Thread.sleep(5000);
		Assert.assertNotEquals(letteredOptionList, buildViewletteredOptionList);
		Thread.sleep(5000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}

}
