package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

import edu.emory.mathcs.backport.java.util.Collections;

public class TM_2108_TC14_MQNumberedQuestButtonDisabled extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	String buildTestName, testName;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	List<WebElement> listitems;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM-2108_TC14_User navigates to Edit Question page from Build Test, adds 26 Numbered Questions and verifies that Add a Numbered Question button is disabled.")
	@Stories("TM-2108_TC14_User navigates to Edit Question page from Build Test, adds 26 Numbered Questions and verifies that Add a Numbered Question button is disabled.")
	@Test()
	public void mqNumberedQuestButtonDisabled() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		Thread.sleep(5000);
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		//
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		
		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);

		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddMatchingQuestionsIcon(1);
		}
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		editQ = new EditQuestions();
		List<String> buildViewNumQuestList = editQ
				.getmoreinfoNumberedQuestList();
		LogUtil.log(buildViewNumQuestList);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		editQ.deleteallNumberedQuestions();
		cnt.clickSave();
		Thread.sleep(5000);
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		Thread.sleep(3000);
		Assert.assertEquals(editQ.getErrorText(),
				"This question required at least two numbered questions to be saved.");
		editQ.clickAddNumberedOptionButton();
		Thread.sleep(1000);
		ReusableMethods.scrollIntoView(driver, editQ.errorSection);
		editQ.selectListValue();
		Thread.sleep(1000);
		editQ.clickNumQuestTextbox();
		WebElement lastsectionEle1 = driver
				.findElement(By
						.xpath("//div[contains(.,'Numbered Questions*')]/following::div[@class='fr-element fr-view'][position()=1]"));
		lastsectionEle1.sendKeys("User has added the " + 1
				+ " Numbered Question");

		Assert.assertEquals(editQ.getErrorText(),
				"This question required at least two numbered questions to be saved.");
		for (int i = 1; i < 25; i++) {
			editQ.addNumberedOptionButton.click();
			Thread.sleep(1000);
			editQ.selectListValue();
			Thread.sleep(1000);
			editQ.clickNumQuestTextbox();
			Thread.sleep(2000);
			WebElement lastsectionEle2 = driver
					.findElement(By
							.xpath("//span[contains(.,'Add a Numbered Question')]/preceding::div[@class='fr-element fr-view'][position()=1]"));
			lastsectionEle2.sendKeys("User has added the " + (i + 1)
					+ " Numbered Question");

			if (i == 24) {
				editQ.addNumberedOptionButton.click();
				Thread.sleep(1000);
				editQ.selectListValue();
				Thread.sleep(1000);
				WebElement lastsectionEle = driver
						.findElement(By
								.xpath("//div[contains(.,'Numbered Questions*')]/following::div[@class='fr-element fr-view'][position()=26]"));
				lastsectionEle.click();
				WebElement lastsectionEle3 = driver
						.findElement(By
								.xpath("//div[contains(.,'Numbered Questions*')]/following::div[@class='fr-element fr-view'][position()=26]"));
				lastsectionEle3.sendKeys("User has added the " + (i + 2)
						+ " Numbered Question");
			}
		}
		Assert.assertNull(editQ.getErrorText());
		boolean isNumQuestionButtondisplayed = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button____empty____')]//div[contains(text(),'Add a Numbered Question')]"))
				.size() > 0;
		if (isNumQuestionButtondisplayed == true) {
			Assert.assertTrue(isNumQuestionButtondisplayed,
					"Button is displayed after 26 Attempts");
		} else {
			Assert.assertFalse(isNumQuestionButtondisplayed,
					"Button is NOT displayed after 26 Attempts");
		}
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//button[starts-with(@id,'__input__button____empty____')]//div[contains(text(),'Add an Answer Choice')]"));

		List<String> NumQuestionList = editQ.getblankvalueinAddNumQuestion();
		Collections.sort(NumQuestionList);
		Thread.sleep(5000);
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
			}
		ReusableMethods.scrollToBottom(driver);
		if (editQ.listValueDisplayed("Bloom") == false) {
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		cnt.clickSave();
		Thread.sleep(5000);
		Assert.assertNotEquals(NumQuestionList, buildViewNumQuestList);
		Thread.sleep(5000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}

}
