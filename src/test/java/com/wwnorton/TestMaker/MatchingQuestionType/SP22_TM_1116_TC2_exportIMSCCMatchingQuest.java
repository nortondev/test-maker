package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.ExportWindow;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP22_TM_1116_TC2_exportIMSCCMatchingQuest extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	ExportWindow export;
	JavascriptExecutor js;
	int getAddQuestionCount;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP22_TM-1116_TC2_Log into Testmaker and Export CC to verify Application does not include Matching question in imscc")
	@Stories("SP22_TM-1116_TC2_Log into Testmaker and Export CC to verify Application does not include Matching question in imscc")
	@Test()
	public void matchingQuestionTypeIMSCCbuttonDisabled() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.matchingQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Types");
		Assert.assertNotNull(buildTest.getCountSelectedFilter("Question Types"));

		List<String> childFilterNames = buildTest.getFilterOptions();
		for (int i = 0; i < childFilterNames.size(); i++) {
			LogUtil.log(childFilterNames.get(i));
			Assert.assertNotNull(childFilterNames.get(i));
		}
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		buildTest.clickApplyButton();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ReusableMethods.checkPageIsReady(driver);

		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		// System.out.println(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);

		if (questionCount == 0) {
			Assert.assertNull(null, "No record Found");
		}
		if (questionCount <= 1) {
			getAddQuestionCount = questionCount;
		} else {
			getAddQuestionCount = GetRandomId.getRandom(questionCount);
		}
		buildTest.clickAddQuestionsIcon(getAddQuestionCount);
		Thread.sleep(2000);
		String questioncount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		String regex1 = "[^\\d]+";
		String[] strcount = questioncount.split(regex1);
		int buildviewQcount = Integer.parseInt(strcount[0]);
		LogUtil.log(buildviewQcount);
		cnt.clickSave();
		Thread.sleep(3000);
		driver.findElement(By.id("regionHeaderNavigation")).click();
		Thread.sleep(3000);
		cnt.clickExportButton();
		export = new ExportWindow();
		boolean isDisabled = export.iMSCCExportButtonDisabled();
		Assert.assertTrue(isDisabled, "imscc radio option is disabled");
		export.clickExportModal();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
