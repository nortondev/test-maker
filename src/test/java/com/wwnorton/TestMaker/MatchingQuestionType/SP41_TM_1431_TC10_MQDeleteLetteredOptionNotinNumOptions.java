package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

import edu.emory.mathcs.backport.java.util.Collections;

public class SP41_TM_1431_TC10_MQDeleteLetteredOptionNotinNumOptions extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	String buildTestName, testName;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	List<WebElement> moreInfo;
	String questionType, IDNumber, Difficulty, Bloomstaxonomy;
	String questionTypeAftercancel, IDNumberAftercancel, DiffcultyAftercancel,
	BloomstaxonomyAftercancel;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM-1431_TC10_User deletes a lettered option, adds a numbered question and verifies that the deleted lettered option is not displayed in newly added numbered question.")
	@Stories("TM-1431_TC10_User deletes a lettered option, adds a numbered question and verifies that the deleted lettered option is not displayed in newly added numbered question.")
	@Test()
	public void deleteLetteredOptionNotinNumberedOption() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		//
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
	
		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);
		
		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddMatchingQuestionsIcon(1);
		}
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		editQ = new EditQuestions();
		List<String> buildViewletteredOptionList =editQ.getmoreinfoOptionsList();
		LogUtil.log(buildViewletteredOptionList);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		
		editQ.deleteLetteredOptions("B.");
		Thread.sleep(5000);		
		List<String> letteredOptionList = editQ.getseqLetteredOptions();		
		Collections.sort(letteredOptionList);
		Thread.sleep(1000);
		editQ.getblankValueNumberedOptions();
		Thread.sleep(1000);
		editQ.clickAddNumberedOptionButton();
		
		List<String> NumOptionList =editQ.getblankvalueinAddNumQuestion();
		Collections.sort(NumOptionList);
		
		Assert.assertEquals(letteredOptionList, NumOptionList);
		Thread.sleep(2000);
		if(editQ.listValueDisplayed("Difficulty")==false){
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
			
		} 
		ReusableMethods.scrollToBottom(driver);
		if (editQ.listValueDisplayed("Bloom") == false) {
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		editQ.getErrormsgQuestionStembox("Numbered Questions*");
		buildTest.clickCancelButton();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(5000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}
	

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
