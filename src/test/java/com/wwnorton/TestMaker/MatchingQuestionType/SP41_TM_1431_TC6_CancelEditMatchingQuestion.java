package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

public class SP41_TM_1431_TC6_CancelEditMatchingQuestion extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	String buildTestName, testName;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	List<WebElement> moreInfo;
	String questionType, IDNumber, Difficulty, Bloomstaxonomy;
	String questionTypeAftercancel, IDNumberAftercancel, DiffcultyAftercancel,
	BloomstaxonomyAftercancel;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP41 TM-1431_TC6_User navigates to edit question from Build Test page, edits question stem, answer choice and numbered question for Matching question, clicks Cancel button. Verify that the edited data is not displayed for Matching question.")
	@Stories("SP41 TM-1431_TC6_User navigates to edit question from Build Test page, edits question stem, answer choice and numbered question for Matching question, clicks Cancel button. Verify that the edited data is not displayed for Matching question.")
	@Test()
	public void editMatchingQuestionandCancel() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		ReusableMethods.checkPageIsReady(driver);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		ReusableMethods.checkPageIsReady(driver);
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		//
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		
		// buildTest.scrollMetadata(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);
		int getAddQuestionCount = GetRandomId.getRandom(questionCount);
		
		Thread.sleep(2000);
		if (getAddQuestionCount == 0) {
			Assert.assertNull(null, "No record Found");
		} else if (getAddQuestionCount > 1) {
			buildTest.clickAddMatchingQuestionsIcon(1);
		}
		ReusableMethods.checkPageIsReady(driver);
		
		editQ = new EditQuestions();
		actions = new Actions(driver);
		// get Edit Question details
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			questionType = buildTest
					.getMetadataAssociatedValuesBuildTestView("Question Type");
			Difficulty = buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			Bloomstaxonomy = buildTest
					.getMetadataAssociatedValuesBuildTestView("Bloom's Taxonomy");
		}
		Thread.sleep(2000);
		WebElement question1ele = driver.findElement(By.xpath("//div[@class='black px2-5 mb2-5'][contains(.,'1 Question')]"));
		ReusableMethods.scrollIntoViewClick(driver, question1ele);
		editQ.clickEditQuestion();
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		editQ.deleteListValue("Bloom");
		editQ.changeListValue("Bloom");
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		// editQ.questionNotSavedpopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(8000);
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(3000);
			questionTypeAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("Question Type");
			DiffcultyAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");
			IDNumberAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			BloomstaxonomyAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("Bloom’s Taxonomy");
		}

		Assert.assertEquals(questionType, questionTypeAftercancel);
		Assert.assertEquals(Difficulty, DiffcultyAftercancel);
		Assert.assertEquals(IDNumber, IDNumberAftercancel);
		Assert.assertEquals(Bloomstaxonomy, BloomstaxonomyAftercancel);
		String idNumber = IDNumber;
		Assert.assertEquals(idNumber, IDNumberAftercancel);
		LogUtil.log(idNumber, IDNumberAftercancel);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
