package com.wwnorton.TestMaker.MatchingQuestionType;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP20_TM_1096_TC3_1162_TC1_MatchingquestPreview extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	getPOS POS = new getPOS();
	JavascriptExecutor js;
	int i;
	int getAddQuestionCount;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP20_TM_1096_TC3_Log into testmaker application to verify Mataching Question on Preview")
	@Stories("SP20_TM_1096_TC3_Log into testmaker application to verify Mataching Question  on Preview")
	@Test()
	public void matchingQuestionTypePreview() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.matchingQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Types");
		Assert.assertNotNull(buildTest.getCountSelectedFilter("Question Types"));

		List<String> childFilterNames = buildTest.getFilterOptions();
		for (int i = 0; i < childFilterNames.size(); i++) {
			LogUtil.log(childFilterNames.get(i));
			Assert.assertNotNull(childFilterNames.get(i));
		}
		buildTest.selectQuestionTypeCheckbox("MATCHING");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		
		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);

		if (questionCount == 0) {
			Assert.assertNull(null, "No record Found");

		}
		if (questionCount <= 1) {
			getAddQuestionCount = questionCount;
		} else if (questionCount >=5){
			getAddQuestionCount = GetRandomId.getRandom(questionCount);
			getAddQuestionCount=3;
		} else {
			getAddQuestionCount = GetRandomId.getRandom(questionCount);
		}

		for (i = 1; i <=getAddQuestionCount; i++) {
			js = (JavascriptExecutor) driver;
			List<WebElement> metadatasection = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 border border-lightGray flex-1-1-auto flex flex-column justify-start bg-lightestGray']/div[@class='full-height block']"));
			for (int j = 0; j < metadatasection.size(); j++) {
				if (i == j) {
					actions = new Actions(driver);
					driver.findElement(By.id("regionAside")).click();
					js.executeScript("arguments[0].scrollIntoView(true);",
							metadatasection.get(j));
					Thread.sleep(3000);
					Assert.assertTrue(buildTest
							.getMatchingQuestionserialisedwithalphabets());
					Assert.assertTrue(buildTest
							.getMatchingQuestionserialisedwithnumberOption());
					Assert.assertTrue(buildTest.isCorrectMarkDisplayed());
					buildTest.getQuestionMetadataText();
					buildTest.getMetadataAssociatedValues("MATCHING");
					buildTest.fetchDuplicateQuestionIDNumber("ID Number");

				}
			}
			buildTest.getBacktotoplink();

		}
		buildTest.clickAddQuestionsIcon(getAddQuestionCount);

		if (getAddQuestionCount > 1) {
			String questionCountAfterSearchQuestionAfteradd = buildTest
					.getQuestionCountAfterQuestionSearch();
			LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		}

		// /Verification in Build View SP22_TM-1162_TC1_Log into testmaker
		// application to verify Matching Question in Build View

		List<WebElement> getQuestionTextBuildView = driver
				.findElements(By
						.xpath("//div[@class='flex-0-0-auto flex flex-row justify-between items-center my1-5']/span"));
		for (int q = 0; q < getQuestionTextBuildView.size(); q++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					getQuestionTextBuildView.get(q));
			// js.executeScript("arguments[0].click();",
			// getQuestionTextBuildView.get(q));
			Thread.sleep(5000);
			Assert.assertTrue(buildTest
					.getMatchingQuestionserialisedwithalphabetsBuildViewMode());
			Assert.assertTrue(buildTest
					.getMatchingQuestionserialisedwithnumberOptionBuildViewMode());
			Assert.assertTrue(buildTest.isCorrectMarkDisplayedBuildView());

			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int k = 0; k < moreInfo.size(); k++) {
				if (q == k) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(k));
					js.executeScript("arguments[0].click();", moreInfo.get(k));
					Thread.sleep(8000);
					buildTest
							.getMetadataAssociatedValuesBuildTestView("Question Type");

					buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");

					buildTest.getChapterdetailsMetaDatainBuildView();

					js.executeScript("arguments[0].click();", moreInfo.get(k));
					Thread.sleep(3000);
				}
			}
		}
		// /Verification on List view SP22_TM-1163_TC1 _Log into application and
		// Verify Matching questions in list view
		driver.findElement(By.xpath("//button[@id='__input__button__List__0']"))
				.sendKeys(Keys.PAGE_UP);
		buildTest.clickBuildTestTabs("List");
		List<WebElement> listviewButton = driver
				.findElements(By
						.xpath("//div[@class='flex flex-column justify-start full-width m0 p0 col xs-col-4 sm-col-4 md-col-8 lg-col-12 bg-lightestGray cursorGrabbing']/div[@class='m0 p0 border border-lightGray flex-1-1-auto bg-white flex flex-column justify-start css-161xm4f']/div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
		for (int l = 0; l < listviewButton.size(); l++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					listviewButton.get(l));
			js.executeScript("arguments[0].click();", listviewButton.get(l));
			Thread.sleep(5000);
			Assert.assertTrue(buildTest
					.getMatchingQuestionserialisedwithalphabetsListViewMode());
			Assert.assertTrue(buildTest
					.getMatchingQuestionserialisedwithnumberOptionListViewMode());
			Assert.assertTrue(buildTest.isCorrectMarkDisplayedListView());
			List<WebElement> moreInfoSectionListView = driver
					.findElements(By
							.xpath("//div/button[@aria-controls='__input__button__Accordion__0']/following::div[@class='full-height block']"));
			for (int m = 0; m < moreInfoSectionListView.size(); m++) {
				if (l == m) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfoSectionListView.get(m));
					js.executeScript("arguments[0].click();",
							moreInfoSectionListView.get(m));
					buildTest
							.getMetadataAssociatedValuesBuildTestView("Question Type");

					buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");

					buildTest.getChapterdetailsMetaDatainListView();

					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(l));
					js.executeScript("arguments[0].click();",
							listviewButton.get(l));
				}
			}
		}

		// verification on Preview page SP22_TM-1164_TC1_Log into testmaker
		// application to verify Matching Question in Preview
		driver.findElement(
				By.xpath("//button[@id='__input__button__Preview__0']"))
				.sendKeys(Keys.PAGE_UP);
		buildTest.clickBuildTestTabs("Preview");
		Thread.sleep(5000);
		Assert.assertEquals(buildTestName, buildTest.getTestNamePreviewMode());
		LogUtil.log(buildTest.getTestNamePreviewMode());
		Assert.assertEquals(TMlogin.getbooktitleBuildTest().trim(), buildTest
				.getCourseNamePreviewMode().trim());
		LogUtil.log(buildTest.getCourseNamePreviewMode());

		List<WebElement> getQuestionNumberPreview = driver.findElements(By
				.xpath("//div[@class='left ']"));
		for (int p = 0; p < getQuestionNumberPreview.size(); p++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					getQuestionNumberPreview.get(p));
			Assert.assertTrue(buildTest
					.getMatchingQuestionserialisedwithalphabetsPreviewMode());
			Assert.assertTrue(buildTest
					.getMatchingQuestionserialisedwithnumberOptionPreviewMode());

		}
		List<WebElement> getAnswerPreview = driver
				.findElements(By
						.xpath("//li[@class='table sub-sup-i']/span[contains(text(),'Answer')]"));
		for (int a = 0; a < getAnswerPreview.size(); a++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					getAnswerPreview.get(a));
			buildTest.getAnswerListPreviewMode();
		}

		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
