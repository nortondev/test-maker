package com.wwnorton.TestMaker.Build_List_PreviewViewfeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_620_TC2_AS94_EmptyMetadata_ListView extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();
	String expectedvalue ="Bloom's Taxonomy";;
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_620_TC2_AS94_Verify Empty Metadata is not displayed in List View ")
	@Stories("TM_620_TC2_AS94_Create a new test and apply filters to verify application does not displays empty metadata in displayed question list in List view")
	@Test()
	public void validateEmptyMetaDataListView() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.profileTestMakerApp();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		TMlogin.appendISBN(ReusableMethods.nonQuestionGroupISBNreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectAllCheckboxesQuestionType();
		buildTest.clickApplyButton();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(2000);
		//ReusableMethods.isQuestionGroupDisplayed(driver);
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		LogUtil.log(getQuestionText);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(1000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		/*
		 * String getText=ReusableMethods.getSearchText(driver);
		 * ArrayList<String> searchKeys = new ArrayList<String>(); String
		 * SearchText
		 * =POS.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText
		 * ,searchKeys); searchKeys.add(SearchText); Thread.sleep(5000);
		 * buildTest.clickSearchQuestionsTextbox(SearchText);
		 * buildTest.clickSearchLinkbutton();
		 */
		// driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		//LogUtil.log(questionCountAfterFilter);
		driver.findElement(By.id("regionAside")).click();
		Thread.sleep(5000);

		WebElement questionsText = driver
				.findElement(By
						.xpath("//div[@class='clearfix mb2 mt3']/div[@class='pl2 col sm-col-4 sm-col-8 h3 font-weight-2 pt1-5 pb1-5 mt1-5']"));
		actions.moveToElement(questionsText).build().perform();
		buildTest.getQuestionMetadataText();
		buildTest.getMetadataAssociatedValues("Question Type");
		buildTest.getMetadataAssociatedValues("Difficulty");
		buildTest.getMetadataAssociatedValues("ID Number");
		buildTest.getMetadataAssociatedValues("Bloom’s Taxonomy");
		buildTest.getChapterdetailsMetaDatasection();
		buildTest.getLearningObjectiveList();
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		List<WebElement> getQuestionText1 = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		LogUtil.log(getQuestionText1);
		for (int ele = 0; ele < 10; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		buildTest.clickAddQuestionsIcon(6);
		buildTest.getQuestionTextinBuildView(1);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		String regex = "[^\\d]+";
		String[] str = questionCountAfterSearchQuestionAfteradd.split(regex);
		Assert.assertNotNull(buildTest.verifyQuestioninTestBuild());
		// buildTest.getQuestionMetadataTextBuildTestView();
		int v = Integer.parseInt(str[0]);
		LogUtil.log(v);
		// int k = v/10;
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		
		buildTest.clickBuildTestTabs("List");
		
		List<String> qids = new ArrayList<String>();
 		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
		String qid = qids.get(i).toString();
		js = (JavascriptExecutor) driver;
		List<WebElement> listviewButton = driver
				.findElements(By
						.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
		for (int j = 0; j < listviewButton.size(); j++) {
			if (i == j) {
				js.executeScript("arguments[0].scrollIntoView(true);",
						listviewButton.get(j));
				js.executeScript("arguments[0].click();",
						listviewButton.get(j));
				Thread.sleep(1000);
				WebElement metadatablock = driver
						.findElement(By
								.xpath("//div[@id='regionYourBuildTest']//div[@id='"+ qid +"']//div/ul[@class='list-reset body-text mt1-5']"));
				ReusableMethods.scrollIntoView(driver, metadatablock);
				String questionType =buildTest
						.getMetadataAssociatedValuesListView("Question Type",qid);
				LogUtil.log(qid + "=" +questionType);
				String difficulty= buildTest.getMetadataAssociatedValuesListView("Difficulty",qid);
				LogUtil.log(qid + "=" +difficulty);
				String idNumber  =buildTest.getMetadataAssociatedValuesListView("ID Number",qid);
				LogUtil.log(qid + "=" +idNumber);
				String ListViewBloomTx;
				if(buildTest.getBloomsTaxonomyTextAddedSectionListView()!=null){
				ListViewBloomTx = buildTest
						.getBloomsTaxonomyTextAddedSectionListView().toString().trim();
				
				Assert.assertEquals(ListViewBloomTx,expectedvalue.toString().trim());
				} else {
				 Assert.assertNull(null);	
				}
				String bloomTaxonomy  = buildTest
						.getMetadataAssociatedValuesListView("Bloom",qid);
				LogUtil.log(qid + "=" +bloomTaxonomy);
				String chapterinfo = buildTest.getMetadataAssociatedValuesListView("Chapter",qid);
				LogUtil.log(qid + "=" +chapterinfo);
				String lobjective  =buildTest.getLearningObjdetailsMetaDatainListView();
				LogUtil.log(qid + "=" +lobjective);
				js.executeScript("arguments[0].scrollIntoView(true);",
						listviewButton.get(j));
				js.executeScript("arguments[0].click();",
						listviewButton.get(j));
			}
		}
	}
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		
		PropertiesFile.tearDownTest();

	}


}
