package com.wwnorton.TestMaker.Build_List_PreviewViewfeatures;

import java.util.List;
import java.util.concurrent.TimeUnit;





import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_343_TC1_AS79PreviewExamAnswers extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_343_TC1_AS79_verify Preview exam answers")
	@Stories("TM_343_TC1_AS79_Log into Testmaker with valid Credentials to verify Preview exam answers")
	@Test()
	public void previewExamAnswer() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		String getBookTitle = TMlogin.getbooktitle();
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("SHORT ANSWER");
		buildTest.clickApplyButton();
		String questionCountAfterSearchQuestion = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestion);
		String questionTypesAfterApplyFilter = buildTest.verifyFilterName(0);
		Assert.assertEquals("SHORT ANSWER", questionTypesAfterApplyFilter.toUpperCase().toString());
		actions = new Actions(driver);
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		@SuppressWarnings("unused")
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		buildTest.clickAddQuestionsIcon(5);
		buildTest.getQuestionTextinBuildView(1);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Thread.sleep(4000);
		Assert.assertEquals("Question 5", buildTest.verifyQuestioninTestBuild());
		buildTest.clickBuildTestTabs("Preview");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@class='bg-white ml1-5 mr1-5 mb1-5  lg-col-12']/div[@class='pt2-5']/div[contains(text(),'Answer Key')]"));
		ReusableMethods.scrollToBottom(driver);
		Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
		LogUtil.log(buildTest.getAnswerKeyText());
		Assert.assertEquals(buildTestName, buildTest.getTestNamePreviewMode());
		LogUtil.log(buildTest.getTestNamePreviewMode());
		Assert.assertEquals(getBookTitle, buildTest.getCourseNamePreviewMode());
		LogUtil.log(buildTest.getCourseNamePreviewMode());
		buildTest.getAnswerListPreviewMode();
        Thread.sleep(2000);
        cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}
}
