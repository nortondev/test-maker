package com.wwnorton.TestMaker.Build_List_PreviewViewfeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_778_TC1_AS97BloomsTaxonomy extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();
	String expectedvalue = "Bloom's Taxonomy";

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_778_TC1_AS97_metadata title displayed as 'Blooms Taxonomy' instead of 'Blooms' ")
	@Stories("TM_778_TC1_AS97_Verify metadata title displayed as 'Blooms Taxonomy' instead of 'Blooms'")
	@Test()
	public void filtersSearchFreeText() throws Exception {
        driver= getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.profileTestMakerApp();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		ReusableMethods.checkPageIsReady(driver);
		buildTest.clickAddFiltersButton();
		buildTest.clickBloomsTaxonomyButton();
		Assert.assertEquals("Bloom's Taxonomy",
				buildTest.BloomsTaxonomy.getText());
		List<String> childFilterNames = buildTest.getFilterOptions();
		for (int i = 0; i < childFilterNames.size(); i++) {
			System.out.println(childFilterNames.get(i));
			Assert.assertNotNull(childFilterNames.get(i));
		}
		buildTest.selectBloomsTaxonomy("Remembering");
		buildTest.clickApplyButton();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String getText = ReusableMethods.getSearchText(driver);
		ArrayList<String> searchKeys = new ArrayList<String>();
		String SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);
		searchKeys.add(SearchText);
		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		Thread.sleep(2000);
		String Text = buildTest.getBloomsTaxonomyText();
		//String result = Text.replaceAll("[^\\x00-\\x7F]", "");
		Assert.assertEquals(Text.toString().trim(),expectedvalue.toString().trim());
				
		buildTest.clickAddQuestionsIcon(2);
		/*
		 * String questionCountAfterSearchQuestionAfteradd
		 * =buildTest.getQuestionCountAfterQuestionSearch();
		 * LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		 */
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		// click the First More info Button
		WebElement moreInfo = driver.findElement(By
				.xpath("//button[contains(text(),'More Info')]"));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", moreInfo);
		js.executeScript("arguments[0].click();", moreInfo);
		Thread.sleep(2000);
		js.executeScript("window.scrollBy(0,300)", "");
		String MoreInfo = buildTest.getBloomsTaxonomyTextAddedSection().toString();
		Assert.assertEquals(MoreInfo.toString().trim(),expectedvalue.toString().trim());
		// js.executeScript("window.scrollBy(0,-800)", "");
		WebElement ListEle = driver
				.findElement(By
						.xpath("//div[@class='bg-white full-width flex flex-row justify-center']/div/div[@class='p1-5']/button[contains(text(),'List')]"));
		ReusableMethods.scrollIntoView(driver, ListEle);
		js.executeScript("window.scrollBy(0,-200)", "");

		buildTest.clickBuildTestTabs("List");
		Thread.sleep(2000);
		List<String> qids = new ArrayList<String>();
 		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
		String qid = qids.get(i).toString();
		js = (JavascriptExecutor) driver;
		List<WebElement> listviewButton = driver
				.findElements(By
						.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
		for (int j = 0; j < listviewButton.size(); j++) {
			if (i == j) {
				js.executeScript("arguments[0].scrollIntoView(true);",
						listviewButton.get(j));
				js.executeScript("arguments[0].click();",
						listviewButton.get(j));
				Thread.sleep(1000);
				WebElement metadatablock = driver
						.findElement(By
								.xpath("//div[@id='regionYourBuildTest']//div[@id='"+ qid +"']//div/ul[@class='list-reset body-text mt1-5']"));
				ReusableMethods.scrollIntoView(driver, metadatablock);
		
		//buildTest.clickQuestionTitleListView(0);
		//js.executeScript("window.scrollBy(0,300)", "");
		String ListView = buildTest.getBloomsTaxonomyTextAddedSectionListView().toString().trim();
		System.out.println(ListView);
		Assert.assertEquals(ListView.toString().trim(), expectedvalue.toString().trim());
		js.executeScript("arguments[0].scrollIntoView(true);",
				listviewButton.get(j));
		js.executeScript("arguments[0].click();",
				listviewButton.get(j));
			}
		}
		}
	
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);
        ReusableMethods.checkPageIsReady(driver);
		buildTest.clickAddFiltersButton();
		buildTest.clickBloomsTaxonomyButton();
		Assert.assertEquals("Bloom's Taxonomy",
				buildTest.BloomsTaxonomy.getText());
		buildTest.selectBloomsTaxonomy("Understanding");
		buildTest.clickApplyButton();
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String getText1 = ReusableMethods.getSearchText(driver);
		ArrayList<String> searchKeys1 = new ArrayList<String>();
		String SearchText1 = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText1,
						searchKeys1);
		searchKeys.add(SearchText1);
		buildTest.clickSearchQuestionsTextbox(SearchText1);

		buildTest.clickSearchLinkbutton();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String questionCountAfterFilter1 = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter1);
		System.out.println(questionCountAfterFilter1);
		Thread.sleep(5000);
		WebElement element = driver.findElement(By.id("regionAside"));
		element.click();
		Thread.sleep(5000);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		String text2 = buildTest.getBloomsTaxonomyText();
		Assert.assertEquals(text2.toString().trim(),expectedvalue.toString().trim());
		// actions.sendKeys(Keys.PAGE_UP).build().perform();
		Thread.sleep(2000);
		buildTest.clickAddQuestionsIcon(1);
		ReusableMethods.scrollToBottom(driver);
		String xpathVaribale = "(//button[contains(text(),'More Info')])"
				+ "[3]";
		WebElement moreInfo3button = driver
				.findElement(By.xpath(xpathVaribale));
		js.executeScript("arguments[0].scrollIntoView(true);", moreInfo3button);
		js.executeScript("arguments[0].click();", moreInfo3button);
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		String MoreInfo1 = buildTest.getBloomsTaxonomyTextAddedSection();
		Assert.assertEquals(MoreInfo1.toString().trim(),expectedvalue.toString().trim());
		js.executeScript("window.scrollBy(0,-7000)", "");
		Thread.sleep(5000);
		WebElement ListEle1 = driver
				.findElement(By
						.xpath("//div[@class='bg-white full-width flex flex-row justify-center']/div/div[@class='p1-5']/button[contains(text(),'List')]"));
		ReusableMethods.scrollIntoView(driver, ListEle1);
		js.executeScript("window.scrollBy(0,-300)", "");
		buildTest.clickBuildTestTabs("List");
		buildTest.clickQuestionTitleListView(2);
		js.executeScript("window.scrollBy(0,300)", "");
		String ListView1 = buildTest
				.getBloomsTaxonomyTextAddedSectionListView().toString().trim();
	//	System.out.println(ListView);
		Assert.assertEquals(ListView1.toString().trim(),expectedvalue.toString().trim());
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		
		PropertiesFile.tearDownTest();

	}
	

}
