package com.wwnorton.TestMaker.Build_List_PreviewViewfeatures;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_620_TC1_AS93_EmptyMetadata_BuildView extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_620_TC1_AS93_Verify Empty Metadata is not displayed in Build View ")
	@Stories("TM_620_TC1_AS93_Create a new test and apply filters to verify application does not displays empty metadata in displayed question list in Build view")
	@Test()
	public void validateEmptyMetaDataBuildView() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.profileTestMakerApp();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		TMlogin.appendISBN(ReusableMethods.nonQuestionGroupISBNreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);
		ReusableMethods.checkPageIsReady(driver);
		buildTest = new BuildTestPage();
		try {
			buildTest.clickAddFiltersButton();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectAllCheckboxesQuestionType();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		// driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(2000);
		//ReusableMethods.isQuestionGroupDisplayed(driver);
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		LogUtil.log(getQuestionText);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		
		// driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(2000);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		// System.out.println(questionCountAfterFilter);
		// driver.findElement(By.id("regionAside")).click();
		Thread.sleep(1000);

		WebElement questionsText = driver
				.findElement(By
						.xpath("//div[@class='clearfix mb2 mt3']/div[@class='pl2 col sm-col-4 sm-col-8 h3 font-weight-2 pt1-5 pb1-5 mt1-5']"));
		actions.moveToElement(questionsText).build().perform();
		buildTest.getQuestionMetadataText();
		buildTest.getMetadataAssociatedValues("Question Type");
		buildTest.getMetadataAssociatedValues("Difficulty");
		buildTest.getMetadataAssociatedValues("ID Number");
		buildTest.getMetadataAssociatedValues("Bloom’s Taxonomy");
		buildTest.getChapterdetailsMetaDatasection();
		buildTest.getLearningObjectiveList();
		
		buildTest.clickAddQuestionsIcon(7);
		buildTest.getQuestionTextinBuildView(1);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		String regex = "[^\\d]+";
		String[] str = questionCountAfterSearchQuestionAfteradd.split(regex);
		Assert.assertNotNull(buildTest.verifyQuestioninTestBuild());
		buildTest.getQuestionMetadataTextBuildTestView();
		int v = Integer.parseInt(str[0]);
		// int k = v/10;
		for (int i = 0; i < v; i++) {
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(8000);
					buildTest
							.getMetadataAssociatedValuesBuildTestView("Question Type");
					buildTest
							.getMetadataAssociatedValuesBuildTestView("Difficulty");
					buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					buildTest
							.getMetadataAssociatedValuesBuildTestView("Bloom’s Taxonomy");
					buildTest.getChapterdetailsMetaDatainBuildView();
					buildTest.getLearningObjdetailsMetaDatainBuildView();
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(3000);
				}
			}
		}
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {	
		PropertiesFile.tearDownTest();

	}

}
