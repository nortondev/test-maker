package com.wwnorton.TestMaker.Build_List_PreviewViewfeatures;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_711_TC3_TC4_AS82_83ChapterNumandName extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	JavascriptExecutor js;
	Actions actions;
	 int getAddQuestionCount;
	
	 @Parameters({ "browser" })
		@BeforeTest
		public void setUp(@Optional("Chrome") String browser) throws Exception {
			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig(browser);
			PropertiesFile.setURL();
		}
	@Severity(SeverityLevel.NORMAL)
	@Description("TM_711_TC3_TC4_AS82_83_Application Display Chapter Number before Chapter Name")
	@Stories("TM_711_TC3_TC4_AS82_83_Log into testmaker and apply filters and create test to verify application displays Chapter number before chapter name in Add question and Build test metadata")
	@Test()
	public void chapterNumberBeforeName() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		buildTest = new BuildTestPage();
		cnt  = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		Thread.sleep(3000);
		String[] chapterNamesNums = buildTest
				.checkChapterNameandChapterNumber();
		LogUtil.log(chapterNamesNums[0]);
		LogUtil.log(chapterNamesNums[1]);
		List<String> chapterNames = buildTest
				.clickMultipleChaptersCheckboxesandChaptersName(3);
		LogUtil.log(chapterNames);
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}else {
			buildTest.clickApplyButton();
		}
		List<String> filterNames = buildTest.verifyMultipleFilterName();
		LogUtil.log(filterNames);
	//	Assert.assertTrue(chapterNames.containsAll(filterNames));
		driver.findElement(By.id("regionAside")).click();
		driver.findElements(By.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
		}
		String getQuestionsCount = buildTest.getQuestionCountAfterQuestionSearch();
	
	//	String getQuestionText =buildTest.clickAddquestiongrouplink();
		int v =ReusableMethods.getDigits(getQuestionsCount);
		if(v==0){
        	Assert.assertNull(null, "No record Found");
        	
        }
        if(v==1){
        		getAddQuestionCount = v;
        		
        } else {
        	getAddQuestionCount = GetRandomId.getRandom(v);
        }
		//int questions =GetRandomId.getRandom(v);
        ReusableMethods.isQuestionGroupDisplayed(driver);
		buildTest.clickAddQuestionsIcon(getAddQuestionCount);
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickcollapseExpandbutton();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement moreInfo = driver.findElement(By
				.xpath("//button[contains(text(),'More Info')]"));
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", moreInfo);
		js.executeScript("arguments[0].click();", moreInfo);
		Thread.sleep(8000);
		// buildTest.clickMoreInfobutton(0);
		String[] chapterNamesNumsinBuildView = buildTest
				.checkChapterNameandChapterNumberMetaDatasection();
		ReusableMethods
				.convertArrayToStringUsingStreamAPI(chapterNamesNumsinBuildView);
		// Assert.assertEquals("[Chapter 14]",
		// chapterNamesNumsinBuildView[0].toString());
		// Assert.assertEquals("[How Much Product Does a Reaction Really Make?]",
		// chapterNamesNumsinBuildView[1].toString());
		LogUtil.log(chapterNamesNumsinBuildView[0].toString());
		LogUtil.log(chapterNamesNumsinBuildView[1].toString());

		buildTest.clickcollapseExpandbutton();
		WebElement ListEle = driver
				.findElement(By
						.xpath("//div[@class='bg-white full-width flex flex-row justify-center']/div/div[@class='p1-5']/button[contains(text(),'List')]"));
		ReusableMethods.scrollIntoView(driver, ListEle);
		js.executeScript("window.scrollBy(0,-200)", "");
		buildTest.clickBuildTestTabs("List");
		buildTest.clickQuestionTitleListView(0);
		String[] chapterNamesNumsinListView = buildTest
				.checkChapterNameandChapterNumberMetaDatasection();
		for (int i = 0; i < chapterNamesNumsinListView.length; i++) {
			if (i == 4) {
				ReusableMethods
						.convertArrayToStringUsingStreamAPI(chapterNamesNumsinListView);
				// Assert.assertEquals("[Chapter 14]",
				// chapterNamesNumsinListView[0].toString());
				// Assert.assertEquals("[How Much Product Does a Reaction Really Make?]",
				// chapterNamesNumsinListView[1].toString());
				LogUtil.log(chapterNamesNumsinListView[0].toString());
				LogUtil.log(chapterNamesNumsinListView[1].toString());
			}
		}
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {	
		PropertiesFile.tearDownTest();

	}
}
