package com.wwnorton.TestMaker.Build_List_PreviewViewfeatures;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_550_TC3_AS80ListViewExpandCollapse extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	getPOS POS = new getPOS();
	String selectedChapterName;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_550_TC3_AS80_List View Expand Collapse Drag and Drop feature")
	@Stories("TM_550_TC3_AS80_Log into testmaker application and navigate to List view to verify Application allow to reorder other collapsed accordions above and below the expanded accordion")
	@Test()
	public void listViewDragDrop() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		ReusableMethods.checkPageIsReady(driver);
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.nonQuestionGroupISBNreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
        int getChapterCount = buildTest.getchaptersFilterscount();
        
        if(getChapterCount==0){
			Assert.assertNull(getChapterCount);
		}else if(getChapterCount<=1){
			selectedChapterName = buildTest
					.clickChaptersCheckboxesandChaptersName(getChapterCount);
			LogUtil.log(selectedChapterName);
		}else {
			int checkboxes = GetRandomId.getRandom(getChapterCount);
			List<String> selectedChaptersName = buildTest
					.clickMultipleChaptersCheckboxesandChaptersName(checkboxes);
			LogUtil.log(selectedChaptersName);
		}
		
        boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}else {
			buildTest.clickApplyButton();
		}
		isQuestionGroupDisplayed();
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		LogUtil.log(getQuestionText);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		String getText = getSearchText();
		ArrayList<String> searchKeys = new ArrayList<String>();
		String SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);
		System.out.println(SearchText);
		searchKeys.add(SearchText);
		Thread.sleep(5000);
		buildTest.clickClearFiltersButton();
		Thread.sleep(5000);
		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(getQuestionsCount);
		int v =ReusableMethods.getDigits(getQuestionsCount);
		int questions =GetRandomId.getRandom(v);
		if(questions<=1){
			questions =questions+1;
			buildTest.clickAddQuestionsIcon(questions);
		} else {
		buildTest.clickAddQuestionsIcon(questions);
		}
		Thread.sleep(5000);
	//	buildTest.clickAddQuestionsIcon(3);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		if(questionCountAfterSearchQuestionAfteradd==null){
			Assert.assertNull(questionCountAfterSearchQuestionAfteradd);
		}
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Thread.sleep(5000);
		//Assert.assertEquals("Question 3", buildTest.verifyQuestioninTestBuild());
		buildTest.clickBuildTestTabs("List");
		buildTest.validateQuestionsCollapsedListwMode();
		buildTest.clickQuestionTitleListView(0);
		buildTest.validateQuestionsCollapsedListwMode();
		buildTest.clickQuestionTitleListView(0);
		buildTest.dragDropQuestionsTitleListView(2);
		buildTest.questionSequenceafterDragDrop();
		buildTest.clickBuildTestTabs("Preview");
		buildTest.questionSequenceafterDragDropOnPreviewMode();
		Thread.sleep(3000);
		buildTest.answerSequenceafterDragDropOnPreviewMode();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		ReusableMethods.checkPageIsReady(driver);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}

	public String getSearchText() {

		WebElement qM0 = driver.findElement(By.xpath("//div[starts-with(@class,'px1-5')]"));
		String id = qM0.getAttribute("id");
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getSText = getIDvalue.getText();
		return getSText;
	}

	public boolean isQuestionGroupDisplayed() throws InterruptedException {
		driver.findElement(By.id("regionAside")).click();
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//button[@type='button'][starts-with(text(),'Show')]"));
		for (int ele = 0; ele < getQuestionText.size(); ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_UP).build().perform();

		}
		return true;
	}
}
