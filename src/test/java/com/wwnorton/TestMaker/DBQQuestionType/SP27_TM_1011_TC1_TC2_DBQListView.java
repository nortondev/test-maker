package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP27_TM_1011_TC1_TC2_DBQListView extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();
	int getAddQuestionCount;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP27_TM-1011_TC1 Log into application and Verify DBQ questions in list view")
	@Stories("SP27_TM-1011_TC1 Log into application and Verify DBQ questions in list view")
	@Test()
	public void questionDBQListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		ReusableMethods.checkPageIsReady(driver);
		buildTest = new BuildTestPage();
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			buildTest.clickAddFiltersButton();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		buildTest.clickApplyButton();
		driver.findElement(By.id("regionAside")).click();

		driver.findElements(By.xpath("//div[@class='mx3']"));
		actions = new Actions(driver);
		/*for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}

		Thread.sleep(2000);*/
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);

		int v = ReusableMethods.getDigits(getQuestionsCount);
		if (v == 0) {
			Assert.assertNull(null, "No record Found");

		}
		if (v <= 3) {
			getAddQuestionCount = v;
		} else {
			int questions = GetRandomId.getRandom(v);
			getAddQuestionCount = GetRandomId.getRandom(questions);
		}
		//buildTest.getDBQDocument();
		List<String> sourceDesc = buildTest.getDocumentSourceDescFilterview();
		for (int i = 0; i < sourceDesc.size(); i++) {
			LogUtil.log(sourceDesc.get(i).toString());
		}
		buildTest.addDBQUestions(getAddQuestionCount);
		String questionText = buildTest
				.getQuestionTextinBuildView(getAddQuestionCount);
		Assert.assertNotNull(questionText);

		String questionCount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		LogUtil.log(questionCount);
		int qcountDigits = ReusableMethods.getDigits(questionCount);
		LogUtil.log(qcountDigits);
		// int listViewQCount =GetRandomId.getRandom(qcountDigits);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		buildTest.clickBuildTestTabs("List");
		buildTest.validateQuestionsCollapsedListwMode();
		buildTest.getDBQtextinListview();
		buildTest.clickQuestionTitleListView(0);
		buildTest.validateQuestionsCollapsedListwMode();
		buildTest.clickQuestionTitleListView(0);

		List<String> qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			String qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> listviewButton = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
			for (int j = 0; j < listviewButton.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					js.executeScript("arguments[0].click();",
							listviewButton.get(j));
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					String questionType = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionType);
					String difficulty = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					LogUtil.log(qid + "=" + difficulty);
					String idNumber = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + idNumber);
					String bloomTaxonomy = buildTest
							.getMetadataAssociatedValuesListView(
									"Bloom’s Taxonomy", qid);
					LogUtil.log(qid + "=" + bloomTaxonomy);
					String chapterinfo = buildTest
							.getMetadataAssociatedValuesListView("Chapter", qid);
					LogUtil.log(qid + "=" + chapterinfo);
					// /String lobjective
					// =buildTest.getLearningObjdetailsMetaDatainListView();
					// LogUtil.log(qid + "=" +lobjective);
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					js.executeScript("arguments[0].click();",
							listviewButton.get(j));
				}
			}
		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}