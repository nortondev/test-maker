package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP38_TM_1670_TC4_insertImageinDBQQuestionStem extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;
	String beforeIdNumberValue;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP38_TM-1670_TC4_Log into Testmaker application and add only image in question stem for Document Based  and try to Save the changes to verify application does not displays error message for Empty filed")
	@Stories("SP38_TM-1670_TC4_Log into Testmaker application and add only image in question stem for Document Based  and try to Save the changes to verify application does not displays error message for Empty filed")
	@Test()
	public void insertImageinQuestionStem() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(2000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));

		Thread.sleep(2000);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);
		int v = ReusableMethods.getDigits(getQuestionsCount);
		for (int ele = 0; ele < v; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}
		if (v == 0) {
			Assert.assertNull(null, "No record Found");
		}
		if (v != 0) {
			getAddQuestionCount = 1;
		}
		buildTest.addDBQUestions(getAddQuestionCount);

		String questionText = buildTest
				.getQuestionTextinBuildView(getAddQuestionCount);
		Assert.assertNotNull(questionText);
		buildTest.getDBQDocumentBuildView();
		Assert.assertNotNull(buildTest.getDocumentSourceDescBuildView());
		buildTest.validateMoreInfoCollapsedBuildView();

		for (int i = 0; i < getAddQuestionCount; i++) {
			// Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
					beforeIdNumberValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
				}
			}
		}
		String questionCount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		System.out.println(questionCount);
		int qcountDigits = ReusableMethods.getDigits(questionCount);
		LogUtil.log(qcountDigits);
		// Verify Edit Question Button is Not displayed
		editQ = new EditQuestions();
		boolean isEditQuestionbutton = editQ.verifyEditQuestion();
		Assert.assertFalse(isEditQuestionbutton,
				"Edit Question Button is displayed for DBQ Question");
		editQ.clickEditQuestion();
		// remove Question Stem
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		Thread.sleep(2000);
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		String getQuestionStemtext = editQ
				.getErrormsgQuestionStembox("Question Stem");
		Assert.assertEquals(getQuestionStemtext.toString().trim(),
				"This is a required field.");
		String filePath = System.getProperty("user.dir")
				+ "\\image\\DBQImage.png";
		editQ.insertImage(filePath);
		ReusableMethods.pressKeyTab(driver);
		Thread.sleep(1000);
		Assert.assertNull(editQ.getErrorText(),
				"The Error Message is disappears");
		Thread.sleep(2000);
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
		}
		Thread.sleep(2000);
		cnt.clickSave();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		boolean isimageDisplayed = driver
				.findElements(
						By.xpath("//div[@class='pb2 mt2']/div//p/img[@data-mimetype='image/png']"))
				.size() > 0;
		if (isimageDisplayed == true) {
			Assert.assertTrue(isimageDisplayed,
					"Image is displayed in Question section");
		} else {
			Assert.assertFalse(isimageDisplayed,
					"Image is NOT displayed in Question section");
		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut = new TestMakerLogOutPage();
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut = new TestMakerLogOutPage();
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
