package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP38_TM_1444_TC9_editDBQQuestion_ListView extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP38_TM-1444_TC9_User navigates to Edit Question mode from List View, removes all documents, clicks Save button and verifies that error message is displayed correctly")
	@Stories("SP38_TM-1444_TC9_User navigates to Edit Question mode from List View, removes all documents, clicks Save button and verifies that error message is displayed correctly")
	@Test()
	public void editquestionDBQListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));

		Thread.sleep(2000);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);
		int v = ReusableMethods.getDigits(getQuestionsCount);
		for (int ele = 0; ele < v; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}
		if (v == 0) {
			Assert.assertNull(null, "No record Found");
		}
		if (v != 0) {
			getAddQuestionCount = 1;
		}
		buildTest.addDBQUestions(getAddQuestionCount);

		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		buildTest.clickBuildTestTabs("List");
		buildTest.validateQuestionsCollapsedListwMode();
		buildTest.getDBQtextinListview();
		buildTest.clickQuestionTitleListView(0);
		buildTest.validateQuestionsCollapsedListwMode();
		buildTest.clickQuestionTitleListView(0);

		List<String> qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			String qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> listviewButton = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
			for (int j = 0; j < listviewButton.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					js.executeScript("arguments[0].click();",
							listviewButton.get(j));
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					String questionType = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionType);
					String difficulty = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					LogUtil.log(qid + "=" + difficulty);
					String idNumber = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + idNumber);
					String bloomTaxonomy = buildTest
							.getMetadataAssociatedValuesListView(
									"Bloom’s Taxonomy", qid);
					LogUtil.log(qid + "=" + bloomTaxonomy);
					String chapterinfo = buildTest
							.getMetadataAssociatedValuesListView("Chapter", qid);
					LogUtil.log(qid + "=" + chapterinfo);
					// /String lobjective
					// =buildTest.getLearningObjdetailsMetaDatainListView();
					// LogUtil.log(qid + "=" +lobjective);
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					js.executeScript("arguments[0].click();",
							listviewButton.get(j));
				}
			}
		}
		// Verify Edit Question Button DBQ displayed
		editQ = new EditQuestions();
		boolean isEditQuestionbutton = editQ.verifyEditQuestion();
		Assert.assertFalse(isEditQuestionbutton,
				"Edit Question Button is displayed for DBQ Question");
		buildTest.clickQuestionTitleListView(0);
		editQ.clickEditQuestion();
		// remove all document
		editQ.removeDBQDocuments(0);
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		Assert.assertEquals("This question requires a document to be saved.",
				editQ.getErrorremoveDBQDocuments("Question Documents*"));
		buildTest.clickCancelButton();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
