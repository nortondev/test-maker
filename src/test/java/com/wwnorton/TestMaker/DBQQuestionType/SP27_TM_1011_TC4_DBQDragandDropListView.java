package com.wwnorton.TestMaker.DBQQuestionType;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP27_TM_1011_TC4_DBQDragandDropListView extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();
	int getAddQuestionCount;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP27_TM-1011_TC4 Instructor selects a question, drags and drops in middle of list and verifies that question number and details are displayed correctly.")
	@Stories("SP27_TM-1011_TC4 Instructor selects a question, drags and drops in middle of list and verifies that question number and details are displayed correctly.")
	@Test()
	public void questionDBQListViewDragandDrop() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		ReusableMethods.checkPageIsReady(driver);
		buildTest = new BuildTestPage();
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			buildTest.clickAddFiltersButton();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		buildTest.clickApplyButton();
		driver.findElement(By.id("regionAside")).click();

		driver.findElements(By.xpath("//div[@class='mx3']"));
		actions = new Actions(driver);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}

		Thread.sleep(2000);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		int v = ReusableMethods.getDigits(getQuestionsCount);
		if (v >= 1) {
			getAddQuestionCount = v;
			buildTest.addDBQUestions(getAddQuestionCount);
		}

		String questionCount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		LogUtil.log(questionCount);
		int qcountDigits = ReusableMethods.getDigits(questionCount);
		LogUtil.log(qcountDigits);
		// int listViewQCount =GetRandomId.getRandom(qcountDigits)

		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-500)");
		buildTest.clickBuildTestTabs("List");
		// buildTest.validateQuestionsCollapsedListwMode();
		buildTest.dragDropQuestionsTitleListView(1);
		buildTest.questionSequenceafterDragDrop();
		String questionCountinListView = buildTest
				.verifyQuestioninTestBuildSearchResult();
		LogUtil.log(questionCountinListView);
		// Verify total number of questions is displayed correctly
		Assert.assertEquals(questionCountinListView, questionCount);
		buildTest.clickQuestionTitleListView(0);
		buildTest.validateQuestionsCollapsedwMode();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
