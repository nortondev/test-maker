package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP26_TM_1009_TC2_DBQSearchresults extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();
	String getText, SearchText;
	ArrayList<String> searchKeys ;
	WebElement getIDvalue;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP26_TM-1009_TC2_Log into testmaker application and perform Search with any valid search term to verify DBQ in Search Result page")
	@Stories("SP26_TM-1009_TC2_Log into testmaker application and perform Search with any valid search term to verify DBQ in Search Result page")
	@Test()
	public void questionDBQSearch() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		buildTest.clickApplyButton();
		driver.findElement(By.id("regionAside")).click();

		driver.findElements(By.xpath("//div[@class='mx3']"));
		actions = new Actions(driver);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
		}

		Thread.sleep(2000);
		String questionIDs = null;

		List<WebElement> dbqaddQuestionslist = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]"));
		List<String> documentQID = new ArrayList<String>();
		for (WebElement qID : dbqaddQuestionslist) {
			questionIDs = qID.getAttribute("id");
			documentQID.add(questionIDs);

			ReusableMethods.scrollIntoView(driver, qID);
			
			getIDvalue = driver.findElement(By.xpath("//div[@id='"
					+ questionIDs + "']/div/div[1]/div"));

			getText = getIDvalue.getText();
			if (getText.startsWith("T")) {
				continue;
			}
		}
	    searchKeys = new ArrayList<String>();
		SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);

		while (getText.startsWith("T")||getText.isEmpty()) {
			String questionIDs1 = null;

			List<WebElement> dbqaddQuestionslist1 = driver
					.findElements(By
							.xpath("//div[starts-with(@class,'px1-5 fr-element fr-view')][@id]"));
			List<String> documentQID1 = new ArrayList<String>();
			for (WebElement qID1 : dbqaddQuestionslist1) {
				questionIDs1 = qID1.getAttribute("id");
				documentQID1.add(questionIDs1);
				ReusableMethods.scrollIntoView(driver, qID1);
				Thread.sleep(2000);
				getIDvalue = driver.findElement(By
						.xpath("//div[@id='" + questionIDs1
								+ "']/div/div[1]/div"));
			}
				getText = getIDvalue.getText();
				if (!getText.startsWith("T")|| !getText.isEmpty()) {
					continue;
				
			}
		}
			
			SearchText = POS.givenPOSModel_whenPOSTagging_thenPOSAreDetected(
					getText, searchKeys);
			System.out.println(SearchText);
		

		Thread.sleep(5000);
		ReusableMethods.scrollToElement(driver,
				By.id("__input__text__search__1"));
		String getTextt = SearchText.toString();
		System.out.println(getTextt);

		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		Thread.sleep(5000);

		String getDBQQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(getDBQQuestionsCount);
		int qcountApplyFilter = ReusableMethods.getDigits(getDBQQuestionsCount);
		int questions = GetRandomId.getRandom(qcountApplyFilter);
		LogUtil.log(questions);
		// buildTest.clickDBQAddQuestionsIcon(questions);
		buildTest.getDBQQuestionDescription();
	//	buildTest.getDBQDocument();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
