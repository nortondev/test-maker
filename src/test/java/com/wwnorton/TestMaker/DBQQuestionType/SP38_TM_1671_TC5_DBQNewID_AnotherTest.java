package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP38_TM_1671_TC5_DBQNewID_AnotherTest extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;
	WebsitePage website;
	String beforeQtypeValue, beforeDiffValue, beforeIdNumberValue,
			beforeBloomtaxValue;
	String afterQtypeValue, afterDiffValue, afterIdNumberValue,
			afterBloomtaxValue, afterEditIdNumberValue;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP38_TM-1671_TC5_Log into Testmaker application and edit User Document based question in another test to verify Application assign new ID Number with updated identifier E and number of edited question")
	@Stories("SP38_TM-1671_TC5_Log into Testmaker application and edit User Document based question in another test to verify Application assign new ID Number with updated identifier E and number of edited question")
	@Test()
	public void idNumberChangeAnotherTest() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(2000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
	//	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			buildTest.clickAddFiltersButton();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));

		Thread.sleep(2000);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);
		String getText =buildTest.getDBQSearchText();
		int v = ReusableMethods.getDigits(getQuestionsCount);
		for (int ele = 0; ele < v; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}
		if (v == 0) {
			Assert.assertNull(null, "No record Found");
		}
		if (v != 0) {
			getAddQuestionCount = 1;
		}
		buildTest.addDBQUestions(getAddQuestionCount);

		String questionText = buildTest
				.getQuestionTextinBuildView(getAddQuestionCount);
		Assert.assertNotNull(questionText);
		buildTest.getDBQDocumentBuildView();
		Assert.assertNotNull(buildTest.getDocumentSourceDescBuildView());
		buildTest.validateMoreInfoCollapsedBuildView();

		for (int i = 0; i < getAddQuestionCount; i++) {
			// Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
					beforeIdNumberValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
				}
			}
		}
		String questionCount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		System.out.println(questionCount);
		int qcountDigits = ReusableMethods.getDigits(questionCount);
		LogUtil.log(qcountDigits);
		// Verify Edit Question Button is Not displayed
		editQ = new EditQuestions();
		boolean isEditQuestionbutton = editQ.verifyEditQuestion();
		Assert.assertFalse(isEditQuestionbutton,
				"Edit Question Button is displayed for DBQ Question");
		editQ.clickEditQuestion();
		// remove Question Stem
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		Thread.sleep(5000);
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		String getQuestionStemtext = editQ
				.getErrormsgQuestionStembox("Question Stem");
		Assert.assertEquals(getQuestionStemtext.toString().trim(),
				"This is a required field.");
		editQ.modifyQuestionText(expectedQuestiontext);
		Thread.sleep(1000);
		editQ.questiondocumentLabel.click();
		Thread.sleep(1000);
		Assert.assertNull(editQ.getErrorText(),
				"The Error Message is disappears");
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		Thread.sleep(2000);
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		cnt.clickSave();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		for (int i = 0; i < getAddQuestionCount; i++) {
			// Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
					afterIdNumberValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
				}
			}
		}
		Assert.assertNotEquals(beforeIdNumberValue, afterIdNumberValue);
		System.out.println(afterIdNumberValue);
		String str = afterIdNumberValue.toString();
		str = str.replaceAll("\\[", "").replaceAll("\\]", "");
		boolean containsEText = str.contains("E");
		Assert.assertTrue(containsEText,
				" The Added Question contains letter E");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName1 = cnt.getTestName();
		cnt.clickSave();
		LogUtil.log(buildTestName1);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName1);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(2000);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(1);
		buildTest.clickApplyButton();
		Thread.sleep(5000);
		buildTest.clickSearchQuestionsTextbox(getText);
		buildTest.clickSearchLinkbutton();
		Thread.sleep(5000);
		editQ.getIDNumberMetadata(afterIdNumberValue);
		editQ.clickEditQuestion();
		// remove Question Stem
		String expectedQuestiontextnew = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontextnew);
		editQ.removeQuestionText();
		actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		Thread.sleep(2000);
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		String getQuestionStemtextNew = editQ
				.getErrormsgQuestionStembox("Question Stem");
		Assert.assertEquals(getQuestionStemtextNew.toString().trim(),
				"This is a required field.");
		editQ.modifyQuestionText(expectedQuestiontextnew + "Modified");
		Thread.sleep(1000);
		editQ.questiondocumentLabel.click();
		Thread.sleep(1000);
		Assert.assertNull(editQ.getErrorText(),
				"The Error Message is disappears");
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		Thread.sleep(2000);
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		cnt.clickSave();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		for (int i = 0; i < getAddQuestionCount; i++) {
			// Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
					afterEditIdNumberValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
				}
			}
		}
		Assert.assertNotEquals(afterEditIdNumberValue, afterIdNumberValue);
		System.out.println(afterEditIdNumberValue);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__0')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut = new TestMakerLogOutPage();
			TMlogOut.logOutTestMakerApp();

		} else {
			cnt.clickTestMakerBackButton();
			ReusableMethods.checkPageIsReady(driver);
			TMlogOut = new TestMakerLogOutPage();
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
