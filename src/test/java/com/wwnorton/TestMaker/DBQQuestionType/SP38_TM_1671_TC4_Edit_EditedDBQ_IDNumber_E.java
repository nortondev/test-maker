package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP38_TM_1671_TC4_Edit_EditedDBQ_IDNumber_E extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;
	WebsitePage website;
	String beforeQtypeValue, beforeDiffValue, beforeIdNumberValue,
			beforeBloomtaxValue;
	String afterQtypeValue, afterDiffValue, afterIdNumberValue,
			afterBloomtaxValue;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP38_TM-1671_TC4_Log into Testmaker application and edit User Document based question in same test to verify Application doesn't assign new ID Number and carry the same Question number of Edited question")
	@Stories("SP38_TM-1671_TC4_Log into Testmaker application and edit User Document based question in same test to verify Application doesn't assign new ID Number and carry the same Question number of Edited question")
	@Test()
	public void savequestionStemDBQBuildView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(2000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		try {
			buildTest.clickAddFiltersButton();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(1);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));

		Thread.sleep(2000);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);
		int v = ReusableMethods.getDigits(getQuestionsCount);
		/*for (int ele = 0; ele < v; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}*/
		if (v == 0) {
			Assert.assertNull(null, "No record Found");
		}
		if (v != 0) {
			getAddQuestionCount = 1;
		}
		buildTest.addDBQUestions(getAddQuestionCount);

		String questionText = buildTest
				.getQuestionTextinBuildView(getAddQuestionCount);
		Assert.assertNotNull(questionText);
		buildTest.getDBQDocumentBuildView();
		Assert.assertNotNull(buildTest.getDocumentSourceDescBuildView());
		buildTest.validateMoreInfoCollapsedBuildView();

		for (int i = 0; i < getAddQuestionCount; i++) {
			// Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
					beforeQtypeValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("Question Type");
					beforeDiffValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("Difficulty");
					beforeIdNumberValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					beforeBloomtaxValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("Bloom’s Taxonomy");
					buildTest.getChapterdetailsMetaDatainBuildView();
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
				}
			}
		}
		String questionCount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		System.out.println(questionCount);
		int qcountDigits = ReusableMethods.getDigits(questionCount);
		LogUtil.log(qcountDigits);
		// Verify Edit Question Button is Not displayed
		editQ = new EditQuestions();
		boolean isEditQuestionbutton = editQ.verifyEditQuestion();
		Assert.assertFalse(isEditQuestionbutton,
				"Edit Question Button is displayed for DBQ Question");
		editQ.clickEditQuestion();
		// remove Question Stem
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		Thread.sleep(2000);
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		String getQuestionStemtext = editQ
				.getErrormsgQuestionStembox("Question Stem");
		Assert.assertEquals(getQuestionStemtext.toString().trim(),
				"This is a required field.");
		
		editQ.modifyQuestionText(expectedQuestiontext);
		Thread.sleep(1000);
		editQ.editQuestionbox.click();
		ReusableMethods.sendShortCut(driver);;
		Thread.sleep(1000);
		Assert.assertNull(editQ.getErrorText(),
				"The Error Message is disappears");
		Thread.sleep(5000);
		
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
		}
		
		cnt.clickSave();
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		for (int i = 0; i < getAddQuestionCount; i++) {
			// Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@id='regionYourBuildTest']//*[name()='svg'][@id='__icon__accordioncollapsgrey__________0']"))
							.size() > 0;
					if (isexpanded == false) {
						js.executeScript("arguments[0].click();",
								moreInfo.get(j));
					}
					/*js.executeScript("arguments[0].click();", moreInfo.get(j));*/
					Thread.sleep(1000);
					afterQtypeValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("Question Type");
					afterDiffValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("Difficulty");
					afterIdNumberValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					afterBloomtaxValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("Bloom’s Taxonomy");
					buildTest.getChapterdetailsMetaDatainBuildView();
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
				}
			}
		}
		Thread.sleep(3000);
		Assert.assertNotEquals(beforeIdNumberValue, afterIdNumberValue);
		System.out.println(afterIdNumberValue);
		String str = afterIdNumberValue.toString();
		str = str.replaceAll("\\[", "").replaceAll("\\]", "");
		boolean containsEText = str.contains("E");
		Assert.assertTrue(containsEText,
				" The Added Question contains letter E");
		buildTest.removeQuestionBuildView(0);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickClearFiltersButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Source");
		Thread.sleep(1000);
		buildTest.selectQuestionSourceOptionNames(1);
		buildTest.clickApplyButton();
		Thread.sleep(3000);
		editQ.getIDNumberMetadata(afterIdNumberValue);
		String str1 = afterIdNumberValue.toString();
		String str2 = beforeIdNumberValue.toString().replaceAll("\\[", "")
				.replaceAll("\\]", "");
		str1 = str1.replaceAll("\\[", "").replaceAll("\\]", "");
		// Get the Second Version of the Question
		String oldVersion = str2;
		String[] splitString = oldVersion.split("E");
		int newVersion = Integer.valueOf(splitString[1]) + 1;
		String completeNewVersion = splitString[0] + newVersion;
		LogUtil.log(completeNewVersion);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
			
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
