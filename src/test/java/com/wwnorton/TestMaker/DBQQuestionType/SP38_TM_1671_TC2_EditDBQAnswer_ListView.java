package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP38_TM_1671_TC2_EditDBQAnswer_ListView extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;
	WebsitePage website;
	String beforeQtypeValue, beforeDiffValue, beforeIdNumberValue,
			beforeBloomtaxValue;
	String afterQtypeValue, afterDiffValue, afterIdNumberValue,
			afterBloomtaxValue;
	String questionType, IDNumber, Diffculty, Bloomstaxonomy;
	String expectedvalue = "Bloom's Taxonomy";
	String questionTypeListView, difficultyListView, idNumberListView,
			bloomTaxonomy, difficultyListView1;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM-1671_TC2_Log into Testmaker application in List view and verify Instructor Edit DBQ Answer and Save the changes and return to List view")
	@Stories("TM-1671_TC2_Log into Testmaker application in List view and verify Instructor Edit DBQ Answer and Save the changes and return to List view")
	@Test()
	public void editDBQQuestionListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(2000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));

		Thread.sleep(2000);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);
		int v = ReusableMethods.getDigits(getQuestionsCount);
		for (int ele = 0; ele < v; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}
		if (v == 0) {
			Assert.assertNull(null, "No record Found");
		}
		if (v != 0) {
			getAddQuestionCount = 1;
		}

		buildTest.addDBQUestions(getAddQuestionCount);

		String questionText = buildTest
				.getQuestionTextinBuildView(getAddQuestionCount);
		Assert.assertNotNull(questionText);
		buildTest.getDBQDocumentBuildView();
		Assert.assertNotNull(buildTest.getDocumentSourceDescBuildView());
		buildTest.validateMoreInfoCollapsedBuildView();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ = new EditQuestions();
		buildTest.clickBuildTestTabs("List");
		getListViewQuestionMetaData();
		String answerText = editQ.getAnswerText();
		String qtype = questionTypeListView;
		String dtype = difficultyListView;
		String idNumber = idNumberListView;
		String bloomtype = bloomTaxonomy;

		String questionCount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		System.out.println(questionCount);
		int qcountDigits = ReusableMethods.getDigits(questionCount);
		LogUtil.log(qcountDigits);
		// Verify Edit Question Button is Not displayed
		editQ = new EditQuestions();
		boolean isEditQuestionbutton = editQ.verifyEditQuestion();
		Assert.assertFalse(isEditQuestionbutton,
				"Edit Question Button is displayed for DBQ Question");
		editQ.clickEditQuestion();
		// remove Question Stem
		editQ.modifyQuestionAnswer("Modified Answer Key");
		Thread.sleep(1000);
		editQ.deleteListValue("Difficulty");
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		cnt.clickSave();
		Thread.sleep(2000);
		getListViewQuestionMetaData();
		String answerTextafterSave = editQ.getAnswerText();
		String qtypeAfterSave = questionTypeListView;
		String dtypeAfterSave = difficultyListView;
		String idNumberAfterSave = idNumberListView;
		String bloomtypeAfterSave = bloomTaxonomy;

		Assert.assertEquals(qtype, qtypeAfterSave);
		if (dtype == null) {
			Assert.assertNull(dtype);
		} else {
			Assert.assertEquals(dtype, dtypeAfterSave);
		}

		Assert.assertEquals(bloomtype, bloomtypeAfterSave);
		Assert.assertNotEquals(idNumber, idNumberAfterSave);
		Assert.assertNotEquals(answerText, answerTextafterSave);

		Thread.sleep(3000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

	public void getListViewQuestionMetaData() throws InterruptedException {
		List<String> qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			String qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> listviewButton = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
			for (int j = 0; j < listviewButton.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@class='border-none bg-white none']"))
							.size() > 0;
					if (isexpanded == true) {
						js.executeScript("arguments[0].click();",
								listviewButton.get(j));
					}
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					questionTypeListView = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionTypeListView);
					difficultyListView = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					if (difficultyListView == null) {
						difficultyListView1 = "Diffculty=" + difficultyListView;
					}
					LogUtil.log(qid + "=" + difficultyListView);
					idNumberListView = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + idNumberListView);
					String ListViewBloomTx = buildTest
							.getBloomsTaxonomyTextAddedSectionListView()
							.toString().trim();

					Assert.assertEquals(ListViewBloomTx, expectedvalue
							.toString().trim());
					bloomTaxonomy = buildTest
							.getMetadataAssociatedValuesListView("Bloom", qid);
					LogUtil.log(qid + "=" + bloomTaxonomy);
				}
			}
		}
	}

}
