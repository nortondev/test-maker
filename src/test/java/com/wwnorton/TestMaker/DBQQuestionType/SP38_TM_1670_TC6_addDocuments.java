package com.wwnorton.TestMaker.DBQQuestionType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

import edu.emory.mathcs.backport.java.util.Collections;

@Listeners({ TestListener.class })
public class SP38_TM_1670_TC6_addDocuments extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	EditQuestions editQ;
	Actions actions;
	JavascriptExecutor js;
	int getAddQuestionCount;
	String beforeIdNumberValue;
	ArrayList<String> sortedList, sortedListAfteradd;
	List<String> documentlist, documentlistAfteradd;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP38_TM-1670_TC6_Log into Testmaker application and try to add more than 10 question documents for Document Based  question")
	@Stories("SP38_TM-1670_TC6_Log into Testmaker application and try to add more than 10 question documents for Document Based  question")
	@Test()
	public void addDocuments() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(2000);
		TMlogin.appendISBN(ReusableMethods.dbqQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("DOCUMENT BASED QUESTION");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		driver.findElement(By.id("regionAside")).click();
		actions = new Actions(driver);
		driver.findElements(By.xpath("//div[@class='mx3']"));

		Thread.sleep(2000);
		String getQuestionsCount = buildTest
				.getQuestionCountAfterQuestionSearch();
		// int qcountApplyFilter =ReusableMethods.getDigits(getQuestionsCount);
		int v = ReusableMethods.getDigits(getQuestionsCount);
		for (int ele = 0; ele < v; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
		}
		if (v == 0) {
			Assert.assertNull(null, "No record Found");
		}
		if (v != 0) {
			getAddQuestionCount = 1;
		}
		buildTest.addDBQUestions(getAddQuestionCount);

		String questionText = buildTest
				.getQuestionTextinBuildView(getAddQuestionCount);
		Assert.assertNotNull(questionText);
		/*
		 * buildTest.getDBQDocumentBuildView();
		 * Assert.assertNotNull(buildTest.getDocumentSourceDescBuildView());
		 * buildTest.validateMoreInfoCollapsedBuildView();
		 */

		for (int i = 0; i < getAddQuestionCount; i++) {
			// Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
			js = (JavascriptExecutor) driver;
			List<WebElement> moreInfo = driver.findElements(By
					.xpath("//button[contains(text(),'More Info')]"));
			for (int j = 0; j < moreInfo.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							moreInfo.get(j));
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
					beforeIdNumberValue = buildTest
							.getMetadataAssociatedValuesBuildTestView("ID Number");
					js.executeScript("arguments[0].click();", moreInfo.get(j));
					Thread.sleep(1000);
				}
			}
		}
		String questionCount = buildTest
				.verifyQuestioninTestBuildSearchResult();
		System.out.println(questionCount);
		int qcountDigits = ReusableMethods.getDigits(questionCount);
		LogUtil.log(qcountDigits);
		// get Document Count
		WebElement getDocumentCount = driver.findElement(By
				.xpath("//button[starts-with(@id,'__input__button__Show__')]"));
		String getDocumentText = getDocumentCount.getText();
		int getDocumentNumber = ReusableMethods.getDigits(getDocumentText);
		LogUtil.log(getDocumentNumber);
		// Verify Edit Question Button is Not displayed
		editQ = new EditQuestions();
		boolean isEditQuestionbutton = editQ.verifyEditQuestion();
		Assert.assertFalse(isEditQuestionbutton,
				"Edit Question Button is displayed for DBQ Question");
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(8000);
		// driver.navigate().refresh();
		// WebElement addDocument =
		// driver.findElement(By.xpath("//button[@type='button']/div/div[text()='Add Document']"));
		WebElement addButton = driver
				.findElement(By
						.xpath("//div[text()='Add Document']/preceding-sibling::*[name()='svg']/parent::div/parent::*"));
		for (int i = 0; i < 10; i++) {
			ReusableMethods
					.scrollToElement(
							driver,
							By.xpath("//div[@class='bold darkGray pt2'][text()='Question Answer']"));
			if (!addButton.isEnabled()) {
				Assert.assertTrue(!addButton.isEnabled(),
						"Add Document Button is Disabled");
				break;
			} else {
				editQ.clickaddDocumentbutton();
				Thread.sleep(2000);
				List<WebElement> documentele = driver
						.findElements(By
								.xpath("//div[@id='regionEditQuestion']//div[@class='pb1-5 pt1']//div[starts-with(@class,'fr-element fr-view')]"));
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", documentele.get(i));
				
				//documentele.get(i).click();
				documentele.get(i).sendKeys(
						"User has added the " + (i + 1) + " Document");
			}

		}
		Actions act = new Actions(driver);
		act.sendKeys(Keys.TAB);
        Thread.sleep(4000);
		documentlistAfteradd = new ArrayList<>();
		sortedListAfteradd = new ArrayList<>();
		documentlistAfteradd = editQ.getListDocument();
		sortedListAfteradd.addAll(documentlistAfteradd);
		Collections.sort(sortedListAfteradd);
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		cnt.clickSave();
		Thread.sleep(2000);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		List<WebElement> documentele = driver
				.findElements(By
						.xpath("//div[@id='regionEditQuestion']//div[@class='pb1-5 pt1']//div[starts-with(@class,'fr-element fr-view')]"));
		for (int i = 0; i < documentele.size(); i++) {
			ReusableMethods.scrollIntoView(driver, documentele.get(i));
			String contentEditable = documentele.get(i).getAttribute(
					"contenteditable");
			if (contentEditable.equalsIgnoreCase("true")) {
				Assert.assertNotNull(contentEditable.equalsIgnoreCase("true"),
						"Document Text is Editable");
			} else {
				Assert.assertNotNull(contentEditable.equalsIgnoreCase("false"),
						"Document Text is NON Editable");
			}
		}
		cnt.cancelButton.click();
		Thread.sleep(2000);
		WebElement getDocumentCountAfterAdd = driver
				.findElement(By
						.xpath("//div[@id='regionYourBuildTest']//button[starts-with(@id,'__input__button__Show__')]"));
		String getDocumentTextAddDoc = getDocumentCountAfterAdd.getText();
		int getDocumentNumberAddDoc = ReusableMethods
				.getDigits(getDocumentTextAddDoc);
		int documentCount = 10;
		Assert.assertEquals(documentCount, getDocumentNumberAddDoc,
				"10 Documents are displayed on the page");
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
