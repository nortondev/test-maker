package com.wwnorton.TestMaker.EditQuestions;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1492_TC3_MULTIPLECHOICE_Build_ListView_Cancel extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber, Difficulty, Bloomstaxonomy;
	String questionTypeAftercancel, IDNumberAftercancel, DiffcultyAftercancel,
			BloomstaxonomyAftercancel;
	List<WebElement> moreInfo;
	String expectedvalue = "Bloom's Taxonomy";
	String questionTypeListView, difficultyListView, idNumberListView,
			bloomTaxonomy;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1492_TC3_User edits MULTIPLE CHOICE type question in Build View, clicks Cancel and verifies that changes are not saved to the test")
	@Stories("SP31_TM-1492_TC3_User edits MULTIPLE CHOICE type question in Build View, clicks Cancel and verifies that changes are not saved to the test")
	@Test()
	public void editQuestionMultipleChoice_ValidateChangesBuildandListView()
			throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}

		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		List<String> filterNames = buildTest.verifyMultipleFilterName();

		for (int i = 0; i < filterNames.size(); i++) {
			LogUtil.log(filterNames.get(i));
		}
		editQ = new EditQuestions();
		actions = new Actions(driver);
		boolean isQgroup = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'pl2 darkGray h5  pt3 pb2 mt1-5')][contains(text(),'Question Group')]"))
				.size() > 0;
		// boolean isIDNumber =
		// driver.findElements(By.xpath("//div[starts-with(@class,'sm-col-2 left-align darkerGray small-body-text font-weight-')][contains(text(),'Question Type')]")).size()
		// > 0;
		while (isQgroup == true) {

			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			actions.keyDown(Keys.CONTROL).release().perform();
			isQgroup = !isQgroup;
		}
		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-5')]"));
		String id = qM0.getAttribute("id");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__0')]"));
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		LogUtil.log(getText);
		editQ.clickAddNortonQuestion();
		// get Edit Question details
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			questionType = buildTest
					.getMetadataAssociatedValuesBuildTestView("Question Type");
			Difficulty = buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			Bloomstaxonomy = buildTest
					.getMetadataAssociatedValuesBuildTestView("Bloom's Taxonomy");
		}
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.changeListValue("Difficulty");
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		// editQ.questionNotSavedpopUp();
		editQ.clickConfirmCancel();

		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			questionTypeAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("Question Type");
			DiffcultyAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");
			IDNumberAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			BloomstaxonomyAftercancel = buildTest
					.getMetadataAssociatedValuesBuildTestView("Bloom’s Taxonomy");
		}

		Assert.assertEquals(questionType, questionTypeAftercancel);
		Assert.assertEquals(Difficulty, DiffcultyAftercancel);
		Assert.assertEquals(IDNumber, IDNumberAftercancel);
		Assert.assertEquals(Bloomstaxonomy, BloomstaxonomyAftercancel);
		// Click on List view tab
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		buildTest.clickBuildTestTabs("List");
		getListViewQuestionMetaData();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ.clickEditQuestion();
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.changeListValue("Difficulty");
		editQ.deleteListValue("Bloom");
		editQ.changeListValue("Bloom");
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		// editQ.questionNotSavedpopUp();
		editQ.clickConfirmCancel();
		getListViewQuestionMetaData();
		String qtype = "Question Type=" + questionType;
		String dtype = "Difficulty=" + Difficulty;
		String idNumber = "ID Number=" + IDNumber;
		String bloomtype = "Bloom's Taxonomy=" + Bloomstaxonomy;
		Assert.assertEquals(qtype, questionTypeListView);
		Assert.assertEquals(dtype, difficultyListView);
		Assert.assertEquals(idNumber, idNumberListView);
		Assert.assertEquals(bloomtype, bloomTaxonomy);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

	public void getListViewQuestionMetaData() throws InterruptedException {
		driver = getDriver();
		List<String> qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			String qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> listviewButton = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
			for (int j = 0; j < listviewButton.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@class='border-none bg-white none']"))
							.size() > 0;
					if (isexpanded == true) {
						js.executeScript("arguments[0].click();",
								listviewButton.get(j));
					}
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					questionTypeListView = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionTypeListView);
					difficultyListView = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					if (difficultyListView == null) {
						difficultyListView = "Difficulty=" + difficultyListView;
					}
					LogUtil.log(qid + "=" + difficultyListView);
					idNumberListView = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + idNumberListView);
					String ListViewBloomTx = buildTest
							.getBloomsTaxonomyTextAddedSectionListView()
							.toString().trim();

					Assert.assertEquals(ListViewBloomTx, expectedvalue
							.toString().trim());
					bloomTaxonomy = buildTest
							.getMetadataAssociatedValuesListView("Bloom", qid);
					LogUtil.log(qid + "=" + bloomTaxonomy);
				}
			}
		}
	}

}
