package com.wwnorton.TestMaker.EditQuestions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP32_TM_1438_TC2_SHORTANSWER_metadata_NewlineValue extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	WebsitePage website;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	WebsitePage Website;
	String questionType, IDNumber, Difficulty, Bloomstaxonomy;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			BloomstaxonomyAfterSave;
	List<WebElement> moreInfo;
	String expectedvalue = "Bloom's Taxonomy";
	List<String> editQBloomstaxonomy;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1438_TC2_User edits test having SHORT ANSWER question with multiple metadata values and verifies that each value is displayed on new line.")
	@Stories("SP32_TM-1438_TC2_User edits test having SHORT ANSWER question with multiple metadata values and verifies that each value is displayed on new line.")
	@Test()
	public void shortAnswerMultipleMetaData() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("SHORT ANSWER");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		System.out.println(questionCountAfterFilter);
		List<String> filterNames = buildTest.verifyMultipleFilterName();

		for (int i = 0; i < filterNames.size(); i++) {
			Assert.assertEquals(filterNames.get(0), "Short Answer");
		}
		// ReusableMethods.scrollQuestionSection(driver);
		Thread.sleep(4000);

		Assert.assertTrue(isQuestionGroupDisplayed(),
				"Question Group questions are displayed");
		buildTest.clickSingleQuestionQGQuestionType();
		Thread.sleep(2000);

		Difficulty = questionmetaData("Difficulty");
		Bloomstaxonomy = questionmetaData("Bloom");

		editQ = new EditQuestions();
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(2000);
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			System.out.println(IDNumber);
		}

		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.selectMultipleListValue("Difficulty");
		Thread.sleep(2000);
		editQBloomstaxonomy = editQ.getListValue("Bloom");
		Assert.assertEquals(
				editQBloomstaxonomy.toString().replaceAll("\\[\\]", ""),
				Bloomstaxonomy.toString());
		editQ.deleteListValue("Bloom");
		editQ.selectMultipleListValue("Bloom");
		cnt.clickSave();
		BloomstaxonomyAfterSave = questionmetaData("Bloom");
		LogUtil.log(BloomstaxonomyAfterSave);
		DiffcultyAfterSave = questionmetaData("Difficulty");
		LogUtil.log(DiffcultyAfterSave);
		// get the Number of div to check each meta data is displayed in new
		// line
		int difficultycount = buildTest.getmetaDataCount("Difficulty");
		if (difficultycount > 0) {
			Assert.assertNotEquals(difficultycount, 0);
		}
		int bloomtaxonomy = buildTest.getmetaDataCount("Bloom");
		if (bloomtaxonomy > 0) {
			Assert.assertNotEquals(bloomtaxonomy, 0);
		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

	public boolean isQuestionGroupDisplayed() throws InterruptedException {
		driver = getDriver();
		actions = new Actions(driver);
		driver.findElement(By.id("regionAside")).click();
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//button[@type='button'][starts-with(text(),'Show')]"));
		for (int ele = 0; ele < getQuestionText.size(); ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_UP).build().perform();

		}
		return true;
	}

	public String questionmetaData(String metadatatext)
			throws InterruptedException {
		driver = getDriver();

		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(3000);
			if (metadatatext.startsWith("Bloom")) {
				List<WebElement> bloomTextList = driver
						.findElements(By
								.xpath("//div[@id='regionYourBuildTest']//div[contains(text(),\"Bloom's Taxonomy\")]/following-sibling::div/p"));
				List<String> bloomTextValues = new ArrayList<String>();
				Iterator<WebElement> itr = bloomTextList.iterator();
				while (itr.hasNext()) {
					WebElement bloomtextEle = itr.next();
					bloomTextValues.add(bloomtextEle.getText());
				}
				metadatatext = bloomTextValues.toString().replaceAll("\\[\\]",
						"");
			} else {
				List<WebElement> difficultyTextList = driver
						.findElements(By
								.xpath("//div[@id='regionYourBuildTest']//div[contains(text(),'Difficulty')]/following-sibling::div/p"));
				List<String> difficultyTextValues = new ArrayList<String>();
				Iterator<WebElement> itr = difficultyTextList.iterator();
				while (itr.hasNext()) {
					WebElement difftextEle = itr.next();
					difficultyTextValues.add(difftextEle.getText());
				}
				metadatatext = difficultyTextValues.toString().replaceAll(
						"\\[\\]", "");
			}
			js.executeScript("arguments[0].click();", moreInfo.get(j));

		}

		return metadatatext;
	}
}
