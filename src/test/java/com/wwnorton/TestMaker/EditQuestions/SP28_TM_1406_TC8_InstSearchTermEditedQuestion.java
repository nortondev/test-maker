package com.wwnorton.TestMaker.EditQuestions;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class SP28_TM_1406_TC8_InstSearchTermEditedQuestion extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	String testName;
	String buildTestName;
	String filterName;
	String questionCountAfterFilter;
	Actions actions;
	getPOS POS = new getPOS();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP28_TM-1406_TC8_Log into testmaker with instructor who edited the question and perform search with valid search term to verify application displays search result from user edited question also")
	@Stories("SP28_TM-1406_TC8_Log into testmaker with instructor who edited the question and perform search with valid search term to verify application displays search result from user edited question also")
	@Test()
	public void SearchtermInstructorEditedQuestion() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		buildTest = new BuildTestPage();
		cnt = new CreateNewTestPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		
		try {
			driver.switchTo().activeElement().click();
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		filterName = buildTest.clickFiltersButton("Question Source");
		Assert.assertNotNull(filterName,
				"Question Source Filter Name is displayed");
		buildTest.selectQuestionSourceOptionNames(1);
		buildTest.clickChaptersButton();
		buildTest.clickChaptersCheckboxesandChaptersName(0);
		buildTest.clickApplyButton();
		
		driver.findElement(By.id("regionAside")).click();

		driver.findElements(By.xpath("//div[@class='mx3']"));
		actions = new Actions(driver);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
		}
		Thread.sleep(2000);
		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-')]"));
		String id = qM0.getAttribute("id");
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		ArrayList<String> searchKeys = new ArrayList<String>();
		String SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);
		Thread.sleep(5000);
		ReusableMethods.scrollToElement(driver,
				By.id("__input__text__search__1"));
		String getTextt = SearchText.toString();
		System.out.println(getTextt);
		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		Thread.sleep(5000);
		String questionCountAfterSearchQuestion = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestion);
		System.out.println(questionCountAfterSearchQuestion);
		String regex = "[^\\d]+";
		String[] str = questionCountAfterSearchQuestion.split(regex);
		System.out.println(str[0]);
		int v = Integer.parseInt(str[0]);
		int k = v / 10;
		driver.findElement(By.id("regionAside")).click();

		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		List<WebElement> element = driver.findElements(By
				.xpath("//div[@class='pb2 mt3 ml1 pl2']"));
		for (k = 0; k < element.size(); k++) {
			element.get(k).getCssValue("background-color");
			ReusableMethods.highLighterMethod(driver, element.get(k));
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			Thread.sleep(5000);

		}
		actions.sendKeys(Keys.PAGE_UP).build().perform();

		buildTest.clickClearFiltersButton();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		ReusableMethods.checkPageIsReady(driver);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
