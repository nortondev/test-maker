package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP30_TM_1674_TC4_TC5_RemoveAnswerChoice_Error extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	WebsitePage website;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber, Diffculty, Bloomstaxonomy;

	List<WebElement> moreInfo;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP30_TM-1674_TC4_TC5_Log into Testmaker application and Remove all answer Choices from Multi Choice group question editor and try to Save the changes to verify application displays error message")
	@Stories("SP30_TM-1674_TC4_TC5_Log into Testmaker application and Remove all answer Choices from Multi Choice group question editor and try to Save the changes to verify application displays error message")
	@Test()
	public void editMultipleChoiceRemoveAnswerChoice() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		boolean isQuestion = ReusableMethods.isQuestionGroupDisplayed(driver);
		System.out.println(isQuestion);
		if (isQuestion == true) {
			buildTest.clickAddQuestionsIcon(1);
		}
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		editQ = new EditQuestions();
		// editQ.clickAddNortonQuestion();
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		// ReusableMethods.scrollIntoView(driver, editQ.editPromptbutton);
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(5000);
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			LogUtil.log(IDNumber);
		}
		Thread.sleep(1000);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.deleteAnswerchoices();
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
		}
		Thread.sleep(2000);
		cnt.clickSave();
		Thread.sleep(4000);
		editQ.questionNotSavedpopUp();
		String parentWindow = driver.getWindowHandle();
		editQ.clickContactUslink();
		website = new WebsitePage();
		website.getWebsiteWindow(driver, parentWindow);
		website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow);
		editQ.clickCloselink();
		String getErrorText = editQ
				.getErrormsgQuestionStembox("Answer Choices*");
		System.out.println(getErrorText);
		cnt.clickCancelButton();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(1000);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		editQ.deleteAnswerchoicesSection(false);
		Thread.sleep(1000);
		cnt.clickSave();
		Thread.sleep(4000);
		editQ.questionNotSavedpopUp();
		String parentWindow1 = driver.getWindowHandle();
		editQ.clickContactUslink();
		website = new WebsitePage();
		website.getWebsiteWindow(driver, parentWindow1);
		website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow1);
		editQ.clickCloselink();
		Assert.assertEquals(editQ.errorSection.getText(),
				"This question requires at least two answer choices to be saved.");
		// Remove all answer choice and verify Error
		cnt.clickCancelButton();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(1000);
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		editQ.deleteAllAnswerchoicesSection();
		Thread.sleep(1000);
		cnt.clickSave();
		Thread.sleep(4000);
		editQ.questionNotSavedpopUp();
		String parentWindow2 = driver.getWindowHandle();
		editQ.clickContactUslink();
		website = new WebsitePage();
		website.getWebsiteWindow(driver, parentWindow2);
		website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow2);
		editQ.clickCloselink();
		Assert.assertEquals(editQ.errorSection.getText(),
				"This question requires an answer choice to be saved.");
		cnt.clickCancelButton();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
