package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1750_TC4_EditTrueFalse_inlineerrormessages extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	WebsitePage website;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName, difficultytext;;
	WebsitePage Website;
	String questionType, IDNumber, Diffculty, Bloomstaxonomy;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			BloomstaxonomyAfterSave;
	List<WebElement> moreInfo;
	String expectedvalue = "Bloom's Taxonomy";

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1750_TC4_User adds TRUE/FALSE type question and verifies that inline error messages for the various required field are updated in real time as user corrects error.")
	@Stories("SP31_TM-1750_TC4_User adds TRUE/FALSE type question and verifies that inline error messages for the various required field are updated in real time as user corrects error.")
	@Test()
	public void editTrueFalseValidateErrorForRequireFields() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("TRUE/FALSE");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		System.out.println(questionCountAfterFilter);
		List<String> filterNames = buildTest.verifyMultipleFilterName();

		for (int i = 0; i < filterNames.size(); i++) {
			Assert.assertEquals(filterNames.get(0), "True/False");
		}
		// ReusableMethods.scrollQuestionSection(driver);
		Thread.sleep(4000);
		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-5')]"));
		String id = qM0.getAttribute("id");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__0')]"));
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		LogUtil.log(getText);

		editQ = new EditQuestions();
		editQ.clickAddNortonQuestion();
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		Thread.sleep(2000);
		driver.findElement(
				By.xpath("//div[starts-with(@class,'bold darkGray pt3-0')]"))
				.click();
		Thread.sleep(2000);
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		String getQuestionStemtext = editQ
				.getErrormsgQuestionStembox("Question Stem");
		Assert.assertEquals(getQuestionStemtext.toString().trim(),
				"This is a required field.");
		boolean isvalueDisplayed = driver
				.findElements(
						By.xpath("//div[@class='pt2']/div[contains(text(),'Difficulty')]/following-sibling::div//div[contains(@class,'control')]/div[@class=' css-2w99ta']"))
				.size() > 0;
		LogUtil.log(isvalueDisplayed);
		
		if (editQ.listValueDisplayed("Difficulty") == true) {
			editQ.deleteListValue("Difficulty");
			cnt.clickSave();
			editQ.questionNotSavedpopUp();
			editQ.clickCloselink();
			String difficultytext = editQ
					.getErrormsgQuestionStembox("Difficulty");
			Assert.assertEquals(difficultytext.toString().trim(),
					"This is a required field.");
		} else {
			cnt.clickSave();
			editQ.questionNotSavedpopUp();
			editQ.clickCloselink();
			String difficultytext = editQ
					.getErrormsgQuestionStembox("Difficulty");
			Assert.assertEquals(difficultytext.toString().trim(),
					"This is a required field.");
		}
		editQ.deleteListValue("Bloom");
		cnt.clickSave();
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		String bloomsTaxonomytext = editQ
				.getErrormsgQuestionStembox("Bloom");
		Assert.assertEquals(bloomsTaxonomytext.toString().trim(),
				"This is a required field.");
		Thread.sleep(2000);

		ReusableMethods.scrollToElement(driver,
				By.xpath("//div[starts-with(@class,'p3-0 full-width')]/div"));

		editQ.modifyQuestionText(expectedQuestiontext.toString());
		Assert.assertNotSame(getQuestionStemtext.toString().trim(),
				"This is a required field.");
		editQ.clickEditListbox("Difficulty");
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		boolean isdisplayedDiff = editQ.isErrorDisplayed("Difficulty");
		Assert.assertTrue(isdisplayedDiff, "This is a required field.");
		editQ.clickEditListbox("Bloom");
		editQ.changeListValue("Bloom");
		Thread.sleep(2000);
		boolean isdisplayedbloom = editQ.isErrorDisplayed("Blooms Taxonomy");
		Assert.assertTrue(isdisplayedbloom, "This is a required field.");
		buildTest.clickCancelButton();
		editQ.cancelConfirmationPopUp();
		editQ.clickConfirmCancel();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		ReusableMethods.checkPageIsReady(driver);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
