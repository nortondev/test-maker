package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP28_TM_1406_TC3_ISBNQuestionSource extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	String testName;
	String buildTestName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP28_TM-1406_TC3_Log into testmaker application with instructor who edited question in any one of the ISBN and verify application does not display Question Source filter for ISBN where instructor do not edited any question")
	@Stories("SP28_TM-1406_TC3_Log into testmaker application with instructor who edited question in any one of the ISBN and verify application does not display Question Source filter for ISBN where instructor do not edited any question")
	@Test()
	public void QuestionSourceISBN() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		Thread.sleep(5000);
		TMlogin.loginTestMakerAppQUestionSource();
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		Thread.sleep(5000);
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		String filterName = buildTest.clickFiltersButton("Question Source");
		Assert.assertNotNull(filterName,
				"Question Source Filter Name is displayed");
		List<String> qsNames = buildTest.getQuestionSourceOptionNames();
		String qsOptionNames = null;
		for (int i = 0; i < qsNames.size(); i++) {
			qsOptionNames = qsNames.get(i).toString();
			LogUtil.log(qsOptionNames);
		}
		buildTest.clickApplyButton();
		List<String> filterNames = buildTest.verifyMultipleFilterName();
		String filterNamesAfterSelection = null;
		for (int i = 0; i < filterNames.size(); i++) {
			filterNamesAfterSelection = filterNames.get(i).toString();
			LogUtil.log(filterNamesAfterSelection);
		}
		ReusableMethods.loadingWaitDisapper(driver);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		// Click back Button
		cnt.clickTestMakerBackButton();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//button[contains(text(),'Create New Test')]")));
		TMlogin.appendISBN(ReusableMethods.nonQuestionSourceISBNreadJson());
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			Assert.assertFalse(buildTest.getFilterName("Question Source"),
					"Question Source is not displayed");
		}
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
