package com.wwnorton.TestMaker.EditQuestions;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP29_TM_1419_TC8_EditEssayQuestionType_SaveChanges_ListView extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	WebsitePage website;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName, qid, questionTypeAfterSave, IDNumberAfterSave,
			DiffcultyAfterSave, BloomstaxonomyAfterSave, questionTypeListView,
			difficultyListView, idNumberListView, bloomTaxonomy,
			difficultyListView1;
	List<WebElement> moreInfo;
	List<String> qids;
	String expectedvalue = "Bloom's Taxonomy";

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP29_TM-1419_TC8_Log into Testmaker application in List view and verify Instructor Edit the Question stem and Save the changes and return to List view")
	@Stories("SP29_TM-1419_TC8_Log into Testmaker application in List view and verify Instructor Edit the Question stem and Save the changes and return to List view")
	@Test()
	public void editQuestionEssay_ValidateChangesBuildandListView()
			throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		ReusableMethods.checkPageIsReady(driver);
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("ESSAY");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		// System.out.println(questionCountAfterFilter);
		List<String> filterNames = buildTest.verifyMultipleFilterName();

		for (int i = 0; i < filterNames.size(); i++) {
			Assert.assertEquals(filterNames.get(0), "Essay");
		}
		// ReusableMethods.scrollQuestionSection(driver);
		Thread.sleep(4000);
		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-5')]"));
		String id = qM0.getAttribute("id");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__0')]"));
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		LogUtil.log(getText);

		editQ = new EditQuestions();
		editQ.clickAddNortonQuestion();
		// get Edit Question details
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		buildTest.clickBuildTestTabs("List");
		qids = new ArrayList<String>();
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> listviewButton = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
			for (int j = 0; j < listviewButton.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@class='border-none bg-white none']"))
							.size() > 0;
					if (isexpanded == true) {
						js.executeScript("arguments[0].click();",
								listviewButton.get(j));
					}
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					questionTypeListView = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionTypeListView);
					System.out.println(questionTypeListView);
					difficultyListView = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					if (difficultyListView == null) {
						difficultyListView1 = "Diffculty=" + difficultyListView;
						LogUtil.log(qid + "=" + difficultyListView1);

					} else {
						LogUtil.log(qid + "=" + difficultyListView);

					}
					idNumberListView = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + idNumberListView);
					System.out.println(idNumberListView);
					String ListViewBloomTx = buildTest
							.getBloomsTaxonomyTextAddedSectionListView()
							.toString().trim();

					Assert.assertEquals(ListViewBloomTx, expectedvalue
							.toString().trim());
					bloomTaxonomy = buildTest
							.getMetadataAssociatedValuesListView("Bloom", qid);
					LogUtil.log(qid + "=" + bloomTaxonomy);

				}
			}
		}

		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		editQ.modifyQuestionText("Modified Question Stem");
		actions = new Actions(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
			Thread.sleep(2000);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		Thread.sleep(2000);
		cnt.clickSave();
		qids = buildTest.getQuestionid();
		LogUtil.log(qids.size());
		for (int i = 0; i < qids.size(); i++) {
			qid = qids.get(i).toString();
			js = (JavascriptExecutor) driver;
			List<WebElement> listviewButton = driver
					.findElements(By
							.xpath("//div[@class='m0 p0 bg-white border-none flex flex-row justify-start align-center']/button"));
			for (int j = 0; j < listviewButton.size(); j++) {
				if (i == j) {
					js.executeScript("arguments[0].scrollIntoView(true);",
							listviewButton.get(j));
					boolean isexpanded = driver
							.findElements(
									By.xpath("//div[@class='border-none bg-white none']"))
							.size() > 0;
					if (isexpanded == true) {
						js.executeScript("arguments[0].click();",
								listviewButton.get(j));
					}
					Thread.sleep(1000);
					WebElement metadatablock = driver
							.findElement(By
									.xpath("//div[@id='regionYourBuildTest']//div[@id='"
											+ qid
											+ "']//div/ul[@class='list-reset body-text mt1-5']"));
					ReusableMethods.scrollIntoView(driver, metadatablock);
					questionTypeAfterSave = buildTest
							.getMetadataAssociatedValuesListView(
									"Question Type", qid);
					LogUtil.log(qid + "=" + questionTypeAfterSave);
					System.out.println(questionTypeListView);
					DiffcultyAfterSave = buildTest
							.getMetadataAssociatedValuesListView("Difficulty",
									qid);
					LogUtil.log(qid + "=" + DiffcultyAfterSave);
					IDNumberAfterSave = buildTest
							.getMetadataAssociatedValuesListView("ID Number",
									qid);
					LogUtil.log(qid + "=" + IDNumberAfterSave);

					String ListViewBloomTx = buildTest
							.getBloomsTaxonomyTextAddedSectionListView()
							.toString().trim();

					Assert.assertEquals(ListViewBloomTx, expectedvalue
							.toString().trim());
					BloomstaxonomyAfterSave = buildTest
							.getMetadataAssociatedValuesListView("Bloom", qid);
					LogUtil.log(qid + "=" + BloomstaxonomyAfterSave);
				}
			}
		}

		Assert.assertEquals(questionTypeListView, questionTypeAfterSave);
		//Assert.assertNotEquals(difficultyListView, DiffcultyAfterSave);
		Assert.assertNotEquals(idNumberListView, IDNumberAfterSave);
	//	Assert.assertNotEquals(bloomTaxonomy, BloomstaxonomyAfterSave);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
