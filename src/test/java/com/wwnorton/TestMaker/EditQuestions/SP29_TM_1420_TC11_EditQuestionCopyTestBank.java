package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP29_TM_1420_TC11_EditQuestionCopyTestBank extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	WebsitePage website;
	String editedQuestionText, idNumber, testName, questionCountAfterFilter,
			filterName, buildTestName,getChapterName,getQuestionType,difficulty;
	JavascriptExecutor js;
	int getAddQuestionCount;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP29_TM-1420_TC11_Log into Testmaker application and edit any true false question successfully and verify application store copy of edited question to test bank")
	@Stories("SP29_TM-1420_TC11_Log into Testmaker application and edit any true false question successfully and verify application store copy of edited question to test bank")
	@Test()
	public void editQuestionStemValidateYourQuestion() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("TRUE/FALSE");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		System.out.println(questionCountAfterFilter);
		List<String> filterNames = buildTest.verifyMultipleFilterName();

		for (int i = 0; i < filterNames.size(); i++) {
			Assert.assertEquals(filterNames.get(0), "True/False");
		}
		editQ = new EditQuestions();
		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-')]"));
		String id = qM0.getAttribute("id");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__0')]"));
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		LogUtil.log(getText);
		editQ.clickAddNortonQuestion();
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		
		editQ.modifyQuestionText(expectedQuestiontext);
		Thread.sleep(2000);
		Thread.sleep(2000);
		getChapterName =editQ.getChapterandValue();
		getQuestionType = editQ.getQuestTypeandValue().toUpperCase();
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
		}
		if (editQ.listValueDisplayed("Bloom") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		Thread.sleep(2000);
		cnt.clickSave();
		editQ.getYourQuestiontag();
		js = (JavascriptExecutor) driver;
		List<WebElement> moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {

			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			idNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			System.out.println(idNumber);
			difficulty = buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");
		}
		cnt.clickTestMakerBackButton();
		ReusableMethods.checkPageIsReady(driver);
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		ReusableMethods.checkPageIsReady(driver);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox(getQuestionType);
		buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(1);	
		buildTest.clickChaptersButton();
		buildTest.checkChapterCheckbox(getChapterName);	
		buildTest.clickDifficultyButton();
		buildTest.selectDifficultyCheckbox(difficulty);
		buildTest.clickApplyButton();
		String idNumbermeatadata = editQ.getIDNumberMetadata(idNumber);
		// Assert.assertEquals(idNumbermeatadata, idNumber);
		LogUtil.log(idNumbermeatadata);
		Thread.sleep(5000);
		buildTest.removebutton.click();
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		//System.out.println(questionCountAfterFilter);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int questionCount = Integer.parseInt(str[0]);

		if (questionCount == 0) {
			Assert.assertNull(null, "No record Found");

		}
		if (questionCount <= 1) {
			getAddQuestionCount = questionCount;
		} else {
			getAddQuestionCount = GetRandomId.getRandom(questionCount);
		}
		buildTest.clickAddQuestionsIcon(getAddQuestionCount);
		Thread.sleep(5000);
		cnt.clickTestMakerBackButton();

		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(3000);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		String regex1 = "[^\\d]+";
		String[] str1 = questionCountAfterSearchQuestionAfteradd.split(regex1);
		int questionCount1 = Integer.parseInt(str1[0]);
		LogUtil.log(questionCount1);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
