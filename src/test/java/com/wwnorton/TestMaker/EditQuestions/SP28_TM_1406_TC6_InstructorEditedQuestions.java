package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP28_TM_1406_TC6_InstructorEditedQuestions extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	String editedQuestionText, idNumber, questionCountAfterFilter, filterName,
			buildTestName, testName, difficulty, getChapterName, tag,getQuestionType;
	JavascriptExecutor js;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP28_TM-1406_TC6_Log into testmaker with instructor who edited the question to verify instructor able to view only user edited questions that listed question edited by logged in user only and not listed question edited by other instructor for same isbn")
	@Stories("SP28_TM-1406_TC6_Log into testmaker with instructor who edited the question to verify instructor able to view only user edited questions that listed question edited by logged in user only and not listed question edited by other instructor for same isbn")
	@Test()
	public void instructorEditedQuestionforSameISBN() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		
		try {
			driver.switchTo().activeElement().click();
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		filterName = buildTest.clickFiltersButton("Question Source");
		Assert.assertNotNull(filterName,
				"Question Source Filter Name is displayed");
		buildTest.selectQuestionSourceOptionNames(0);
		buildTest.clickApplyButton();
		driver.findElement(By.id("regionAside")).click();

		driver.findElements(By.xpath("//div[@class='mx3']"));
		actions = new Actions(driver);
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(3000);
		}
		buildTest.clickAddQuestionsIcon(1);
		editQ = new EditQuestions();
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		editedQuestionText = editQ.getQuestionText();
		
		Thread.sleep(1000);
		editQ.clickAddAnswerChoice();
		boolean isAnsChoiceButtonDisplayed = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button____empty____')]//div[contains(text(),'Add an Answer Choice')]"))
				.size() > 0;
		if (isAnsChoiceButtonDisplayed == true) {
			ReusableMethods.scrollIntoView(driver, editQ.answerChoicesLabel);
		} else {
			ReusableMethods.scrollIntoView(driver, editQ.answerChoicesLabel);
		}
		
		editQ.clickLastremovebutton();
		if (editQ.listValueDisplayed("Difficulty") == false) {
			
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
		}
		if (editQ.listValueDisplayed("Bloom") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		Thread.sleep(2000);
		getChapterName =editQ.getChapterandValue();
		getQuestionType = editQ.getQuestTypeandValue().toUpperCase();
		cnt.clickSave();
		editQ.getYourQuestiontag();
		js = (JavascriptExecutor) driver;
		List<WebElement> moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {

			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			idNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			difficulty =buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");	  
			
		}
		ReusableMethods.scrollIntoView(driver, buildTest.BuildViewLink);
		// ReusableMethods.scrollIntoView(driver, buildTest.removebutton);
		buildTest.removebutton.click();
		cnt.clickSave();
		buildTest.clickClearFiltersButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox(getQuestionType);
		buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(1);	
		buildTest.clickChaptersButton();
		buildTest.checkChapterCheckbox(getChapterName);		
		buildTest.clickDifficultyButton();
		buildTest.selectDifficultyCheckbox(difficulty);
		buildTest.clickApplyButton();
		
		driver.findElement(By.id("regionAside")).click();
		Thread.sleep(1000);
		for (int ele = 0; ele < 2; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		WebElement questionsText = driver.findElement(By
				.xpath("//button[@id='__input__button__addbutton__0']"));
		actions.moveToElement(questionsText).build().perform();
		buildTest.getQuestionMetadataText();
		buildTest.clickSearchQuestionsTextbox(editedQuestionText);
		buildTest.clickSearchLinkbutton();
		Thread.sleep(5000);
		String idNumbermeatadata = editQ.getIDNumberMetadata(idNumber);
		Assert.assertEquals(idNumbermeatadata, idNumber);
		tag = editQ.getYourQuestionlabel();
		cnt.clickTestMakerBackButton();

		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
		Thread.sleep(5000);
		TMlogin.loginTestMakerAppQUestionSource();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt.clickCreateNewTest();
		ReusableMethods.checkPageIsReady(driver);
		testName = cnt.createNewTestwithTestNameCourseName();
		ReusableMethods.checkPageIsReady(driver);
		buildTestName = cnt.getTestName();
		cnt.clickSave();
		buildTest.clickSearchQuestionsTextbox("\"" + editedQuestionText + "\"");
		buildTest.clickSearchLinkbutton();
		Assert.assertNotSame(tag, "Your Questions label is not displayed");
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		ReusableMethods.checkPageIsReady(driver);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
