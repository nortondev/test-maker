package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.GetRandomId;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP29_TM_1419_TC12_EditEssayQ_ValidateEditedQuestion_Testbank
		extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	WebsitePage website;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName,getChapterName,getQuestionType;
	String questionType, IDNumber, Diffculty, Bloomstaxonomy;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			BloomstaxonomyAfterSave;
	List<WebElement> moreInfo;
	List<String> filterNames;
	String expectedvalue = "Bloom's Taxonomy";
	String questionTypeListView, difficultyListView, idNumberListView,
			bloomTaxonomy, difficultyListView1;
	int getAddQuestionCount;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP29_TM-1419_TC12_Log into Testmaker application in build view and edit any Essay question successfully and verify application store copy of edited question to test bank")
	@Stories("SP29_TM-1419_TC12_Log into Testmaker application in build view and edit any Essay question successfully and verify application store copy of edited question to test bank")
	@Test()
	public void editedEssayQValidateinBuildandListView() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("ESSAY");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		// System.out.println(questionCountAfterFilter);
		filterNames = buildTest.verifyMultipleFilterName();

		for (int i = 0; i < filterNames.size(); i++) {
			Assert.assertEquals(filterNames.get(0), "Essay");
		}
		// ReusableMethods.scrollQuestionSection(driver);
		Thread.sleep(4000);
		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-5')]"));
		String id = qM0.getAttribute("id");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__0')]"));
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		LogUtil.log(getText);

		editQ = new EditQuestions();
		editQ.clickAddNortonQuestion();
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			// System.out.println("*****"+IDNumber);
		}
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		String expectedQuestiontext = editQ.getQuestionText();
		LogUtil.log(expectedQuestiontext);
		editQ.removeQuestionText();
		ReusableMethods.pressKeyTab(driver);
		Thread.sleep(2000);
		getChapterName =editQ.getChapterandValue();
		getQuestionType = editQ.getQuestTypeandValue().toUpperCase();
		ReusableMethods.checkPageIsReady(driver);
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
		}
		ReusableMethods.scrollToBottom(driver);
		if (editQ.listValueDisplayed("Bloom") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		try {
			
			cnt.clickSave();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		editQ.questionNotSavedpopUp();
		editQ.clickCloselink();
		String getQuestionStemtext = editQ
				.getErrormsgQuestionStembox("Question Stem");
		Assert.assertEquals(getQuestionStemtext.toString().trim(),
				"This is a required field.");		
		editQ.modifyQuestionText(expectedQuestiontext+"Test");
		Thread.sleep(2000);
		actions = new Actions(driver);
		actions.sendKeys(Keys.ARROW_DOWN).build().perform();
		Thread.sleep(2000);
		editQ.editQuestionbox.sendKeys(Keys.TAB);
		Thread.sleep(1000);
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
		}
		ReusableMethods.scrollToBottom(driver);
		try {
			cnt.clickSave();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			IDNumberAfterSave = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			System.out.println("###### " + IDNumberAfterSave);
			Diffculty =buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");	  
		}

		cnt.clickTestMakerBackButton();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("ESSAY");
		buildTest.clickApplyButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(1);
		buildTest.clickChaptersButton();
		buildTest.checkChapterCheckbox(getChapterName);		
		buildTest.clickDifficultyButton();
		buildTest.selectDifficultyCheckbox(Diffculty);
		buildTest.clickApplyButton();
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		editQ.getIDNumberMetadata(IDNumberAfterSave);
		filterNames = buildTest.verifyMultipleFilterName();
		// Remove Filter icon
		for (int i = 0; i < filterNames.size(); i++) {
			if (filterNames.get(i).equalsIgnoreCase("Your Questions")) {
				buildTest.removeFiltericon.click();
			}
		}

		driver.findElement(By.id("regionAside")).click();
		driver.findElements(By.xpath("//div[@class='mx3']"));
		actions = new Actions(driver);
		for (int ele = 0; ele < 2; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
		}
		Thread.sleep(2000);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		int v = ReusableMethods.getDigits(questionCountAfterFilter);
		if (v == 0) {
			Assert.assertNull(null, "No record Found");

		}
		if (v <= 2) {
			getAddQuestionCount = v;
		} else {
			int questions = GetRandomId.getRandom(v);
			getAddQuestionCount = GetRandomId.getRandom(questions);
			if(getAddQuestionCount>10){
				getAddQuestionCount=3;
			}
		}
		buildTest.clickAddQuestionsIcon(getAddQuestionCount);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.verifyQuestioninTestBuildSearchResult();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(8000);
			IDNumberAfterSave = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			
		}
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		Thread.sleep(2000);
		String questionCountAfterSearchQuestionAfteradd1 = buildTest
				.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals(questionCountAfterSearchQuestionAfteradd,
				questionCountAfterSearchQuestionAfteradd1);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
