package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP28_TM_1406_TC5_QuestionSourceFilterOptionSelection extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	String testName;
	String buildTestName;
	String filterName;
	String questionCountAfterFilter;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP28_TM-1406_TC5_Log into testmaker application with instructor who already edited one or more questions in respective ISBN to verify Question Source filter in filter panel and apply filter for each option")
	@Stories("SP28_TM-1406_TC5_Log into testmaker application with instructor who already edited one or more questions in respective ISBN to verify Question Source filter in filter panel and apply filter for each option")
	@Test()
	public void QuestionSourceISBNOptionSelection() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		String questionsCount = buildTest.getQuestionsCount();
		buildTest.clickAddFiltersButton();
		filterName = buildTest.clickFiltersButton("Question Source");
		Assert.assertNotNull(filterName,
				"Question Source Filter Name is displayed");
		List<String> qsNames = buildTest.getQuestionSourceOptionNames();
		String qsOptionNames = null;

		for (int i = 0; i < qsNames.size(); i++) {
			qsOptionNames = qsNames.get(i).toString();
			LogUtil.log(qsOptionNames);
		}
		buildTest.clickApplyButton();
		Thread.sleep(3000);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		Assert.assertEquals(questionsCount.trim().toString(),
				questionCountAfterFilter.trim().toString());
		buildTest.clickClearFiltersButton();
		buildTest.clickAddFiltersButton();
		filterName = buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(1);
		buildTest.clickApplyButton();
		Thread.sleep(3000);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		Assert.assertNotEquals(questionsCount.trim().toString(),
				questionCountAfterFilter.trim().toString());
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
