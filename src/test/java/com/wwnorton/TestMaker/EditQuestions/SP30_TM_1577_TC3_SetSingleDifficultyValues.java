package com.wwnorton.TestMaker.EditQuestions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP30_TM_1577_TC3_SetSingleDifficultyValues extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	WebsitePage website;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	WebsitePage Website;
	String questionType, IDNumber, Difficulty, Bloomstaxonomy;
	String questionTypeAfterSave, IDNumberAfterSave, DiffcultyAfterSave,
			BloomstaxonomyAfterSave, id;
	List<WebElement> moreInfo;
	String expectedvalue = "Bloom's Taxonomy";
	List<String> editQBloomstaxonomy;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP30_TM-1577_TC3_Log into application and edit question having multiple difficulty to verify instructor can edit to single metadata")
	@Stories("SP30_TM-1577_TC3_Log into application and edit question having multiple difficulty to verify instructor can edit to single metadata")
	@Test()
	public void editMultipleDifficultyValueSetToSingle() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.mutipleMetaISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		buildTest = new BuildTestPage();
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		}
		buildTest.clickAddQuestionsIcon(1);
		editQ = new EditQuestions();
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		editQ.selectMultipleListValue("Difficulty");
		if (editQ.listValueDisplayed("Bloom") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Bloom");
			editQ.changeListValue("Bloom");
		}
		Thread.sleep(2000);
		cnt.clickSave();
		buildTest.clickClearFiltersButton();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("SHORT ANSWER");
		buildTest.clickFiltersButton("Question Source");
		buildTest.selectQuestionSourceOptionNames(1);
		buildTest.clickApplyButton();
		id = buildTest.getmultipleDifficultyMetadata();
		editQ.clickEditQuestion();
		ReusableMethods.scrollToBottom(driver);
		editQ.deleteListValue("Difficulty");
		Thread.sleep(2000);
		//editQ.clickEditListbox("Difficulty");
		editQ.changeListValue("Difficulty");
		Thread.sleep(2000);
		cnt.clickSave();
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(2000);
		}
		int difficultycount = buildTest.getmetaDataCount("Difficulty");
		if (difficultycount == 1) {
			Assert.assertEquals(difficultycount, 1);
		}

		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
