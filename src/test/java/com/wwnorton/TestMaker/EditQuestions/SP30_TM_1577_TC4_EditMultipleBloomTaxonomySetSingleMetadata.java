package com.wwnorton.TestMaker.EditQuestions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.EditQuestions;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP30_TM_1577_TC4_EditMultipleBloomTaxonomySetSingleMetadata extends
		PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	EditQuestions editQ;
	JavascriptExecutor js;
	String editedQuestionText, testName, questionCountAfterFilter, filterName,
			buildTestName;
	String questionType, IDNumber, Difficulty, Bloomstaxonomy, id,
			BloomstaxonomyAfterSave;
	List<String> editQBloomstaxonomy;

	List<WebElement> moreInfo;

	String bloomfilterName;
	com.wwnorton.TestMaker.utilities.ReadUIJsonFile readJsonObject = new com.wwnorton.TestMaker.utilities.ReadUIJsonFile();
	com.google.gson.JsonObject jsonObj = readJsonObject.readUIJason();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP30_TM-1577_TC4_Log into application and edit question having multiple Blooms taxonomy  to verify instructor can edit to single metadata")
	@Stories("SP30_TM-1577_TC4_Log into application and edit question having multiple Blooms taxonomy  to verify instructor can edit to single metadata")
	@Test()
	public void editMultipleBloomTaxonomySetSingleBloom() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.mutipleMetaISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		testName = cnt.createNewTestwithTestNameCourseName();
		buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		ReusableMethods.checkPageIsReady(driver);
		cnt.clickSave();
		ReusableMethods.checkPageIsReady(driver);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		// select filters
		try {
			buildTest.clickAddFiltersButton();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * buildTest.clickQuestionTypesButton();
		 * buildTest.selectmultipleCheckboxesQuestionType();
		 */
		// buildTest.clickAddFiltersButton();
		// buildTest.collapsgrey.click();
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.collapsgrey.click();
		}
		Thread.sleep(3000);
		String ChapterName = jsonObj.getAsJsonObject("BloomFilterName")
				.get("ChapterName").getAsString();
		buildTest.clickChaptersButton();
		Thread.sleep(3000);
		buildTest.checkChapterCheckbox(ChapterName);
		Thread.sleep(3000);
		buildTest.clickBloomsTaxonomyButton();
		String bloomName1 = jsonObj.getAsJsonObject("BloomFilterName")
				.get("bloomFilterName1").getAsString();
		String bloomName2 = jsonObj.getAsJsonObject("BloomFilterName")
				.get("bloomFilterName2").getAsString();
		buildTest.selectBloomsTaxonomy(bloomName1);
		buildTest.selectBloomsTaxonomy(bloomName2);
		buildTest.clickApplyButton();
		// Scroll down the Questions and Check the Multiple Bloom Taxonomy
		id = buildTest.getbloomtaxonomyMetadata();
		editQ = new EditQuestions();
		Bloomstaxonomy = questionmetaData();
		System.out.println(Bloomstaxonomy);
		editQ.clickEditQuestion();
		ReusableMethods.checkPageIsReady(driver);
		if (editQ.listValueDisplayed("Difficulty") == false) {
			ReusableMethods.scrollToBottom(driver);
			editQ.deleteListValue("Difficulty");
			editQ.changeListValue("Difficulty");
		}

		ReusableMethods.scrollToBottom(driver);
		editQBloomstaxonomy = editQ.getListValue("Bloom");
		Assert.assertEquals(
				editQBloomstaxonomy.toString().replaceAll("\\[\\]", ""),
				Bloomstaxonomy.toString());
		
		boolean isindicatorContainer = driver
				.findElements(
						By.xpath("//div[@class='pt2']/div[contains(text(),'Bloom')]/following::div[contains(@class,'indicatorContainer')][@aria-hidden='true']//*[name()='svg']"))
				.size() > 0;
		if (isindicatorContainer == true) {
			driver.findElement(
					By.xpath("//div[@class='pt2']/div[contains(text(),'Bloom')]/following::div[contains(@class,'indicatorContainer')][@aria-hidden='true']//*[name()='svg']"))
					.click();
		}
		Thread.sleep(2000);
		editQ.changeListValue("Bloom");
		cnt.clickSave();
		Thread.sleep(2000);
		BloomstaxonomyAfterSave = questionmetaData();
		System.out.println(BloomstaxonomyAfterSave);
		Assert.assertNotSame(Bloomstaxonomy.toString(),
				BloomstaxonomyAfterSave.toString());
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

	public String questionmetaData() throws InterruptedException {
		driver = getDriver();
		String Bloomstaxonomy = null;
		js = (JavascriptExecutor) driver;
		moreInfo = driver.findElements(By
				.xpath("//button[contains(text(),'More Info')]"));
		for (int j = 0; j < moreInfo.size(); j++) {
			js.executeScript("arguments[0].scrollIntoView(true);",
					moreInfo.get(j));
			js.executeScript("arguments[0].click();", moreInfo.get(j));
			Thread.sleep(3000);
			questionType = buildTest
					.getMetadataAssociatedValuesBuildTestView("Question Type");
			Difficulty = buildTest
					.getMetadataAssociatedValuesBuildTestView("Difficulty");
			IDNumber = buildTest
					.getMetadataAssociatedValuesBuildTestView("ID Number");
			/*
			 * List<WebElement> bloomTextList = driver .findElements(By
			 * .xpath("//div[@id='"+ id +
			 * "']//div[contains(text(),\"Bloom's Taxonomy\")]/following-sibling::div/p"
			 * ));
			 */
			List<WebElement> bloomTextList = driver
					.findElements(By
							.xpath("//div[@id='regionYourBuildTest']//div[contains(text(),'Bloom')]/following-sibling::div/p"));
			List<String> bloomTextValues = new ArrayList<String>();
			Iterator<WebElement> itr = bloomTextList.iterator();
			while (itr.hasNext()) {
				WebElement bloomtextEle = itr.next();
				bloomTextValues.add(bloomtextEle.getText());
			}
			Bloomstaxonomy = bloomTextValues.toString()
					.replaceAll("\\[\\]", "");

		}
		return Bloomstaxonomy;
	}
}
