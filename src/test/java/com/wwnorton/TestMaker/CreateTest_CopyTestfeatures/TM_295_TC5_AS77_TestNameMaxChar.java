package com.wwnorton.TestMaker.CreateTest_CopyTestfeatures;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_295_TC5_AS77_TestNameMaxChar extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Create TestName and Course Name with max Characters")
	@Stories("Log into Testmaker and Create a Test with Max number of Test name with max number of Course name ")
	@Test()
	public void createTestMaxChar() throws Exception {
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String[] test = cnt.createNewTestwithTestNameCourseNameMaxChar();

		int testNameCount = 0, courseNameCount = 0;
		// Counts each character except space
		for (int i = 0; i < test[0].length(); i++) {
			if (test[0].charAt(i) != ' ')
				testNameCount++;
		}
		// Displays the total number of characters present in the TestName
		// String
		LogUtil.log("Total number of characters in a TestName String: "
				+ testNameCount);

		for (int j = 0; j < test[1].length(); j++) {
			if (test[1].charAt(j) != ' ')
				courseNameCount++;
		}
		LogUtil.log("Total number of characters in a CourseName String: "
						+ courseNameCount);
		String buildTestName = cnt.getTestName();
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		System.out.println(buildTestName);
		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String[] buildViewTestCourseNames = cnt.getTestCourseName();
		int testNameCountafterSave = 0, courseNameCountAfterSave = 0;
		// Counts each character except space
		for (int i = 0; i < buildViewTestCourseNames[0].length(); i++) {
			if (buildViewTestCourseNames[0].charAt(i) != ' ')
				testNameCountafterSave++;
		}
		// Displays the total number of characters present in the TestName
		// String After Saving Test Name
		// System.out.println("Total number of characters in a TestName String: "
		// + testNameCountafterSave);
		Assert.assertEquals(100, testNameCountafterSave);
		for (int j = 0; j < buildViewTestCourseNames[1].length(); j++) {
			if (buildViewTestCourseNames[1].charAt(j) != ' ')
				courseNameCountAfterSave++;
		}
		// System.out.println("Total number of characters in a CourseName String: "
		// + courseNameCountAfterSave);
		Assert.assertEquals(36, courseNameCountAfterSave);
		cnt.clickTestMakerBackButton();
		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		TMYourTests.clickActionlink(buildTestName);
		TMYourTests.clickExportOrCopyOrDeletelink("Delete");
		TMYourTests.DeleteTestButton.click();
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}
}
