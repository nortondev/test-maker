package com.wwnorton.TestMaker.CreateTest_CopyTestfeatures;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.BaseDriver;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP32_TM_1837_TC7_InvalidLoginTestMaker extends PropertiesFile {
	BaseDriver basePage;
	TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1837_TC7_Log into testmaker application with invalid user credentials to verify application does not displays Cookies are Blocked error message")
	@Stories("SP32_TM-1837_TC7_Log into testmaker application with invalid user credentials to verify application does not displays Cookies are Blocked error message")
	@Test()
	public void invalidlogin() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		ReusableMethods.checkPageIsReady(driver);
		TMlogin.loginTestMakerAppInvalidCred();
		Thread.sleep(2000);
		String getErrorText =TMlogin.getErrorText.getText();
		Assert.assertEquals("Invalid email/password", getErrorText);
		
		
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}