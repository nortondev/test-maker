package com.wwnorton.TestMaker.CreateTest_CopyTestfeatures;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_295_TC4_AS76_CreateTestErrorMessage extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_285_TC4_AS76_Log into testmaker to validate error message on Create Test model and Remove a question from an existing test and save it")
	@Stories("TM_285_TC4_AS76_Log into testmaker to validate error message on Create Test model and Remove a question from an existing test and save it ")
	@Test()
	public void validateCreateTestError() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
	    cnt.clickCreateNewTest();
	    ReusableMethods.checkPageIsReady(driver);
		cnt.CreateTestButton.click();
		cnt.CreateTestErrorMessage.getText();
		Assert.assertEquals("This is a required field.",
				cnt.CreateTestErrorMessage.getText());
		WebElement colorElement = driver
				.findElement(By
						.xpath("//div/form[@class='flex ']/input[@id='__input__text__testName__0']"));
		Assert.assertEquals("rgb(224, 44, 41)",
				colorElement.getCssValue("border-color"));
		Assert.assertEquals("2px", colorElement.getCssValue("border-width"));
		cnt.createTestCloseModal.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("SHORT ANSWER");
		buildTest.clickApplyButton();
		actions = new Actions(driver);
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		@SuppressWarnings("unused")
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(1000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		buildTest.clickAddQuestionsIcon(2);
		buildTest.getQuestionTextinBuildView(1);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		System.out.println(questionCountAfterSearchQuestionAfteradd);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Assert.assertEquals("Question 2", buildTest.verifyQuestioninTestBuild());
		buildTest.verifyQuestioninTestBuild();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(5000);
		buildTest.removeQuestionBuildView(1);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Assert.assertEquals("1 Question",
				buildTest.verifyQuestioninTestBuildSearchResult());
		Assert.assertEquals("Question 1", buildTest.verifyQuestioninTestBuild());
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertEquals("1 Question",
				buildTest.verifyQuestioninTestBuildSearchResult());
		Assert.assertEquals("Question 1", buildTest.verifyQuestioninTestBuild());
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {			
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);			
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}
}
