package com.wwnorton.TestMaker.CreateTest_CopyTestfeatures;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
@Listeners({ TestListener.class })
public class SP32_TM_1837_TC1_defaultISBN extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	com.wwnorton.TestMaker.utilities.ReadUIJsonFile readJsonObject = new com.wwnorton.TestMaker.utilities.ReadUIJsonFile();
	com.google.gson.JsonObject jsonObj = readJsonObject.readUIJason();	
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1837_TC1_Log into Testmaker application with instructor and logout and again login to application with different Instructor credentials to verify application displays default ISBN Tests page")
	@Stories("SP32_TM-1837_TC1_Log into Testmaker application with instructor and logout and again login to application with different Instructor credentials to verify application displays default ISBN Tests page")
	@Test()
	public void instructorLogindefaultISBN() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		ReusableMethods.checkPageIsReady(driver);
		String userName = jsonObj.getAsJsonObject("TMdefaultInstructor1LoginCredentials").get("userName").getAsString();
		String passWord = jsonObj.getAsJsonObject("TMdefaultInstructor1LoginCredentials").get("password").getAsString();
		TMlogin.instloginTestMakerApp(userName,passWord);
		Thread.sleep(3000);
		TMlogin.appendISBN(ReusableMethods.editQuestionISBN());
		boolean isYourTest = driver.findElements(By.xpath("//div[@id='regionYourTests']/div/span[contains(.,'These are tests you’ve created from Norton test bank questions.')]")).size() > 0;
		String currenturl = driver.getCurrentUrl();
		if(isYourTest==true){
			Assert.assertTrue(isYourTest, "Default ISBN is mapped" + currenturl);
		}else {
			Assert.assertFalse(isYourTest, "Default ISBN is NOT mapped" + currenturl);
		}
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
		String url = driver.getCurrentUrl();
		String getURL = PropertiesFile.url;
		String newURL = getURL.replace(url, getURL);
		driver.navigate().to(newURL);
		String userName1 = jsonObj.getAsJsonObject("TMdefaultInstructor2LoginCredentials").get("userName").getAsString();
		String passWord1 = jsonObj.getAsJsonObject("TMdefaultInstructor2LoginCredentials").get("password").getAsString();
		TMlogin.instloginTestMakerApp(userName1,passWord1);
		Thread.sleep(3000);
		boolean isYourTestagain = driver.findElements(By.xpath("//div[@id='regionYourTests']/div/span[contains(.,'These are tests you’ve created from Norton test bank questions.')]")).size() > 0;
		String currenturlnew = driver.getCurrentUrl();
		if(isYourTestagain==true){
			Assert.assertTrue(isYourTest, "Default ISBN is mapped" + currenturlnew);
		}else {
			Assert.assertFalse(isYourTest, "Default ISBN is NOT mapped" + currenturlnew);
		}
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
		
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
