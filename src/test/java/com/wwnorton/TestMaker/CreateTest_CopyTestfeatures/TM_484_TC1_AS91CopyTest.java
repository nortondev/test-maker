package com.wwnorton.TestMaker.CreateTest_CopyTestfeatures;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_484_TC1_AS91CopyTest extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	CreateNewTestPage TMcreateNewTest;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Copy Existing Test")
	@Stories("Log into testmaker and Copy existing Test to verify application added the copied test in list without refreshing entire Test list from Your Tests page")
	@Test()
	public void copyTestWithoutRefreshPage() throws Exception {
		// Login to the application
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		TMlogin.getbookinformation();
		cnt = new CreateNewTestPage();
		TMYourTests = new RegionYourTestsPage();
		// call Method
		String Notest = noTest();
		String strExpected = "You haven’t created any tests yet.";

		if (Notest.startsWith(strExpected)) {
			// Clicking on create new test button
			cnt.clickCreateNewTest();
			String TestName = cnt.createNewTestwithTestNameCourseName();
			// Saving new Test
			cnt.clickSave();

			// Clicking back button
			cnt.clickTestMakerBackButton();

			// Selecting Sort as Test Name
			cnt.selectSortByOptionsName("Test Name");

			LogUtil.log(TestName);

			// Click on action button
			TMYourTests.clickActionlink(TestName);
			TMYourTests.clickExportOrCopyOrDeletelink("Copy");
			String CopiedtestName = TMYourTests.getTestNamelinkfromList();
			Assert.assertTrue(CopiedtestName.contains("copy"));

		} else {
			// cnt.selectSortByOptionsName("Test Name");
			WebElement actionButtontoclick = getDriver().
					findElement(By
							.xpath("//div[@id='regionYourTests']/div[@class='m0 p0']/div[@class='pt2 px1 pb1-5 flex flex-row']/div/div/button"));
			actionButtontoclick.click();
			TMYourTests.clickExportOrCopyOrDeletelink("Copy");
			Thread.sleep(2000);
			String CopiedtestName = TMYourTests.getTestNamelinkfromList();
			Thread.sleep(2000);
			// cnt.selectSortByOptionsName("Last Modified");
			Assert.assertTrue(CopiedtestName.contains("copy"));

		}

		/*
		 * WebElement DropDownTest = getDriver().findElement(By
		 * .xpath("//span[contains(text(), 'Test Name')]")); String ExisitngTest
		 * = DropDownTest.getText();
		 * 
		 * if (ExisitngTest.equalsIgnoreCase("Test Name")) {
		 * System.out.println("Page is not refreshed"); Thread.sleep(1000); }
		 * else { System.out.println("Page is refreshed"); }
		 */
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	public static String noTest() {
		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(getDriver(), 50);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath("//span[@class='bg-lighterGray mx2 mt2 break-word  py4 px3 center large-body-text'][contains(text(),'You haven’t created any tests yet.')]")));
			WebElement NoTestText = getDriver()
					.findElement(By
							.xpath("//span[@class='bg-lighterGray mx2 mt2 break-word  py4 px3 center large-body-text'][contains(text(),'You haven’t created any tests yet.')]"));
			return NoTestText.getText();

		} catch (Exception e1) {
			String TestNameExist = "Test Already Exist";
			return TestNameExist;
		}
		
	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}

}
