package com.wwnorton.TestMaker.CreateTest_CopyTestfeatures;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.utilities.*;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;

@Listeners({ TestListener.class })
public class CreateNewTest extends PropertiesFile {

	TestMakerLoginPage TMlogin;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage TMcreateNewTest;
	RegionYourTestsPage TMYourTests;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Edge") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Create a New Test from Test Maker Application and Validate the Created test ")
	@Stories("Login to Test Maker application as an Instructor and Create a New Test ")
	@Test()
	public void createNewTest() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		TMcreateNewTest = new CreateNewTestPage();
		TMcreateNewTest.clickCreateNewTest();
		String testName = TMcreateNewTest.createNewTestwithTestNameCourseName();
		String buildTestName = TMcreateNewTest.getTestName();
		AssertJUnit.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		TMcreateNewTest.clickSave();
		TMcreateNewTest.clickTestMakerBackButton();
		Thread.sleep(3000);
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='bg-white m1-5 xs-col-4 sm-col-4 md-col-8 lg-col-10 lg-col-12']/div/div/div[@class='h1 pl2-5 pb1-5 pt1 undefined']")));
		AssertJUnit.assertEquals(TMcreateNewTest.getTestName(), testName);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();

	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}
}
