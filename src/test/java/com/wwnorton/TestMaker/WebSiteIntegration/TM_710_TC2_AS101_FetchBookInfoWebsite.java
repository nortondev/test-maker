package com.wwnorton.TestMaker.WebSiteIntegration;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_710_TC2_AS101_FetchBookInfoWebsite extends PropertiesFile {

	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	WebsitePage website;
	JavascriptExecutor js;
	String websiteBookDetails, bookdetails;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Compare Book Info on Test Maker Region Page and Website ")
	@Stories("Log into testmaker and verify Application fetch the Book information from website and displays same on Your Test page")
	@Test()
	public void compareBookInfo() throws Exception {
		driver=getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.apOptionreadJson());
		String URL = driver.getCurrentUrl();
		System.out.println(URL);
		String getISBN = URL.substring(URL.lastIndexOf("/") + 1);
		System.out.println(getISBN);
		Assert.assertNotNull(TMlogin.profileTestMakerApp());
		String bookTitle = TMlogin.getbooktitle();
		String bookSubTitle = TMlogin.getbooksubtitle();
		String bookEdition = TMlogin.getbookEdition();
		String bookAuthorsName = TMlogin.getbookAuthors();
		String str = bookAuthorsName.toString().replace("[", " ")
				.replace("]", "");
		if(bookEdition==null){
			bookdetails = bookTitle + ":" + " "
					+ bookSubTitle + "," + " " + str + "";
		}else {
		bookdetails = bookTitle + ":" + " "
				+ bookSubTitle + "," + " " + bookEdition + ","
				+ " " + str + "";
		}
		//WebSite login 
		website = new WebsitePage();
		website.navigateWebsiteURL();
		website.clickSearch(getISBN);
		website.clickSearchIcon();
		String websiteBookTitle = website.getBookTitle();
		String webSiteBookSubTitle = website.getBookSubTitle();
		String websiteBookEdition = website.getBookEdition();
		List<String> getAuthorsNameList = website.getBookAuthors();
		String webstr = getAuthorsNameList.toString().replace("[", " ")
				.replace("]", "");
		System.out.println(webstr.trim());
		Assert.assertEquals(bookTitle.trim(), websiteBookTitle.trim());
		Assert.assertEquals(bookSubTitle.trim(), webSiteBookSubTitle.trim());
        if(websiteBookEdition==null){
       	websiteBookDetails = websiteBookTitle + ":" + " "
    				+ webSiteBookSubTitle + "," +" " + "by" + webstr + "";
		} else {
		// Concatenate String
		 websiteBookDetails = websiteBookTitle + ":" + " "
				+ webSiteBookSubTitle + "," + " " + websiteBookEdition + ","
				+ " " + "by" + webstr + "";
		}
		Assert.assertEquals(bookdetails.trim(), websiteBookDetails.trim());
		

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
