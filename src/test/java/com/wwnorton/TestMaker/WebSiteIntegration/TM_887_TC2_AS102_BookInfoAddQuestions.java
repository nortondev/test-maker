package com.wwnorton.TestMaker.WebSiteIntegration;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_887_TC2_AS102_BookInfoAddQuestions extends PropertiesFile {

	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	BuildTestPage buildTest;
	String websiteBookDetails;
	WebsitePage website;
	JavascriptExecutor js;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Compare Book Info on Test Maker Add Question Page and Website ")
	@Stories("Log into testmaker and verify Application fetch the Book information from website and displays same on Add question page")
	@Test()
	public void compareBookInfoonAddQuestions() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.nonQuestionGroupISBNreadJson());
		String URL = driver.getCurrentUrl();
		String getISBN = URL.substring(URL.lastIndexOf("/") + 1);
		System.out.println(getISBN);
		TMlogin.profileTestMakerApp();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		Thread.sleep(3000);
		Assert.assertEquals("Add Questions", buildTest.getAddQuestionsText()
				.trim());
		Assert.assertEquals("Test Bank:", buildTest.getTestBankText().trim());
		String bookdetails = buildTest.getAddQuestionsBookDetails();
		website = new WebsitePage();
		website.navigateWebsiteURL();
		website.clickSearch(getISBN);
		website.clickSearchIcon();
		String websiteBookTitle = website.getBookTitle();
		String webSiteBookSubTitle = website.getBookSubTitle();
		String websiteBookEdition = website.getBookEdition();

		List<String> getAuthorsNameList = website.getBookAuthors();
		String webstr = getAuthorsNameList.toString().replace("[", " ")
				.replace("]", "");
		if (websiteBookEdition == null) {
			websiteBookDetails = websiteBookTitle + " " + webSiteBookSubTitle
					+ "," + " " + "by" + webstr + "";
		} else {
			// Concatenate String
			websiteBookDetails = websiteBookTitle + " " + webSiteBookSubTitle
					+ "," + " " + websiteBookEdition + "," + " " + "by"
					+ webstr + "";
		}
	Assert.assertEquals(bookdetails.trim().replaceAll(".", ""), websiteBookDetails.trim().replaceAll(".", ""));

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
