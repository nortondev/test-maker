package com.wwnorton.TestMaker.WebSiteIntegration;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_887_TC3_AS103_OverlapBookInfoWithQuestionsText extends
		PropertiesFile {

	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	BuildTestPage buildTest;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Book Specification on Add Question page is not overallped with Questions Text ")
	@Stories("Log into testmaker and verify Application displays Test bank label extends to the end of the Add Questions container, content wrap to the second line")
	@Test()
	public void bookinfowraptoNextline() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.apOptionreadJson());
		TMlogin.profileTestMakerApp();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		cnt.clickSave();
		cnt.clickTestMakerBackButton();
		TMYourTests = new RegionYourTestsPage();
		TMYourTests.clickTestNamelink(buildTestName);
		LogUtil.log(buildTestName);
		Thread.sleep(5000);
		buildTest = new BuildTestPage();
		WebElement bookinfo = driver
				.findElement(By
						.xpath("//div[@id='addQuestionsHeaderDiv']/div[@class='px3-0 mt3-5 pb1-5']/div[@class='clearfix mb1-5 mt1 flex']/span[@class='col sm-col-3 col-8']"));
		WebElement questionsText = driver
				.findElement(By
						.xpath("//div[@id='addQuestionsHeaderDiv']/div[@class='px3-0 mt3-5 pb1-5']/div[@class='clearfix mb1-5 mt1 flex']/span[@class='col sm-col-1 col-8 right-align']"));
		buildTest.areElementsOverlapping(bookinfo, questionsText);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
