package com.wwnorton.TestMaker.FiltersandSorting;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

import edu.emory.mathcs.backport.java.util.Arrays;

@Listeners({ TestListener.class })
public class TM_44_TC2_AS78SortingActions extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Sorting Actions")
	@Stories("Log into Testmaker with valid Credentials and Verify Sorting Actions")
	@Test()
	public void sortingActions() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		// verify Page Title
		cnt = new CreateNewTestPage();
		Assert.assertEquals("Your Tests", cnt.pageTitle());
		Assert.assertEquals("Last Modified", cnt.defaultSortBy());
		String sortOptionsName = cnt.getSortByOptionsName();
		@SuppressWarnings("unchecked")
		List<String> optionsName = Arrays.asList(new String[] { "Test Name",
				"Course", "Last Modified" });
		LogUtil.log(optionsName.toString() + sortOptionsName.toString());
		Assert.assertEquals(optionsName.toString(), sortOptionsName.toString());
		// Application displays Last modified option by default selected and
		// sorted Tests list by Last Modified in descending order
		cnt.sortOrderforLastModified();
		cnt.selectSortByOptionsName("Test Name");
		cnt.sortbyTestName();
		cnt.selectSortByOptionsName("Course");
		cnt.sortbyCourseName();
		Thread.sleep(1000);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
