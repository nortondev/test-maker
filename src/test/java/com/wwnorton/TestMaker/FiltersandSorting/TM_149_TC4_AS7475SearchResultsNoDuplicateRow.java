package com.wwnorton.TestMaker.FiltersandSorting;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_149_TC4_AS7475SearchResultsNoDuplicateRow extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	Actions actions;
	JavascriptExecutor js;
	getPOS POS = new getPOS();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("verify application does not displays duplicate row for same search result ")
	@Stories("Log in to TM application with instructor and enter some free text that should be available in Question Stem, Answer and verify application does not displays duplicate row for same search result")
	@Test()
	public void searchResultsDuplicateRows() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.clickMultipleChaptersCheckboxesandChaptersName(1);
		boolean isdisplayed = buildTest.getFilterName("Question Source");
		if (isdisplayed == true) {
			buildTest.clickFiltersButton("Question Source");
			buildTest.selectQuestionSourceOptionNames(0);
			buildTest.clickApplyButton();
		} else {
			buildTest.clickApplyButton();
		}
		Thread.sleep(2000);
		actions = new Actions(getDriver());
		getDriver().findElement(By.id("regionAside")).click();

		getDriver().findElements(By.xpath("//div[@class='mx3']"));
		@SuppressWarnings("unused")
		List<WebElement> getQuestionText = getDriver().findElements(By
				.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		Thread.sleep(5000);
		WebElement qM0 = getDriver().findElement(By.xpath("//div[starts-with(@class,'px1-5')]"));
		String id = qM0.getAttribute("id");
		ReusableMethods.scrollToElement(getDriver(), By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton__0')]"));
		WebElement getIDvalue = getDriver().findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		ArrayList<String> searchKeys = new ArrayList<String>();
		String SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);
		Thread.sleep(5000);
		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		Thread.sleep(5000);
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		String regex = "[^\\d]+";
		String[] str = questionCountAfterFilter.split(regex);
		int v = Integer.parseInt(str[0]);
		int k = v / 10;
		for (int i = 0; i < k; i++) {
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			boolean isPresent = getDriver().findElements(
					By.id("__input__button__backToTop__0")).size() > 0;
			if (isPresent == true) {
				Thread.sleep(1000);
				String notificationText = getDriver()
						.findElement(
								By.xpath("//div[@class='center large-body-text mt2-5']/span"))
						.getText();
				
				Assert.assertEquals(
						"You have reached the end of your results.",
						notificationText);
				boolean backtoTop = getDriver().findElement(
						By.id("__input__button__backToTop__0")).isDisplayed();
				if (backtoTop == true) {
					getDriver().findElement(By.id("__input__button__backToTop__0"))
							.getText();
					actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
					break;
					// /find the Duplicates

				}
			} else {
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			}

		}
		buildTest.fetchDuplicateQuestionIDNumber("ID Number");
		buildTest.fetchDuplicateQuestionText();
		Thread.sleep(3000);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}
}
