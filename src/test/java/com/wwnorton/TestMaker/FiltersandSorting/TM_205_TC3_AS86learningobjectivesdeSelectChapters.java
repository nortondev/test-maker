package com.wwnorton.TestMaker.FiltersandSorting;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_205_TC3_AS86learningobjectivesdeSelectChapters extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_205_TC3_AS86 Deselect the Chapters and Validate Learning Objective")
	@Stories("TM_205_TC3_AS86 select any one or more chapter and associated learning objective and deselect the Chapter to verify application removes the learning objective which are associated with deselected chapters")
	@Test()
	public void deSelectlearningObjectiveChapters() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.LOISBN());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		// buildTest.clickFiltersButton("Chapters");
		// buildTest.clickMultipleChaptersCheckboxesandChaptersName(2);
		// String count = buildTest.getCountSelectedFilter("Chapters");
		// buildTest.clickFiltersButton("Chapters");
		// buildTest.clickFiltersButton("Learning Objectives");
		List<String> loObjectivelist1 = buildTest
				.getLearningObjectiveFilterforChapter();
		LogUtil.log(loObjectivelist1);
		// buildTest.clickLearningObjectivecheckboxes(1);
		// buildTest.clickApplyButton();
		List<String> filterNamesfirstAttempt = buildTest
				.verifyMultipleFilterName();
		for (int i = 0; i < filterNamesfirstAttempt.size(); i++) {
			LogUtil.log(filterNamesfirstAttempt.get(i));
		}
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.clickChaptersCheckboxesandChaptersName(2);
		String countAfterRemove = buildTest.getCountSelectedFilter("Chapters");
		LogUtil.log(countAfterRemove);
		// Assert.assertNotEquals(count, countAfterRemove);
		buildTest.clickFiltersButton("Chapters");
		buildTest.clickFiltersButton("Learning Objectives");
		List<String> loObjectivelist2 = buildTest
				.getLearningObjectiveFilterforChapter();
		Assert.assertFalse(compareList(loObjectivelist1, loObjectivelist2));

		List<String> filterNamessecondAttempt = buildTest
				.verifyMultipleFilterName();
		for (int i = 0; i < filterNamessecondAttempt.size(); i++) {
			LogUtil.log(filterNamessecondAttempt.get(i));
		}
		Assert.assertFalse(compareList(filterNamesfirstAttempt,
				filterNamessecondAttempt));
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.unCheckChecboxes("Chapters");
		String countAfterunselectingall = buildTest
				.getCountSelectedFilter("Chapters");
		LogUtil.log(countAfterunselectingall.equalsIgnoreCase(null));
		buildTest.clickFiltersButton("Learning Objectives");
		ReusableMethods
				.scrollToElement(
						getDriver(),
						By.xpath("//div/p[contains(text(),'Select a chapter to see Learning Objectives')]"));
		WebElement learningObjectiveText = getDriver()
				.findElement(
						By.xpath("//div/p[contains(text(),'Select a chapter to see Learning Objectives')]"));
		Assert.assertEquals("Select a chapter to see Learning Objectives",
				learningObjectiveText.getText());
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@SuppressWarnings("rawtypes")
	public static boolean compareList(List ls1, List ls2) {
		return ls1.toString().contentEquals(ls2.toString()) ? true : false;
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
