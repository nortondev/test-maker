package com.wwnorton.TestMaker.FiltersandSorting;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
import com.wwnorton.TestMaker.utilities.getPOS;

@Listeners({ TestListener.class })
public class TM_149_TC2_AS73ApplyFilter_SearchText extends PropertiesFile {
	TestMakerLoginPage TMlogin;
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	Actions actions;
	CreateNewTestPage cnt;
	getPOS POS = new getPOS();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Apply Filters and Search with Free Text")
	@Stories("Log in to TM application with instructor and apply some filters and enter some free text in search bar to verify application displays Search result within selected filters and retain search term as it is ")
	@Test()
	public void filtersSearchFreeText() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		actions = new Actions(driver);
		// Select Filters
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		// select the chapter checkboxes
		String selectedChapterName = buildTest
				.clickChaptersCheckboxesandChaptersName(14);
		LogUtil.log(selectedChapterName);
		// click BloomsTaxonomy and get the Name of Taxonomy
		buildTest.clickBloomsTaxonomyButton();
		buildTest.clickBloomsTaxonomy(2);
		// click Apply Button
		buildTest.clickApplyButton();
		// get the Questions Count
		@SuppressWarnings("unused")
		List<String> filterNames = buildTest.verifyMultipleFilterName();
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		driver.findElement(By.id("regionAside")).click();

		driver.findElements(By.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 5; ele++) {
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(5000);
		}

		Thread.sleep(5000);
		WebElement qM0 = driver.findElement(By
				.xpath("//div[starts-with(@class,'px1-5 ')]"));
		String id = qM0.getAttribute("id");
		ReusableMethods
				.scrollToElement(
						driver,
						By.xpath("//div[@id]/div/button[starts-with(@id,'__input__button__addbutton')]"));
		WebElement getIDvalue = driver.findElement(By.xpath("//div[@id='" + id
				+ "']/div/div[1]/div"));
		String getText = getIDvalue.getText();
		ArrayList<String> searchKeys = new ArrayList<String>();
		String SearchText = POS
				.givenPOSModel_whenPOSTagging_thenPOSAreDetected(getText,
						searchKeys);
		Thread.sleep(5000);
		ReusableMethods.scrollToElement(driver,
				By.id("__input__text__search__1"));
		String getTextt = SearchText.toString();
		LogUtil.log(getTextt);
		buildTest.clickSearchQuestionsTextbox(SearchText);
		buildTest.clickSearchLinkbutton();
		Thread.sleep(5000);
		String questionCountAfterSearchQuestion = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestion);

		String regex = "[^\\d]+";
		String[] str = questionCountAfterSearchQuestion.split(regex);
		LogUtil.log(str[0]);
		int v = Integer.parseInt(str[0]);
		int k = v / 10;
		driver.findElement(By.id("regionAside")).click();

		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		List<WebElement> element = driver.findElements(By
				.xpath("//div[starts-with(@class,'pb2 mt3 ml1 pl2')]"));
		for (k = 0; k < element.size(); k++) {
			element.get(k).getCssValue("background-color");
			ReusableMethods.highLighterMethod(driver, element.get(k));
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			Thread.sleep(5000);

		}
		actions.sendKeys(Keys.PAGE_UP).build().perform();

		buildTest.clickClearFiltersButton();
		Thread.sleep(2000);
		// Assert.assertTrue(filterNames.size()==0);
		String enteredsearchText = buildTest.getSearchKey();
		LogUtil.log(enteredsearchText);
		Assert.assertEquals(getTextt.trim(), enteredsearchText.trim());
		buildTest.getBacktotoplink();
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
