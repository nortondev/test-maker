package com.wwnorton.TestMaker.FiltersandSorting;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_202_TC2_AS87SelectDeselectFiltersgetCount extends
		PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	String questionCountAfterFilter, questionCountAfterFilterRemove;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("TM_202_TC2_AS87 select and deselect the filters criteria and application update the filter count on filter panel for respective category.")
	@Stories("TM_202_TC2_AS87 Log into testmaker and verify user able to select and deselect the filters criteria and application update the filter count on filter panel for respective category.")
	@Test()
	public void selectDeselectFiltersandgetCount() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Types");
		Assert.assertNotNull(buildTest.getCountSelectedFilter("Question Types"));

		List<String> childFilterNames = buildTest.getFilterOptions();
		for (int i = 0; i < childFilterNames.size(); i++) {
			LogUtil.log(childFilterNames.get(i));
			Assert.assertNotNull(childFilterNames.get(i));
		}
		buildTest.selectQuestionTypeCheckbox("MULTIPLE CHOICE");
		buildTest.selectQuestionTypeCheckbox("SHORT ANSWER");
		Assert.assertEquals("2",
				buildTest.getCountSelectedFilter("Question Types"));
		buildTest.clickFiltersButton("Chapters");
		buildTest.clickMultipleChaptersCheckboxesandChaptersName(1);
		Assert.assertNotNull(buildTest.getCountSelectedFilter("Chapters"));
		buildTest.clickApplyButton();
		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);

		// get the Filter Names above Question Count
		List<String> filterNamesfirstAttempt = buildTest
				.verifyMultipleFilterName();
		for (int i = 0; i < filterNamesfirstAttempt.size(); i++) {
			LogUtil.log(filterNamesfirstAttempt.get(i));
			Assert.assertNotNull(filterNamesfirstAttempt.get(i));
		}
		// Remove one or more filter from Selected filter list
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Chapters");
		buildTest.clickMultipleChaptersCheckboxesandChaptersName(1);
		buildTest.clickApplyButton();
		Thread.sleep(3000);
		// ReusableMethods.loadingWaitDisapper(getDriver());
		questionCountAfterFilterRemove = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilterRemove);

		List<String> filterNamesAfterRemove = buildTest
				.verifyMultipleFilterName();
		for (int i = 0; i < filterNamesAfterRemove.size(); i++) {
			LogUtil.log(filterNamesAfterRemove.get(i));
			Assert.assertNotNull(filterNamesAfterRemove.get(i));
		}
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Assert.assertNotEquals(questionCountAfterFilterRemove,
				questionCountAfterFilter);
		buildTest.clickAddFiltersButton();
		buildTest.clickFiltersButton("Question Types");
		buildTest.unCheckChecboxesName("SHORT ANSWER");
		buildTest.clickCancelButton();
		List<String> filterNamesAfterCancelButton = buildTest
				.verifyMultipleFilterName();
		for (int i = 0; i < filterNamesAfterCancelButton.size(); i++) {
			LogUtil.log(filterNamesAfterCancelButton.get(i));
			Assert.assertNotNull(filterNamesAfterCancelButton.get(i));
		}
		Assert.assertEquals(filterNamesAfterCancelButton,
				filterNamesAfterRemove);
		Thread.sleep(2000);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
	
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
