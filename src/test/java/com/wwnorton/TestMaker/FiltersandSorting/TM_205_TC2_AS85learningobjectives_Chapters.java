package com.wwnorton.TestMaker.FiltersandSorting;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class TM_205_TC2_AS85learningobjectives_Chapters extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	JavascriptExecutor js;
	Actions actions;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Validate Learning Objective for selected Chapters")
	@Stories("Log into testmaker and select any one chapter from filter panel to verify application displays list of learning objectives associated to selected chapter only")
	@Test()
	public void learningObjectiveChapters() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		//TMlogin.appendISBN(ReusableMethods.LOISBN());
		TMlogin.appendISBN(ReusableMethods.questionGroupreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.getLearningObjectiveFilterforChapter();
		String questionCountAfterFilter = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterFilter);
		LogUtil.log(questionCountAfterFilter);
		List<String> filterNames = buildTest.verifyMultipleFilterName();

		for (int i = 0; i < filterNames.size(); i++) {
			LogUtil.log(filterNames.get(i));
		}
		// String regex = "[^\\d]+";
		// String[] str = questionCountAfterFilter.split(regex);
		// int v = Integer.parseInt(str[0]);
		List<String> chapterList = buildTest.getChapterNamesList();
		List<String> learningobjectiveList = buildTest
				.getLearningObjectiveList();

		// List<String> qIDs = buildTest.getQuestionidAddQuestionSection();
		List<WebElement> metaDataSection = getDriver()
				.findElements(
						By.xpath("//div[@class='clearfix col sm-col-9 pb1 small-body-text']/p"));
		for (int i = 0; i < metaDataSection.size(); i++) {
			Thread.sleep(5000);
			actions = new Actions(getDriver());
			getDriver().findElement(By.id("regionAside")).click();
			js = (JavascriptExecutor) getDriver();
			// List<WebElement> metaDataSection =
			// getDriver().findElements(By.xpath("//div[@class='full-height block']/div"));
			js.executeScript("arguments[0].scrollIntoView(true);",
					metaDataSection.get(i));
			Thread.sleep(5000);
			LogUtil.log(i);
			String filterName_c = filterNames.get(0);
			String loFilterName1 = filterNames.get(1);
			String loFilterName2 = filterNames.get(2);
			Assert.assertEquals(filterName_c, chapterList.get(i).toString());
			Assert.assertTrue(loFilterName1
					.equalsIgnoreCase(learningobjectiveList.get(i).toString())
					|| loFilterName2.equalsIgnoreCase(learningobjectiveList
							.get(i).toString()));

		}

		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
