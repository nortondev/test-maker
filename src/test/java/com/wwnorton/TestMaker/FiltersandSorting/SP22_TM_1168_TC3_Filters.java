package com.wwnorton.TestMaker.FiltersandSorting;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP22_TM_1168_TC3_Filters extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP22_TM-1168_TC3_Log into testmaker application and apply one or more filters and perform search with any search term to verify application displays search result within applied filters ")
	@Stories("SP22_TM-1168_TC3_Log into testmaker application and apply one or more filters and perform search with any search term to verify application displays search result within applied filters ")
	@Test()
	public void filterQuestions() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();
		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		buildTest.clickChaptersCheckboxesandChaptersName(0);
		buildTest.clickChaptersCheckboxesandChaptersName(1);
		buildTest.clickCancelButton();
		buildTest.clickAddFiltersButton();
		buildTest.clickChaptersButton();
		String chapterName1 = buildTest
				.clickChaptersCheckboxesandChaptersName(0);
		LogUtil.log(chapterName1);
		String chapterName2 = buildTest
				.clickChaptersCheckboxesandChaptersName(1);
		LogUtil.log(chapterName2);
		buildTest.clickApplyButton();
		String chapterName1AfterApplyFilter = buildTest.verifyFilterName(0);
		Assert.assertEquals(chapterName1, chapterName1AfterApplyFilter);
		String chapterName2AfterApplyFilter = buildTest.verifyFilterName(1);
		Assert.assertEquals(chapterName2, chapterName2AfterApplyFilter);
		String questionCountAfterSearchQuestion = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestion);
		LogUtil.log(questionCountAfterSearchQuestion);
		String chapterDetails = buildTest.verifyChaptersDetails();
		LogUtil.log(chapterDetails);
		Thread.sleep(2000);
		cnt.clickTestMakerBackButton();
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		boolean isCreateTPage = driver
				.findElements(
						By.xpath("//button[starts-with(@id,'__input__button__Create__New__Test__')]"))
				.size() > 0;
		if (isCreateTPage == true) {
			TMlogOut.logOutTestMakerApp();
		} else {
			cnt.clickTestMakerBackButton();
			Thread.sleep(2000);
			TMlogOut.logOutTestMakerApp();
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
