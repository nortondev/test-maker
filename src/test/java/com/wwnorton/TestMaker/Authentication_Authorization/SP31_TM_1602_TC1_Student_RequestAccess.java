package com.wwnorton.TestMaker.Authentication_Authorization;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1602_TC1_Student_RequestAccess extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	WebsitePage Website;
	
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();		
	}


	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1602_TC1_Log into testmaker application with student login and verify Contact us link navigate to Norton help page")
	@Stories("SP31_TM-1602_TC1_Log into testmaker application with student login and verify Contact us link navigate to Norton help page")
	@Test()
	public void studentRequestAccess() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		ReusableMethods.checkPageIsReady(driver);
		TMlogin.unAuthLoginTestMakerApp();
		//TMlogin.appendISBN(ReusableMethods.nonQuestionGroupISBNreadJson());
		Thread.sleep(2000);		
		ReusableMethods.checkPageIsReady(driver);
		String unauthorizedmsg = TMlogin.getUnauthorizedMessage();
		//String unauthorizedmsg1 = new String(unauthorizedmsg.getBytes("UTF-8"),"UTF-8");
		LogUtil.log(unauthorizedmsg);
		unauthorizedmsg = unauthorizedmsg.replaceAll("[^\\p{ASCII}]", "'");
		unauthorizedmsg =unauthorizedmsg.replaceAll("\\s", "");
	    String actualUnauthmsg
	   ="You don't have access to view this page.\r\nTo access Testmaker, try logging in with an authorized instructor account or \ncontact us\n.\nor \r\nClick here to Request Access";
	    actualUnauthmsg = actualUnauthmsg.replaceAll("\\s", "");
		Assert.assertEquals(actualUnauthmsg.trim().toString(),unauthorizedmsg.trim().toString());
		String parentWindow = driver.getWindowHandle();
		TMlogin.clickContactUslink();
		Website = new WebsitePage();
		Website.getWebsiteWindow(driver, parentWindow);
		Website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow);
		Thread.sleep(2000);
		TMlogin.clickRequestAccessButton();
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	@AfterTest
	public void closeTest() throws Exception {		
		PropertiesFile.tearDownTest();

	}
}
