package com.wwnorton.TestMaker.Authentication_Authorization;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
@Listeners({ TestListener.class })
public class SP32_TM_1809_TC6_ISBNRedirection extends PropertiesFile {
	
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1809_TC6_Log into testmaker application with instructor and access another ISBN Your test page and logout to verify Redirection URL take user to last location after relog in")
	@Stories("SP32_TM-1809_TC6_Log into testmaker application with instructor and access another ISBN Your test page and logout to verify Redirection URL take user to last location after relog in")
	@Test()
	public void redirectionISBN() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		Thread.sleep(2000);
		String getURL =driver.getCurrentUrl();
		String getValue =getURL.substring(getURL.lastIndexOf("/")+1);
	    LogUtil.log(getValue);
		TMlogin.appendISBN(ReusableMethods.nonQuestionGroupISBNreadJson());
		String getISBNURL =driver.getCurrentUrl();
		String getISBNValue =getISBNURL.substring(getISBNURL.lastIndexOf("/")+1);
		
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
		Thread.sleep(2000);
		TMlogin.loginTestMakerApp();
		String getISBNURL1 =driver.getCurrentUrl();
		String getISBNValue1 =getISBNURL1.substring(getISBNURL1.lastIndexOf("/")+1);
		Assert.assertEquals(getISBNValue, getISBNValue1);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
