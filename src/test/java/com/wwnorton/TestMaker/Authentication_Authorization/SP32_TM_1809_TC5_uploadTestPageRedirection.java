package com.wwnorton.TestMaker.Authentication_Authorization;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
@Listeners({ TestListener.class })
public class SP32_TM_1809_TC5_uploadTestPageRedirection extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1809_TC5_Log into testmaker application with instructor and navigate to Upload test page and logout to verify Redirection URL take user to last location after relog in")
	@Stories("SP32_TM-1809_TC5_Log into testmaker application with instructor and navigate to Upload test page and logout to verify Redirection URL take user to last location after relog in")
	@Test()
	public void uploadTestPageedirection() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		String getURL =driver.getCurrentUrl();
		String testuploadURL = getURL.replace("tests", "testbankupload");
		driver.get(testuploadURL);
		ReusableMethods.checkPageIsReady(driver);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.clickIconGearButton.click();
		Thread.sleep(4000);
		WebElement logOut = driver
				.findElement(By
						.xpath("//div[starts-with(@class,'mr0 p1-5 bg-white border')]//button[starts-with(@id,'__input__button____empty____0')]"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", logOut);
		driver.navigate().refresh();
		Thread.sleep(4000);
		TMlogin.loginTestMakerApp();
		String getuploadURL =driver.getCurrentUrl();
		Assert.assertEquals(testuploadURL, getuploadURL);
		
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
