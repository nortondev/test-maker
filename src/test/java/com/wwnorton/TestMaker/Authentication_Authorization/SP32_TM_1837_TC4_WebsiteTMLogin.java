package com.wwnorton.TestMaker.Authentication_Authorization;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReadUIJsonFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
@Listeners({ TestListener.class })
public class SP32_TM_1837_TC4_WebsiteTMLogin extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	WebsitePage website;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJason();
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();		
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1837_TC4_Create new user (student) from Website and log into Tesmaker application with same credentials to verify application does not displays Cookies are Blocked error message")
	@Stories("SP32_TM-1837_TC4_Create new user (student) from Website and log into Tesmaker application with same credentials to verify application does not displays Cookies are Blocked error message")
	@Test()
	public void loginTMfromWebsiteUser() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		ReusableMethods.checkPageIsReady(driver);
		try {
			TMlogin.clickForgotPasswordlink();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		website = new WebsitePage();		
		String userName = website.NewAccount_NortonApplication();
		System.out.println(userName);	
		website.Logout_NortonApplication();
		driver.get(url);
		Thread.sleep(2000);
		driver.navigate().refresh();
		TMlogin.TestMakerLoginEmail.sendKeys(userName);
		TMlogin.TestMakerLoginPassword.sendKeys(jsonobject
				.getAsJsonObject("TMInstructorLoginCredentials").get("password")
				.getAsString());
		TMlogin.TestMakerLogInButton.click();
		ReusableMethods.checkPageIsReady(driver);
		String profileName =TMlogin.profileTestMakerApp();
		Thread.sleep(5000);
		Assert.assertEquals(profileName.toString().toLowerCase(), userName.toLowerCase().toString());
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
