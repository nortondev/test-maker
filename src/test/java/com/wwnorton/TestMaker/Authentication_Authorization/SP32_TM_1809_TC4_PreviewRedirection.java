package com.wwnorton.TestMaker.Authentication_Authorization;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.utilities.LogUtil;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
@Listeners({ TestListener.class })
public class SP32_TM_1809_TC4_PreviewRedirection extends PropertiesFile {
	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	CreateNewTestPage cnt;
	TestMakerLoginPage TMlogin;
	RegionYourTestsPage TMYourTests;
	Actions actions;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP32_TM-1809_TC4_Log into testmaker application with instructor and navigate to Preview page and logout to verify Redirection URL take user to last location after relog in")
	@Stories("SP32_TM-1809_TC4_Log into testmaker application with instructor and navigate to Preview page and logout to verify Redirection URL take user to last location after relog in")
	@Test()
	public void previewLoginredirection() throws Exception {
		driver = getDriver();
		actions = new Actions(driver);
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.uniqueQuestionreadJson());
		cnt = new CreateNewTestPage();
		cnt.clickCreateNewTest();
		String testName = cnt.createNewTestwithTestNameCourseName();
		String buildTestName = cnt.getTestName();

		Assert.assertEquals(testName.toLowerCase().trim(), buildTestName
				.toLowerCase().trim());
		cnt.clickSave();
		LogUtil.log(buildTestName);
		buildTest = new BuildTestPage();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		buildTest.clickAddFiltersButton();
		buildTest.clickQuestionTypesButton();
		buildTest.selectQuestionTypeCheckbox("SHORT ANSWER");
		buildTest.clickApplyButton();
		String questionCountAfterSearchQuestion = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestion);
		String questionTypesAfterApplyFilter = buildTest.verifyFilterName(0);
		Assert.assertEquals("SHORT ANSWER", questionTypesAfterApplyFilter
				.toUpperCase().toString());
		actions = new Actions(driver);
		driver.findElement(By.id("regionAside")).click();
		// Enter Search term which available in selected filtered
		@SuppressWarnings("unused")
		List<WebElement> getQuestionText = driver.findElements(By
				.xpath("//div[@class='mx3']"));
		for (int ele = 0; ele < 3; ele++) {
			actions.sendKeys(Keys.PAGE_UP).build().perform();
			Thread.sleep(1000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1000);
			actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		}
		buildTest.clickAddQuestionsIcon(5);
		buildTest.getQuestionTextinBuildView(1);
		String questionCountAfterSearchQuestionAfteradd = buildTest
				.getQuestionCountAfterQuestionSearch();
		LogUtil.log(questionCountAfterSearchQuestionAfteradd);
		buildTest.verifyQuestioninTestBuildSearchResult();
		Thread.sleep(4000);
		Assert.assertEquals("Question 5", buildTest.verifyQuestioninTestBuild());
		buildTest.clickBuildTestTabs("Preview");
		ReusableMethods.scrollToBottom(driver);
		Assert.assertEquals("Answer Key", buildTest.getAnswerKeyText());
		LogUtil.log(buildTest.getAnswerKeyText());
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
		Thread.sleep(1000);
		TMlogin.loginTestMakerApp();
		String tabValue =buildTest.getTabsName("Preview");
		String tabName =tabValue.substring(tabValue.lastIndexOf("yellow"));
		Assert.assertEquals("yellow", tabName);
		
		String getURL =driver.getCurrentUrl();
		String getValue =getURL.substring(getURL.lastIndexOf("=")+1);
		Assert.assertEquals("Preview", getValue);
		
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
