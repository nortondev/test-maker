package com.wwnorton.TestMaker.Authentication_Authorization;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;
@Listeners({ TestListener.class })
public class SP33_TM_1808_TC1_NotDefaultISBN_InstLogin extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	WebsitePage Website;
	String parentWindow,getValue;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP33_TM-1808_TC1_Log into testmaker application with Instructor that don't have default ISBN set to verify application displays common error message page")
	@Stories("SP33_TM-1808_TC1_Log into testmaker application with Instructor that don't have default ISBN set to verify application displays common error message page")
	@Test()
	public void instLoginNotaDefaultISBN() throws Exception {
		driver = getDriver();
		Thread.sleep(2000);
		String getURL = driver.getCurrentUrl();
		getValue = getURL.substring(getURL.lastIndexOf("?") + 1);
		if (getURL.contains("redirect=/")) {
			String newURL = getURL.replace("?redirect=/", "");
			driver.get(newURL);
		}
		TMlogin = new TestMakerLoginPage();
		ReusableMethods.checkPageIsReady(driver);
		TMlogin.unAuthloginTestMakerApp();
		parentWindow = driver.getWindowHandle();
		String ErrorMsg1 = TMlogin.ErrorMessage(0);
		Website = new WebsitePage();
		Website.getWebsiteWindow(driver, parentWindow);
		Website.getInstructorResourcesstextText();
		driver.close();
		driver.switchTo().window(parentWindow);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		String msg1 = "Make sure you have a verified instructor account. Test banks are only available to verified instructors. To request access, find your book’s Instructor Resources page on W. W. Norton’s website.";
		String msg2 = "You will need to select the Request Access button on your book’s Instructor Resources page and complete the form.";
		String msg3 = "Instructor resources are only available for verified Instructors. All requests for instructors access are verified by Norton employees. Approval may take up to 2 business days.";
		String combinedmsg = msg1.trim().toString() + "\n" + "\n"
				+ msg2.trim().toString() + "\n" + "\n" + msg3.trim().toString();
		Assert.assertEquals(ErrorMsg1.trim().toString(), combinedmsg.trim()
				.toString());
		String ErrorMsg2 = TMlogin.ErrorMessage(1);
		Website.getWebsiteWindow(driver, parentWindow);
		Website.getInstructorResourcesstextText();
		driver.close();
		driver.switchTo().window(parentWindow);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		String msg4 = "Check if your test bank is offered through Norton Testmaker. Find your book’s Instructor Resources page on W. W. Norton’s website, then click the Access Norton Testmaker link under the Test Bank category.";
		Assert.assertEquals(ErrorMsg2.trim().toString(), msg4.trim().toString());

		String ErrorMsg3 = TMlogin.ErrorMessage(2);
		Website.getWebsiteWindow(driver, parentWindow);
		Website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		String msg5 = "If you have a verified instructor account and are unable to log in, please visit W. W. Norton Tech Support.";
		Assert.assertEquals(ErrorMsg3.trim().toString(), msg5.trim().toString());
		Thread.sleep(2000);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
