package com.wwnorton.TestMaker.Authentication_Authorization;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.wwnorton.TestMaker.ObjectFactories.BuildTestPage;
import com.wwnorton.TestMaker.ObjectFactories.CreateNewTestPage;
import com.wwnorton.TestMaker.ObjectFactories.RegionYourTestsPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLogOutPage;
import com.wwnorton.TestMaker.ObjectFactories.TestMakerLoginPage;
import com.wwnorton.TestMaker.ObjectFactories.WebsitePage;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;
import com.wwnorton.TestMaker.utilities.TestListener;

@Listeners({ TestListener.class })
public class SP31_TM_1602_TC4_Instructor_InvalidISBN extends PropertiesFile {

	BuildTestPage buildTest;
	TestMakerLogOutPage TMlogOut;
	TestMakerLoginPage TMlogin;
	CreateNewTestPage cnt;
	RegionYourTestsPage TMYourTests;
	WebsitePage Website;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();		
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("SP31_TM-1602_TC4_Log into testmaker application with instructor credentials and visit to invalid page by updating URL")
	@Stories("SP31_TM-1602_TC4_Log into testmaker application with instructor credentials and visit to invalid page by updating URL")
	@Test()
	public void checkUnauthorisedAccessInvalidISBN() throws Exception {
		driver = getDriver();
		TMlogin = new TestMakerLoginPage();
		TMlogin.loginTestMakerApp();
		Thread.sleep(5000);
		TMlogin.appendISBN(ReusableMethods.invalidISBN());
		String parentWindow = driver.getWindowHandle();
		TMlogin.clickwwNortonTechSupportlink();
		Website = new WebsitePage();
		Website.getWebsiteWindow(driver, parentWindow);
		Website.getWWNortonHelpText();
		driver.close();
		driver.switchTo().window(parentWindow);
		TMlogin.clickInstresourceslink();
		String TMWindow = driver.getWindowHandle();
		Website.getWebsiteWindow(driver, TMWindow);
		Thread.sleep(5000);
		Website.getInstructorResourcesstextText();
		
		Website.clickSearch(ReusableMethods.invalidISBN());
		Website.clickSearchIcon();
		Website.getpageNotfoundText();
		driver.close();
		driver.switchTo().window(TMWindow);
		TMlogOut = new TestMakerLogOutPage();
		TMlogOut.logOutTestMakerApp();
		
		
	}

	@AfterMethod
	public void closeTest() throws Exception {	
		PropertiesFile.tearDownTest();

	}
}
