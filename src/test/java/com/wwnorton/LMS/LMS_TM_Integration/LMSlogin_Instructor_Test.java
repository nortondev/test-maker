package com.wwnorton.LMS.LMS_TM_Integration;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.TestMaker.ObjectFactories.LMSCANVAS;
import com.wwnorton.TestMaker.utilities.PropertiesFile;
import com.wwnorton.TestMaker.utilities.ReadUIJsonFile;
import com.wwnorton.TestMaker.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


public class LMSlogin_Instructor_Test extends PropertiesFile{
	LMSCANVAS LMSpage;
	
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJason();
	
	@Parameters("Browser")
	@BeforeTest
		
	// Call to Properties file initiate Browser and set Test URL.
		public void callPropertiesFile() throws Exception {
		PropertiesFile.readPropertiesFile();
		//PropertiesFile.setBrowserConfig();
		PropertiesFile.setlmsURL();
		
		
	}

	
	@Severity(SeverityLevel.NORMAL)
	@Description("LMS Integration with Test Maker Application to Validate IMSCC file")
	@Stories("LMS Integration with Test Maker Application to Validate IMSCC file")
	@Test()
	public void Login_LMS() throws Exception{		
		LMSpage = new LMSCANVAS();
		LMSpage.instloginLMSCANVAS();
		LMSpage.clicklink("Dashboard");
		Thread.sleep(5000);
		LMSpage.clickDashboardCourse();
		LMSpage.clickimportexistingContentlink();
		LMSpage.selectContentType("Common Cartridge 1.x Package");
		
		
	}
	public void uploadFile(String path){
		LMSpage.clickChooseFileButton(path);
	}
	
	public void selectContentType(){
		LMSpage.selectContentType();
	}
	
	public void clickImportbutton(){
		LMSpage.importbutton.click();
	}
	
	public void clickAssignmentButton(){
		LMSpage.clickAssignmentlink();
		ReusableMethods.scrollToBottom(driver);
	}
	public void clickPreviewQuizButton(){
		LMSpage.clickpreviewQuizButton();
		
	}
	public int getQuestionscount(){
		return LMSpage.getQuestionList();
	}
	
	public boolean getStatus() throws InterruptedException{
		return LMSpage.checkCompltedStatus();
	}
}
